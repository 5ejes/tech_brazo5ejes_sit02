﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class recargarLetrero : MonoBehaviour
{
    public GameObject textoSituacion;

    private void Start()
    {
        StartCoroutine(tiempoDeEspera());
    }

    IEnumerator tiempoDeEspera()
    {
        textoSituacion.SetActive(false);
        yield return new WaitForSeconds(1);
        textoSituacion.SetActive(true);
    }
}
