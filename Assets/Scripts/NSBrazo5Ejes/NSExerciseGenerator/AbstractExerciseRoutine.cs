﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
	public abstract class AbstractExerciseRoutine
	{

		public abstract System.Action OnEvaluationCompleted { get; set; }

		/// <summary>
		/// Límite mínimo para ganar el ejercicio
		/// </summary>
		/// <value>Valor del límite mínimo para ganar el ejercicio, 0 siendo muy bajo y 1 siendo lo más alto</value>
		public float QualificationThreshold
		{
			get;
			protected set;
		} = 0.5f;

		/// <summary>
		/// Retorna la calificación actual en base a los ejercicios logrados
		/// </summary>
		/// <value>Calificación actual 0 siendo la más baja y 1 la más alta</value>
		public float CurrentQualification
		{
			get;
			protected set;
		} = 0f;

		/// <summary>
		/// Evalua la rutina
		/// </summary>
		/// <returns>Retorna si pasa o no pasa la rutina</returns>
		public abstract bool EvaluateRoutine();

		/// <summary>
		/// Lista con las posiciones de los cubos
		/// </summary>
		/// <value></value>
		public abstract List<Vector3> Positions { get; set; }
	}
}
