﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class LanguageSelector : MonoBehaviour {

	public Text _title;
	public Text _textLang1;
	public Text _textLang2;
	public Button _buttonLang1;
	public Button _buttonLang2;
	public Sprite _imageSpanish;
	public Sprite _imageEnglish;
	public Sprite _imagePortuguese;
	public GameObject _backgroundPref;

	public delegate void BtAction();
	public BtAction bt_action;

	private GameObject _background;
	// Use this for initialization
	void Start () {

		_title.font = (Font)Resources.Load("Fonts/ArialBold26");
		_textLang1.font = (Font)Resources.Load("Fonts/ArialBold26");
		_textLang2.font = (Font)Resources.Load("Fonts/ArialBold26");

		_background = Instantiate(_backgroundPref, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_background.name = "background";
		_background.transform.SetParent (GameObject.Find("Canvas").transform, false);
		_background.transform.localPosition = new Vector3(0,0,0);
		_background.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () - 1);

		switch (Manager.Instance.globalLanguageConf) {
		case BaseSimulator.ConfigureLanguageList.Spanish_English:
			_title.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='language_selector']").InnerText;
			_textLang1.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='english']").InnerText;
            _textLang2.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='spanish']").InnerText;
                _buttonLang1.GetComponent<Image> ().sprite = _imageEnglish;
                _buttonLang2.GetComponent<Image> ().sprite = _imageSpanish;
                _buttonLang1.onClick.AddListener(loadEnglish);
                _buttonLang2.onClick.AddListener(loadSpanish);
			break;
		case BaseSimulator.ConfigureLanguageList.Spanish_Portuguese:
			_title.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='language_selector']").InnerText;
			_textLang1.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='spanish']").InnerText;
			_textLang2.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='portuguese']").InnerText;
			_buttonLang1.GetComponent<Image> ().sprite = _imageSpanish;
			_buttonLang2.GetComponent<Image> ().sprite = _imagePortuguese;
			_buttonLang1.onClick.AddListener(loadSpanish);
			_buttonLang2.onClick.AddListener(loadPortuguese);
			break;
		case BaseSimulator.ConfigureLanguageList.English_Portuguese:
			_title.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='language_selector']").InnerText;
			_textLang1.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='english']").InnerText;
			_textLang2.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='portuguese']").InnerText;
			_buttonLang1.GetComponent<Image> ().sprite = _imageEnglish;
			_buttonLang2.GetComponent<Image> ().sprite = _imagePortuguese;
			_buttonLang1.onClick.AddListener(loadEnglish);
			_buttonLang2.onClick.AddListener(loadPortuguese);
			break;
		}
	}

	public void setPostAction(BtAction _btAction){
		bt_action = _btAction;
	}

	void loadSpanish(){
		PlayerPrefs.SetString ("language","Spanish");

		Manager.Instance.globalLanguage = BaseSimulator.LanguageList.Spanish;
		bt_action.Invoke ();

		iTween.MoveTo(
			gameObject,
			iTween.Hash(
				"position", new Vector3(0,1200,0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal",true
			)
		);

		Image _bg = _background.GetComponent<Image>();
		_bg.CrossFadeAlpha (0f, 1f, false);
		Destroy(_background, 1.2f);

		Destroy(gameObject, 2);

	}

	void loadEnglish(){
		PlayerPrefs.SetString ("language","English");

		Manager.Instance.globalLanguage = BaseSimulator.LanguageList.English;
		bt_action.Invoke ();

		iTween.MoveTo(
			gameObject,
			iTween.Hash(
				"position", new Vector3(0,1200,0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal",true
			)
		);

		Image _bg = _background.GetComponent<Image>();
		_bg.CrossFadeAlpha (0f, 1f, false);
		Destroy(_background, 1.2f);

		Destroy(gameObject, 2);

	}

	void loadPortuguese(){
		PlayerPrefs.SetString ("language","Portuguese");

		Manager.Instance.globalLanguage = BaseSimulator.LanguageList.Portuguese;
		bt_action.Invoke ();

		iTween.MoveTo(
			gameObject,
			iTween.Hash(
				"position", new Vector3(0,1200,0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal",true
			)
		);

		Image _bg = _background.GetComponent<Image>();
		_bg.CrossFadeAlpha (0f, 1f, false);
		Destroy(_background, 1.2f);

		Destroy(gameObject, 2);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
