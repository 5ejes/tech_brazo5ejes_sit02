﻿using System;
using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
	public abstract class AbstractExerciseGenerator : MonoBehaviour
	{
		/// <summary>
		/// Distancia mínima entre cubo y cubo
		/// </summary>
		private const float _minDistanceBetweenCubes = 0.2f;

		[Header("Settings")]
		[Tooltip("Enumeración ID usada por los tokens para evaluar su respuesta")]
		[SerializeField] private ScriptableEnumID _targetEnumId = null;
		[SerializeField] private LocalPositionParser _armPositionParser = null;
		[SerializeField] private int _generatedPositions = 3;
		[Space]
		[SerializeField] private LayerMask _tokenCheckLayer = -1;

		[Space]
		[Header("Restricted Areas (position, scale)")]
		[SerializeField] private Transform[] _restrictedAreas = new Transform[] { };
		[SerializeField] private Vector3 _cubeScaleCheck = Vector3.one;

		[Header("Limit Overrides")]
		[SerializeField] protected Transform _minLimit;
		[SerializeField] protected Transform _maxLimit;


		private int _semilla = -1;

		public ScriptableEnumID TargetEnumId { get => _targetEnumId; set => _targetEnumId = value; }
		public LocalPositionParser ArmPositionParser { get => _armPositionParser; private set => _armPositionParser = value; }
		public LayerMask TokenCheckLayer { get => _tokenCheckLayer; set => _tokenCheckLayer = value; }
		public int GeneratedPositionCount { get => _generatedPositions; set => _generatedPositions = value; }

		private TransformBound[] _bounds;

		public void Awake()
		{
			// Genero una lista temporal con los límites
			var TempBoundList = new List<TransformBound>();

			// Recorro cada límite
			foreach (var transform in _restrictedAreas)
			{
				// Los añado a la lista
				TempBoundList.Add(new TransformBound(transform));
			}

			// Inicializo el arreglo de bounds
			_bounds = TempBoundList.ToArray();
		}

		public void Start()
		{
			// Inicializo la semilla
			InitializeSeed();
		}

		/// <summary>
		/// Inicializa la semilla de la generación aleatoria
		/// </summary>
		public void InitializeSeed()
		{
			// Si la semilla no ha sido inicializada
			if (Semilla < 0)
			{
				// Creo una semilla aleatoria
				Semilla = UnityEngine.Random.Range(0, 10000000);
			}
		}

		public int Semilla

		{
			get => _semilla; set
			{
				// Registro la semilla
				_semilla = value;

				// Inicializo la semilla dada
				UnityEngine.Random.InitState(value);
			}
		}

		/// <summary>
		/// Genero las posiciones para los cubos
		/// </summary>
		/// <param name="count">Cantidad de posiciones únicas a generar</param>
		/// <returns>Retorna un arreglo con todas las posiciones</returns>
		public Vector3[] GenerateCubePositions(int count)
		{
			// Genero la lista de posiciones
			List<Vector3> positions = new List<Vector3>();

			// Recorro una cantidad de posiciones
			for (int i = 0; i < count; i++)
			{
				// Genero una posición aleatoria
				var generatedPosition = GeneratePosition(positions);

				// Si la posición retornada es una posición por defecto
				if (generatedPosition.Equals(-Vector3.one)) continue;

				// Anexo la posición generada
				positions.Add(generatedPosition);
			}

			// Retorno las posiciones procesadas
			return positions.ToArray();
		}

		/// <summary>
		/// Genera una posición aleatoria
		/// </summary>
		/// <param name="addedPositions">Lista de posiciones previamente añadidas para evitar la repetición, si es nulo, se ignora</param>
		/// <returns>Retorna la posición aleatoria generada</returns>
		public Vector3 GeneratePosition(List<Vector3> addedPositions = null)
		{

			// Inicializo los contadores
			int i = 0;
			int MAX_ATTEMPTS = 1000;

			// Inicializo la variable de control
			bool validPosition = false;

			Vector3 output = new Vector3();

			// Mientras no se haya generado una posición válida y los intentos no superen el límite máximo
			while (!validPosition && i < MAX_ATTEMPTS)
			{

				// Aumento el índice de la posición
				i++;

				// Genero cada dimensión del vector3
				var x = UnityEngine.Random.Range(0, 0.8f);
				var y = 0f;
				var z = UnityEngine.Random.Range(0, 0.8f);

				// Construyo el vector3
				output = new Vector3(x, y, z);

				// Ajusto el vector 
				output = SnapAdjustVector(output, 0.05f);

				// Si no existe la lista
				if (addedPositions == null)
				{

					// Comprueblo la posición sobre las areas restringidas
					validPosition = !IsInsideRestrictedAreas(output, localCoordinate: true);

					// Obtengo la posición en coordenadas globales
					Vector3 globalCoordinates;
					if (_minLimit && _maxLimit)
					{
						globalCoordinates = ArmPositionParser.ParseVector(output, _minLimit, _maxLimit);
					}
					else
					{
						globalCoordinates = ArmPositionParser.ParseVector(output);
					}

					// Obtengo los límites
					var cubeLimits = GeneratePositionExtentsXZ(globalCoordinates, _cubeScaleCheck);

					// Por cada límite
					foreach (Vector3 limit in cubeLimits)
					{
						// Comprueblo el límite
						validPosition = !IsInsideRestrictedAreas(limit);

						// Si la posición no es válida
						if (!validPosition)
						{
							// Salgo del bucle
							break;
						}

					}
				}
				else
				{
					// Si existe la lista

					// Si hay más de una posición generada
					if (addedPositions.Count > 0)
					{
						//Verifico si ya ha sido generada
						// Verifico la distancia entre el output y las posiciones generadas
						validPosition = (CheckDistance(output, addedPositions.ToArray(), _minDistanceBetweenCubes));

					}
					else
					{
						// Si no hay posiciones que evaluar en la lista

						// Marco la posición como válida
						validPosition = true;
					}


					// Si la posición elegida es válida (Comprobación de distancia frente a otras posiciones previas)
					if (validPosition)
					{
						// Realizo una nueva comprobación a todas las posiciones

						// La posición es válida si no está dentro de zonas restringidas
						validPosition = !IsInsideRestrictedAreas(output, localCoordinate: true);

						// Obtengo la posición en coordenadas globales
						Vector3 globalCoordinates;
						if (_minLimit && _maxLimit)
						{
							globalCoordinates = ArmPositionParser.ParseVector(output, _minLimit, _maxLimit);
						}
						else
						{
							globalCoordinates = ArmPositionParser.ParseVector(output);
						}

						// Obtengo los límites
						var cubeLimits = GeneratePositionExtentsXZ(globalCoordinates, _cubeScaleCheck);

						// Por cada límite
						foreach (Vector3 limit in cubeLimits)
						{
							// Comprueblo el límite
							validPosition = !IsInsideRestrictedAreas(limit);

							// Si la posición no es válida
							if (!validPosition)
							{
								// Salgo del bucle
								break;
							}

						}
					}
				}
			}

			if (i >= MAX_ATTEMPTS)
			{
				Debug.LogError("Max attempts reached!");
				output = -Vector3.one;
			}

			// retorno la posición generada
			return output;
		}

		protected Vector3[] GeneratePositionExtentsXZ(Vector3 position, Vector3 scale)
		{
			// Obtengo la mitad de la escala
			var halfScale = scale / 2f;

			// Retorno un arreglo de posiciones 
			return new Vector3[] {
		new Vector3(halfScale.x,0f,halfScale.z) + position,
		new Vector3(-halfScale.x,0f,halfScale.z) + position,
		new Vector3(halfScale.x,0f,-halfScale.z) + position,
		new Vector3(-halfScale.x,0f,-halfScale.z) + position
	  };
		}

		/// <summary>
		/// Evalua si un punto está dentro de las límites definidos
		/// </summary>
		/// <param name="target">Punto en coordeanadas globales a evaluar</param>
		/// <param name="localCoordinate">Describe si el vector dado, describe coordenadas locales</param>
		/// <returns>'true' si el punto está dentro de alguna area restringida definida, sino 'false'</returns>
		private bool IsInsideRestrictedAreas(Vector3 target, bool localCoordinate = false)
		{

			// Genero la variable de comprobación
			bool isInsideRestrictedArea = false;

			// Si las areas restringidas están definidas
			if (_bounds != null)
			{
				// Si hay al menos una area restringida
				if (_bounds.Length > 0)
				{
					// Recorro cada posición restringida
					foreach (var bound in _bounds)
					{

						// Si las coordenadas son puntos locales
						if (localCoordinate)
							// Genero la posición global
							if (_minLimit && _maxLimit)
								target = ArmPositionParser.ParseVector(target, _minLimit, _maxLimit);
							else 
								target = ArmPositionParser.ParseVector(target);

						// Evaluo cada zona restringida
						isInsideRestrictedArea = bound.IsInsideXZ(target);

						// Define los colores del debug, y dibuja un rayo en la posición
						var checkColor = isInsideRestrictedArea ? Color.blue : Color.yellow;
						Debug.DrawRay(target, Vector3.one.normalized * 0.1f, checkColor, 2f);

						// Si se ha comprobado que la posición es inválida
						if (isInsideRestrictedArea)
							// Rompo el ciclo, ya no es necesario recorrer más elementos
							break;
					}
				}
			}

			// Retorno el resultado de la evaluación
			return isInsideRestrictedArea;
		}

		/// <summary>
		/// Verifica la distancia entre el objetivo y las posiciones usando un mínimo
		/// </summary>
		/// <param name="targetPosition">Posición a evaluar</param>
		/// <param name="positions">Posición sobre las que evaluar</param>
		/// <param name="minDistance">Distancia mínima antes de considerarse inválida</param>
		/// <returns>'true' si la posición no está a menos del límite, 'false' si ocurre lo contrario</returns>
		private bool CheckDistance(Vector3 targetPosition, Vector3[] positions, float minDistance)
		{

			// Si hay al menos una posición
			int length = positions.Length;
			if (length > 0)
			{
				// Defino la variable de validez distancia máxima
				bool valid = true;

				// Recorro cada posición
				for (int i = 0; i < length; i++)
				{
					var position = positions[i];
					// Obtengo la distancia actual entre el objetivo y la posición
					var currentDistance = Vector3.Distance(targetPosition, position);

					// Si la distancia es menor a la distancia mínima o la regla de vecinos se incumple
					if (currentDistance < minDistance)
					{

						// Defino el valor de validez a falso
						valid = false;

						// Salgo del bucle
						break;
					}
				}

				// Retorno el resultado de la evaluación
				return valid;
			}

			// Retorno true, al no haber posiciones
			return true;
		}

		/// <summary>
		/// Ajusta el vector en incrementos dados
		/// </summary>
		/// <param name="target">Vector a ajustar</param>
		/// <param name="steps">Parámetrp de ajuste</param>
		/// <returns>Retorno el vector ajustado</returns>
		public Vector3 SnapAdjustVector(Vector3 target, float steps)
		{
			// Ajusto cada dimensión del vector
			target.x = Mathf.Round(target.x / steps) * steps;
			target.y = Mathf.Round(target.y / steps) * steps;
			target.z = Mathf.Round(target.z / steps) * steps;

			// Retorno el vector ajustado
			return target;
		}

		private void OnDrawGizmos()
		{
			// Si las areas restringidas no son nulas
			if (_restrictedAreas != null)
			{

				// Si hay al menos un area restringida
				if (_restrictedAreas.Length > 0)
				{

					// Defino el valor de los colores
					Color gizmosLine, gizmosFill = gizmosLine = Color.red;

					gizmosLine.a = 0.8f;
					gizmosFill.a = 0.2f;

					// Recorro cada zona restringida
					foreach (var area in _restrictedAreas)
					{
						// Dibujo los indicadores
						Gizmos.color = gizmosFill;
						Gizmos.DrawCube(area.position, area.localScale);

						Gizmos.color = gizmosLine;
						Gizmos.DrawWireCube(area.position, area.localScale);
					}

				}
			}
		}

		public abstract void GenerateRoutine();
	}
}