﻿using UnityEngine;
using UnityEngine.Events;

namespace Modular
{
	public class InspectorActionStoreExecute : MonoBehaviour
	{
		[Header("Read-Only Value")]
		[Tooltip("Cuando está activo, la ejecución es almacenada hasta que se desactive, donde se ejecutará la acción si no es cancelada")]
		[SerializeField] private bool _storing;
		[Header("Events")]
		[Tooltip("Al evaluar la ejecución o al cambiar el estado de Storing, se ejecuta cuando es false y hay una orden de ejecución activa")]
		[SerializeField] private UnityEvent _onExecuteOrder;
		[Tooltip("Al evaluar la ejecución o al cambiar el estado de Storing, se ejecuta cuando es true")]
		[SerializeField] private UnityEvent _onStoringOrder;

		public bool ExecuteScheduled { get => _executeScheduled; }
		private bool _executeScheduled = false;
		public bool Storing
		{
			get => _storing;
			set
			{
				_storing = value;

				// Si cancelo el store
				if (!value)
				{
					if (ExecuteScheduled)
					{
						OnExecuteOrder.Invoke();
						CancelScheduledExecution();
					}
				}
				else
				{
					OnStoringOrder.Invoke();
				}
			}
		}
		public UnityEvent OnExecuteOrder { get => _onExecuteOrder; set => _onExecuteOrder = value; }
		public UnityEvent OnStoringOrder { get => _onStoringOrder; set => _onStoringOrder = value; }

		public void EvaluateExecution()
		{
			// Si la orden no está programada para ser almacenada
			if (!Storing)
			{
				if (ExecuteScheduled)
				{
					OnExecuteOrder.Invoke();
					CancelScheduledExecution();
				}
			}
			else
			{
				OnStoringOrder.Invoke();
			}
		}

		public void ScheduleExecute()
		{
			_executeScheduled = true;
		}

		public void CancelScheduledExecution()
		{
			_executeScheduled = false;
		}

	}
}
