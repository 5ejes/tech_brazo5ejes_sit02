﻿using System.Collections;
using System.Collections.Generic;
using NSCompilador;
using NSEditorAcciones;
using UnityEngine;

public class ProgramDemo : MonoBehaviour
{

	[Header("Referencias")]
	[SerializeField] private SensorDemo refSensorDemo;

	private void CreateProgram()
	{

		// Programa con 5 acciones

		// MoveOperation 10 10 10

		// IfStatement ColorSensor Matching(red)
		// --> MoveOperation 15 15 15
		// --> GripOperation UnGrip(); -> Abrir

		// GripOperation Grip() -> Cerrar

		if (refSensorDemo == null) throw new System.Exception("El sensor físico no ha sido referenciado, no se puede continuar");

		var colorSensor = new ColorSensor(refSensorDemo);
		var barrierSensor = new BarrierSensor(refSensorDemo);

		var blocks = new LogicBlock[]
		{
			new IfElseStatement(barrierSensor)
				{
					IfExecutionBlocks = new LogicBlock[]
						{
							new MoveOperation() { Target = new Vector3Int(10, 10, 10) },
								new GripOperation() { Closed = true },
								new IfStatement(colorSensor)
								{
									IfExecutionBlocks = new LogicBlock[]
									{
										new MoveOperation() { Target = new Vector3Int(0, 1, 2) },
											new GripOperation() { Closed = true }
									}
								}
						},
						ElseExecutionBlocks = new LogicBlock[]
						{
							new IfElseStatement(barrierSensor)
								{
									IfExecutionBlocks = new LogicBlock[]
										{
											new MoveOperation() { Target = new Vector3Int(4, 5, 6) },
												new GripOperation() { Closed = true }
										},
										ElseExecutionBlocks = new LogicBlock[]
										{
											new GripOperation() { Closed = false },
												new GripOperation() { Closed = false }
										}
								},
								new MoveOperation() { Target = new Vector3Int(90, 90, 90) },
								new GripOperation() { Closed = false }
						}
				},
				new MoveOperation() { Target = new Vector3Int(0, 0, 100) }
		};

		Debug.Log($"Program Created! {blocks.Length} execution blocks");
		Debug.Log($"Creating sequence...");

		// Creo una secuencia a través del compilador
		var sequence = Compiler.Compile(blocks);

		Debug.Log($"Logging...");

		var program = new Program(sequence);

		program.RunFromStart();

	}

	private void LogBlock(LogicBlock argBlock)
	{
		// Creo un string para el log
		string logString = string.Empty;
		logString = $"Order : {argBlock.ExecutionOrder} ";

		// Proceso el bloque según su tipo
		switch (argBlock)
		{
			case MoveOperation mo:
				logString += $"Target : {mo.Target}";
				break;
			case GripOperation gr:
				logString += $"Grip : {gr.Closed}";
				break;
			case Conditional co:
				logString += $"Condition : {co.CurrentCondition}";
				break;
			default:
				logString += $"No Preset : {argBlock.ToString()}";
				break;
		}

		// Logueo a la consola la cadena
		Debug.Log(logString);
	}

	private void LogBlocks(ExecutionSequence argSequence)
	{
		foreach (var logicBlock in argSequence.Sequence)
		{
			// Logueo el bloque a la consola
			LogBlock(logicBlock);
		}
	}

	private void OnGUI()
	{
		GUI.skin.button.fontSize = Screen.width / 20;
		if (GUILayout.Button("Create Sequence"))
		{
			CreateProgram();
		}
	}
}