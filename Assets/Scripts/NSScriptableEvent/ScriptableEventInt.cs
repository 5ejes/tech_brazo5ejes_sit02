using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
    [CreateAssetMenu(fileName = "Event int", menuName = "NSScriptableEvent/Int", order = 0)]
    public class ScriptableEventInt : AbstractScriptableEvent<int>
    {
			[System.Serializable] public class IntEvent : UnityEvent<int> {}
    }
}