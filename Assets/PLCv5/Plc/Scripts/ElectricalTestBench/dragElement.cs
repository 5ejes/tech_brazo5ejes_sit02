﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class dragElement : MonoBehaviour {

	public enum typesDragElement
	{
		Input,
		Output
	}

	public enum typesComponents
	{
		capacitive_sensor,
		inductive_sensor,
		optical_sensor,
		red_button_sensor,
		green_button_sensor,
		limit_switch_sensor,
		switch_sensor,
		sound_actuator,
		green_light_actuator,
		yellow_light_actuator,
		red_light_actuator
	}

	public Image _actualImage;
	public Image _pressedImage;
	public Sprite _normalPosition;
	public Sprite _benchPosition;

	public typesDragElement _typeElement;
	public typesComponents _typeComponent;
	public string textTag;

	public bool isSwitch = false;

	public int normalValue = 0;
	public int pressedValue = 0;

	public int actualValue = 0;

	public int rowUsed = 0;
	public int columnUsed = 0;

	public bool dragEnabled = true;
	public bool inBench = false;

	public AudioClip _downAudio;
	public AudioClip _upAudio;

	public GameObject _situation;

	public GameObject _redCablePrefab;
	public GameObject _blueCablePrefab;
	public GameObject _blackCablePrefab;
	public GameObject _whiteCablePrefab;
	public GameObject _grayCablePrefab;

	public GameObject _leftCable;
	public GameObject _rightCable;
	public GameObject _topCable;

	private GameObject _leftOvers;
	private GameObject _leftColliders;

	private GameObject _rightOvers;
	private GameObject _rightColliders;

	private Image left_over_1_1;
	private Image left_over_1_2;
	private Image left_over_1_3;
	private Image left_over_1_4;

	private Image left_over_2_1;
	private Image left_over_2_2;
	private Image left_over_2_3;
	private Image left_over_2_4;

	private Image left_over_3_1;
	private Image left_over_3_2;
	private Image left_over_3_3;
	private Image left_over_3_4;

	private Image left_over_4_1;
	private Image left_over_4_2;
	private Image left_over_4_3;
	private Image left_over_4_4;

	private Image left_collider_1_1;
	private Image left_collider_1_2;
	private Image left_collider_1_3;
	private Image left_collider_1_4;

	private Image left_collider_2_1;
	private Image left_collider_2_2;
	private Image left_collider_2_3;
	private Image left_collider_2_4;

	private Image left_collider_3_1;
	private Image left_collider_3_2;
	private Image left_collider_3_3;
	private Image left_collider_3_4;

	private Image left_collider_4_1;
	private Image left_collider_4_2;
	private Image left_collider_4_3;
	private Image left_collider_4_4;

	private Image right_over_1_1;
	private Image right_over_1_2;
	private Image right_over_1_3;
	private Image right_over_1_4;

	private Image right_over_2_1;
	private Image right_over_2_2;
	private Image right_over_2_3;
	private Image right_over_2_4;

	private Image right_over_3_1;
	private Image right_over_3_2;
	private Image right_over_3_3;
	private Image right_over_3_4;

	private Image right_over_4_1;
	private Image right_over_4_2;
	private Image right_over_4_3;
	private Image right_over_4_4;

	private Image right_collider_1_1;
	private Image right_collider_1_2;
	private Image right_collider_1_3;
	private Image right_collider_1_4;

	private Image right_collider_2_1;
	private Image right_collider_2_2;
	private Image right_collider_2_3;
	private Image right_collider_2_4;

	private Image right_collider_3_1;
	private Image right_collider_3_2;
	private Image right_collider_3_3;
	private Image right_collider_3_4;

	private Image right_collider_4_1;
	private Image right_collider_4_2;
	private Image right_collider_4_3;
	private Image right_collider_4_4;

	private bool objectDragged;
	private Vector3 initialPosition;
	private Vector3 startPosition;
	private int initialIndex;
	private int startIndex;

	private bool _mouseOver = false;
	// Use this for initialization
	void Start () {

		actualValue = normalValue;

		initialPosition = gameObject.transform.localPosition;
		initialIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

		if (gameObject.GetComponent<EventTrigger> () != null) {
			EventTrigger _prev_trigger_Object = gameObject.GetComponent<EventTrigger> ();
			List<EventTrigger.Entry> entriesToRemove = new List<EventTrigger.Entry>();

			foreach (var entry in _prev_trigger_Object.triggers) {        
				entry.callback.RemoveAllListeners ();
				entriesToRemove.Add (entry);
			}

			foreach(var entry in entriesToRemove)
			{
				_prev_trigger_Object.triggers.Remove(entry);
			}
		} else {
			gameObject.AddComponent<EventTrigger> ();
		}

		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();

		EventTrigger.Entry entry_0 = new EventTrigger.Entry();
		entry_0.eventID = EventTriggerType.BeginDrag;
		entry_0.callback.AddListener((data) => { BeginDrag_((PointerEventData)data); });

		EventTrigger.Entry entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.Drag;
		entry_1.callback.AddListener((data) => { ObjectDrag_((PointerEventData)data); });

		EventTrigger.Entry entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.EndDrag;
		entry_2.callback.AddListener((data) => { ObjectDrop_((PointerEventData)data); });

		EventTrigger.Entry entry_3 = new EventTrigger.Entry();
		entry_3.eventID = EventTriggerType.PointerDown;
		entry_3.callback.AddListener((data) => { ObjectMouseDown_((PointerEventData)data); });

		EventTrigger.Entry entry_4 = new EventTrigger.Entry();
		entry_4.eventID = EventTriggerType.PointerUp;
		entry_4.callback.AddListener((data) => { ObjectMouseUp_((PointerEventData)data); });

		EventTrigger.Entry entry_5 = new EventTrigger.Entry();
		entry_5.eventID = EventTriggerType.PointerEnter;
		entry_5.callback.AddListener((data) => { ObjectMouseOver_((PointerEventData)data); });

		EventTrigger.Entry entry_6 = new EventTrigger.Entry();
		entry_6.eventID = EventTriggerType.PointerExit;
		entry_6.callback.AddListener((data) => { ObjectMouseOut_((PointerEventData)data); });

		trigger_Object.triggers.Add(entry_0);
		trigger_Object.triggers.Add(entry_1);
		trigger_Object.triggers.Add(entry_2);
		trigger_Object.triggers.Add(entry_3);
		trigger_Object.triggers.Add(entry_4);

		trigger_Object.triggers.Add(entry_5);
		trigger_Object.triggers.Add(entry_6);

		_situation = GameObject.Find ("ElectricalBench");

		_leftOvers = GameObject.Find ("Canvas/ElectricalBench/leftOvers");
		_leftColliders = GameObject.Find ("Canvas/ElectricalBench/leftColliders");

		_rightOvers = GameObject.Find ("Canvas/ElectricalBench/rightOvers");
		_rightColliders = GameObject.Find ("Canvas/ElectricalBench/rightColliders");

		left_over_1_1 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_1_1").GetComponent<Image>();
		left_over_1_2 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_1_2").GetComponent<Image>();
		left_over_1_3 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_1_3").GetComponent<Image>();
		left_over_1_4 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_1_4").GetComponent<Image>();

		left_over_2_1 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_2_1").GetComponent<Image>();
		left_over_2_2 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_2_2").GetComponent<Image>();
		left_over_2_3 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_2_3").GetComponent<Image>();
		left_over_2_4 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_2_4").GetComponent<Image>();

		left_over_3_1 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_3_1").GetComponent<Image>();
		left_over_3_2 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_3_2").GetComponent<Image>();
		left_over_3_3 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_3_3").GetComponent<Image>();
		left_over_3_4 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_3_4").GetComponent<Image>();

		left_over_4_1 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_4_1").GetComponent<Image>();
		left_over_4_2 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_4_2").GetComponent<Image>();
		left_over_4_3 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_4_3").GetComponent<Image>();
		left_over_4_4 = GameObject.Find ("Canvas/ElectricalBench/leftOvers/over_4_4").GetComponent<Image>();

		left_collider_1_1 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_1_1").GetComponent<Image>();
		left_collider_1_2 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_1_2").GetComponent<Image>();
		left_collider_1_3 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_1_3").GetComponent<Image>();
		left_collider_1_4 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_1_4").GetComponent<Image>();

		left_collider_2_1 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_2_1").GetComponent<Image>();
		left_collider_2_2 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_2_2").GetComponent<Image>();
		left_collider_2_3 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_2_3").GetComponent<Image>();
		left_collider_2_4 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_2_4").GetComponent<Image>();

		left_collider_3_1 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_3_1").GetComponent<Image>();
		left_collider_3_2 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_3_2").GetComponent<Image>();
		left_collider_3_3 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_3_3").GetComponent<Image>();
		left_collider_3_4 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_3_4").GetComponent<Image>();

		left_collider_4_1 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_4_1").GetComponent<Image>();
		left_collider_4_2 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_4_2").GetComponent<Image>();
		left_collider_4_3 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_4_3").GetComponent<Image>();
		left_collider_4_4 = GameObject.Find ("Canvas/ElectricalBench/leftColliders/over_4_4").GetComponent<Image>();


		right_over_1_1 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_1_1").GetComponent<Image>();
		right_over_1_2 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_1_2").GetComponent<Image>();
		right_over_1_3 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_1_3").GetComponent<Image>();
		right_over_1_4 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_1_4").GetComponent<Image>();

		right_over_2_1 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_2_1").GetComponent<Image>();
		right_over_2_2 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_2_2").GetComponent<Image>();
		right_over_2_3 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_2_3").GetComponent<Image>();
		right_over_2_4 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_2_4").GetComponent<Image>();

		right_over_3_1 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_3_1").GetComponent<Image>();
		right_over_3_2 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_3_2").GetComponent<Image>();
		right_over_3_3 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_3_3").GetComponent<Image>();
		right_over_3_4 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_3_4").GetComponent<Image>();

		right_over_4_1 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_4_1").GetComponent<Image>();
		right_over_4_2 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_4_2").GetComponent<Image>();
		right_over_4_3 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_4_3").GetComponent<Image>();
		right_over_4_4 = GameObject.Find ("Canvas/ElectricalBench/rightOvers/over_4_4").GetComponent<Image>();

		right_collider_1_1 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_1_1").GetComponent<Image>();
		right_collider_1_2 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_1_2").GetComponent<Image>();
		right_collider_1_3 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_1_3").GetComponent<Image>();
		right_collider_1_4 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_1_4").GetComponent<Image>();

		right_collider_2_1 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_2_1").GetComponent<Image>();
		right_collider_2_2 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_2_2").GetComponent<Image>();
		right_collider_2_3 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_2_3").GetComponent<Image>();
		right_collider_2_4 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_2_4").GetComponent<Image>();

		right_collider_3_1 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_3_1").GetComponent<Image>();
		right_collider_3_2 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_3_2").GetComponent<Image>();
		right_collider_3_3 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_3_3").GetComponent<Image>();
		right_collider_3_4 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_3_4").GetComponent<Image>();

		right_collider_4_1 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_4_1").GetComponent<Image>();
		right_collider_4_2 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_4_2").GetComponent<Image>();
		right_collider_4_3 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_4_3").GetComponent<Image>();
		right_collider_4_4 = GameObject.Find ("Canvas/ElectricalBench/rightColliders/over_4_4").GetComponent<Image>();

		/*
		left_over_1_1.enabled = false;
		left_over_1_2.enabled = false;
		left_over_1_3.enabled = false;
		left_over_1_4.enabled = false;

		left_over_2_1.enabled = false;
		left_over_2_2.enabled = false;
		left_over_2_3.enabled = false;
		left_over_2_4.enabled = false;

		left_over_3_1.enabled = false;
		left_over_3_2.enabled = false;
		left_over_3_3.enabled = false;
		left_over_3_4.enabled = false;

		left_over_4_1.enabled = false;
		left_over_4_2.enabled = false;
		left_over_4_3.enabled = false;
		left_over_4_4.enabled = false;
		*/

		left_over_1_1.CrossFadeAlpha(0, 0.01f, false);
		left_over_1_2.CrossFadeAlpha(0, 0.01f, false);
		left_over_1_3.CrossFadeAlpha(0, 0.01f, false);
		left_over_1_4.CrossFadeAlpha(0, 0.01f, false);

		left_over_2_1.CrossFadeAlpha(0, 0.01f, false);
		left_over_2_2.CrossFadeAlpha(0, 0.01f, false);
		left_over_2_3.CrossFadeAlpha(0, 0.01f, false);
		left_over_2_4.CrossFadeAlpha(0, 0.01f, false);

		left_over_3_1.CrossFadeAlpha(0, 0.01f, false);
		left_over_3_2.CrossFadeAlpha(0, 0.01f, false);
		left_over_3_3.CrossFadeAlpha(0, 0.01f, false);
		left_over_3_4.CrossFadeAlpha(0, 0.01f, false);

		left_over_4_1.CrossFadeAlpha(0, 0.01f, false);
		left_over_4_2.CrossFadeAlpha(0, 0.01f, false);
		left_over_4_3.CrossFadeAlpha(0, 0.01f, false);
		left_over_4_4.CrossFadeAlpha(0, 0.01f, false);

		left_collider_1_1.enabled = false;
		left_collider_1_2.enabled = false;
		left_collider_1_3.enabled = false;
		left_collider_1_4.enabled = false;

		left_collider_2_1.enabled = false;
		left_collider_2_2.enabled = false;
		left_collider_2_3.enabled = false;
		left_collider_2_4.enabled = false;

		left_collider_3_1.enabled = false;
		left_collider_3_2.enabled = false;
		left_collider_3_3.enabled = false;
		left_collider_3_4.enabled = false;

		left_collider_4_1.enabled = false;
		left_collider_4_2.enabled = false;
		left_collider_4_3.enabled = false;
		left_collider_4_4.enabled = false;

		/*
		right_over_1_1.enabled = false;
		right_over_1_2.enabled = false;
		right_over_1_3.enabled = false;
		right_over_1_4.enabled = false;

		right_over_2_1.enabled = false;
		right_over_2_2.enabled = false;
		right_over_2_3.enabled = false;
		right_over_2_4.enabled = false;

		right_over_3_1.enabled = false;
		right_over_3_2.enabled = false;
		right_over_3_3.enabled = false;
		right_over_3_4.enabled = false;

		right_over_4_1.enabled = false;
		right_over_4_2.enabled = false;
		right_over_4_3.enabled = false;
		right_over_4_4.enabled = false;
		*/

		right_over_1_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_4.CrossFadeAlpha(0, 0.01f, false);

		right_over_2_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_4.CrossFadeAlpha(0, 0.01f, false);

		right_over_3_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_4.CrossFadeAlpha(0, 0.01f, false);

		right_over_4_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_4.CrossFadeAlpha(0, 0.01f, false);

		right_collider_1_1.enabled = false;
		right_collider_1_2.enabled = false;
		right_collider_1_3.enabled = false;
		right_collider_1_4.enabled = false;

		right_collider_2_1.enabled = false;
		right_collider_2_2.enabled = false;
		right_collider_2_3.enabled = false;
		right_collider_2_4.enabled = false;

		right_collider_3_1.enabled = false;
		right_collider_3_2.enabled = false;
		right_collider_3_3.enabled = false;
		right_collider_3_4.enabled = false;

		right_collider_4_1.enabled = false;
		right_collider_4_2.enabled = false;
		right_collider_4_3.enabled = false;
		right_collider_4_4.enabled = false;

	}

	private void BeginDrag_(PointerEventData data)
	{
		if(dragEnabled == true){
			objectDragged = true;
			startPosition = gameObject.transform.localPosition;
			startIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

			gameObject.GetComponent<RectTransform> ().SetAsLastSibling();

			_actualImage.sprite = _benchPosition;
			_actualImage.SetNativeSize ();

			if (_typeElement == typesDragElement.Input) {
				_leftColliders.transform.SetAsLastSibling ();
				activateLeftColliders ();
			} else {
				_rightColliders.transform.SetAsLastSibling ();
				activateRightColliders ();
			}
				
		}
	}

	private void ObjectDrag_(PointerEventData data)
	{
		if (objectDragged == true) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			gameObject.GetComponent<RectTransform>().position = new Vector3(mousePos.x, mousePos.y, 0);

			if(data.pointerEnter){
				switch(data.pointerEnter.name){
				case "over_1_1":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (1, 1);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 1)) {
							left_over_1_1.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (1, 1);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 1)) {
							right_over_1_1.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_1_2":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (1, 2);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 2)) {
							left_over_1_2.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (1, 1);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 2)) {
							right_over_1_2.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_1_3":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (1, 3);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 3)) {
							left_over_1_3.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (1, 3);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 3)) {
							right_over_1_3.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_1_4":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (1, 4);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 4)) {
							left_over_1_4.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (1, 4);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 4)) {
							right_over_1_4.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_2_1":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (2, 1);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 1)) {
							left_over_2_1.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (2, 1);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 1)) {
							right_over_2_1.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_2_2":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (2, 2);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 2)) {
							left_over_2_2.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (2, 2);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 2)) {
							right_over_2_2.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_2_3":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (2, 3);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 3)) {
							left_over_2_3.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (2, 3);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 3)) {
							right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_2_4":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (2, 4);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 4)) {
							left_over_2_4.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (2, 4);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 4)) {
							right_over_2_4.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_3_1":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (3, 1);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 1)) {
							left_over_3_1.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (3, 1);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 1)) {
							right_over_3_1.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_3_2":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (3, 2);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 2)) {
							left_over_3_2.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (3, 2);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 2)) {
							right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_3_3":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (3, 3);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 3)) {
							left_over_3_3.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (3, 3);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 3)) {
							right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_3_4":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (3, 4);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 4)) {
							left_over_3_4.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (3, 4);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 4)) {
							right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_4_1":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (4, 1);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 1)) {
							left_over_4_1.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (4, 1);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 1)) {
							right_over_4_1.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_4_2":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (4, 2);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 2)) {
							left_over_4_2.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (4, 2);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 2)) {
							right_over_4_2.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_4_3":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (4, 3);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 3)) {
							left_over_4_3.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (4, 3);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 3)) {
							right_over_4_3.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_4_4":
					if (_typeElement == typesDragElement.Input) {
						deactivateLeftOversWithException (4, 4);
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 4)) {
							left_over_4_4.CrossFadeAlpha (1, 0.2f, false);
						}
					} else {
						deactivateRightOversWithException (4, 4);
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 4)) {
							right_over_4_4.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				default:
					deactivateOvers ();
					break;	
				}
			}

		}
	}

	private void ObjectDrop_(PointerEventData data)
	{
		if (objectDragged == true) {
			objectDragged = false;

			deactivateOvers ();
			deactivateLeftColliders ();
			deactivateRightColliders ();

			if (data.pointerEnter) {
				switch (data.pointerEnter.name) {
				case "over_1_1":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 1)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy (1, 1);
							gameObject.transform.position = left_over_1_1.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 1;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 1)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy (1, 1);
							gameObject.transform.position = right_over_1_1.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 1;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_1_2":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 2)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(1, 2);
							gameObject.transform.position = left_over_1_2.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 2;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 2)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(1, 2);
							gameObject.transform.position = right_over_1_2.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 2;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_1_3":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 3)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(1, 3);
							gameObject.transform.position = left_over_1_3.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 3;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 3)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(1, 3);
							gameObject.transform.position = right_over_1_3.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 3;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_1_4":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 4)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(1, 4);
							gameObject.transform.position = left_over_1_4.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 4;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 4)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(1, 4);
							gameObject.transform.position = right_over_1_4.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 4;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_2_1":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 1)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(2, 1);
							gameObject.transform.position = left_over_2_1.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 1;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 1)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(2, 1);
							gameObject.transform.position = right_over_2_1.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 1;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_2_2":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 2)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(2, 2);
							gameObject.transform.position = left_over_2_2.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 2;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 2)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(2, 2);
							gameObject.transform.position = right_over_2_2.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 2;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_2_3":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 3)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(2, 3);
							gameObject.transform.position = left_over_2_3.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 3;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 3)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(2, 3);
							gameObject.transform.position = right_over_2_3.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 3;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_2_4":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 4)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(2, 4);
							gameObject.transform.position = left_over_2_4.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 4;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 4)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(2, 4);
							gameObject.transform.position = right_over_2_4.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 4;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_3_1":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 1)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(3, 1);
							gameObject.transform.position = left_over_3_1.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 1;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 1)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(3, 1);
							gameObject.transform.position = right_over_3_1.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 1;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_3_2":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 2)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(3, 2);
							gameObject.transform.position = left_over_3_2.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 2;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 2)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(3, 2);
							gameObject.transform.position = right_over_3_2.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 2;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_3_3":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 3)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(3, 3);
							gameObject.transform.position = left_over_3_3.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 3;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 3)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(3, 3);
							gameObject.transform.position = right_over_3_3.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 3;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_3_4":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 4)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(3, 4);
							gameObject.transform.position = left_over_3_4.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 4;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 4)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(3, 4);
							gameObject.transform.position = right_over_3_4.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 4;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_4_1":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 1)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(4, 1);
							gameObject.transform.position = left_over_4_1.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 1;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 1)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(4, 1);
							gameObject.transform.position = right_over_4_1.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 1;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_4_2":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 2)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(4, 2);
							gameObject.transform.position = left_over_4_2.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 2;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 2)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(4, 2);
							gameObject.transform.position = right_over_4_2.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 2;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_4_3":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 3)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(4, 3);
							gameObject.transform.position = left_over_4_3.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 3;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 3)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(4, 3);
							gameObject.transform.position = right_over_4_3.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 3;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_4_4":
					if (_typeElement == typesDragElement.Input) {
						if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 4)) {
							_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(4, 4);
							gameObject.transform.position = left_over_4_4.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 4;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addInputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index);
							setBlackCable (rowUsed, columnUsed, _index);
							if(_grayCablePrefab){
								setGrayCable(rowUsed, columnUsed, _index);	
							}
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 4)) {
							_rightOvers.GetComponent<benchAvailability> ().setAreaBusy(4, 4);
							gameObject.transform.position = right_over_4_4.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 4;
							int _index = _situation.GetComponent<ElectricalTestBenchMainClass> ().addOutputElement (gameObject);
							_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				default:
					tweenToInitialPosition ();
					break;	
				}
			} else {
				tweenToInitialPosition ();
			}
		}
	}

	private void ObjectMouseDown_(PointerEventData data){
		if(inBench == true && _typeElement == typesDragElement.Input){
			if (isSwitch == true) {
				if (actualValue == normalValue) {

					actualValue = pressedValue;

					if( gameObject.GetComponent<AudioSource> () ){
						if (_downAudio) {
							gameObject.GetComponent<AudioSource> ().clip = _downAudio;
							gameObject.GetComponent<AudioSource> ().Play ();
						}
					}

					if(_pressedImage){
						_pressedImage.enabled = true;
					}

					_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
				} else {
					actualValue = normalValue;

					if( gameObject.GetComponent<AudioSource> () ){
						if (_upAudio) {
							gameObject.GetComponent<AudioSource> ().clip = _upAudio;
							gameObject.GetComponent<AudioSource> ().Play ();
						}
					}

					if(_pressedImage){
						_pressedImage.enabled = false;
					}

					_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
				}
			} else {
				actualValue = pressedValue;

				if( gameObject.GetComponent<AudioSource> () ){
					if (_downAudio) {
						gameObject.GetComponent<AudioSource> ().clip = _downAudio;
						gameObject.GetComponent<AudioSource> ().Play ();
					}
				}

				if(_pressedImage){
					_pressedImage.enabled = true;
				}

				_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
			}
		}
	}

	private void ObjectMouseUp_(PointerEventData data){
		if(inBench == true && _typeElement == typesDragElement.Input){
			if (isSwitch == false) {

				actualValue = normalValue;

				if( gameObject.GetComponent<AudioSource> () ){
					if (_upAudio) {
						gameObject.GetComponent<AudioSource> ().clip = _upAudio;
						gameObject.GetComponent<AudioSource> ().Play ();
					}
				}

				if(_pressedImage){
					_pressedImage.enabled = false;
				}

				_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
			}
		}
	}

	private void ObjectMouseOver_(PointerEventData data){
		if(objectDragged == false){
			_mouseOver = true;
			_situation.GetComponent<ElectricalTestBenchMainClass> ()._msgArea.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='" + textTag + "']").InnerText;	
		}
	}

	private void ObjectMouseOut_(PointerEventData data){
		if(_mouseOver == true){
			_mouseOver = false;
			_situation.GetComponent<ElectricalTestBenchMainClass> ()._msgArea.text = "";
		}
	}

	private void tweenToInitialPosition(){
		iTween.MoveTo(
			gameObject,
			iTween.Hash(
				"position", initialPosition,
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 0.2f,
				"islocal",true
			)
		);

		Invoke ("goToInitialState", 0.1f);
	}

	private void goToInitialState(){
		_actualImage.sprite = _normalPosition;
		_actualImage.SetNativeSize ();
		gameObject.GetComponent<RectTransform> ().SetSiblingIndex (initialIndex);
	}

	private void activateLeftColliders(){
		left_collider_1_1.enabled = true;
		left_collider_1_2.enabled = true;
		left_collider_1_3.enabled = true;
		left_collider_1_4.enabled = true;

		left_collider_2_1.enabled = true;
		left_collider_2_2.enabled = true;
		left_collider_2_3.enabled = true;
		left_collider_2_4.enabled = true;

		left_collider_3_1.enabled = true;
		left_collider_3_2.enabled = true;
		left_collider_3_3.enabled = true;
		left_collider_3_4.enabled = true;

		left_collider_4_1.enabled = true;
		left_collider_4_2.enabled = true;
		left_collider_4_3.enabled = true;
		left_collider_4_4.enabled = true;
	}

	private void deactivateLeftColliders(){
		left_collider_1_1.enabled = false;
		left_collider_1_2.enabled = false;
		left_collider_1_3.enabled = false;
		left_collider_1_4.enabled = false;

		left_collider_2_1.enabled = false;
		left_collider_2_2.enabled = false;
		left_collider_2_3.enabled = false;
		left_collider_2_4.enabled = false;

		left_collider_3_1.enabled = false;
		left_collider_3_2.enabled = false;
		left_collider_3_3.enabled = false;
		left_collider_3_4.enabled = false;

		left_collider_4_1.enabled = false;
		left_collider_4_2.enabled = false;
		left_collider_4_3.enabled = false;
		left_collider_4_4.enabled = false;
	}

	private void activateRightColliders(){
		right_collider_1_1.enabled = true;
		right_collider_1_2.enabled = true;
		right_collider_1_3.enabled = true;
		right_collider_1_4.enabled = true;

		right_collider_2_1.enabled = true;
		right_collider_2_2.enabled = true;
		right_collider_2_3.enabled = true;
		right_collider_2_4.enabled = true;

		right_collider_3_1.enabled = true;
		right_collider_3_2.enabled = true;
		right_collider_3_3.enabled = true;
		right_collider_3_4.enabled = true;

		right_collider_4_1.enabled = true;
		right_collider_4_2.enabled = true;
		right_collider_4_3.enabled = true;
		right_collider_4_4.enabled = true;
	}

	private void deactivateRightColliders(){
		right_collider_1_1.enabled = false;
		right_collider_1_2.enabled = false;
		right_collider_1_3.enabled = false;
		right_collider_1_4.enabled = false;

		right_collider_2_1.enabled = false;
		right_collider_2_2.enabled = false;
		right_collider_2_3.enabled = false;
		right_collider_2_4.enabled = false;

		right_collider_3_1.enabled = false;
		right_collider_3_2.enabled = false;
		right_collider_3_3.enabled = false;
		right_collider_3_4.enabled = false;

		right_collider_4_1.enabled = false;
		right_collider_4_2.enabled = false;
		right_collider_4_3.enabled = false;
		right_collider_4_4.enabled = false;
	}

	private void deactivateOvers(){

		left_over_1_1.CrossFadeAlpha(0, 0.2f, false);
		left_over_1_2.CrossFadeAlpha(0, 0.2f, false);
		left_over_1_3.CrossFadeAlpha(0, 0.2f, false);
		left_over_1_4.CrossFadeAlpha(0, 0.2f, false);

		left_over_2_1.CrossFadeAlpha(0, 0.2f, false);
		left_over_2_2.CrossFadeAlpha(0, 0.2f, false);
		left_over_2_3.CrossFadeAlpha(0, 0.2f, false);
		left_over_2_4.CrossFadeAlpha(0, 0.2f, false);

		left_over_3_1.CrossFadeAlpha(0, 0.2f, false);
		left_over_3_2.CrossFadeAlpha(0, 0.2f, false);
		left_over_3_3.CrossFadeAlpha(0, 0.2f, false);
		left_over_3_4.CrossFadeAlpha(0, 0.2f, false);

		left_over_4_1.CrossFadeAlpha(0, 0.2f, false);
		left_over_4_2.CrossFadeAlpha(0, 0.2f, false);
		left_over_4_3.CrossFadeAlpha(0, 0.2f, false);
		left_over_4_4.CrossFadeAlpha(0, 0.2f, false);

		right_over_1_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_4.CrossFadeAlpha(0, 0.2f, false);

		right_over_2_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_4.CrossFadeAlpha(0, 0.2f, false);

		right_over_3_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_4.CrossFadeAlpha(0, 0.2f, false);

		right_over_4_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_4.CrossFadeAlpha(0, 0.2f, false);
	}

	public void activateLeftOver(int _row = 0, int _column = 0, bool _hideAutomatically = true){

		bool feedbackActivated = true;

		if(_row == 0 && _column == 0){
			feedbackActivated = false;
			_row = rowUsed;
			_column = columnUsed;
		}

		switch(_row){
		case 1:
			switch(_column){
			case 1:
				left_over_1_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				left_over_1_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				left_over_1_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				left_over_1_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		case 2:
			switch(_column){
			case 1:
				left_over_2_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				left_over_2_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				left_over_2_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				left_over_2_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		case 3:
			switch(_column){
			case 1:
				left_over_3_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				left_over_3_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				left_over_3_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				left_over_3_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		case 4:
			switch(_column){
			case 1:
				left_over_4_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				left_over_4_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				left_over_4_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				left_over_4_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		}

		if(_hideAutomatically == true){
			Invoke ("deactivateOvers", 0.3f);
			if(feedbackActivated == true){
				Invoke ("PostactivateLeftOver", 0.7f);	
			}
		}
	}

	private void PostactivateLeftOver(){
		activateLeftOver ();
	}

	public void activateRightOver(int _row = 0, int _column = 0, bool _hideAutomatically = true){
		bool feedbackActivated = true;

		if(_row == 0 && _column == 0){
			feedbackActivated = false;
			_row = rowUsed;
			_column = columnUsed;
		}

		switch(_row){
		case 1:
			switch(_column){
			case 1:
				right_over_1_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				right_over_1_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				right_over_1_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				right_over_1_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		case 2:
			switch(_column){
			case 1:
				right_over_2_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				right_over_2_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				right_over_2_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		case 3:
			switch(_column){
			case 1:
				right_over_3_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		case 4:
			switch(_column){
			case 1:
				right_over_4_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				right_over_4_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				right_over_4_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				right_over_4_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		}

		if(_hideAutomatically == true){
			Invoke ("deactivateOvers", 0.3f);
			if(feedbackActivated == true){
				Invoke ("PostactivateRightOver", 0.7f);	
			}
		}
	}

	private void PostactivateRightOver(){
		activateRightOver ();
	}

	public void setValue(int _value){
		actualValue = _value;
		if (actualValue == 1) {
			if( gameObject.GetComponent<AudioSource> () ){
				if (_downAudio) {
					gameObject.GetComponent<AudioSource> ().clip = _downAudio;
					if(gameObject.GetComponent<AudioSource> ().isPlaying == false){
						gameObject.GetComponent<AudioSource> ().Play ();
					}
				}
			}
			if(_pressedImage){
				_pressedImage.enabled = true;
			}
		} else {
			if( gameObject.GetComponent<AudioSource> () ){
				if (_downAudio) {
					gameObject.GetComponent<AudioSource> ().Stop ();
				}
			}
			if(_pressedImage){
				_pressedImage.enabled = false;
			}
		}
	}

	private void deactivateLeftOversWithException(int _row, int _column){
		if(!(_row == 1 && _column == 1)){
			left_over_1_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 1 && _column == 2)){
			left_over_1_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 1 && _column == 3)){
			left_over_1_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 1 && _column == 4)){
			left_over_1_4.CrossFadeAlpha(0, 0.2f, false);
		}

		if(!(_row == 2 && _column == 1)){
			left_over_2_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 2 && _column == 2)){
			left_over_2_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 2 && _column == 3)){
			left_over_2_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 2 && _column == 4)){
			left_over_2_4.CrossFadeAlpha(0, 0.2f, false);
		}

		if(!(_row == 3 && _column == 1)){
			left_over_3_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 3 && _column == 2)){
			left_over_3_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 3 && _column == 3)){
			left_over_3_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 3 && _column == 4)){
			left_over_3_4.CrossFadeAlpha(0, 0.2f, false);
		}

		if(!(_row == 4 && _column == 1)){
			left_over_4_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 4 && _column == 2)){
			left_over_4_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 4 && _column == 3)){
			left_over_4_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 4 && _column == 4)){
			left_over_4_4.CrossFadeAlpha(0, 0.2f, false);
		}
	}

	private void deactivateRightOversWithException(int _row, int _column){
		if(!(_row == 1 && _column == 1)){
			right_over_1_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 1 && _column == 2)){
			right_over_1_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 1 && _column == 3)){
			right_over_1_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 1 && _column == 4)){
			right_over_1_4.CrossFadeAlpha(0, 0.2f, false);
		}

		if(!(_row == 2 && _column == 1)){
			right_over_2_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 2 && _column == 2)){
			right_over_2_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 2 && _column == 3)){
			right_over_2_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 2 && _column == 4)){
			right_over_2_4.CrossFadeAlpha(0, 0.2f, false);
		}

		if(!(_row == 3 && _column == 1)){
			right_over_3_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 3 && _column == 2)){
			right_over_3_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 3 && _column == 3)){
			right_over_3_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 3 && _column == 4)){
			right_over_3_4.CrossFadeAlpha(0, 0.2f, false);
		}

		if(!(_row == 4 && _column == 1)){
			right_over_4_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 4 && _column == 2)){
			right_over_4_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 4 && _column == 3)){
			right_over_4_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 4 && _column == 4)){
			right_over_4_4.CrossFadeAlpha(0, 0.2f, false);
		}
	}

	private void addNewElement (){
		GameObject newElement = Instantiate (gameObject, initialPosition, Quaternion.identity) as GameObject;
		newElement.transform.SetParent (GameObject.Find ("Canvas/ElectricalBench").transform, false);
		newElement.transform.SetSiblingIndex (initialIndex);
		newElement.GetComponent<dragElement> ()._actualImage.sprite = newElement.GetComponent<dragElement> ()._normalPosition;
		newElement.GetComponent<dragElement> ()._actualImage.SetNativeSize ();
	}

	private void setRedCable (int _row, int _column, int _input){
		_rightCable = Instantiate(_redCablePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_rightCable.transform.SetParent(GameObject.Find("Canvas/ElectricalBench").transform, false);
		_rightCable.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () + 1);
		_rightCable.GetComponent<redCable> ()._parentElement = gameObject;
		_rightCable.GetComponent<redCable> ().setPosition (_row, _column, _input);
	}

	private void setBlueCable (int _row, int _column, int _input){
		_leftCable = Instantiate(_blueCablePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_leftCable.transform.SetParent(GameObject.Find("Canvas/ElectricalBench").transform, false);
		_leftCable.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () + 1);
		_leftCable.GetComponent<blueCable> ()._parentElement = gameObject;
		_leftCable.GetComponent<blueCable> ().setPosition (_row, _column, _input);
	}

	private void setBlackCable (int _row, int _column, int _input){
		_leftCable = Instantiate(_blackCablePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_leftCable.transform.SetParent(GameObject.Find("Canvas/ElectricalBench").transform, false);
		_leftCable.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () + 1);
		_leftCable.GetComponent<blackCable> ()._parentElement = gameObject;
		_leftCable.GetComponent<blackCable> ().setPosition (_row, _column, _input);
	}

	private void setWhiteCable (int _row, int _column, int _input){
		_rightCable = Instantiate(_whiteCablePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_rightCable.transform.SetParent(GameObject.Find("Canvas/ElectricalBench").transform, false);
		_rightCable.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () + 1);
		_rightCable.GetComponent<whiteCable> ()._parentElement = gameObject;
		_rightCable.GetComponent<whiteCable> ().setPosition (_row, _column, _input);
	}

	private void setGrayCable (int _row, int _column, int _input){
		_topCable = Instantiate(_grayCablePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_topCable.transform.SetParent(GameObject.Find("Canvas/ElectricalBench").transform, false);
		_topCable.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () + 1);
		_topCable.GetComponent<grayCable> ()._parentElement = gameObject;
		_topCable.GetComponent<grayCable> ().setPosition (_row, _column, _input);
	}

	// Update is called once per frame
	void Update () {
		
	}
}
