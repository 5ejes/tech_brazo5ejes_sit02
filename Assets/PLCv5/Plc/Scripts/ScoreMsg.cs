﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreMsg : MonoBehaviour {

	public Text _title;
	public Button _acceptBt;
	public Text _scoreText;
	public Text _user;
	public Text _titleMsg;
	public Text _msg;
	// Use this for initialization
	void Start () {

		_title.font = (Font)Resources.Load("Fonts/ArialBold28");
		_acceptBt.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold34");
		_scoreText.font = (Font)Resources.Load("Fonts/ArialBold72");

		_title.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='score']").InnerText;
		_acceptBt.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText;
		_user.text = Manager.Instance.globalUser;
		Debug.Log ("here 1");
	}

	public void showScore(string _score, float _total){

		_scoreText.text = _score;

		if (_total >= Manager.Instance.globalMinimalSuccessScore) {
			_titleMsg.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='score_success']").InnerText;
			_msg.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='score_success_msg']").InnerText;
		} else {
			_titleMsg.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='score_fail']").InnerText;
			_msg.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='score_fail_msg']").InnerText;
		}

		iTween.MoveTo(
			gameObject,
			iTween.Hash(
				"position", new Vector3(-15,0,0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal",true
			)
		);

		StartCoroutine (loadListener());

	}

	IEnumerator loadListener(){
		if (Application.platform == RuntimePlatform.Android) {
			yield return new WaitForSeconds (4f);
		} else {
			yield return new WaitForSeconds (2f);
		}
		_acceptBt.onClick.AddListener (closeAction);
	}

	private void closeAction(){


		Debug.Log ("log 2");
		GameObject _background = GameObject.Find ("backgroundEval");
		if (_background != null) {
			Image _bg = GameObject.Find ("backgroundEval").GetComponent<Image>();
			_bg.CrossFadeAlpha (0f, 1f, false);
			Destroy(_background, 1.2f);
		}
		iTween.MoveTo(
			gameObject,
			iTween.Hash(
				"position", new Vector3(0,1200,0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal",true
			)
		);
        GameObject bg = GameObject.Find("TransversalBg");
		if (bg != null) {
			Image _bg1 = bg.GetComponent<Image>();
			if (_bg1)
			{
				_bg1.CrossFadeAlpha(0f, 1f, false);
				Destroy(bg, 1.2f);
			}
		}

        GameObject bg_Msg = GameObject.Find("blackoutWarning");
        if(bg_Msg) Destroy(bg_Msg, 1.2f);


        Destroy(gameObject, 1.1f);
	} 
	// Update is called once per frame
	void Update () {
		
	}
}
