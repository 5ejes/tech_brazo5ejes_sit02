﻿#pragma warning disable 0649
using NSUtilities;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace NSActiveZones
{
    [RequireComponent(typeof(Button))]
    public class ActiveZoneButtonUI : MonoBehaviour, IButtonWorld
    {
        #region members

        [SerializeField] private SpriteRenderer spriteRendererImagen;

        [SerializeField] private float animationVelocity = 1;

        [SerializeField] private float animationUmbral = 1;

        [Header("Event"), Space(10)] public UnityEvent OnButtonClickDown;

        #endregion

        private void OnEnable()
        {
            StartCoroutine(CouButtonAnimation());
        }

        public void OnClickDown()
        {
            Debug.Log("OnClickDown : " + gameObject.name);
            OnButtonClickDown.Invoke();
        }

        private IEnumerator CouButtonAnimation()
        {
            var tmpAngle = 0f;

            while (true)
            {
                tmpAngle += 0.5f * animationVelocity;
                tmpAngle %= 180f;
                spriteRendererImagen.color = new Color(1, 1, 1, Mathf.Sin(tmpAngle * Mathf.Deg2Rad) * animationUmbral);
                yield return null;
            }
        }
    }
}