﻿using System;

[Serializable]
public struct actionStructure
{
	public float x;
	public float y;
	public int _id;
	public string name;
	public int _type;
	public string _value;
	public int _index;
	public string _value2;
	public int _index2;
	public string _value3;
	public int _index3;
	public string _value4;
	public int _index4;
	public int _scale;
	public bool _constant1;
	public bool _constant2;
	public string _options;
	public int _editorType;
	public bool _isInverse;
	public bool _isReset;
	public string _title;
	public string _msg;
    public int _typeRobotic;
    public bool _isRobotic;
	public connModuleStructure _r_conn;
	public connModuleStructure _l_conn;
    public actionStructure[] _childActions;
}