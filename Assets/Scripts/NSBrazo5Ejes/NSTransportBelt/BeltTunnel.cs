﻿using System;
using System.Collections;
using System.Collections.Generic;
using Modular;
using NSUtilities;
using UnityEngine;

namespace NSBrazo5Ejes.NSTransportBelt
{
	public class BeltTunnel : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private Transform _spawnTransform;
		[Header("Event")]
		[SerializeField] private TransformEvent _onObjectUnpacked;
		private Queue<Transform> _queue = new Queue<Transform>();
		private List<Transform> _registeredObjects = new List<Transform>();

		/// <summary>
		/// Lista con los elementos almacenados
		/// </summary>
		/// <value></value>
		public List<Transform> StoredObjects { get => _registeredObjects; }

		/// <summary>
		/// Cuenta de los elementos que están almacenados
		/// </summary>
		public int PackedObjects => Queue.Count;

		/// <summary>
		/// Queue de los elementos listos para desempaquetar
		/// </summary>
		/// <value></value>
		public Queue<Transform> Queue { get => _queue; set => _queue = value; }

		/// <summary>
		/// Guarda el elemento dentro de la Queue
		/// </summary>
		/// <param name="storeObject">Transform que se va a almacenar</param>
		public void Store(Transform storeObject)
		{
			// Si el objeto a almacenar existe
			if (storeObject != null)
			{
				// Si el objeto no está en la lista
				if (!StoredObjects.Contains(storeObject))
				{
					// Desactivo el objeto
					storeObject.gameObject.SetActive(false);

					// Agrego el objeto a la lista
					StoredObjects.Add(storeObject);
				}
			}
		}

		public void PrintCount()
		{
			Debug.Log(PackedObjects);
		}

		/// <summary>
		/// Elimina los registros de los objetos del tunel
		/// </summary>
		public void ClearRegister()
		{
			// Limpio la lista y la queue
			StoredObjects.Clear();
			Queue.Clear();
		}

		/// <summary>
		/// Saca el elemento de la Queue
		/// </summary>
		public void Unpack()
		{
			// Si hay más de un elemento en la queue
			if (Queue.Count > 0)
			{

				// Obtengo el objeto agregado a la lista
				var storedObject = Queue.Dequeue();

				// Si la posición de spawn existe
				if (_spawnTransform)
				{
					// Tomo la posición del spawn
					storedObject.position = _spawnTransform.position;

					// Tomo la rotación del spawn
					storedObject.rotation = _spawnTransform.rotation;
				}

				// Activo el objeto
				storedObject.gameObject.SetActive(true);

				// Ejecuto el evento
				_onObjectUnpacked.WrapInvoke(storedObject);
			}
		}

		/// <summary>
		/// Prepara la queue con los elementos registrados en la lista
		/// </summary>
		public void CreateQueue()
		{
			// Si la queue no existe
			if (Queue == null) Queue = new Queue<Transform>();

			// Si la lista no es nula
			if (_registeredObjects != null)
			{
				// Obtengo la cuenta de la lista
				int length = _registeredObjects.Count;

				// Si hay al menos un elemento
				if (length > 0)
				{

					// Vacío la queue
					Queue.Clear();

					// Por cada elemento en la lista de registrados
					for (int listIndex = 0; listIndex < length; listIndex++)
					{
						// Obtengo el elemento actual
						var currentTransform = _registeredObjects[listIndex];

						// Agrego la referencia del transform a la queue
						Queue.Enqueue(currentTransform);
					}
				}
			}
		}
	}
}
