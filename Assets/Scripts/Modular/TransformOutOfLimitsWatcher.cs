﻿using System.Collections.Generic;
using NSUtilities;
using UnityEngine;

namespace Modular
{
	public class TransformOutOfLimitsWatcher : MonoBehaviour
	{
		[Header("List of values")]
		[SerializeField] private List<Transform> _watchValues = new List<Transform>();
		[SerializeField] private bool _readChildren = false;
		[SerializeField] private int _updatePeriod = 0;
		[Header("Limits")]
		[SerializeField] private Vector3 _maxLimits;
		[SerializeField] private Vector3 _minLimits;
		[Space]
		[SerializeField] private bool _ignoreX;
		[SerializeField] private bool _ignoreY;
		[SerializeField] private bool _ignoreZ;
		[Header("Events")]
		[SerializeField] private TransformEvent _onOutOfBoundsTransform = new TransformEvent();
		public List<Transform> WatchValues { get => _watchValues; set => _watchValues = value; }
		public bool ReadChildren { get => _readChildren; set => _readChildren = value; }

		public Vector3 MinLimits { get => _minLimits; set => _minLimits = value; }
		public Vector3 MaxLimits { get => _maxLimits; set => _maxLimits = value; }
		public bool IgnoreX { get => _ignoreX; set => _ignoreX = value; }
		public bool IgnoreY { get => _ignoreY; set => _ignoreY = value; }
		public bool IgnoreZ { get => _ignoreZ; set => _ignoreZ = value; }
		public int UpdatePeriod { get => _updatePeriod; set => _updatePeriod = value; }

		private void Awake()
		{
			if (ReadChildren)
				ReadChildrenFromTransform(transform);
		}

		private void Update()
		{
			// Si no hay update period
			if (UpdatePeriod == 0)
			{
				// Realizo la operación cada frame
				WatchOutOfBounds();
			}
			else if (Time.frameCount % UpdatePeriod == 0)
			{
				// Realizo la operación cada módulo de update period
				WatchOutOfBounds();
			}
		}

		private void WatchOutOfBounds()
		{
			if (WatchValues == null) return;

			int count = WatchValues.Count;
			if (count == 0) return;

			for (int i = 0; i < count; i++)
			{
				var currentWatchTransform = WatchValues[i];

				if (!IsInside(currentWatchTransform.position))
				{
					// Enviar el transform actual
					_onOutOfBoundsTransform.WrapInvoke(currentWatchTransform);
				}

			}
		}

		private bool IsInside(Vector3 point)
		{
			var maxWorldSpace = transform.TransformPoint(MaxLimits);
			var minWorldSpace = transform.TransformPoint(MinLimits);

			bool insideX, insideY, insideZ;

			insideX = !IgnoreX ? IsInsideBounds(point.x, minWorldSpace.x, maxWorldSpace.x) : true;
			insideY = !IgnoreY ? IsInsideBounds(point.y, minWorldSpace.y, maxWorldSpace.y) : true;
			insideZ = !IgnoreZ ? IsInsideBounds(point.x, minWorldSpace.y, maxWorldSpace.y) : true;

			return insideX && insideY && insideZ;
		}

		private static bool IsInsideBounds(float point, float min, float max)
		{
			return (point > min && point < max);
		}

		public void ReadChildrenFromTransform(Transform target)
		{
			if (target == null) return;

			// Limpio la lista
			WatchValues.Clear();

			foreach (Transform child in target)
			{
				// Anexo el hijo a la lista
				WatchValues.Add(child);
			}
		}
	}
}
