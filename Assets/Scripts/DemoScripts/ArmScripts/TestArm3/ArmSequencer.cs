using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NSArm3Axis
{

	public class ArmSequencer : MonoBehaviour
	{
		#region members

		/// <summary>
		/// Para saber que accion esta haciendo el brazo actualmente
		/// </summary>
		[Header("Settings")][SerializeField] private ActualAction actualAction;
		[Header("References")]
		[SerializeField] private Transform _transformPositionMinConverter;
		/// <summary>
		/// Target que sigue los modelos 3D del brazo
		/// </summary>
		[SerializeField] private Transform _transformGripPivot;

		/// <summary>
		/// posicion hacia donde se mueve el brazo
		/// </summary>
		private Vector3 positionObjective;

		/// <summary>
		/// Lista con todas las acciones, para ejecutar la secuencia
		/// </summary>
		private List<GripAction> _currentRegisteredSequences = new List<GripAction>();
		private Queue<GripAction> _executionQueue = new Queue<GripAction>();

		private bool _executing = false;
		private bool _pause = false;

		/// <summary>
		/// Referencia a la clase del objeto recolectado
		/// </summary>
		private AbstractCollectableObject refAbstractCollectableObject;

		[Header("Events")]

		[SerializeField] private Modular.Vector3Event _onGripMoveDownOrder;
		[SerializeField] private Modular.Vector3Event _onGripMoveUpOrder;
		[SerializeField] private Modular.Vector3Event _onGripGoIntoPosition;
		[SerializeField] private UnityEvent _onGripReleaseOrder;
		[SerializeField] private UnityEvent _onGripCollectOrder;
		[SerializeField] private UnityEvent _onWaitingForNewActionOrder;

		public bool Executing { get => _executing; }
		public bool Pause { get => _pause; }

		private Vector3 previousActionPosition
		{
			get
			{
				// Obtengo la cuenta de las acciones registradas
				int count = _currentRegisteredSequences.Count;
				if (count > 0)
				{
					// Itero sobre las acciones registradas en reversa
					for (int i = count - 1; i >= 0; i--)
					{
						// Obtengo la ultima secuencia actual
						var lastSequence = _currentRegisteredSequences[i];

						// Si la ultima accion es collecting o es releasing
						if (lastSequence.action.Equals(ActualAction.GriperCollecting) || lastSequence.action.Equals(ActualAction.GriperReleasing))
						{
							// Ignoro
							continue;
						}
						else
						{
							// Si no, retorno la posición de la acción actual
							return lastSequence.position;
						}
					}

					// Si no se encuentran acciones válidas, retorno el valor por defecto
					return Vector3.zero;
				}
				else
				{
					// Retorno el valor por defecto
					return Vector3.zero;
				}
			}
		}

		#endregion

		#region properties

		// TODO: Comentar código

		public void SetActualAction(GripAction gripAction)
		{

			actualAction = gripAction.action;

			switch (actualAction)
			{
				case ActualAction.GriperMoveDown:
					if (_onGripMoveDownOrder != null) _onGripMoveDownOrder.Invoke(gripAction.position);
					break;

				case ActualAction.GriperMoveUp:
					if (_onGripMoveUpOrder != null) _onGripMoveUpOrder.Invoke(gripAction.position);
					break;

				case ActualAction.GriperGoIntoPosition:
					if (_onGripGoIntoPosition != null) _onGripGoIntoPosition.Invoke(gripAction.position);
					break;

				case ActualAction.GriperCollecting:
					if (_onGripCollectOrder != null) _onGripCollectOrder.Invoke();
					break;

				case ActualAction.GriperReleasing:
					if (_onGripReleaseOrder != null) _onGripReleaseOrder.Invoke();
					break;
				case ActualAction.WaitingForNewAction:
					if (_onWaitingForNewActionOrder != null) _onWaitingForNewActionOrder.Invoke();
					break;
			}
		}

		#endregion

		#region MonoBehaviour

		private void Start()
		{

			//AddActionPolarCoordinate(45f, 1f);
			//AddActionGripMoveDown(0.25f);

			// AddActionPlaneCoordinate(0.7f, 0.3f);
			// AddActionGripMoveDown(0f);
			// AddActionGripCollect();
			// AddActionGripMoveUp(0.5f);
			// AddActionPlaneCoordinate(0.5f, -0.1f);
			// AddActionGripMoveDown(0);
			// AddActionGripRelease();
			// AddActionGripMoveUp(0.5f);

			// AddActionPlaneCoordinate(0.1f, 0.6f);
			// AddActionGripMoveDown(0f);
			// AddActionGripCollect();
			// AddActionGripMoveUp(0.5f);
			// AddActionPlaneCoordinate(0.5f, -0.1f);
			// AddActionGripMoveDown(0.1f);
			// AddActionGripRelease();
			// AddActionGripMoveUp(0.5f);

			// AddActionPlaneCoordinate(0.4f, 0.4f);
			// AddActionGripMoveDown(0f);
			// AddActionGripCollect();
			// AddActionGripMoveUp(0.5f);
			// AddActionPlaneCoordinate(0.5f, -0.1f);
			// AddActionGripMoveDown(0.2f);
			// AddActionGripRelease();
			// AddActionGripMoveUp(0.5f);

			// AddActionPlaneCoordinate(0.4f, 0.7f);
			// AddActionGripMoveDown(0f);
			// AddActionGripCollect();
			// AddActionGripMoveUp(0.5f);
			// AddActionPlaneCoordinate(0.5f, -0.1f);
			// AddActionGripMoveDown(0.3f);
			// AddActionGripRelease();
			// AddActionGripMoveUp(0.5f);

			//AddActionPolarCoordinate(35f, 1f);
			//AddActionGripMoveDown(0.25f);
			//AddActionPlaneCoordinate(0.5f, 0.5f);
			//AddActionGripMoveUp(0.75f);

			StartCoroutine(DelayedStart(1f));

		}

		private IEnumerator DelayedStart(float seconds)
		{
			yield return new WaitForSeconds(seconds);

			AddActionGripMoveUp(0.5f);
			AddActionPlaneCoordinate(0.2f, 0.2f);
			AddActionGripMoveDown(0.35f);
			AddActionGripCollect();

			AddActionGripMoveUp(0.5f);
			AddActionPlaneCoordinate(0.4f, 0.4f);
			AddActionGripMoveDown(0.05f);
			AddActionGripRelease();
			StartSequence();
		}

		private float RoundToNearest(float value, float snap)
		{
			return Mathf.Round(value / snap) * snap;
		}

		#endregion

		#region private methods

		private Vector3 GetLocalPositionFromPolarCoordinates(float argAngles, float argRadius)
		{
			var tmpLocalPosition = (new Vector3(Mathf.Cos(argAngles * Mathf.Deg2Rad), _transformGripPivot.position[1], Mathf.Sin(argAngles * Mathf.Deg2Rad))) * argRadius;
			return tmpLocalPosition;
		}

		#endregion

		#region public methods

		public void AddActionPolarCoordinate(float argAngles, float argRadius)
		{
			_currentRegisteredSequences.Add(new GripAction(ActualAction.GriperGoIntoPosition, GetLocalPositionFromPolarCoordinates(argAngles, argRadius)));
		}

		public void AddActionPlaneCoordinate(float argPositionX, float argPositionZ)
		{
			float localYCoordinate = previousActionPosition[1];

			float snap = 0.05f;

			argPositionX = RoundToNearest(argPositionX, snap);
			localYCoordinate = RoundToNearest(localYCoordinate, snap);
			argPositionZ = RoundToNearest(argPositionZ, snap);

			_currentRegisteredSequences.Add(new GripAction(ActualAction.GriperGoIntoPosition, new Vector3(argPositionX, localYCoordinate, argPositionZ)));
		}

		public void AddActionGripMoveUp(float argPositionY)
		{
			var tmpPosition = previousActionPosition;

			float snap = 0.05f;

			tmpPosition.x = RoundToNearest(tmpPosition.x, snap);
			argPositionY = RoundToNearest(argPositionY, snap);
			tmpPosition.z = RoundToNearest(tmpPosition.z, snap);

			_currentRegisteredSequences.Add(new GripAction(ActualAction.GriperMoveUp, new Vector3(tmpPosition[0], argPositionY, tmpPosition[2])));
		}

		public void AddActionGripMoveDown(float argPositionY)
		{

			var tmpPosition = previousActionPosition;

			float snap = 0.05f;

			tmpPosition.x = RoundToNearest(tmpPosition.x, snap);
			argPositionY = RoundToNearest(argPositionY, snap);
			tmpPosition.z = RoundToNearest(tmpPosition.z, snap);

			_currentRegisteredSequences.Add(new GripAction(ActualAction.GriperMoveDown, new Vector3(tmpPosition[0], argPositionY, tmpPosition[2])));
		}

		public void AddActionGripCollect()
		{
			_currentRegisteredSequences.Add(new GripAction(ActualAction.GriperCollecting));
		}

		public void AddActionGripRelease()
		{
			_currentRegisteredSequences.Add(new GripAction(ActualAction.GriperReleasing));
		}

		public void StartSequence(GripAction[] argActions = null)
		{
			// Genero las acciones a partir del parametro o los datos de la instancia
			Queue<GripAction> qActions = argActions != null ? GetQueueFromArray(argActions) : GetQueueFromArray(_currentRegisteredSequences.ToArray());

			// Paso como argumento la secuencia a ejecutar
			StartCoroutine(CoExecuteSequence(qActions));
		}

		private Queue<GripAction> GetQueueFromArray(GripAction[] argActions)
		{
			// Genero la variable de salida
			var output = new Queue<GripAction>();

			// Recorro las acciones
			int count = argActions.Length;
			for (int i = 0; i < count; i++)
			{
				// Agrego la acción actual al Queue
				output.Enqueue(argActions[i]);
			}

			// Retorno la variable
			return output;
		}

		public void SetAsWaiting()
		{
			// Marco la acción actual como de espera
			actualAction = ActualAction.WaitingForNewAction;
		}

		#endregion

		#region Courutines

		public void StopSequence()
		{
			// Detengo la ejecución de la secuencia
			_executing = false;
		}

		public void ClearSequence()
		{
			// Remuevo todos los elementos registrados
			_currentRegisteredSequences.Clear();
		}

		/// <summary>
		/// Defino el valor de pausa para la secuencia; este valor no tiene sentido si la ejeución de la secuencia no se ha iniciado
		/// </summary>
		/// <param name="value">Valor booleano para la pausa</param>
		public void SetPause(bool value) => _pause = value;
		/// <summary>
		/// Invierto el valor de la pausa de la secuencia
		/// </summary>
		public void TogglePause() => _pause = !_pause;

		private IEnumerator CoExecuteSequence(Queue<GripAction> actions)
		{
			// Marco la secuencia como en ejecución
			_executing = true;

			// Reinicio el valor de pausa
			_pause = false;

			while (_executing)
			{

				// Si se ha pausado la ejecución
				yield return new WaitWhile(() => _pause);

				// Si esta esperando por una accion y la lista de secuencias no esta vacia
				if (actualAction == ActualAction.WaitingForNewAction && actions.Count > 0)
				{
					yield return new WaitForSeconds(1f);
					var tmpGripAction = actions.Dequeue();

					Debug.Log($"Grip Position: {GetConvertedLocalPosition(_transformGripPivot.position, _transformPositionMinConverter)} GO TO: {System.Enum.GetNames(typeof(ActualAction))[(int)tmpGripAction.action]}{tmpGripAction.position}");
					SetActualAction(tmpGripAction);
				}

				yield return null;
			}

			// Reinicio el valor de la pausa
			_pause = false;

			// Marco la secuencia como finalizada
			_executing = false;
		}

		public Vector3 GetConvertedLocalPosition(Vector3 argPosition, Transform argTransformer)
		{
			if (!argTransformer)
			{
				Debug.Log($"The transformer can't be null : {gameObject.name}");
				return Vector3.zero;
			}

			Vector3 output = argTransformer.InverseTransformPoint(argPosition);
			return output;
		}

		#endregion

		public class GripAction
		{
			public ActualAction action;
			public Vector3 position;

			public GripAction(ActualAction argAction, Vector3 argPosition)
			{
				action = argAction;
				position = argPosition;
			}

			public GripAction(ActualAction argAction)
			{
				action = argAction;
				position = Vector3.zero;
			}

			public static implicit operator GripAction(ActualAction action)
			{
				return new GripAction(action);
			}
		}

		public enum ActualAction
		{
			WaitingForNewAction,
			GriperMoveDown,
			GriperMoveUp,
			GriperGoIntoPosition,
			GriperCollecting,
			GriperReleasing
		}
	}
}