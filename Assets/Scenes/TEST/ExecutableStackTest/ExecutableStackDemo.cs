﻿using UnityEngine;
using Modular;
public class ExecutableStackDemo : MonoBehaviour
{
	[SerializeField] private Camera _camera;
	[SerializeField] private MeshRenderer _cube;

	private void Start()
	{
		
		SequenceStack stack = new SequenceStack(
			new IStackExecutableSequence[]{
				new BaseStackExecutableSequence(
					() => { }, // Proceso
					() => !IsOnLeft(_cube.transform.position) // Requerimiento
				),
				new SecondDelayExecutableSequence(
					() => {_cube.material.color = Color.magenta;}, // Proceso
					2f // Requerimiento
				),
				new BaseStackExecutableSequence(
					() => {_camera.fieldOfView = 150f; }, // Proceso
					() => true	// Requerimiento
				)
			}
		);

		StackSequenceProcessor.ExecuteStack(stack);
	}

	private bool IsOnLeft(Vector3 worldPosition)
	{
		return _camera.ScreenToViewportPoint(_camera.WorldToScreenPoint(worldPosition)).x < 0.5f;
	}
}