﻿using NSScriptableValues;
using UnityEngine;

namespace Modular.CurveProcessor
{
	public class ValueCurveProcessor : AbstractCurveProcessor, IValue<AnimationCurve>
	{

		[Header("Settings")]
		[SerializeField] private ValueAnimationCurve _curve;

		[Header("Debug")]
		[SerializeField] private float _curvePercent;

		public AnimationCurve Value
		{
			get => _curve.Value;
			set => _curve.Value = value;
		}

		[ContextMenu("Print Percent Curve")]
		private void PrintCurvePercent()
		{
			float value = GetCurveValue(_curvePercent);
			Debug.Log($"The value is -> {value}", this);
		}

		public float GetCurveValue(float weight)
		{
			var curveValue = _curve.Value.Evaluate(weight);
			return Mathf.LerpUnclamped(MinValue, MaxValue, curveValue);
		}
	}
}
