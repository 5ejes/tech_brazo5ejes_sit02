﻿using UnityEngine;

namespace Modular.Log
{

	public class InspectorDebugLog : MonoBehaviour
	{
		public void Log(string msg) => Debug.Log($"{msg} : {this}", this);
		public void LogError(string msg) => Debug.LogError($"{msg} : {this}", this);
		public void LogWarning(string msg) => Debug.LogWarning($"{msg} : {this}", this);

		// System Object
		public void Log(object value) => Debug.Log($"{value} : {this}", this);
		public void LogError(object value) => Debug.LogError($"{value} : {this}", this);
		public void LogWarning(object value) => Debug.LogWarning($"{value} : {this}", this);

		// Int
		public void Log(int value) => Debug.Log($"{value} : {this}", this);
		public void LogError(int value) => Debug.LogError($"{value} : {this}", this);
		public void LogWarning(int value) => Debug.LogWarning($"{value} : {this}", this);


	}
}
