﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MonoListener : MonoBehaviour
{
	[Header("Events")]
	[SerializeField] private UnityEvent _onAwake = new UnityEvent();
	[SerializeField] private UnityEvent _onStart = new UnityEvent();
	[SerializeField] private UnityEvent _onEnable = new UnityEvent();
	[SerializeField] private UnityEvent _onDisable = new UnityEvent();
	[SerializeField] private UnityEvent _onDestroy = new UnityEvent();

	private void Awake() => InvokeEvent(_onAwake);
	private void Start() => InvokeEvent(_onStart);
	private void OnEnable() => InvokeEvent(_onEnable);
	private void OnDisable() => InvokeEvent(_onDisable);
	private void OnDestroy() => InvokeEvent(_onDestroy);	


	private void InvokeEvent(UnityEvent unityEvent)
	{
		if (unityEvent != null) unityEvent.Invoke();
	}

}
