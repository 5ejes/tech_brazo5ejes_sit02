﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InspectorReferenceMethods : MonoBehaviour
{
	/// <summary>
	/// Activo o desactivo el objeto cada que se llama la acción
	/// </summary>
	/// <param name="target">El objeto al cual se quiere activar o desactivar</param>
	public void ToggleActivation(GameObject target)
	{
		// Invierto la activación del objeto
		target.SetActive(!target.activeSelf);
	}

	/// <summary>
	/// Activa o desactiva el Behaviour cada que llama la acción
	/// </summary>
	/// <param name="target">El Behaviour al cual se quiere activar o desactivar</param>
	public void ToggleComponentActivation(Behaviour target)
	{
		// Invierto la activación del behaviour
		target.enabled = !target.enabled;
	}

}
