﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menu : BaseSimulator
{
    public Image Fundido_;
    public Image Over_1;
    public Image Over_2;
    public Image Over_3;
    public Image Over_4;

    private float TimeOver;
    public bool resetPlayerPrefs;
    // Use this for initialization
    void Start()
    {
        resetPrefs(resetPlayerPrefs);
        Fundido_ = GameObject.Find("Fundido").GetComponent<Image>();
        Fundido_.CrossFadeAlpha(0, 0.5f, false);
        GameObject.Find("Fundido").SetActive(false);

        base.startSimulator ();

        Over_1 = GameObject.Find("over_sit1").GetComponent<Image>();
        Over_2 = GameObject.Find("over_sit2").GetComponent<Image>();
        Over_3 = GameObject.Find("over_sit3").GetComponent<Image>();
        Over_4 = GameObject.Find("over_sit4").GetComponent<Image>();

        Over_1.CrossFadeAlpha(0, 0.01f, false);
        Over_2.CrossFadeAlpha(0, 0.01f, false);
        Over_3.CrossFadeAlpha(0, 0.01f, false);
        Over_4.CrossFadeAlpha(0, 0.01f, false);

        TimeOver = 1.0f;
        GameObject infoBt = GameObject.Find("info_bt");
        Button btn1 = infoBt.GetComponent<Button>();
        btn1.onClick.AddListener(InfoAction);

        StartCoroutine(start_over_action_1());
    }

    IEnumerator start_over_action_1()
    {

        Over_1.CrossFadeAlpha(1, TimeOver, false);
        yield return new WaitForSeconds(TimeOver);
        Over_1.CrossFadeAlpha(0, TimeOver, false);
        yield return new WaitForSeconds(TimeOver);
        StartCoroutine(start_over_action_2());

    }

    IEnumerator start_over_action_2()
    {

        Over_2.CrossFadeAlpha(1, TimeOver, false);
        yield return new WaitForSeconds(TimeOver);
        Over_2.CrossFadeAlpha(0, TimeOver, false);
        yield return new WaitForSeconds(TimeOver);
        StartCoroutine(start_over_action_3());

    }

    IEnumerator start_over_action_3()
    {

        Over_3.CrossFadeAlpha(1, TimeOver, false);
        yield return new WaitForSeconds(TimeOver);
        Over_3.CrossFadeAlpha(0, TimeOver, false);
        yield return new WaitForSeconds(TimeOver);
        StartCoroutine(start_over_action_4());

    }


    IEnumerator start_over_action_4()
    {

        Over_4.CrossFadeAlpha(1, TimeOver, false);
        yield return new WaitForSeconds(TimeOver);
        Over_4.CrossFadeAlpha(0, TimeOver, false);
        yield return new WaitForSeconds(TimeOver);
        StartCoroutine(start_over_action_1());

    }

	public void resetPrefs(bool reset){

		if(reset){
			PlayerPrefs.DeleteAll();
		}
	}

    public void lanza_sit1()
    {
        SceneManager.LoadScene("situation_1");
    }

    public void lanza_sit2()
    {
        SceneManager.LoadScene("situation_2");
    }


    public void lanza_sit3()
    {
        SceneManager.LoadScene("situation_3");
    }


    public void lanza_sit4()
    {
        SceneManager.LoadScene("situation_4");
    }

    private void InfoAction()
    {
        _alert = Instantiate(_alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        _alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
        _alert.transform.localPosition = new Vector3(-700, 700, 0);
        _alert.GetComponent<Alert>().showAlert(3, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='instructions']").InnerText, "", Manager.Instance.globalInfo.SelectSingleNode("/data/instructions").InnerText);
    }

}
