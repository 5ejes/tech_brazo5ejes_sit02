﻿using NSInterfazControlPLC;
using UnityEngine;

namespace NSBrazo5Ejes
{
	public class ArmGrafcetWriter : MonoBehaviour
	{
		[Header("References")]
		[SerializeField] public grafcetOutputInspector _grafcetOutput;

		public void WritePositionX(float x)
		{
			WritePositionX(Mathf.RoundToInt(x));
		}
		public void WritePositionY(float y)
		{
			WritePositionY(Mathf.RoundToInt(y));
		}
		public void WritePositionZ(float z)
		{
			WritePositionZ(Mathf.RoundToInt(z));
		}

		public void WritePositionX(int x)
		{
			if (!IsGrafcetDefined()) return;

			_grafcetOutput.GrafcetOutput.PositionX = (uint)x;
		}


		public void WritePositionY(int y)
		{
			if (!IsGrafcetDefined()) return;
			
			_grafcetOutput.GrafcetOutput.PositionY = (uint)y;
		}

		public void WritePositionZ(int z)
		{
			if (!IsGrafcetDefined()) return;

			_grafcetOutput.GrafcetOutput.PositionZ = (uint)z;
		}
		private bool IsGrafcetDefined()
		{
			if (_grafcetOutput == null || _grafcetOutput.GrafcetOutput == null)
				return false;
			return true;
		}
	}
}
