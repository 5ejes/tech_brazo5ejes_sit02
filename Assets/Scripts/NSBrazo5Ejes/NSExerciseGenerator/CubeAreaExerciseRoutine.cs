﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
  public class CubeAreaExerciseRoutine : AreaExerciseRoutine
  {
    [SerializeField] private List<AreaEvaluationToken> _evaluators;
    [SerializeField] private List<Vector3> _positions;

    public CubeAreaExerciseRoutine(params AreaEvaluationToken[] tokens)
    {

      // Creo la lista que contiene los evaluadores y las posiciones de los cubos
      Evaluators = new List<AreaEvaluationToken>();
      _positions = new List<Vector3>();

      // Anexo todos los evaluadores
      Evaluators.AddRange(tokens);
    }

    private System.Action _onEvaluationCompleted;

    /// <summary>
    /// Lista de las posiciones usadas en la rutina
    /// </summary>
    /// <value></value>
    public override List<Vector3> Positions { get => _positions; set => _positions = value; }

    /// <summary>
    /// Lista de los evaluadores a usar para generar la calificación
    /// </summary>
    public new List<AreaEvaluationToken> Evaluators { get => _evaluators; set => _evaluators = value; }
    public override Action OnEvaluationCompleted { get => _onEvaluationCompleted; set => _onEvaluationCompleted = value; }

    public override bool EvaluateRoutine()
    {

      // Reinicio la calificación de la rutina
      CurrentQualification = 0f;

      // Si la lista no es nula
      if (_evaluators != null)
      {
        // Obtengo la cuenta de elementos en la lista
        int count = _evaluators.Count;

        // Si la lista tiene al menos un elemento
        if (count > 0)
        {
          // Genero un arreglo con los resultados
          bool[] results = new bool[count];

          // Genero un arreglo con las calificaciones
          float[] qualifications = new float[count];

          // Fracción de calificación
          float qualificationFraction = 1 / (float)count;

          // // Creo arreglos para guardar las acciones originales
          // var successActions = new System.Action[count];
          // var failedActions = new System.Action[count];

          // Defino las acciones que me escribirian los resultados
          System.Action onEvaluationCheck = null;

          // Por cada evaluador
          for (int i = 0; i < count; i++)
          {

            // Obtengo el evaluador actual
            var currentEvaluator = _evaluators[i];

            // // Guardo los successActions
            // successActions[i] = currentEvaluator.OnSuccess;
            // failedActions[i] = currentEvaluator.OnFailure;

            // Creo un ìndice de evaluador para los datos
            var evaluatorDataIndex = i;

            onEvaluationCheck = () =>
            {

              // Obtengo la calificación actual basada en los elementos evaluados
              var currentQualification = Mathf.Clamp01(currentEvaluator.CheckedElements / (float)currentEvaluator.CheckedElementGoal);

              // Defino el valor total de la calificación
              float totalQualification = qualificationFraction * currentQualification;
              qualifications[evaluatorDataIndex] = totalQualification;

              // Modifico los resultados en la posición del arreglo definida
              results[evaluatorDataIndex] = currentQualification > QualificationThreshold;

              // Defino el texto de evaluación con respecto a si ha pasado la prueba
              var resultText = (results[evaluatorDataIndex]) ? "Succeded" : "Failed";

              Debug.Log($"Evaluation {evaluatorDataIndex}: {resultText}");
            };

            // Genero los eventos
            currentEvaluator.OnSuccess += onEvaluationCheck;
            currentEvaluator.OnFailure += onEvaluationCheck;

						// Defino el evento de finalizar la evaluación
						currentEvaluator.OnEvaluationEnd += () =>
            {
              // Substraigo las rutinas de evaluación
              currentEvaluator.OnSuccess -= onEvaluationCheck;
              currentEvaluator.OnFailure -= onEvaluationCheck;

              // Substraigo esta rutina
              currentEvaluator.OnEvaluationEnd = null;
            };

            // Ejecuto el evaluador
            currentEvaluator.Evaluate();
          }

          // Evaluo los resultados
          // Por cada resultado
          for (int i = 0; i < count; i++)
          {
            // Obtengo los resultados
            var result = results[i];
            var currentQualification = qualifications[i];

            // Incremento la calificación actual
            CurrentQualification += currentQualification;

          }

          // Ejecuto la acción, si está definida
          if (_onEvaluationCompleted != null) _onEvaluationCompleted.Invoke();

          // Evaluo si el resultado es mayor al threshold de calificación
          return CurrentQualification > QualificationThreshold;

        }
        else
        {
          // No hay elementos en la lista

          // Ejecuto la acción, si está definida
          if (_onEvaluationCompleted != null) _onEvaluationCompleted.Invoke();

          // Retorno falso
          return false;
        }
      }
      else
      {
        // La referencia a la lista es nula

        // Ejecuto la acción, si está definida
        if (_onEvaluationCompleted != null) _onEvaluationCompleted.Invoke();

        // Retorno falso
        return false;
      }
    }
  }
}
