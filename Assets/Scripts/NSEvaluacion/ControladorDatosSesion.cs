﻿#pragma warning disable 0649
using System;
using NSScriptableEvent;
using NSScriptableValues;
using UnityEngine;

namespace NSCalificacionSituacion
{
    public class ControladorDatosSesion : MonoBehaviour
    {
        #region members

        [SerializeField] private SOSessionData soSessionData;

        private bool contarTiempoSituacion;
        
        [SerializeField] private ScriptableEventInt seAttemps;

        [SerializeField] private ScriptableEventString seTime;
        #endregion
        
        #region monoBehaviour

        private void OnEnable()
        {
            BeginNewSession();
        }

        // Update is called once per frame
        private void Update()
        {
            ContarTiempo();
        }

        #endregion

        #region private methods

        private void ContarTiempo()
        {
            if (contarTiempoSituacion)
            {
                soSessionData.timeSituationFloat += Time.deltaTime;
                seTime.ExecuteEvent(soSessionData.TimeSituationString);
            }
        }

        #endregion

        #region public methods

        public void SetContarTiempo(bool argContarTiempo)
        {
            contarTiempoSituacion = argContarTiempo;
        }

        public void AddIntentos()
        {
            soSessionData.quantityAttempts++;
            seAttemps.ExecuteEvent(soSessionData.quantityAttempts);
        }

        public void BeginNewSession()
        {
            SetContarTiempo(true);
            soSessionData.quantityAttempts = 0;
            soSessionData.timeSituationFloat = 0f;
            seAttemps.ExecuteEvent(soSessionData.quantityAttempts);
            seTime.ExecuteEvent(soSessionData.TimeSituationString);
        }

        #endregion
    }
}