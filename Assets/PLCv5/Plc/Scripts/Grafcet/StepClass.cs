﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class StepClass : MonoBehaviour {

	public GameObject _situation;

	public bool _active = false;
	public int _id = 0;
	public bool _onSim = false;

	public Text _textElement;
	public Image _topConnector;
	public Image _rightConnector;
	public Image _bottomConnector;
	public Button _deleteBt;
	public Button _deleteRightBt;
	public Button _deleteBottomBt;
	public Button _deleteTopBt;
	public Image _bgElement;

	public float _totalWidth = 0;

	private float x_offset = 0;
	private float y_offset = 0;

	public bool dragEnabled = true;
	private bool objectDragged;
	private Vector3 initialPosition;
	private Vector3 startPosition;
	private int initialIndex;
	private int startIndex;

	private EventTrigger.Entry entry_0;
	private EventTrigger.Entry entry_1;
	private EventTrigger.Entry entry_2;
	private EventTrigger.Entry entry_3;

	public bool _menuActive = false;

	private bool _mouseOver = false;

	// Use this for initialization
	void Start () {

		_situation = GameObject.Find ("Canvas/Grafcet");

		initialPosition = gameObject.transform.position;

		_textElement.text = "";
		_topConnector.enabled = false;
		_rightConnector.enabled = false;
		_bottomConnector.enabled = false;

		_deleteBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);
		_deleteRightBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);
		_deleteBottomBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);
		_deleteTopBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);

		_deleteBt.onClick.AddListener (deleteAction);
		_deleteRightBt.onClick.AddListener (deleteRightAction);
		_deleteBottomBt.onClick.AddListener (deleteBottomAction);
		_deleteTopBt.onClick.AddListener (deleteTopAction);

		gameObject.AddComponent<EventTrigger> ();

		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();

		entry_0 = new EventTrigger.Entry();
		entry_0.eventID = EventTriggerType.BeginDrag;
		entry_0.callback.AddListener((data) => { BeginDrag_((PointerEventData)data); });

		entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.Drag;
		entry_1.callback.AddListener((data) => { ObjectDrag_((PointerEventData)data); });

		entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.EndDrag;
		entry_2.callback.AddListener((data) => { ObjectDrop_((PointerEventData)data); });

		EventTrigger.Entry entry_4 = new EventTrigger.Entry();
		entry_4.eventID = EventTriggerType.PointerEnter;
		entry_4.callback.AddListener((data) => { ObjectMouseOver_((PointerEventData)data); });

		EventTrigger.Entry entry_5 = new EventTrigger.Entry();
		entry_5.eventID = EventTriggerType.PointerExit;
		entry_5.callback.AddListener((data) => { ObjectMouseOut_((PointerEventData)data); });

		trigger_Object.triggers.Add(entry_0);
		trigger_Object.triggers.Add(entry_1);
		trigger_Object.triggers.Add(entry_2);
		trigger_Object.triggers.Add(entry_4);
		trigger_Object.triggers.Add(entry_5);

		gameObject.name = "step_base";
	}

	private void BeginDrag_(PointerEventData data)
	{
		if (dragEnabled == true) {
			objectDragged = true;

			if (_active == false) {

				gameObject.transform.SetParent (GameObject.Find ("Canvas/Grafcet").transform, true);

				startPosition = gameObject.transform.position;
				startIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

				gameObject.GetComponent<RectTransform> ().SetAsLastSibling ();

				_situation.GetComponent<GrafcetMainClass> ().activateWorkbenchCollider ();

				x_offset = 0;
				y_offset = 0;
			} else {
				Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				x_offset = gameObject.GetComponent<RectTransform> ().position.x - mousePos.x;
				y_offset = gameObject.GetComponent<RectTransform> ().position.y - mousePos.y;
			}
		}
	}

	private void ObjectDrag_(PointerEventData data)
	{
		if (objectDragged == true) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			gameObject.GetComponent<RectTransform> ().position = new Vector3 (mousePos.x + x_offset, mousePos.y + y_offset, 0);
        }
	}

	private void ObjectDrop_(PointerEventData data)
	{
		if (objectDragged == true) {
			objectDragged = false;

			_situation.GetComponent<GrafcetMainClass> ().deactivateWorkbenchCollider ();

			if (_active == false) {
				if (data != null && data.pointerEnter != null && data.pointerEnter.name == "workbenchCollider") {

					if (_situation.GetComponent<GrafcetMainClass> ().stepsModules == 0) {

						_situation.GetComponent<GrafcetMainClass> ().showInfoAlert (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='step_no_insert']").InnerText);
					
						iTween.MoveTo(
							gameObject,
							iTween.Hash(
								"position", startPosition,
								"looktarget", Camera.main,
								"easeType", iTween.EaseType.easeOutExpo,
								"time", 0.2f,
								"islocal",false
							)
						);

						Invoke ("goToInitialState", 0.2f);

					} else {
						gameObject.transform.SetParent ( _situation.GetComponent<GrafcetMainClass>()._workbenchArea.transform , true);

						cloneElement ();

						initializeElement ();
					}

				} else {
					iTween.MoveTo(
						gameObject,
						iTween.Hash(
							"position", startPosition,
							"looktarget", Camera.main,
							"easeType", iTween.EaseType.easeOutExpo,
							"time", 0.2f,
							"islocal",false
						)
					);

					Invoke ("goToInitialState", 0.2f);
				}
			} else {

			}
		}
	}

	private void ObjectMouseOver_(PointerEventData data){
		if(_active == false){
			_mouseOver = true;
			_situation.GetComponent<GrafcetMainClass> ().showInfoText (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='step']").InnerText);
		}
	}

	private void ObjectMouseOut_(PointerEventData data){
		if(_mouseOver == true){
			_mouseOver = false;
			_situation.GetComponent<GrafcetMainClass> ().hideInfoText ();
		}
	}

	private void goToInitialState(){
		//inScenario = false;
		gameObject.GetComponent<RectTransform> ().SetSiblingIndex (initialIndex);
		gameObject.transform.SetParent(GameObject.Find("space2").transform, true);
	}

	public void cloneElement(){
		GameObject _clone = Instantiate(_situation.GetComponent<GrafcetMainClass>().stepPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_clone.transform.SetParent (GameObject.Find("Canvas/Grafcet/leftMenu/containerElements/Viewport/Content/space2").transform, false);
		_clone.transform.localPosition = new Vector3 (0,0,0);
	}

	public void initializeElement(){
		this._active = true;
		this._id = _situation.GetComponent<GrafcetMainClass> ().stepsModules;
		_situation.GetComponent<GrafcetMainClass> ().stepsModules++;
		this.name = "step_" + this._id.ToString ();

		_textElement.text = this._id.ToString ();

		entry_3 = new EventTrigger.Entry();
		entry_3.eventID = EventTriggerType.PointerClick;
		entry_3.callback.AddListener((data) => { ObjectClicked_((PointerEventData)data); });
		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();
		trigger_Object.triggers.Add(entry_3);

		initialAction();
	}

	public void ObjectClicked_(PointerEventData data){
		if (objectDragged == false && _active == true) {
			if (_menuActive == false) {
				_situation.GetComponent<GrafcetMainClass> ().deactivateEditWithoutElement (gameObject.name);
				_menuActive = true;
				dragEnabled = false;
				iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0.6f, 0.6f, 0.6f), 0.3f);
				if(_rightConnector.GetComponent<connModuleClass>().lineIsBusy()){
					iTween.ScaleTo (_deleteRightBt.gameObject, new Vector3 (0.4f, 0.4f, 0.4f), 0.3f);
				}
				if(_bottomConnector.GetComponent<connModuleClass>().lineIsBusy()){
					iTween.ScaleTo (_deleteBottomBt.gameObject, new Vector3 (0.4f, 0.4f, 0.4f), 0.3f);
				}
				if(_topConnector.GetComponent<connModuleClass>().lineIsBusy()){
					iTween.ScaleTo (_deleteTopBt.gameObject, new Vector3 (0.4f, 0.4f, 0.4f), 0.3f);
				}
			} else {
				iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
				iTween.ScaleTo (_deleteRightBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
				iTween.ScaleTo (_deleteBottomBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
				iTween.ScaleTo (_deleteTopBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
				_menuActive = false;
				dragEnabled = true;
			}
		}

	}

	public void updateNameAndId(int _id){
		this._id = _id;
		this.name = "step_" + this._id.ToString ();
		_textElement.text = this._id.ToString ();
	}

	public void deleteAction(){
		for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().jumpModules; i++) {
			GameObject _elementJump = GameObject.Find ("jump_" + i.ToString ());
			if (_elementJump.GetComponent<JumpClass> ().endObject.GetInstanceID() == gameObject.GetInstanceID()) {
				_elementJump.GetComponent<JumpClass> ().deleteAction ();
				break;
			}
		}

		for(int i = this._id + 1; i < _situation.GetComponent<GrafcetMainClass> ().stepsModules; i++){
			GameObject _element = GameObject.Find ("step_" + i.ToString());
			_element.GetComponent<StepClass> ().updateNameAndId (i - 1);
		}

		_situation.GetComponent<GrafcetMainClass> ().stepsModules--;

		deleteBottomAction ();
		deleteRightAction ();
		Destroy (gameObject);
	}

	private void deleteRightAction(){
		if(_rightConnector.GetComponent<connModuleClass>()._line){
			GameObject _remoteStartElement = _rightConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().startConnector;
			GameObject _remoteEndElement = _rightConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector;

			if (_rightConnector.name == _remoteStartElement.name) {
				_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ActionClass> ().dragEnabled = true;
				Destroy (_remoteEndElement.GetComponent<connModuleClass> ()._line);
				_remoteEndElement.GetComponent<connModuleClass> ()._line = null;

			} else {
				_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ActionClass> ().dragEnabled = true;
				Destroy (_remoteStartElement.GetComponent<connModuleClass> ()._line);
				_remoteStartElement.GetComponent<connModuleClass> ()._line = null;
			}

			Destroy (_rightConnector.GetComponent<connModuleClass>()._line);
			_rightConnector.GetComponent<connModuleClass> ()._line = null;
			iTween.ScaleTo (_deleteRightBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		}
	}

	private void deleteBottomAction(){
		if(_bottomConnector.GetComponent<connModuleClass>()._line){
			GameObject _remoteStartElement = _bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().startConnector;
			GameObject _remoteEndElement = _bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector;

			if(_bottomConnector.name == _remoteStartElement.name){
				if(_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("transition") == 0){
					_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<TransitionClass> ().dragEnabled = true;
				}
				else if(_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("divergence") == 0){
					_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<DivergenceClass> ().dragEnabled = true;
				} else if(_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("convergence") == 0){
					if(_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ConvergenceClass> ().countConnected == 1){
						_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ConvergenceClass> ().dragEnabled = true;
					}
				}
				Destroy (_remoteEndElement.GetComponent<connModuleClass>()._line);
				_remoteEndElement.GetComponent<connModuleClass> ()._line = null;
			} else {
				if(_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("transition") == 0){
					_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<TransitionClass> ().dragEnabled = true;
				} else if(_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("divergence") == 0){
					_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<DivergenceClass> ().dragEnabled = true;
				} else if(_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("convergence") == 0){
					if(_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ConvergenceClass> ().countConnected == 1){
						_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ConvergenceClass> ().dragEnabled = true;
					}
				}
				Destroy (_remoteStartElement.GetComponent<connModuleClass> ()._line);
				_remoteStartElement.GetComponent<connModuleClass> ()._line = null;
			}

			Destroy (_bottomConnector.GetComponent<connModuleClass>()._line);
			_bottomConnector.GetComponent<connModuleClass> ()._line = null;
			iTween.ScaleTo (_deleteBottomBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		}
	}

	private void deleteTopAction(){
		if(_topConnector.GetComponent<connModuleClass>()._line){
			GameObject _remoteStartElement = _topConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().startConnector;
			GameObject _remoteEndElement = _topConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector;

			if (_topConnector.name == _remoteStartElement.name) {
				Destroy (_remoteEndElement.GetComponent<connModuleClass> ()._line);
				_remoteEndElement.GetComponent<connModuleClass> ()._line = null;

			} else {
				Destroy (_remoteStartElement.GetComponent<connModuleClass> ()._line);
				_remoteStartElement.GetComponent<connModuleClass> ()._line = null;
			}

			Destroy (_topConnector.GetComponent<connModuleClass>()._line);
			_topConnector.GetComponent<connModuleClass> ()._line = null;
			iTween.ScaleTo (_deleteTopBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		}
	}

	public void initialAction(){
		//TODO: check if element is used;
		_rightConnector.enabled = true;
		//TODO: check if element is used;
		_topConnector.enabled = true;
		//TODO: check if element is used;
		_bottomConnector.enabled = true;

	}

	public stepStructure getJSONData(){
		stepStructure _result = new stepStructure ();
		_result.x = gameObject.transform.localPosition.x;
		_result.y = gameObject.transform.localPosition.y;
		_result._id = this._id;
		_result.name = this.name;
		_result._title = this._textElement.text;
		_result._onSim = this._onSim;
		_result._r_conn = _rightConnector.GetComponent<connModuleClass> ().getJSONData ();
		_result._u_conn = _topConnector.GetComponent<connModuleClass> ().getJSONData ();
		_result._d_conn = _bottomConnector.GetComponent<connModuleClass> ().getJSONData ();
		return _result;
	}
	
	// Update is called once per frame
	void Update () {
		if(this._active == true){
			if(_topConnector.GetComponent<connModuleClass>()._line){
				if(_topConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().startConnector && _topConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().endConnector){
					dragEnabled = false;

					Vector3 _newPosition = new Vector3 (0,0,0);
					GameObject _parentElement = _topConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement;

					Vector2 _offsetVector = new Vector2 (0,0);

					if (_parentElement.name.IndexOf ("divergence") == 0) {
						Vector3 _positionConn = _topConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector.transform.parent.localPosition;
						_offsetVector = new Vector2 (_positionConn.x, -115f);
					} else if(_parentElement.name.IndexOf ("convergence") == 0){
						_offsetVector = new Vector2 (0, -115f);
					} else {
						_offsetVector = new Vector2 (0, -110f);
					}

					_newPosition.x = _parentElement.transform.localPosition.x + _offsetVector.x;
					_newPosition.y = _parentElement.transform.localPosition.y + _offsetVector.y;

					gameObject.transform.localPosition = _newPosition;


				}
			}

			bool _outputActive = false;
			if(_situation.GetComponent<GrafcetMainClass>().grafcetEngine != null && _situation.GetComponent<GrafcetMainClass>().simulationRunning == true){
				SimpleJSON.JSONNode _jsonData = _situation.GetComponent<GrafcetMainClass> ().grafcetEngine.getStepObjectPerName (this.name);
				if (_jsonData != null && _jsonData ["_onSim"].AsBool == true) {
					_bgElement.color = new Color (0, 113f / 255f, 183f / 255f, 90f / 255f);
					_outputActive = true;
				} else {
					_bgElement.color = new Color (1, 1, 1, 1);
					_outputActive = false;
				}
			}

			float lineWidth = 40f;
			_totalWidth = 52f;

			connModuleClass _rightSequence = _rightConnector.GetComponent<connModuleClass> ();
			while(_rightSequence.lineIsBusy()){
				_totalWidth = _totalWidth + lineWidth + 208f;
				_rightSequence = _rightSequence._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement.GetComponent<ActionClass> ()._rightConnector.GetComponent<connModuleClass> ();
				if(_rightSequence._parentElement.GetComponent<ActionClass>()){
					if (_outputActive == true) {
						_rightSequence._parentElement.GetComponent<ActionClass> ()._bgElement.color = new Color (0, 113f / 255f, 183f / 255f, 90f / 255f);
					} else {
						_rightSequence._parentElement.GetComponent<ActionClass> ()._bgElement.color = new Color (1, 1, 1, 1);
					}
				}
			}
		}
	}
}
