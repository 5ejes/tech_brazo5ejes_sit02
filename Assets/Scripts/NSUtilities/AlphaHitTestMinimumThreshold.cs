﻿#pragma warning disable 0649
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NSUtilities
{
    public class AlphaHitTestMinimumThreshold : MonoBehaviour
    {
        [SerializeField] private float threshold = 0.5f;

        private void Awake()
        {
            GetComponent<Image>().alphaHitTestMinimumThreshold = threshold;
        }
    }
}