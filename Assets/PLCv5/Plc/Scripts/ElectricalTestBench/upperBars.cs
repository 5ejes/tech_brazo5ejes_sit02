﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class upperBars : MonoBehaviour {

	public GameObject _situation;

	public Text availableInputsLabel;
	public Text availableInputsText;
	public Text availableOutputsLabel;
	public Text availableOutputsText;
	public Text inputBarLabel;
	public Text outputBarLabel;

	public Dropdown ddlInputs;
	public Dropdown ddlOutputs;

	public Image input00On;
	public Image input01On;
	public Image input02On;
	public Image input03On;
	public Image input04On;
	public Image input05On;
	public Image input06On;
	public Image input07On;
	public Image input08On;
	public Image input09On;
	public Image input10On;
	public Image input11On;
	public Image input12On;
	public Image input13On;
	public Image input14On;
	public Image input15On;

	public Image output00On;
	public Image output01On;
	public Image output02On;
	public Image output03On;
	public Image output04On;
	public Image output05On;
	public Image output06On;
	public Image output07On;
	public Image output08On;
	public Image output09On;
	public Image output10On;
	public Image output11On;
	public Image output12On;
	public Image output13On;
	public Image output14On;
	public Image output15On;

	// Use this for initialization
	void Start () {
		input00On.enabled = false;
		input01On.enabled = false;
		input02On.enabled = false;
		input03On.enabled = false;
		input04On.enabled = false;
		input05On.enabled = false;
		input06On.enabled = false;
		input07On.enabled = false;
		input08On.enabled = false;
		input09On.enabled = false;
		input10On.enabled = false;
		input11On.enabled = false;
		input12On.enabled = false;
		input13On.enabled = false;
		input14On.enabled = false;
		input15On.enabled = false;

		output00On.enabled = false;
		output01On.enabled = false;
		output02On.enabled = false;
		output03On.enabled = false;
		output04On.enabled = false;
		output05On.enabled = false;
		output06On.enabled = false;
		output07On.enabled = false;
		output08On.enabled = false;
		output09On.enabled = false;
		output10On.enabled = false;
		output11On.enabled = false;
		output12On.enabled = false;
		output13On.enabled = false;
		output14On.enabled = false;
		output15On.enabled = false;

		ddlInputs.options = new List<Dropdown.OptionData> ();
		ddlOutputs.options = new List<Dropdown.OptionData> ();
	}
	
	// Update is called once per frame
	void Update () {
		switch(Manager.Instance.globalGraphicInterface){
		case baseSystem.graphicInterfaces.electrical_bench:
			availableInputsText.text = (_situation.GetComponent<ElectricalTestBenchMainClass> ().simulatorInputs - (_situation.GetComponent<ElectricalTestBenchMainClass> ().usedInputs)).ToString ();
			availableOutputsText.text = (_situation.GetComponent<ElectricalTestBenchMainClass> ().simulatorOutputs - (_situation.GetComponent<ElectricalTestBenchMainClass> ().usedOutputs)).ToString ();
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			availableInputsText.text = (_situation.GetComponent<PnematicTestBenchClass> ().simulatorInputs - (_situation.GetComponent<PnematicTestBenchClass> ().usedInputs)).ToString ();
			availableOutputsText.text = (_situation.GetComponent<PnematicTestBenchClass> ().simulatorOutputs - (_situation.GetComponent<PnematicTestBenchClass> ().usedOutputs)).ToString ();
			break;
		}

		setInputValueIndicators ();
		setOutputValueIndicators ();
	}

	public void setTexts(){
		availableInputsLabel.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='electric_bench_available_inputs']").InnerText;
		availableOutputsLabel.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='electric_bench_available_outputs']").InnerText;
		inputBarLabel.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='electric_bench_inputs']").InnerText;
		outputBarLabel.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='electric_bench_outputs']").InnerText;
	}

	public void updateInputsDropdown(){

		ddlInputs.onValueChanged.RemoveAllListeners ();

		List<Dropdown.OptionData> _options = new List<Dropdown.OptionData> ();

		switch(Manager.Instance.globalGraphicInterface){
		case baseSystem.graphicInterfaces.electrical_bench:
			for(int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass>().inputElements.Length; i++){
				if( _situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i] ){
					Dropdown.OptionData _dataList = new Dropdown.OptionData ();
					_dataList.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='input']").InnerText + " " + (i + 1).ToString () + ": " + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='" + _situation.GetComponent<ElectricalTestBenchMainClass> ().inputElements [i].GetComponent<dragElement> ().textTag + "']").InnerText;
					_options.Add (_dataList);
				}
			}
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			for(int i = 0; i < _situation.GetComponent<PnematicTestBenchClass>().inputElements.Length; i++){
				if( _situation.GetComponent<PnematicTestBenchClass>().inputElements[i] ){
					Dropdown.OptionData _dataList = new Dropdown.OptionData ();
					_dataList.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='input']").InnerText + " " + (i + 1).ToString () + ": " + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='" + _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().textTag + "']").InnerText;
					_options.Add (_dataList);
				}
			}
			break;
		}
			
		ddlInputs.options = _options;
		ddlInputs.value = ddlInputs.options.Count - 1;

		ddlInputs.onValueChanged.AddListener (changeInputDdlAction);
	}

	public void updateOutputsDropdown(){

		ddlOutputs.onValueChanged.RemoveAllListeners ();

		List<Dropdown.OptionData> _options = new List<Dropdown.OptionData> ();

		switch(Manager.Instance.globalGraphicInterface){
		case baseSystem.graphicInterfaces.electrical_bench:
			for(int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements.Length; i++){
				if( _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i] ){
					Dropdown.OptionData _dataList = new Dropdown.OptionData ();
					_dataList.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='output']").InnerText + " " + (i + 1).ToString () + ": " + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='" + _situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements [i].GetComponent<dragElement> ().textTag + "']").InnerText;
					_options.Add (_dataList);
				}
			}
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			for(int i = 0; i < _situation.GetComponent<PnematicTestBenchClass>().outputsElements.Length; i++){
				if( _situation.GetComponent<PnematicTestBenchClass>().outputsElements[i] ){
					Dropdown.OptionData _dataList = new Dropdown.OptionData ();
					if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> () != null) {
						_dataList.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='output']").InnerText + " " + (i + 1).ToString () + ": " + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='" + _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ().textTag + "']").InnerText;
					} else if(_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> () != null){
						_dataList.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='output']").InnerText + " " + (i + 1).ToString () + ": " + _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ().getText ();
					}
					_options.Add (_dataList);
				}
			}
			break;
		}
			
		ddlOutputs.options = _options;
		ddlOutputs.value = ddlOutputs.options.Count - 1;

		ddlOutputs.onValueChanged.AddListener (changeOutputDdlAction);
	}

	public void setInputValueIndicators(){

		input00On.enabled = false;
		input01On.enabled = false;
		input02On.enabled = false;
		input03On.enabled = false;
		input04On.enabled = false;
		input05On.enabled = false;
		input06On.enabled = false;
		input07On.enabled = false;
		input08On.enabled = false;
		input09On.enabled = false;
		input10On.enabled = false;
		input11On.enabled = false;
		input12On.enabled = false;
		input13On.enabled = false;
		input14On.enabled = false;
		input15On.enabled = false;

		switch(Manager.Instance.globalGraphicInterface){
		case baseSystem.graphicInterfaces.electrical_bench:
			for(int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass>().inputElements.Length; i++){
				if( _situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i] ){

					bool indicatorOn = false;
					if (_situation.GetComponent<ElectricalTestBenchMainClass> ().inputElements [i].GetComponent<dragElement> ().actualValue == 1) {
						indicatorOn = true;
					} else {
						indicatorOn = false;
					}

					switch(i){
					case 0:
						input00On.enabled = indicatorOn;
						break;
					case 1:
						input01On.enabled = indicatorOn;
						break;
					case 2:
						input02On.enabled = indicatorOn;
						break;
					case 3:
						input03On.enabled = indicatorOn;
						break;
					case 4:
						input04On.enabled = indicatorOn;
						break;
					case 5:
						input05On.enabled = indicatorOn;
						break;
					case 6:
						input06On.enabled = indicatorOn;
						break;
					case 7:
						input07On.enabled = indicatorOn;
						break;
					case 8:
						input08On.enabled = indicatorOn;
						break;
					case 9:
						input09On.enabled = indicatorOn;
						break;
					case 10:
						input10On.enabled = indicatorOn;
						break;
					case 11:
						input11On.enabled = indicatorOn;
						break;
					case 12:
						input12On.enabled = indicatorOn;
						break;
					case 13:
						input13On.enabled = indicatorOn;
						break;
					case 14:
						input14On.enabled = indicatorOn;
						break;
					case 15:
						input15On.enabled = indicatorOn;
						break;
					}
				}
			}
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			for(int i = 0; i < _situation.GetComponent<PnematicTestBenchClass>().inputElements.Length; i++){
				if( _situation.GetComponent<PnematicTestBenchClass>().inputElements[i] ){

					bool indicatorOn = false;
					if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().actualValue == 1) {
						indicatorOn = true;
					} else {
						indicatorOn = false;
					}

					switch(i){
					case 0:
						input00On.enabled = indicatorOn;
						break;
					case 1:
						input01On.enabled = indicatorOn;
						break;
					case 2:
						input02On.enabled = indicatorOn;
						break;
					case 3:
						input03On.enabled = indicatorOn;
						break;
					case 4:
						input04On.enabled = indicatorOn;
						break;
					case 5:
						input05On.enabled = indicatorOn;
						break;
					case 6:
						input06On.enabled = indicatorOn;
						break;
					case 7:
						input07On.enabled = indicatorOn;
						break;
					case 8:
						input08On.enabled = indicatorOn;
						break;
					case 9:
						input09On.enabled = indicatorOn;
						break;
					case 10:
						input10On.enabled = indicatorOn;
						break;
					case 11:
						input11On.enabled = indicatorOn;
						break;
					case 12:
						input12On.enabled = indicatorOn;
						break;
					case 13:
						input13On.enabled = indicatorOn;
						break;
					case 14:
						input14On.enabled = indicatorOn;
						break;
					case 15:
						input15On.enabled = indicatorOn;
						break;
					}
				}
			}
			break;
		}
	}

	public void setOutputValueIndicators(){
		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			for (int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass> ().outputsValues.Length; i++) {
				bool indicatorOn = false;
				if (_situation.GetComponent<ElectricalTestBenchMainClass> ().outputsValues [i] == true) {
					indicatorOn = true;
				} else {
					indicatorOn = false;
				}

				switch(i){
				case 0:
					output00On.enabled = indicatorOn;
					break;
				case 1:
					output01On.enabled = indicatorOn;
					break;
				case 2:
					output02On.enabled = indicatorOn;
					break;
				case 3:
					output03On.enabled = indicatorOn;
					break;
				case 4:
					output04On.enabled = indicatorOn;
					break;
				case 5:
					output05On.enabled = indicatorOn;
					break;
				case 6:
					output06On.enabled = indicatorOn;
					break;
				case 7:
					output07On.enabled = indicatorOn;
					break;
				case 8:
					output08On.enabled = indicatorOn;
					break;
				case 9:
					output09On.enabled = indicatorOn;
					break;
				case 10:
					output10On.enabled = indicatorOn;
					break;
				case 11:
					output11On.enabled = indicatorOn;
					break;
				case 12:
					output12On.enabled = indicatorOn;
					break;
				case 13:
					output13On.enabled = indicatorOn;
					break;
				case 14:
					output14On.enabled = indicatorOn;
					break;
				case 15:
					output15On.enabled = indicatorOn;
					break;
				}
			}
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			for (int i = 0; i < _situation.GetComponent<PnematicTestBenchClass> ().outputsValues.Length; i++) {
				bool indicatorOn = false;
				if (_situation.GetComponent<PnematicTestBenchClass> ().outputsValues [i] == true) {
					indicatorOn = true;
				} else {
					indicatorOn = false;
				}

				switch(i){
				case 0:
					output00On.enabled = indicatorOn;
					break;
				case 1:
					output01On.enabled = indicatorOn;
					break;
				case 2:
					output02On.enabled = indicatorOn;
					break;
				case 3:
					output03On.enabled = indicatorOn;
					break;
				case 4:
					output04On.enabled = indicatorOn;
					break;
				case 5:
					output05On.enabled = indicatorOn;
					break;
				case 6:
					output06On.enabled = indicatorOn;
					break;
				case 7:
					output07On.enabled = indicatorOn;
					break;
				case 8:
					output08On.enabled = indicatorOn;
					break;
				case 9:
					output09On.enabled = indicatorOn;
					break;
				case 10:
					output10On.enabled = indicatorOn;
					break;
				case 11:
					output11On.enabled = indicatorOn;
					break;
				case 12:
					output12On.enabled = indicatorOn;
					break;
				case 13:
					output13On.enabled = indicatorOn;
					break;
				case 14:
					output14On.enabled = indicatorOn;
					break;
				case 15:
					output15On.enabled = indicatorOn;
					break;
				}
			}
			break;
		}
	}

	public void changeInputDdlAction(int _vale){
		int _count = 0;

		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			for(int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass>().inputElements.Length; i++){
				if( _situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i] ){
					if (_count == ddlInputs.value) {
						//
						int _row = _situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i].GetComponent<dragElement>().rowUsed;
						int _column = _situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i].GetComponent<dragElement>().columnUsed;
						_situation.GetComponent<ElectricalTestBenchMainClass> ().inputElements [i].GetComponent<dragElement> ().activateLeftOver (_row, _column, true);
						break;
					} else {
						_count++;
					}
				}
			}
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			for(int i = 0; i < _situation.GetComponent<PnematicTestBenchClass>().inputElements.Length; i++){
				if( _situation.GetComponent<PnematicTestBenchClass>().inputElements[i] ){
					if (_count == ddlInputs.value) {
						if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._typeInput == dragElementPneumatic.typesInputs.Left) {
							int _row = _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().rowUsed;
							int _column = _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().columnUsed;
							_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().activateLeftOver (_row, _column, true);
							break;
						} else {
							int _row = _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().rowUsed;
							int _column = _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().columnUsed;
							_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().activateRightOver (_row, _column, true);
							break;
						}
					} else {
						_count++;
					}
				}
			}
			break;
		}
	}

	public void changeOutputDdlAction(int _value){
		int _count = 0;

		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			for(int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements.Length; i++){
				if( _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i] ){
					if (_count == ddlOutputs.value) {
						//
						int _row = _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i].GetComponent<dragElement>().rowUsed;
						int _column = _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i].GetComponent<dragElement>().columnUsed;
						_situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements [i].GetComponent<dragElement> ().activateRightOver (_row, _column, true);
						break;
					} else {
						_count++;
					}
				}
			}
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			for(int i = 0; i < _situation.GetComponent<PnematicTestBenchClass>().outputsElements.Length; i++){
				if( _situation.GetComponent<PnematicTestBenchClass>().outputsElements[i] ){
					if (_count == ddlOutputs.value) {
						if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> () != null) {
							int _row = _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ().rowUsed;
							int _column = _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ().columnUsed;
							_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ().activateRightOver (_row, _column, true);
						} else {
							Vector2[] _rowsColumnsUsed = new Vector2[0];
							if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> () != null) {
								_rowsColumnsUsed = _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ().rowsColumnsUsed;
								_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ().activateRightOver (_rowsColumnsUsed,true);
							} else if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<motorActuatorElement> () != null) {
								_rowsColumnsUsed = _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<motorActuatorElement> ().rowsColumnsUsed;
								_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<motorActuatorElement> ().activateRightOver (_rowsColumnsUsed,true);
							}
						}
						break;
					} else {
						_count++;
					}
				}
			}
			break;
		}
	}
}
