﻿using UnityEngine;

using TypedEvent = Modular.AnimationCurveEvent;

namespace NSInspectorValue
{
	public class AnimationCurveInspectorValue : GenericInspectorValue<AnimationCurve>
	{
		[Header("Events")]
		[SerializeField] private TypedEvent _onSetValue = new TypedEvent();
		[SerializeField] private TypedEvent _onGetValue = new TypedEvent();

		private void Awake()
		{
			// Inicializo el script
			Initialize();
		}

		public override void Initialize()
		{
			// Defino los eventos de respuesta
			GetResponse = _onGetValue;
			SetResponse = _onSetValue;
		}
	}
}
