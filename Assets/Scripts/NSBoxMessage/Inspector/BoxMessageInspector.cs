﻿using System.Collections;
using System.Collections.Generic;
using NSTraduccionIdiomas;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace NSBoxMessage.Inspector
{
	public class BoxMessageInspector : MonoBehaviour
	{

		public abstract class BoxMessageDisplayData
		{
			public string Message { get; set; }

			public string AcceptButtonText { get; set; } = "OK";

			public UnityEvent AcceptAction { get; set; }
		}
		public class BoxMessageDecisionDisplayData : BoxMessageDisplayData
		{
			public BoxMessageDecisionDisplayData() { }

			public string CancelButtonText { get; set; } = "NO";
			public UnityEvent CancelAction { get; set; }
		}
		public class BoxMessageInfoDisplayData : BoxMessageDisplayData
		{
			public BoxMessageInfoDisplayData() { }
		}

		[Header("Events")]
		[SerializeField] private UnityEvent _onAcceptDefaultAction;
		[SerializeField] private UnityEvent _onCancelDefaultAction;

		private Queue<BoxMessageDisplayData> _queue = new Queue<BoxMessageDisplayData>();
		private DiccionarioIdiomas _diccionario;

		private bool _speculativeDisplayingInfoBox = false;

		/// <summary>
		/// Diccionario usado para traducir los textos
		/// </summary>
		/// <value></value>
		public DiccionarioIdiomas Diccionario
		{
			get
			{
				if (_diccionario == null) _diccionario = DiccionarioIdiomas.Instance;
				return _diccionario;
			}
		}

		/// <summary>
		/// Cadena por defecto del texto aceptar
		/// </summary>
		/// <value></value>
		public string TextAceptar
		{
			get => Diccionario?.Traducir("TextAceptarMayusculas") ?? "Ok";
		}

		/// <summary>
		/// Cadena traducida del texto cancelar
		/// </summary>
		/// <value></value>
		public string TextCancelar
		{
			get => Diccionario?.Traducir("TextCancelar") ?? "No";
		}
		public Queue<BoxMessageDisplayData> BoxMessageDataQueue { get => _queue; }
		public UnityEvent OnAcceptDefaultAction { get => _onAcceptDefaultAction; }
		public UnityEvent OnCancelDefaultAction { get => _onCancelDefaultAction; }

		/// <summary>
		/// Muestra un info box con parámetros por defecto
		/// </summary>
		/// <param name="textMsg">Texto del info box a mostrar</param>
		public void ShowInfoBox(string textMsg)
		{

			// Si la queue existe
			if (BoxMessageDataQueue != null)
			{

				// Si hay al menos un elemento en la queue o estoy mostrando el texto (especulación)
				if (BoxMessageDataQueue.Count > 0 || _speculativeDisplayingInfoBox)
				{


					// Agrego la información correspondiente a la queue
					BoxMessageDataQueue.Enqueue(new BoxMessageInfoDisplayData()
					{
						Message = textMsg,
						AcceptButtonText = TextAceptar
					}
					);

				}
				else
				{
					// No hay elementos en la queue

					// Si el mensaje no es nulo
					if (!string.IsNullOrEmpty(textMsg))
						// Creo un info box con el mensaje de información, y el evento por defecto
						BoxMessageManager.Instance.CreateBoxMessageInfo(
							textMsg,                                        // Mensaje a mostrar
							TextAceptar,                                    // Texto del botón aceptar
							() => { OnAcceptDefaultAction.WrapInvoke(); ShowNext(); }       // Acción Aceptar registrada por defecto
						);

					// Defino el elemento especulativo
					_speculativeDisplayingInfoBox = true;
				}
			}
		}

		/// <summary>
		/// Crea un decision box con los parámetros por defecto
		/// </summary>
		/// <param name="textMsg">Mensaje a mostrar en el BoxMessageDecision</param>
		public void ShowDecisionBox(string textMsg)
		{
			// Si la queue existe
			if (BoxMessageDataQueue != null)
			{

				// Si hay al menos un elemento en la queue o estoy mostrando el texto (especulación)
				if (BoxMessageDataQueue.Count > 0 || _speculativeDisplayingInfoBox)
				{


					// Agrego la información correspondiente a la queue
					BoxMessageDataQueue.Enqueue(new BoxMessageDecisionDisplayData()
					{
						Message = textMsg,
						AcceptButtonText = TextAceptar,
						CancelButtonText = TextCancelar
					}
					);

				}
				else
				{
					// No hay elementos en la queue

					// Si el mensaje no es nulo
					if (!string.IsNullOrEmpty(textMsg))
						// Creo un info box con el mensaje de información, y el evento por defecto
						BoxMessageManager.Instance.CreateBoxMessageDecision(
							textMsg,                                        // Mensaje a mostrar
							TextAceptar,                                    // Texto del botón aceptar
							TextCancelar,
							() => { OnAcceptDefaultAction.WrapInvoke(); ShowNext(); },       // Acción Aceptar registrada por defecto
							() => { OnCancelDefaultAction.WrapInvoke(); ShowNext(); }       // Acción Cancelar registrada por defecto
						);

					// Defino el elemento especulativo
					_speculativeDisplayingInfoBox = true;
				}
			}
		}


		/// <summary>
		/// Muestra un info box con parámetros por defecto, y con sobreescritura de parámetros
		/// </summary>
		/// <param name="textMsg">Texto del info box a mostrar</param>
		public void ShowInfoBoxOverride(string textMsg, UnityEvent overrideAcceptAction)
		{
			// Si la queue existe
			if (BoxMessageDataQueue != null)
			{

				// Si hay al menos un elemento en la queue o estoy mostrando el texto (especulación)
				if (BoxMessageDataQueue.Count > 0 || _speculativeDisplayingInfoBox)
				{


					// Agrego la información correspondiente a la queue
					BoxMessageDataQueue.Enqueue(new BoxMessageInfoDisplayData()
					{
						Message = textMsg,
						AcceptButtonText = TextAceptar,
						AcceptAction = overrideAcceptAction
					}
					);

				}
				else
				{
					// No hay elementos en la queue

					// Sobrescribo el evento de aceptar si disponible
					var buttonAcceptEvent = overrideAcceptAction != null ? overrideAcceptAction : OnAcceptDefaultAction;

					// Si el mensaje no es nulo
					if (!string.IsNullOrEmpty(textMsg))
						// Creo un info box con el mensaje de información, y el evento por defecto
						BoxMessageManager.Instance.CreateBoxMessageInfo(
							textMsg,                                        // Mensaje a mostrar
							TextAceptar,                                    // Texto del botón aceptar
							() => { buttonAcceptEvent.WrapInvoke(); ShowNext(); }       // Acción Aceptar registrada por defecto
						);

					// Defino el elemento especulativo
					_speculativeDisplayingInfoBox = true;
				}
			}
		}

		/// <summary>
		/// Muestra un BoxDecision con un mensaje, y sobrescritura de las acciones aceptar y cancelar
		/// </summary>
		/// <param name="textMsg">Mensaje </param>
		/// <param name="overrideAcceptAction"></param>
		public void ShowDecisionBoxOverride(string textMsg, UnityEvent overrideAcceptAction, UnityEvent overrideCancelAction)
		{
			// Si la queue existe
			if (BoxMessageDataQueue != null)
			{

				// Si hay al menos un elemento en la queue o estoy mostrando el texto (especulación)
				if (BoxMessageDataQueue.Count > 0 || _speculativeDisplayingInfoBox)
				{


					// Agrego la información correspondiente a la queue
					BoxMessageDataQueue.Enqueue(new BoxMessageDecisionDisplayData()
					{
						Message = textMsg,
						AcceptButtonText = TextAceptar,
						AcceptAction = overrideAcceptAction,
						CancelAction = overrideCancelAction
					}
					);

				}
				else
				{
					// No hay elementos en la queue

					// Sobrescribo el evento de aceptar si disponible
					var buttonAcceptEvent = overrideAcceptAction != null ? overrideAcceptAction : OnAcceptDefaultAction;
					var buttonCancelEvent = overrideCancelAction != null ? overrideCancelAction : OnCancelDefaultAction;

					// Si el mensaje no es nulo
					if (!string.IsNullOrEmpty(textMsg))
						// Creo un info box con el mensaje de información, y el evento por defecto
						BoxMessageManager.Instance.CreateBoxMessageDecision(
							textMsg,                                        // Mensaje a mostrar
							TextAceptar,
							TextCancelar,                                    // Texto del botón aceptar
							() => { buttonAcceptEvent.WrapInvoke(); ShowNext(); },       // Acción Aceptar registrada por defecto
							() => { buttonCancelEvent.WrapInvoke(); ShowNext(); }
						);

					// Defino el elemento especulativo
					_speculativeDisplayingInfoBox = true;
				}
			}
		}

		public void CleanQueue()
		{
			// Limpio la queue
			_queue.Clear();
		}

		public void ShowNext()
		{
			// Si la queue existe
			if (BoxMessageDataQueue != null)
			{
				// Si la queue contiene al menos un elemento
				if (BoxMessageDataQueue.Count > 0)
				{
					// Obtengo el elemento de la queue
					var data = BoxMessageDataQueue.Dequeue();

					// Filtro el dato
					switch (data)
					{
						case BoxMessageInfoDisplayData infoDisplayData:
							// Muestro el elemento InfoBox
							ShowBoxMessageInfoData(infoDisplayData);
							break;
						case BoxMessageDecisionDisplayData decisionDisplayData:
							ShowBoxMessageDecisionData(decisionDisplayData);
							break;
						default:
							break;
					}


				}
				else
				{
					//Si no hay elementos en la queue

					// Defino la variable especulativa
					_speculativeDisplayingInfoBox = false;
				}
			}
		}

		private void ShowBoxMessageDecisionData(BoxMessageDecisionDisplayData data)
		{
			// Si la data existe
			if (data != null)
			{
				// Sobrescribo el evento de aceptar si disponible
				var buttonAcceptEvent = data.AcceptAction != null ? data.AcceptAction : OnAcceptDefaultAction;
				var buttonCancelEvent = data.CancelAction != null ? data.CancelAction : OnCancelDefaultAction;

				// Creo un mensaje info con la información de la queue, y el evento por defecto
				BoxMessageManager.Instance.CreateBoxMessageDecision(
					data.Message,                                        // Mensaje a mostrar
					data.AcceptButtonText,
					data.CancelButtonText,                                    // Texto del botón aceptar
					() => { buttonAcceptEvent.WrapInvoke(); ShowNext(); },       // Acción Aceptar registrada por defecto
					() => { buttonCancelEvent.WrapInvoke(); ShowNext(); }
				);
			}
		}

		private void ShowBoxMessageInfoData(BoxMessageInfoDisplayData data)
		{
			// Si la data existe
			if (data != null)
			{
				// Sobrescribo el evento de aceptar si disponible
				var buttonAcceptEvent = data.AcceptAction != null ? data.AcceptAction : OnAcceptDefaultAction;

				// Creo un mensaje info con la información de la queue, y el evento por defecto
				BoxMessageManager.Instance.CreateBoxMessageInfo(
					data.Message,                                        // Mensaje a mostrar
					data.AcceptButtonText,                                    // Texto del botón aceptar
					() => { buttonAcceptEvent.WrapInvoke(); ShowNext(); }       // Acción Aceptar registrada por defecto
				);
			}
		}
	}
}
