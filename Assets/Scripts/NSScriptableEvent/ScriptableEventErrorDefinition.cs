﻿using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	[CreateAssetMenu(fileName = "Event ErrorDefinition", menuName = "NSScriptableEvent/ErrorDefinition", order = 0)]
	public class ScriptableEventErrorDefinition : AbstractScriptableEvent<ErrorDefinition>
	{
		[System.Serializable]
		public class ErrorDefinitionEvent : UnityEvent<ErrorDefinition> { }
	}
}