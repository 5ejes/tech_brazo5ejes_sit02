#pragma warning disable 0649
using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent.GenericListeners
{
	public abstract class GenericScriptableEventListener<T> : MonoBehaviour 
	{

		protected abstract void OnEnable();			
		protected abstract void OnDisable();
	}
}