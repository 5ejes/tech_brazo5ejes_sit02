﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Xml;

public class lineObject : MonoBehaviour {

	public dragElementLadder._type_elements typeElement;
	public dragElementLadder._typesIO allowedType;

	public GameObject lineEmptyComponent;
	public GameObject lineInputComponent;
	public GameObject lineInputNComponent;
	public GameObject lineOutputComponent;
	public GameObject lineOutputNComponent;
	public GameObject lineOutputSRComponent;
	public GameObject lineOutputResComponent;
	public GameObject lineObjectComponent;

	public Image colliderImage;

	public Text lineInputTitle;
	public Text lineInputNTitle;
	public Text lineOutputTitle;
	public Text lineOutputNTitle;
	public Text lineOutputSRText;
	public Text lineOutputSRTitle;
	public Text lineOutputResText;
	public Text lineOutputResTitle;
	public Text lineObjectText1A;
	public Text lineObjectText2A;
	public Text lineObjectText3A;
	public Text lineObjectText1B;
	public Text lineObjectText2B;
	public Text lineObjectText3B;
	public TEXDraw lineObjectTitle;
	public Text lineObjectText2AOff;
	public Text lineObjectText3AOff;
	public Text lineObjectText2BOff;
	public Text lineObjectText3BOff;
	public Text lineObjectBottomTitle;

	public bool overActive = false;

	public int lineNumber;

	public string value1;
	public int index1;
	public string value2;
	public int index2;
	public string value3;
	public int index3;
	public string value4;
	public string value5;
	public string value6;
	public bool constant1;
	public bool constant2;

	public int scaleTP = 0;

	public int branchLevel = 0;

	public Image leftBLine;
	public Image rightBLine;

	public Image emptyLeftLine;
	public Image emptyRightLine;

	public Image lineInputLeftLine;
	public Image lineInputRightLine;
	public Image lineInputVertical1Line;
	public Image lineInputVertical2Line;

	public Image lineInputNLeftLine;
	public Image lineInputNRightLine;
	public Image lineInputNVertical1Line;
	public Image lineInputNVertical2Line;
	public Image lineInputNCloseLine;

	public Image lineOutputLeftLine;
	public Image lineOutputRightLine;
	public Image lineOutputVertical1Curve;
	public Image lineOutputVertical2Curve;

	public Image lineOutputNLeftLine;
	public Image lineOutputNRightLine;
	public Image lineOutputNVertical1Curve;
	public Image lineOutputNVertical2Curve;
	public Image lineOutputNCloseLine;

	public Image lineOutputS_RLeftLine;
	public Image lineOutputS_RRightLine;
	public Image lineOutputS_RVertical1Curve;
	public Image lineOutputS_RVertical2Curve;

	public Image lineOutputResLeftLine;
	public Image lineOutputResRightLine;
	public Image lineOutputResVertical1Curve;
	public Image lineOutputResVertical2Curve;

	public Image lineObjectLeftLine;
	public Image lineObjectRightLine;
	public Image lineObjectBg;

	public Image lineObjectText2AOffBg;
	public Image lineObjectText3AOffBg;
	public Image lineObjectText2BOffBg;
	public Image lineObjectText3BOffBg;

	public bool actualValue = false;

	public GameObject parentLineGroup;
	public GameObject parentElement;
	public GameObject branchElement;
	public GameObject rightBranchElement;
	public GameObject leftBranchElement;
	private GameObject _situation;

	// Use this for initialization
	void Start () {

		typeElement = dragElementLadder._type_elements.none;
		colliderImage.enabled = false;

		_situation = GameObject.Find ("Canvas/Ladder");

		lineEmptyComponent.SetActive (true);
		lineInputComponent.SetActive (false);
		lineInputNComponent.SetActive (false);
		lineOutputComponent.SetActive (false);
		lineOutputNComponent.SetActive (false);
		lineOutputSRComponent.SetActive (false);
		lineOutputResComponent.SetActive (false);
		lineObjectComponent.SetActive (false);

		gameObject.AddComponent<EventTrigger> ();
		colliderImage.gameObject.AddComponent<EventTrigger> ();
		EventTrigger trigger_Main_Object = gameObject.GetComponent<EventTrigger>();
		EventTrigger trigger_Object = colliderImage.gameObject.GetComponent<EventTrigger>();

		EventTrigger.Entry entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.PointerExit;
		entry_1.callback.AddListener((data) => { ObjectMouseOut_((PointerEventData)data); });

		EventTrigger.Entry entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.PointerClick;
		entry_2.callback.AddListener((data) => { ObjectClicked_((PointerEventData)data); });

		trigger_Object.triggers.Add(entry_1);
		trigger_Main_Object.triggers.Add (entry_2);
	}

	public void showCollider(){
		colliderImage.transform.SetParent (GameObject.Find("Canvas/Ladder").transform, true);
		colliderImage.transform.SetAsLastSibling ();
		colliderImage.enabled = true;
	}

	public void hideCollider(){
		colliderImage.transform.SetParent (gameObject.transform, true);
		colliderImage.enabled = false;
	}

	public void setComponent(dragElementLadder._type_elements dragTypeElement, dragElementLadder._typesIO dragTypeIO){
		setOverComponent (dragTypeElement, dragTypeIO);
		if(allowedType == dragTypeIO){
			typeElement = dragTypeElement;	
		}
	}

	public void setOverComponent(dragElementLadder._type_elements dragTypeElement, dragElementLadder._typesIO dragTypeIO){
		if(typeElement == dragElementLadder._type_elements.none){
			if (allowedType == dragTypeIO) {
				switch(dragTypeElement){
				case dragElementLadder._type_elements.open_contact:
					lineEmptyComponent.SetActive (false);
					lineInputComponent.SetActive (true);
					lineInputNComponent.SetActive (false);
					lineOutputComponent.SetActive (false);
					lineOutputNComponent.SetActive (false);
					lineOutputSRComponent.SetActive (false);
					lineOutputResComponent.SetActive (false);
					lineObjectComponent.SetActive (false);
					lineInputTitle.text = "";
					break;
				case dragElementLadder._type_elements.close_contact:
					lineEmptyComponent.SetActive (false);
					lineInputComponent.SetActive (false);
					lineInputNComponent.SetActive (true);
					lineOutputComponent.SetActive (false);
					lineOutputNComponent.SetActive (false);
					lineOutputSRComponent.SetActive (false);
					lineOutputResComponent.SetActive (false);
					lineObjectComponent.SetActive (false);
					lineInputNTitle.text = "";
					break;
				case dragElementLadder._type_elements.open_coil:
					lineEmptyComponent.SetActive (false);
					lineInputComponent.SetActive (false);
					lineInputNComponent.SetActive (false);
					lineOutputComponent.SetActive (true);
					lineOutputNComponent.SetActive (false);
					lineOutputSRComponent.SetActive (false);
					lineOutputResComponent.SetActive (false);
					lineObjectComponent.SetActive (false);
					lineOutputTitle.text = "";
					break;
				case dragElementLadder._type_elements.close_coil:
					lineEmptyComponent.SetActive (false);
					lineInputComponent.SetActive (false);
					lineInputNComponent.SetActive (false);
					lineOutputComponent.SetActive (false);
					lineOutputNComponent.SetActive (true);
					lineOutputSRComponent.SetActive (false);
					lineOutputResComponent.SetActive (false);
					lineObjectComponent.SetActive (false);
					lineOutputNTitle.text = "";
					break;
				case dragElementLadder._type_elements.set_coil:
				case dragElementLadder._type_elements.reset_coil:
					lineEmptyComponent.SetActive (false);
					lineInputComponent.SetActive (false);
					lineInputNComponent.SetActive (false);
					lineOutputComponent.SetActive (false);
					lineOutputNComponent.SetActive (false);
					lineOutputSRComponent.SetActive (true);
					lineOutputResComponent.SetActive (false);
					lineObjectComponent.SetActive (false);
					lineOutputSRTitle.text = "";
					configureSRComponent (dragTypeElement);
					break;
				case dragElementLadder._type_elements.reset:
					lineEmptyComponent.SetActive (false);
					lineInputComponent.SetActive (false);
					lineInputNComponent.SetActive (false);
					lineOutputComponent.SetActive (false);
					lineOutputNComponent.SetActive (false);
					lineOutputSRComponent.SetActive (false);
					lineOutputResComponent.SetActive (true);
					lineObjectComponent.SetActive (false);
					lineOutputResTitle.text = "";
					break;
				case dragElementLadder._type_elements.add:
				case dragElementLadder._type_elements.subs:
				case dragElementLadder._type_elements.prod:
				case dragElementLadder._type_elements.division:
				case dragElementLadder._type_elements.equal:
				case dragElementLadder._type_elements.different:
				case dragElementLadder._type_elements.great:
				case dragElementLadder._type_elements.great_equal:
				case dragElementLadder._type_elements.less:
				case dragElementLadder._type_elements.less_equal:
				case dragElementLadder._type_elements.and:
				case dragElementLadder._type_elements.or:
				case dragElementLadder._type_elements.not:
				case dragElementLadder._type_elements.xor:
				case dragElementLadder._type_elements.mov:
				case dragElementLadder._type_elements.ctu:
				case dragElementLadder._type_elements.ctd:
				case dragElementLadder._type_elements.ton:
				case dragElementLadder._type_elements.toff:
				case dragElementLadder._type_elements.tp:
				case dragElementLadder._type_elements.pid:
					lineEmptyComponent.SetActive (false);
					lineInputComponent.SetActive (false);
					lineInputNComponent.SetActive (false);
					lineOutputComponent.SetActive (false);
					lineOutputNComponent.SetActive (false);
					lineOutputSRComponent.SetActive (false);
					lineOutputResComponent.SetActive (false);
					lineObjectComponent.SetActive (true);
					lineObjectBottomTitle.text = "";
					configureObjectComponent (dragTypeElement);
					break;
				default:
					lineEmptyComponent.SetActive (true);
					lineInputComponent.SetActive (false);
					lineInputNComponent.SetActive (false);
					lineOutputComponent.SetActive (false);
					lineOutputNComponent.SetActive (false);
					lineOutputSRComponent.SetActive (false);
					lineOutputResComponent.SetActive (false);
					lineObjectComponent.SetActive (false);
					break;
				}
			} else {
				lineEmptyComponent.SetActive (true);
				lineInputComponent.SetActive (false);
				lineInputNComponent.SetActive (false);
				lineOutputComponent.SetActive (false);
				lineOutputNComponent.SetActive (false);
				lineOutputSRComponent.SetActive (false);
				lineOutputResComponent.SetActive (false);
				lineObjectComponent.SetActive (false);
			}
		}
	}

	public void hideOverComponent(){
		if (typeElement == dragElementLadder._type_elements.none) {
			lineEmptyComponent.SetActive (true);
			lineInputComponent.SetActive (false);
			lineInputNComponent.SetActive (false);
			lineOutputComponent.SetActive (false);
			lineOutputNComponent.SetActive (false);
			lineOutputSRComponent.SetActive (false);
			lineOutputResComponent.SetActive (false);
			lineObjectComponent.SetActive (false);
		}
	}

	public void resetObject(){
		if (parentElement == null || (parentElement != null && typeElement != dragElementLadder._type_elements.none )) {
			typeElement = dragElementLadder._type_elements.none;
			lineEmptyComponent.SetActive (true);
			lineInputComponent.SetActive (false);
			lineInputNComponent.SetActive (false);
			lineOutputComponent.SetActive (false);
			lineOutputNComponent.SetActive (false);
			lineOutputSRComponent.SetActive (false);
			lineOutputResComponent.SetActive (false);
			lineObjectComponent.SetActive (false);

			value1 = "";
			value2 = "";
			value3 = "";
			index1 = 0;
			index2 = 0;
			index3 = 0;
			constant1 = false;
			constant2 = false;
			scaleTP = 0;	
		} else {
			bool moveNextLinesRequired = true;
			for(int i = 0; i < parentLineGroup.GetComponent<lineGroupObject>().branchLines.Count; i++){
				if(parentLineGroup.GetComponent<lineGroupObject>().branchLines[i].GetComponent<lineObject>().branchLevel >= (branchLevel)){
					if(parentLineGroup.GetComponent<lineGroupObject>().branchLines[i].GetInstanceID() != gameObject.GetInstanceID()){
						moveNextLinesRequired = false;
						break;	
					}
				}
			}

			if(moveNextLinesRequired == true){
				int branchIndex = -1;
				for(int i = 0; i < _situation.GetComponent<LadderMainClass>().lineGroupObjects.Count; i++){
					if(_situation.GetComponent<LadderMainClass>().lineGroupObjects[i].GetInstanceID() == parentLineGroup.GetInstanceID()){
						branchIndex = i;
						break;
					}
				}

				if(branchIndex >= 0){
					for(int i = branchIndex + 1; i < _situation.GetComponent<LadderMainClass>().lineGroupObjects.Count; i++ ){
						_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition = new Vector3 (_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition.x, _situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition.y + 140, _situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition.z);
						_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].GetComponent<lineGroupObject> ().initialPosition += 140f;
					}

					_situation.GetComponent<LadderMainClass> ().updateWorkbenchArea ();
				}
			}

			GameObject parentLElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber - 1, branchLevel - 1);
			GameObject parentRElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber + 1, branchLevel - 1);

			if(parentElement.GetComponent<lineObject> ().branchLevel == 0){
				parentElement.GetComponent<lineObject> ().rightBranchElement = null;
				parentElement.GetComponent<lineObject> ().leftBranchElement = null;

				if(parentLElement != null){
					parentLElement.GetComponent<lineObject> ().rightBranchElement = null;
				}

				if (parentRElement != null) {
					parentRElement.GetComponent<lineObject> ().leftBranchElement = null;
				}
			}

			parentLineGroup.GetComponent<lineGroupObject> ().branchLines.Remove (gameObject);
			parentLineGroup.GetComponent<lineGroupObject> ().updateLineGroupHeight ();
			_situation.GetComponent<LadderMainClass> ().updateWorkbenchArea ();
			Destroy (gameObject);
		}
		_situation.GetComponent<LadderMainClass>().debugMode = false;
		_situation.GetComponent<LadderMainClass> ().hideEditMenu ();
	}

	private void ObjectMouseOut_(PointerEventData data){
		if(overActive == true){
			overActive = false;
			hideOverComponent ();
		}
	}

	private void ObjectClicked_(PointerEventData data){
		_situation.GetComponent<LadderMainClass> ().openEditMenu (gameObject);
	}

	private void configureSRComponent(dragElementLadder._type_elements dragTypeElement){
		if (dragTypeElement == dragElementLadder._type_elements.set_coil) {
			lineOutputSRText.text = "S";
		} else {
			lineOutputSRText.text = "R";
		}
	}

	private void configureObjectComponent(dragElementLadder._type_elements dragTypeElement){
		switch (dragTypeElement) {
		case dragElementLadder._type_elements.add:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "IN2";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "OUT";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] +";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.subs:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "IN2";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "OUT";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] -";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.prod:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "IN2";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "OUT";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] \\times";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.division:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "IN2";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "OUT";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] \\div";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.equal:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "IN2";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] =";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.different:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "IN2";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] ≠";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.great:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "IN2";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] \\gtr";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.great_equal:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "IN2";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] \\geq";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.less:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "IN2";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] \\less";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.less_equal:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "IN2";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] \\leq";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.and:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "IN2";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] AND";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.or:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "IN2";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] OR";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.not:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] NOT";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.xor:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "IN2";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] XOR";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.mov:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN1";
			lineObjectText3A.text = "";
			lineObjectText1B.text = "ENO";
			lineObjectText2B.text = "";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] MOV";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.ctu:
			lineObjectText1A.text = "CU";
			lineObjectText2A.text = "R";
			lineObjectText3A.text = "PV";
			lineObjectText1B.text = "Q";
			lineObjectText2B.text = "CV";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] CTU";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.ctd:
			lineObjectText1A.text = "CD";
			lineObjectText2A.text = "LD";
			lineObjectText3A.text = "PV";
			lineObjectText1B.text = "Q";
			lineObjectText2B.text = "CV";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] CTD";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.ton:
			lineObjectText1A.text = "IN";
			lineObjectText2A.text = "RT";
			lineObjectText3A.text = "";
			lineObjectText1B.text = "Q";
			lineObjectText2B.text = "ET";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] TON";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.toff:
			lineObjectText1A.text = "IN";
			lineObjectText2A.text = "RT";
			lineObjectText3A.text = "";
			lineObjectText1B.text = "Q";
			lineObjectText2B.text = "ET";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] TOFF";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.tp:
			lineObjectText1A.text = "IN";
			lineObjectText2A.text = "RT";
			lineObjectText3A.text = "";
			lineObjectText1B.text = "Q";
			lineObjectText2B.text = "ET";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] TP";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		case dragElementLadder._type_elements.pid:
			lineObjectText1A.text = "EN";
			lineObjectText2A.text = "IN";
			lineObjectText3A.text = "SP";
			lineObjectText1B.text = "";
			lineObjectText2B.text = "OUT";
			lineObjectText3B.text = "";
			lineObjectTitle.text = "\\opens[b] PID";
			lineObjectText2AOff.text = "";
			lineObjectText3AOff.text = "";
			lineObjectText2BOff.text = "";
			lineObjectText3BOff.text = "";
			break;
		}
	}

	public void addBranch(){
		bool moveNextLinesRequired = true;
		_situation.GetComponent<LadderMainClass> ().debugMode = false;
		if(parentLineGroup.GetComponent<lineGroupObject>().branchLines.Count > 0){
			for(int i = 0; i < parentLineGroup.GetComponent<lineGroupObject>().branchLines.Count; i++){
				if(parentLineGroup.GetComponent<lineGroupObject>().branchLines[i].GetComponent<lineObject>().branchLevel >= (branchLevel + 1)){
					moveNextLinesRequired = false;
					break;
				}
			}
		}

		if(moveNextLinesRequired == true){
			int branchIndex = -1;
			for(int i = 0; i < _situation.GetComponent<LadderMainClass>().lineGroupObjects.Count; i++){
				if(_situation.GetComponent<LadderMainClass>().lineGroupObjects[i].GetInstanceID() == parentLineGroup.GetInstanceID()){
					branchIndex = i;
					break;
				}
			}

			if(branchIndex >= 0){
				for(int i = branchIndex + 1; i < _situation.GetComponent<LadderMainClass>().lineGroupObjects.Count; i++ ){
					_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition = new Vector3 (_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition.x, _situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition.y - 140, _situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition.z);
					_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].GetComponent<lineGroupObject> ().initialPosition -= 140f;
				}

				_situation.GetComponent<LadderMainClass> ().updateWorkbenchArea ();
			}
		}

		branchElement = Instantiate (parentLineGroup.GetComponent<lineGroupObject>().lineObjectPrefab, parentLineGroup.transform);
		branchElement.GetComponent<lineObject> ().allowedType = allowedType;
		branchElement.GetComponent<lineObject> ().lineNumber = lineNumber;
		branchElement.GetComponent<lineObject> ().parentLineGroup = parentLineGroup;
		branchElement.GetComponent<lineObject> ().parentElement = gameObject;
		branchElement.GetComponent<lineObject> ().branchLevel = branchLevel + 1;
		branchElement.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y - 140, gameObject.transform.localPosition.z);

		GameObject parentLElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber - 1, branchLevel + 1);
		GameObject parentRElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber + 1, branchLevel + 1);

		GameObject parentLSubElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber - 1, branchLevel);
		GameObject parentRSubElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber + 1, branchLevel);

		if(parentLElement != null){
			parentLElement.GetComponent<lineObject> ().rightBranchElement = branchElement;
			branchElement.GetComponent<lineObject> ().leftBranchElement = parentLElement;

			parentLSubElement.GetComponent<lineObject> ().rightBranchElement = gameObject;
			leftBranchElement = parentLSubElement;
		}

		if(parentRElement != null){
			parentRElement.GetComponent<lineObject> ().leftBranchElement = branchElement;
			branchElement.GetComponent<lineObject> ().rightBranchElement = parentRElement;

			parentRSubElement.GetComponent<lineObject> ().leftBranchElement = gameObject;
			rightBranchElement = parentRSubElement;
		}

		parentLineGroup.GetComponent<lineGroupObject> ().branchLines.Add (branchElement);
		parentLineGroup.GetComponent<lineGroupObject> ().updateLineGroupHeight ();
		_situation.GetComponent<LadderMainClass> ().updateWorkbenchArea ();
	}

	public void addRightBranch(){
		_situation.GetComponent<LadderMainClass> ().debugMode = false;
		GameObject parentRElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber + 1, branchLevel - 1);

		rightBranchElement = Instantiate (parentLineGroup.GetComponent<lineGroupObject>().lineObjectPrefab, parentLineGroup.transform);
		rightBranchElement.GetComponent<lineObject> ().allowedType = allowedType;
		rightBranchElement.GetComponent<lineObject> ().lineNumber = lineNumber + 1;
		rightBranchElement.GetComponent<lineObject> ().parentLineGroup = parentLineGroup;
		rightBranchElement.GetComponent<lineObject> ().parentElement = parentRElement;
		rightBranchElement.GetComponent<lineObject> ().leftBranchElement = gameObject;
		rightBranchElement.GetComponent<lineObject> ().branchLevel = branchLevel;
		rightBranchElement.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 124f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);

		parentRElement.GetComponent<lineObject> ().branchElement = rightBranchElement;

		parentRElement.GetComponent<lineObject> ().leftBranchElement = parentElement;
		parentElement.GetComponent<lineObject> ().rightBranchElement = parentRElement;

		GameObject parentR1Element = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber + 2, branchLevel);
		GameObject parentR1SubElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber + 2, branchLevel - 1);

		if(parentR1Element != null){
			parentR1Element.GetComponent<lineObject> ().leftBranchElement = rightBranchElement;
			rightBranchElement.GetComponent<lineObject> ().rightBranchElement = parentR1Element;

			parentR1SubElement.GetComponent<lineObject> ().leftBranchElement = parentRElement;
			parentRElement.GetComponent<lineObject> ().rightBranchElement = parentR1SubElement;
		}

		parentLineGroup.GetComponent<lineGroupObject> ().branchLines.Add (rightBranchElement);
		parentLineGroup.GetComponent<lineGroupObject> ().updateLineGroupHeight ();
		_situation.GetComponent<LadderMainClass> ().updateWorkbenchArea ();
	}

	public void setValue(string valueLoaded, dragElementLadder._typesIO typeInput, int outputNameAtt){
		Debug.Log ("lineValue: " + valueLoaded);
		int valueNumeric1;
		int valueNumeric2;
		int valueNumeric3;
		string[] InputArray;
		string valueLoadedEdited = "";
		if (valueLoaded.IndexOf ("AADD") == 0) {
			setComponent (dragElementLadder._type_elements.add, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("AADD(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			if(int.TryParse(InputArray[1],out valueNumeric2)){
				constant2 = true;
			}
			value2 = InputArray [1];
			index2 = -1;

			value3 = InputArray [2];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("ASUB") == 0) {
			setComponent (dragElementLadder._type_elements.subs, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("ASUB(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			if(int.TryParse(InputArray[1],out valueNumeric2)){
				constant2 = true;
			}
			value2 = InputArray [1];
			index2 = -1;

			value3 = InputArray [2];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("APROD") == 0) {
			setComponent (dragElementLadder._type_elements.prod, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("APROD(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			if(int.TryParse(InputArray[1],out valueNumeric2)){
				constant2 = true;
			}
			value2 = InputArray [1];
			index2 = -1;

			value3 = InputArray [2];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("ADIVI") == 0) {
			setComponent (dragElementLadder._type_elements.division, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("ADIVI(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			if(int.TryParse(InputArray[1],out valueNumeric2)){
				constant2 = true;
			}
			value2 = InputArray [1];
			index2 = -1;

			value3 = InputArray [2];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("AEQU") == 0) {
			setComponent (dragElementLadder._type_elements.equal, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("AEQU(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			if(int.TryParse(InputArray[1],out valueNumeric2)){
				constant2 = true;
			}
			value2 = InputArray [1];
			index2 = -1;
		} else if (valueLoaded.IndexOf ("ANEQ") == 0) {
			setComponent (dragElementLadder._type_elements.different, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("ANEQ(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			if(int.TryParse(InputArray[1],out valueNumeric2)){
				constant2 = true;
			}
			value2 = InputArray [1];
			index2 = -1;
		} else if (valueLoaded.IndexOf ("AGEQ") == 0) {
			setComponent (dragElementLadder._type_elements.great_equal, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("AGEQ(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			if(int.TryParse(InputArray[1],out valueNumeric2)){
				constant2 = true;
			}
			value2 = InputArray [1];
			index2 = -1;
		} else if (valueLoaded.IndexOf ("AGRE") == 0) {
			setComponent (dragElementLadder._type_elements.great, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("AGRE(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			if(int.TryParse(InputArray[1],out valueNumeric2)){
				constant2 = true;
			}
			value2 = InputArray [1];
			index2 = -1;
		} else if (valueLoaded.IndexOf ("ALEQ") == 0) {
			setComponent (dragElementLadder._type_elements.less_equal, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("ALEQ(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			if(int.TryParse(InputArray[1],out valueNumeric2)){
				constant2 = true;
			}
			value2 = InputArray [1];
			index2 = -1;
		} else if (valueLoaded.IndexOf ("ALES") == 0) {
			setComponent (dragElementLadder._type_elements.less, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("ALES(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			if(int.TryParse(InputArray[1],out valueNumeric2)){
				constant2 = true;
			}
			value2 = InputArray [1];
			index2 = -1;
		} else if (valueLoaded.IndexOf ("LAND") == 0) {
			setComponent (dragElementLadder._type_elements.and, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("LAND(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			if(int.TryParse(InputArray[1],out valueNumeric2)){
				constant2 = true;
			}
			value2 = InputArray [1];
			index2 = -1;

			value3 = InputArray [2];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("LXOR") == 0) {
			setComponent (dragElementLadder._type_elements.xor, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("LXOR(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			if(int.TryParse(InputArray[1],out valueNumeric2)){
				constant2 = true;
			}
			value2 = InputArray [1];
			index2 = -1;

			value3 = InputArray [2];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("LOOR") == 0) {
			setComponent (dragElementLadder._type_elements.or, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("LOOR(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			if(int.TryParse(InputArray[1],out valueNumeric2)){
				constant2 = true;
			}
			value2 = InputArray [1];
			index2 = -1;

			value3 = InputArray [2];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("LNOT") == 0) {
			setComponent (dragElementLadder._type_elements.not, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("LNOT(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			value3 = InputArray [1];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("LMOV") == 0) {
			setComponent (dragElementLadder._type_elements.mov, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("LMOV(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			if(int.TryParse(InputArray[0],out valueNumeric1)){
				constant1 = true;
			}
			value1 = InputArray [0];
			index1 = -1;

			value3 = InputArray [1];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("CTU") == 0) {
			setComponent (dragElementLadder._type_elements.ctu, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("CTU(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			value1 = InputArray [0];
			index1 = -1;

			value2 = InputArray [1];

			value3 = InputArray [2];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("CTD") == 0) {
			setComponent (dragElementLadder._type_elements.ctd, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("CTD(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			value1 = InputArray [0];
			index1 = -1;

			value2 = InputArray [1];

			value3 = InputArray [2];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("TON") == 0) {
			setComponent (dragElementLadder._type_elements.ton, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("TON(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			value1 = InputArray [0];
			index1 = -1;

			if (int.TryParse (InputArray [1], out valueNumeric2)) {
				if (valueNumeric2 > (60 * 60 * 1000)) {
					value2 = (valueNumeric2 / (60 * 60 * 1000)).ToString ();
					scaleTP = 3;
				} else if (valueNumeric2 > (60 * 1000)) {
					value2 = (valueNumeric2 / (60 * 1000)).ToString ();
					scaleTP = 2;
				} else if (valueNumeric2 > (1000)) {
					value2 = (valueNumeric2 / (1000)).ToString ();
					scaleTP = 1;
				} else {
					value2 = InputArray [1];
					scaleTP = 0;
				}
			} else {
				value2 = "0";
				scaleTP = 0;
			}

			value3 = InputArray [2];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("TOF") == 0) {
			setComponent (dragElementLadder._type_elements.toff, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("TOF(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			value1 = InputArray [0];
			index1 = -1;

			if (int.TryParse (InputArray [1], out valueNumeric2)) {
				if (valueNumeric2 > (60 * 60 * 1000)) {
					value2 = (valueNumeric2 / (60 * 60 * 1000)).ToString ();
					scaleTP = 3;
				} else if (valueNumeric2 > (60 * 1000)) {
					value2 = (valueNumeric2 / (60 * 1000)).ToString ();
					scaleTP = 2;
				} else if (valueNumeric2 > (1000)) {
					value2 = (valueNumeric2 / (1000)).ToString ();
					scaleTP = 1;
				} else {
					value2 = InputArray [1];
					scaleTP = 0;
				}
			} else {
				value2 = "0";
				scaleTP = 0;
			}

			value3 = InputArray [2];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("TPU") == 0) {
			setComponent (dragElementLadder._type_elements.tp, typeInput);

			valueLoadedEdited = valueLoaded.Replace ("TPU(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			value1 = InputArray [0];
			index1 = -1;

			if (int.TryParse (InputArray [1], out valueNumeric2)) {
				if (valueNumeric2 > (60 * 60 * 1000)) {
					value2 = (valueNumeric2 / (60 * 60 * 1000)).ToString ();
					scaleTP = 3;
				} else if (valueNumeric2 > (60 * 1000)) {
					value2 = (valueNumeric2 / (60 * 1000)).ToString ();
					scaleTP = 2;
				} else if (valueNumeric2 > (1000)) {
					value2 = (valueNumeric2 / (1000)).ToString ();
					scaleTP = 1;
				} else {
					value2 = InputArray [1];
					scaleTP = 0;
				}
			} else {
				value2 = "0";
				scaleTP = 0;
			}

			value3 = InputArray [2];
			index3 = -1;
		} else if (valueLoaded.IndexOf ("PID") == 0) {
			setComponent (dragElementLadder._type_elements.pid, typeInput);
			valueLoadedEdited = valueLoaded.Replace ("PID(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			InputArray = valueLoadedEdited.Split (',');

			value1 = InputArray [5];
			value2 = InputArray [6];
			value3 = InputArray [3];
			value4 = InputArray [0];
			value5 = InputArray [1];
			value6 = InputArray [2];
			index1 = -1;
			index2 = -1;
		} else if (valueLoaded.IndexOf ("TRE") == 0) {
			setComponent (dragElementLadder._type_elements.reset, typeInput);
			valueLoadedEdited = valueLoaded.Replace ("TRE(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			value1 = valueLoadedEdited;
			index1 = -1;
		} else if (valueLoaded.IndexOf ("CRE") == 0) {
			setComponent (dragElementLadder._type_elements.reset, typeInput);
			valueLoadedEdited = valueLoaded.Replace ("CRE(", "");
			valueLoadedEdited = valueLoadedEdited.Replace (")", "");
			value1 = valueLoadedEdited;
			index1 = -1;
		} else if (valueLoaded.IndexOf ("NOLIN") == 0) {

		} else if (valueLoaded.IndexOf ("!") == 0) {
			setComponent (dragElementLadder._type_elements.close_contact, typeInput);
			value1 = valueLoaded.Substring(1);
			index1 = -1;
		} else {
			if (outputNameAtt == -1) {
				setComponent (dragElementLadder._type_elements.open_contact, typeInput);
			} else if(outputNameAtt == 1){
				//output open coil
				setComponent (dragElementLadder._type_elements.open_coil, typeInput);
			} else if(outputNameAtt == 2){
				//output closed coil
				setComponent (dragElementLadder._type_elements.close_coil, typeInput);
			} else if(outputNameAtt == 3){
				//output set coil
				setComponent (dragElementLadder._type_elements.set_coil, typeInput);
			} else if(outputNameAtt == 4){
				//output reset coil
				setComponent (dragElementLadder._type_elements.reset_coil, typeInput);
			} 

			value1 = valueLoaded;
			index1 = -1;
		}
	}

	public string getXMLValue(){
		string xmlValue = "";

		string outputValue1 = "0";
		string outputValue2 = "0";
		string outputValue3 = "0";

		string outputValue4 = "0";
		string outputValue5 = "0";
		string outputValue6 = "0";

		if (value1 != "") {
			outputValue1 = value1;
		}

		if (value2 != "") {
			outputValue2 = value2;
		}

		if (value3 != "") {
			outputValue3 = value3;
		}

		if (value4 != "") {
			outputValue4 = value4;
		}

		if (value5 != "") {
			outputValue5 = value5;
		}

		if (value6 != "") {
			outputValue6 = value6;
		}

		switch(typeElement){
		case dragElementLadder._type_elements.open_contact:
		case dragElementLadder._type_elements.open_coil:
		case dragElementLadder._type_elements.reset_coil:
		case dragElementLadder._type_elements.set_coil:
		case dragElementLadder._type_elements.close_coil:
			xmlValue = outputValue1;
			break;
		case dragElementLadder._type_elements.close_contact:
			xmlValue = "!" + outputValue1;
			break;
		case dragElementLadder._type_elements.reset:
			if(outputValue1.IndexOf("C") == 0){
				xmlValue = "CRE(" + outputValue1 + ")";	
			} else if(outputValue1.IndexOf("T") == 0){
				xmlValue = "TRE(" + outputValue1 + ")";	
			}
			break;
		case dragElementLadder._type_elements.add:
			xmlValue = "AADD(" + outputValue1 + "," + outputValue2 + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.subs:
			xmlValue = "ASUB(" + outputValue1 + "," + outputValue2 + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.prod:
			xmlValue = "APRO(" + outputValue1 + "," + outputValue2 + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.division:
			xmlValue = "ADIV(" + outputValue1 + "," + outputValue2 + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.equal:
			xmlValue = "AEQU(" + outputValue1 + "," + outputValue2 + ")";
			break;
		case dragElementLadder._type_elements.different:
			xmlValue = "ANEQ(" + outputValue1 + "," + outputValue2 + ")";
			break;
		case dragElementLadder._type_elements.great:
			xmlValue = "AGRE(" + outputValue1 + "," + outputValue2 + ")";
			break;
		case dragElementLadder._type_elements.great_equal:
			xmlValue = "AGEQ(" + outputValue1 + "," + outputValue2 + ")";
			break;
		case dragElementLadder._type_elements.less:
			xmlValue = "ALES(" + outputValue1 + "," + outputValue2 + ")";
			break;
		case dragElementLadder._type_elements.less_equal:
			xmlValue = "ALEQ(" + outputValue1 + "," + outputValue2 + ")";
			break;
		case dragElementLadder._type_elements.and:
			xmlValue = "LAND(" + outputValue1 + "," + outputValue2 + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.or:
			xmlValue = "LOOR(" + outputValue1 + "," + outputValue2 + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.xor:
			xmlValue = "LXOR(" + outputValue1 + "," + outputValue2 + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.not:
			xmlValue = "LNOT(" + outputValue1 + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.mov:
			xmlValue = "LMOV(" + outputValue1 + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.ctu:
			xmlValue = "CTU(" + outputValue1 + "," + outputValue2 + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.ctd:
			xmlValue = "CTD(" + outputValue1 + "," + outputValue2 + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.ton:
			float TPon = 0;
			if (scaleTP == 0) {
				if (float.TryParse (value2, out TPon)) {
				}
			} else if (scaleTP == 1) {
				if (float.TryParse (value2, out TPon)) {
					TPon = TPon * 1000f;
				}
			} else if (scaleTP == 2) {
				if (float.TryParse (value2, out TPon)) {
					TPon = TPon * 1000f * 60f;
				}
			} else if (scaleTP == 3) {
				if (float.TryParse (value2, out TPon)) {
					TPon = TPon * 1000f * 60f * 60f;
				}
			}
			xmlValue = "TON(" + outputValue1 + "," + TPon.ToString () + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.toff:
			float TPoff = 0;
			if (scaleTP == 0) {
				if (float.TryParse (value2, out TPoff)) {
				}
			} else if (scaleTP == 1) {
				if (float.TryParse (value2, out TPoff)) {
					TPoff = TPoff * 1000f;
				}
			} else if (scaleTP == 2) {
				if (float.TryParse (value2, out TPoff)) {
					TPoff = TPoff * 1000f * 60f;
				}
			} else if (scaleTP == 3) {
				if (float.TryParse (value2, out TPoff)) {
					TPoff = TPoff * 1000f * 60f * 60f;
				}
			}
			xmlValue = "TOF(" + outputValue1 + "," + TPoff.ToString () + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.tp:
			float TPd = 0;
			if (scaleTP == 0) {
				if (float.TryParse (value2, out TPd)) {
				}
			} else if (scaleTP == 1) {
				if (float.TryParse (value2, out TPd)) {
					TPd = TPd * 1000f;
				}
			} else if (scaleTP == 2) {
				if (float.TryParse (value2, out TPd)) {
					TPd = TPd * 1000f * 60f;
				}
			} else if (scaleTP == 3) {
				if (float.TryParse (value2, out TPd)) {
					TPd = TPd * 1000f * 60f * 60f;
				}
			}
			xmlValue = "TPU(" + outputValue1 + "," + TPd.ToString () + "," + outputValue3 + ")";
			break;
		case dragElementLadder._type_elements.none:
			xmlValue = "NOLIN";
			break;
		case dragElementLadder._type_elements.pid:
			xmlValue = "PID(" + outputValue4 + "," + outputValue5 + "," + outputValue6 + "," + outputValue3 + ",," + outputValue1 + "," + outputValue2 + ")";
			break;
		}

		return xmlValue;
	}

	// Update is called once per frame
	void Update () {
		if (typeElement != dragElementLadder._type_elements.none) {
			switch (typeElement) {
			case dragElementLadder._type_elements.open_contact:
				lineInputTitle.text = value1;
				break;
			case dragElementLadder._type_elements.close_contact:
				lineInputNTitle.text = value1;
				break;
			case dragElementLadder._type_elements.open_coil:
				lineOutputTitle.text = value1;
				break;
			case dragElementLadder._type_elements.close_coil:
				lineOutputNTitle.text = value1;
				break;
			case dragElementLadder._type_elements.reset_coil:
			case dragElementLadder._type_elements.set_coil:
				lineOutputSRTitle.text = value1;
				break;
			case dragElementLadder._type_elements.add:
			case dragElementLadder._type_elements.subs:
			case dragElementLadder._type_elements.prod:
			case dragElementLadder._type_elements.division:
			case dragElementLadder._type_elements.equal:
			case dragElementLadder._type_elements.different:
			case dragElementLadder._type_elements.great:
			case dragElementLadder._type_elements.great_equal:
			case dragElementLadder._type_elements.less:
			case dragElementLadder._type_elements.less_equal:
			case dragElementLadder._type_elements.and:
			case dragElementLadder._type_elements.or:
			case dragElementLadder._type_elements.xor:
				lineObjectText2AOff.text = value1;
				lineObjectText3AOff.text = value2;
				break;
			case dragElementLadder._type_elements.not:
			case dragElementLadder._type_elements.mov:
				lineObjectText2AOff.text = value1;
				break;
			case dragElementLadder._type_elements.ctu:
			case dragElementLadder._type_elements.ctd:
				lineObjectText3AOff.text = value2;
				lineObjectBottomTitle.text = value1;
				break;
			case dragElementLadder._type_elements.reset:
				lineOutputResTitle.text = value1;
				break;
			case dragElementLadder._type_elements.ton:
			case dragElementLadder._type_elements.toff:
			case dragElementLadder._type_elements.tp:
				lineObjectBottomTitle.text = value1;
				float numericValue1 = 0;
				if (float.TryParse (value2, out numericValue1)) {
					lineObjectText2AOff.text = numericValue1.ToString("F1");
				} else {
					lineObjectText2AOff.text = "";
				}
				break;
			case dragElementLadder._type_elements.pid:
				lineObjectText2AOff.text = value1;
				lineObjectText3AOff.text = value3;
				break;
			}
		}

		bool leftValue = false;

		if (_situation.GetComponent<LadderMainClass> ().simulationRunning == true && _situation.GetComponent<LadderMainClass> ().debugMode == true) {
			
			if (lineNumber == 0) {
				leftValue = true;
			} else {
				if (branchLevel == 0) {
					leftValue = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber - 1, 0).GetComponent<lineObject> ().actualValue;

					GameObject leftElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber - 1, 1);
					int i = 1;
					while (leftElement != null && leftElement.GetComponent<lineObject>().rightBLine.enabled == true &&  i < 1000) {
						i += 1;
						leftValue = leftValue | leftElement.GetComponent<lineObject> ().actualValue;
						leftElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber - 1, i);
					}
				} else {
					//TODO:
					if (lineNumber == 12) {
						leftValue = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber - 1, 0).GetComponent<lineObject> ().actualValue;
					} else {
						GameObject leftElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber - 1, branchLevel);

						if (leftElement != null) {

							leftValue = leftElement.GetComponent<lineObject> ().actualValue;
							leftElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber - 1, branchLevel + 1);
							//up check
							int i = 1;
							while (leftElement != null && leftElement.GetComponent<lineObject>().rightBLine.enabled == true && i < 1000) {
								i += 1;
								leftValue = leftValue | leftElement.GetComponent<lineObject> ().actualValue;
								leftElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber - 1, branchLevel + i);
							}
						} else {
							//down check
							int i = 0;
							while (leftElement == null && i < 1000) {
								i += 1;
								leftElement = parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (lineNumber - 1, branchLevel - i);
							}
							if (leftElement != null) {
								leftValue = leftElement.GetComponent<lineObject> ().actualValue;
							} else {
								leftValue = false;
							}
						}
					}
				}
			}
				
			if (typeElement != dragElementLadder._type_elements.none) {
				if (allowedType == dragElementLadder._typesIO.Input) {
					XmlDocument debugDocument = new XmlDocument ();
					XmlNode debugNode = debugDocument.CreateNode (XmlNodeType.Element, "entrada", "");
					debugNode.InnerText = getXMLValue ();
					if (_situation.GetComponent<LadderMainClass> ().ladderEngine != null) {
						actualValue = leftValue & _situation.GetComponent<LadderMainClass> ().ladderEngine._processInput (debugNode.ChildNodes);
					} else {
						actualValue = false;
					}	
				} else {
					//output
					actualValue = leftValue;
				}
			} else {
				actualValue = leftValue;
			}
				
		} else {
			actualValue = false;
		}

		//update UI:
		string valueOutputDebug = "";

		if(branchLevel > 0){
			if (rightBranchElement == null) {
				rightBLine.enabled = true;

				if (branchElement != null) {
				
				}

				if ((leftValue == true && actualValue == true) || (branchElement != null && branchElement.GetComponent<lineObject>().rightBLine.color == _situation.GetComponent<LadderMainClass> ().lineActiveColor )) {
					rightBLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				} else {
					rightBLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				}
			} else {
				rightBLine.enabled = false;
			}
			if (leftBranchElement == null) {
				leftBLine.enabled = true;
				if (leftValue == true) {
					leftBLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				} else {
					leftBLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				}
			} else {
				leftBLine.enabled = false;
			}
		}
			
		switch (typeElement) {
		case dragElementLadder._type_elements.none:
			if (leftValue == true) {
				emptyLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				emptyLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			if (actualValue == true) {
				emptyRightLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				emptyRightLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			break;
		case dragElementLadder._type_elements.open_contact:
			if (leftValue == true) {
				lineInputLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineInputVertical1Line.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineInputLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineInputVertical1Line.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			if (leftValue == true && actualValue == true) {
				lineInputRightLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineInputVertical2Line.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineInputRightLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineInputVertical2Line.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			break;
		case dragElementLadder._type_elements.close_contact:
			if (leftValue == true) {
				lineInputNLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineInputNVertical1Line.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineInputNLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineInputNVertical1Line.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			if (leftValue == true && actualValue == true) {
				lineInputNRightLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineInputNVertical2Line.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineInputNCloseLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineInputNRightLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineInputNVertical2Line.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineInputNCloseLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			break;
		case dragElementLadder._type_elements.open_coil:
			if (leftValue == true) {
				lineOutputLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineOutputVertical1Curve.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineOutputLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineOutputVertical1Curve.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			if (leftValue == true && actualValue == true) {
				lineOutputRightLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineOutputVertical2Curve.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineOutputRightLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineOutputVertical2Curve.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			break;
		case dragElementLadder._type_elements.close_coil:
			if (leftValue == true) {
				lineOutputNLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineOutputNVertical1Curve.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineOutputNLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineOutputNVertical1Curve.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			if (leftValue == true && actualValue == true) {
				lineOutputNRightLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineOutputNVertical2Curve.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineOutputNCloseLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineOutputNRightLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineOutputNVertical2Curve.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineOutputNCloseLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			break;
		case dragElementLadder._type_elements.reset_coil:
		case dragElementLadder._type_elements.set_coil:
			if (leftValue == true) {
				lineOutputS_RLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineOutputS_RVertical1Curve.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineOutputS_RLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineOutputS_RVertical1Curve.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			if (leftValue == true && actualValue == true) {
				lineOutputS_RRightLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineOutputS_RVertical2Curve.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineOutputSRText.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineOutputS_RRightLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineOutputS_RVertical2Curve.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineOutputSRText.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			break;
		case dragElementLadder._type_elements.reset:
			if (leftValue == true) {
				lineOutputResLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineOutputResVertical1Curve.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineOutputResLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineOutputResVertical1Curve.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			if (leftValue == true && actualValue == true) {
				lineOutputResRightLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineOutputResVertical2Curve.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineOutputResText.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineOutputResRightLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineOutputResVertical2Curve.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineOutputResText.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			break;
		case dragElementLadder._type_elements.equal:
		case dragElementLadder._type_elements.different:
		case dragElementLadder._type_elements.great:
		case dragElementLadder._type_elements.great_equal:
		case dragElementLadder._type_elements.less:
		case dragElementLadder._type_elements.less_equal:
			if (leftValue == true) {
				lineObjectLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineObjectLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			if (leftValue == true && actualValue == true) {
				lineObjectRightLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineObjectBg.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineObjectRightLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineObjectBg.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			break;
		case dragElementLadder._type_elements.add:
		case dragElementLadder._type_elements.subs:
		case dragElementLadder._type_elements.prod:
		case dragElementLadder._type_elements.division:
		case dragElementLadder._type_elements.and:
		case dragElementLadder._type_elements.or:
		case dragElementLadder._type_elements.xor:
		case dragElementLadder._type_elements.not:
		case dragElementLadder._type_elements.mov:
			if (leftValue == true) {
				lineObjectLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineObjectLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			if (leftValue == true && actualValue == true) {
				lineObjectRightLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineObjectBg.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineObjectRightLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineObjectBg.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}

			if (_situation.GetComponent<LadderMainClass> ().debugMode == true && _situation.GetComponent<LadderMainClass> ().simulationRunning == true) {
				valueOutputDebug = _situation.GetComponent<LadderMainClass> ().ladderEngine.getValuePerName_Uint (value3).ToString ();
				lineObjectBottomTitle.text = valueOutputDebug;	
			} else {
				lineObjectBottomTitle.text = "";
			}
			break;
		case dragElementLadder._type_elements.ctu:
		case dragElementLadder._type_elements.ctd:
		case dragElementLadder._type_elements.ton:
		case dragElementLadder._type_elements.toff:
		case dragElementLadder._type_elements.tp:
			if (leftValue == true) {
				lineObjectLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineObjectLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			if (leftValue == true && actualValue == true) {
				lineObjectRightLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineObjectBg.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineObjectRightLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineObjectBg.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			if (_situation.GetComponent<LadderMainClass> ().debugMode == true && _situation.GetComponent<LadderMainClass> ().simulationRunning == true) {
				valueOutputDebug = _situation.GetComponent<LadderMainClass> ().ladderEngine.getValuePerName_Uint (value3).ToString ();
				lineObjectText2BOff.text = valueOutputDebug;
			} else {
				lineObjectText2BOff.text = "";
			}
			break;
		case dragElementLadder._type_elements.pid:
			if (leftValue == true) {
				lineObjectLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineObjectLeftLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			if (leftValue == true && actualValue == true) {
				lineObjectRightLine.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
				lineObjectBg.color = _situation.GetComponent<LadderMainClass> ().lineActiveColor;
			} else {
				lineObjectRightLine.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
				lineObjectBg.color = _situation.GetComponent<LadderMainClass> ().lineNormalColor;
			}
			if (_situation.GetComponent<LadderMainClass> ().debugMode == true && _situation.GetComponent<LadderMainClass> ().simulationRunning == true) {
				valueOutputDebug = _situation.GetComponent<LadderMainClass> ().ladderEngine.getValuePerName_Uint (value2).ToString();
				lineObjectBottomTitle.text = valueOutputDebug;	
			} else {
				lineObjectBottomTitle.text = "";
			}
			break;
		}

		if (lineObjectText2AOff.text != "") {
			lineObjectText2AOffBg.enabled = true;
		} else {
			lineObjectText2AOffBg.enabled = false;
		}

		if (lineObjectText3AOff.text != "") {
			lineObjectText3AOffBg.enabled = true;
		} else {
			lineObjectText3AOffBg.enabled = false;
		}

		if (lineObjectText2BOff.text != "") {
			lineObjectText2BOffBg.enabled = true;
		} else {
			lineObjectText2BOffBg.enabled = false;
		}

		if (lineObjectText3BOff.text != "") {
			lineObjectText3BOffBg.enabled = true;
		} else {
			lineObjectText3BOffBg.enabled = false;
		}
	}
}
