﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class selectorComponent : MonoBehaviour {

	public Button _leftBt;
	public Button _rightBt;
	public Text _valueText;
    public InputField _inputText;

	public List<string> _data;
	public int _actualIndex = -1;

	public delegate void BtAction();
	public BtAction bt_action = null;

	// Use this for initialization
	void Start () {
		_leftBt.interactable = false;
		_rightBt.interactable = false;
		_valueText.text = "";

		_leftBt.onClick.AddListener (leftAction);
		_rightBt.onClick.AddListener (rightAction);

		_valueText.font = (Font)Resources.Load("Fonts/ArialBold28");

        if (_inputText != null)
        {
            _inputText.text = "";
            _valueText.gameObject.SetActive(false);
            _inputText.onValueChanged.AddListener(ValueChanged);
        }

    }

    private void ValueChanged(string val)
    {
        if (val != "" && val != "-")
        {
            int min = int.Parse(_data[0]);
            int max = int.Parse(_data[_data.Count - 1]);

            if (int.Parse(val) <= min)
            {
                _actualIndex = 0;
                _inputText.text = _data[_actualIndex];

                _leftBt.interactable = false;
                _rightBt.interactable = true;
            } else if (int.Parse(val) >= max)
            {
                _actualIndex = _data.Count - 1;
                _inputText.text = _data[_actualIndex];

                _leftBt.interactable = true;
                _rightBt.interactable = false;
            }
            else
            {
                _leftBt.interactable = true;
                _rightBt.interactable = true;
                _actualIndex = _data.FindIndex(item => item == int.Parse(val).ToString());
                _inputText.text = int.Parse(val).ToString();
            }
        }
        else
        {
            _actualIndex = 0;
            _inputText.text = _data[_actualIndex];

            _leftBt.interactable = false;
            _rightBt.interactable = true;
        }
    }

    public void initializeElement(List<string> _configData){
		_data = _configData;
		if (_data.Count > 0) {
			_actualIndex = 0;
            SetValue(_data[_actualIndex]);
			checkStatusButtons ();

			if (_data.Count == 1) {
				_leftBt.interactable = false;
				_rightBt.interactable = false;
			} else {
				_leftBt.interactable = false;
				_rightBt.interactable = true;
			}

		} else {
			_actualIndex = -1;
			_leftBt.interactable = false;
			_rightBt.interactable = false;
            SetValue("");
        }
		Invoke ("refleshData", 0.2f);
	}

	public void setValue(string _value){
		for(int i = 0; i < _data.Count; i++){
			if(_value == _data[i]){
				_actualIndex = i;
                SetValue(_data[_actualIndex]);
				checkStatusButtons ();
				break;
			}
		}
	}

	public void setValuePerIndex(int _index){
		_actualIndex = _index;
        SetValue(_data[_actualIndex]);
		checkStatusButtons ();
	}

	public string getValue(){
        if (_inputText != null)
        {
            return _inputText.text;
        }
        else
        {
            return _valueText.text;
        }
	}

	public int getIndex(){
		return _actualIndex;
	}

	public void setBtAction(BtAction listener_bt){
		bt_action = listener_bt;
	}

	private void refleshData(){
        SetValue(_data[_actualIndex]);
		checkStatusButtons ();
	}

	private void leftAction(){
		if(_actualIndex > 0){
			_actualIndex--;
            SetValue(_data[_actualIndex]);
			checkStatusButtons ();

			if (_actualIndex == 0) {
				_leftBt.interactable = false;
			} else {
				_leftBt.interactable = true;
			}
		}

		if(bt_action != null){
			bt_action.Invoke ();
		}
	}

	private void rightAction(){
		if(_actualIndex < (_data.Count - 1)){
			_actualIndex++;
            SetValue(_data[_actualIndex]);
			checkStatusButtons();

			if (_actualIndex == (_data.Count - 1)) {
				_rightBt.interactable = false;
			} else {
				_rightBt.interactable = true;
			}
		}

		if(bt_action != null){
			bt_action.Invoke ();
		}
	}

	private void checkStatusButtons(){
		if (_data.Count <= 1) {
			_rightBt.interactable = false;
			_leftBt.interactable = false;
		} else {
			if (_actualIndex == 0) {
				_leftBt.interactable = false;

				if (_actualIndex == (_data.Count - 1)) {
					_rightBt.interactable = false;
				} else {
					_rightBt.interactable = true;
				}
			} else {
				_leftBt.interactable = true;

				if (_actualIndex == (_data.Count - 1)) {
					_rightBt.interactable = false;
				} else {
					_rightBt.interactable = true;
				}
			}	
		}
	}

    private void SetValue(string text)
    {
        if (_inputText != null)
        {
            _inputText.text = text;
        }
        else
        {
            _valueText.text = text;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
