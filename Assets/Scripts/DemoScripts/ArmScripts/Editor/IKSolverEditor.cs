﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(IKSolver)), CanEditMultipleObjects]
public class E_IKSolver : Editor
{
	public override void OnInspectorGUI()
	{
		IKSolver solver = (IKSolver) target;
		if (solver.needResetOption)
		{
			GUI.enabled = false;
		}
		DrawDefaultInspector();
		if (solver.needResetOption)
		{
			GUI.enabled = true;
			if (GUILayout.Button("Reset Scene Hierarchy"))
			{
				solver.ResetHierarchy();
			}
		}
	}
}