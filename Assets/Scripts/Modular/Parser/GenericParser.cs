﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Modular.Parser
{
  public abstract class GenericParser<T> : MonoBehaviour
  {
    [Header("Events")]
    [SerializeField] private UnityEvent _onParseAction;

    public UnityEvent OnParseAction { get => _onParseAction; set => _onParseAction = value; }

    public abstract void Parse(T valueToParse);
  }
}
