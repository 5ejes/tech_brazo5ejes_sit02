﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using static NSBrazo5Ejes.NSExerciseGenerator.PointDirectionEvaluationToken;

namespace NSBrazo5Ejes.NSExerciseGenerator
{

	public class PileExerciseGenerator : AbstractExerciseGenerator
	{
		[SerializeField] private float _radiusCheckSize = 0.05f;
		
		[Tooltip("La distancia debe de ser mayor a 0 y menor al radio especificado, de otro modo, será ignorada")]
		[SerializeField]private float _maxDistanceCheck;

		[Header("Events")]
		[Tooltip("Al generar la rutina de ejercicio, su resultado se comunica a través de este evento")]
		[SerializeField] private PileExerciseEvent _onExerciseGenerated;

		/// <summary>
		/// Genera una rutina de ejercicio
		/// </summary>
		public override void GenerateRoutine()
		{

			// Inicializo posiciones de los cubos
			#region Posiciones Fijas (comentado)

			// // Defino las tres posiciones de testing fijas
			// 	var generatedPositions = new Vector3[] {
			// 	new Vector3(0.1f,0,0.5f),
			// 	new Vector3(0.3f,0,0.35f),
			// 	new Vector3(0.6f,0,0),
			// 	new Vector3(0.6f,0.0f, 0.6f)	// La ultima posición es para el objetivo de apilamiento de la routine
			// };

			#region Test de pila
			// 	var generatedPositions = new Vector3[] {
			// 		new Vector3(0.6f, 0, 0.6f),
			// 		new Vector3(0.6f, 0.1f, 0.6f),
			// 		new Vector3(0.6f, 0.2f, 0.6f),
			// 		new Vector3(0.6f, 0, 0.6f)
			// };
			#endregion

			#endregion

			// Genero las 3 posiciones aleatorias, más una posición que usaremos como objetivo
			var generatedPositions = GenerateCubePositions(GeneratedPositionCount + 1);

			List<PointDirectionEvaluationToken> _evaluatorTokens = new List<PointDirectionEvaluationToken>();

			// Obtengo la cuenta de posiciones
			int count = generatedPositions.Length;

			// Si la cuenta menos la posición del evaluador supera el límite
			if (count - 1 > 5)
			{
				// Lanzo un error
				Debug.LogError($"La cuenta no puede ser mayor a 5 por limitaciones actuales : {gameObject}");
				return;
			}


			// Proceso la posición de los evaluadores

			// Obtengo la posición de evaluación
			var evaluatorPosition = generatedPositions[count - 1];

			// Defino el valor del radio para los comprobantes
			var radius = _radiusCheckSize;

			// Por cada posición generada excepto la última
			for (int i = 0; i < count - 1; i++)
			{


				// Genero la posición del token
				Vector3 currentTokenPosition = evaluatorPosition;

				// Modifico la altura de la posición para crear una comprobación de pila
				currentTokenPosition.y = (0.1f * i);

				// Incremento la posición del token para incluir el radio
				currentTokenPosition.y += radius;

				Vector3 globalPosition;

				// Si los override están definidos
				if (_minLimit && _maxLimit)
				{
					// Convierto la posición con los límites definidos
					globalPosition = ArmPositionParser.ParseVector(currentTokenPosition, _minLimit, _maxLimit);
				}
				else
				{
					// Convierto la posición global
					globalPosition = ArmPositionParser.ParseVector(currentTokenPosition);
				}

				// Genero un token evaluador
				PointDirectionEvaluationToken token = new PointDirectionEvaluationToken(globalPosition, Vector3.up, EvaluationAxis.Up, AxisOption.Both);

				// Escribo la información del token				
				token.TargetEnumID = TargetEnumId;
				token.Radius = radius - (radius * 0.15f);
				token.MaxDistance = _maxDistanceCheck;
				token.CheckLayerMask = TokenCheckLayer;

				// Lo agrego a la lista de tokens
				_evaluatorTokens.Add(token);
			}

			// Creo la rutina de ejecucíon
			PileExerciseRoutine routine = new PileExerciseRoutine(_evaluatorTokens.ToArray());

			// Obtengo todas las posiciones excepto la última
			var extractedPositions = new List<Vector3>();

			// Recorro las posiciones generadas, excepto la última
			for (int i = 0; i < count - 1; i++)
			{
				// Obtengo la posición actual
				var currentPosition = generatedPositions[i];

				// Anexo la posición a la lista de posiciones
				extractedPositions.Add(currentPosition);
			}

			// Anexo las posiciones extraidas
			routine.Positions.AddRange(extractedPositions.ToArray());

			// Comunicar la rutina a través del evento predefinido
			_onExerciseGenerated.WrapInvoke(routine);
		}
	}
}