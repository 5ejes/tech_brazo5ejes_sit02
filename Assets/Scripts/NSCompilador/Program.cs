﻿using System.Collections;
using System.Collections.Generic;
using NSCompilador;
using NSEditorAcciones;
using UnityEngine;

namespace NSCompilador
{

	public class Program
	{

		private int _executionIndex;
		private ExecutionSequence _executionSequence;

		private LogicBlock _lastLogicBlock;

		public Program(ExecutionSequence argSequence)
		{
			// Defino la secuencia del programa
			_executionSequence = argSequence;
			_executionIndex = 0;
		}

		private void SetIgnoreFlagGroup(bool argFlagValue, params LogicBlock[] argBlocks)
		{
			// Recorro cada parametro de bloque
			foreach (var block in argBlocks)
			{
				// Defino el valor a cada bloque
				block.IgnoreExecution = argFlagValue;

				// Si es un valor con bloques logicos anidados, ejecuto la misma orden sobre ellos
				var _ifStatement = block as IfStatement;

				// Si es un if statement
				if (_ifStatement != null)
				{
					// Llamo la misma función sobre sus valores
					SetIgnoreFlagGroup(argFlagValue, _ifStatement.IfExecutionBlocks);

					// Si es un bloque If else
					var _ifElseStatement = block as IfElseStatement;

					// Si es un if Else statement
					if (_ifElseStatement != null)
					{
						// Llamo la misma función sobre sus valores
						SetIgnoreFlagGroup(argFlagValue, _ifElseStatement.ElseExecutionBlocks);
					}
				}

			}
		}

		public void RunFromStart()
		{
			// Reinicion el índice de ejecución
			_executionIndex = 0;

			int attempt = 0;
			int maxAttempts = 100;

			// Reinicio el ignoreExecution de la sequencia
			SetIgnoreFlagGroup(false, _executionSequence.Sequence);

			while (_executionIndex < _executionSequence.Sequence.Length && attempt < maxAttempts)
			{
				// Ejecuto el índice actual
				RunSingleAt(_executionIndex);

				// Incremento los intentos
				attempt++;
			}

			// Si supera los intentos límites
			if (attempt >= maxAttempts) Debug.LogWarning("Max Attempts reached on execution!");
		}

		public void RunSingleAt(int argIndex)
		{
			if (_executionSequence == null) throw new System.Exception("La secuencia es nula, no existe");
			if (argIndex < 0 || argIndex >= _executionSequence.Sequence.Length) throw new System.Exception("El índice esta fuera de los límites");

			// Obtengo el objeto índice desde la secuencia
			var currentLogicBlock = _executionSequence.Sequence[argIndex];

			// Proceso el bloque
			ProcessBlock(currentLogicBlock);
		}

		public void ProcessBlock(LogicBlock argBlock)
		{

			// Si ignoro la ejecución del bloque, retorno
			if (argBlock.IgnoreExecution)
			{
				// Aumento el indice de ejecución
				_executionIndex++;
				return;
			}

			// Defino el valor para el skip de la acción actual
			int desiredIndex = 0;

			switch (argBlock)
			{
				case IfStatement _ifStatement:

					// casteo a IfElse
					var _ifElseStatement = _ifStatement as IfElseStatement;

					// Si la condición es verdadera
					if (_ifStatement.EvaluateCondition())
					{
						Debug.Log($"{_ifStatement.ExecutionOrder} : Condición aceptada!");
						// El indice deseado es el siguiente
						desiredIndex = _executionIndex + 1;

						if (_ifElseStatement != null)
						{
							// Ignoro los bloques else
							SetIgnoreFlagGroup(true, _ifElseStatement.ElseExecutionBlocks);
						}
					}
					else
					{
						Debug.Log($"{_ifStatement.ExecutionOrder} : Condición rechazada!");
						// Si no es verdadera
						// El indice deseado se salta el if
						desiredIndex = _executionIndex + 1;

						// Ignoro los bloques if
						SetIgnoreFlagGroup(true, _ifStatement.IfExecutionBlocks);
					}

					break;
				case GripOperation _gripOperation:
					// TODO: Enviar datos a la máquina

					string gripState = _gripOperation.Closed ? "Cerrada" : "Abierta";
					Debug.Log($"{_gripOperation.ExecutionOrder} : Grip : {gripState}");

					// Indice deseado
					desiredIndex = _executionIndex + 1;
					break;
				case MoveOperation _moveOperation:
					// TODO: Enviar datos a la máquina

					Debug.Log($"{_moveOperation.ExecutionOrder} : Move Target : {_moveOperation.Target}");

					// Indice deseado
					desiredIndex = _executionIndex + 1;
					break;
				default:
					Debug.Log($"Bloque no configurado : {argBlock.BlockID}");
					break;
			}

			// Defino el ultimo bloque ejecutado como el actual
			_lastLogicBlock = argBlock;

			// Defino el índice deseado para el orden de ejecución
			_executionIndex = desiredIndex;
		}
	}
}