﻿using NSBoxMessage;
using NSTraduccionIdiomas;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace NSBoxMessage.Inspector
{
	public class BoxDecisionSingleInspector : MonoBehaviour
	{

		[Header("Captions")]
		[SerializeField] private string _decisionMessage = "textENtrarenEvaluacion";
		[SerializeField] private string _cancelarButtonLabel = "TextCancelar";
		[SerializeField] private string _aceptarButtonLabel = "TextAceptarMayusculas";

		[SerializeField] private bool _useCaptionTextIfNoTranslationFound = false;

		[Header("Events")]
		[SerializeField] private UnityEvent _onEntrarDecision;
		[SerializeField] private UnityEvent _onCancelDecision;

		private DiccionarioIdiomas _diccionario;
		public DiccionarioIdiomas Diccionario
		{
			get
			{
				if (_diccionario == null) _diccionario = DiccionarioIdiomas.Instance;
				return _diccionario;
			}
		}

		private BoxMessageManager _messageManager;
		public BoxMessageManager MsgManager
		{
			get
			{
				if (_messageManager == null) _messageManager = BoxMessageManager.Instance;
				return _messageManager;
			}
		}




		private string Translate(string captionID)
		{
			if (string.IsNullOrEmpty(captionID)) return string.Empty;

			string output = Diccionario.Traducir(captionID);

			if (_useCaptionTextIfNoTranslationFound)
			{
				if (output.Contains("sin traducción") || output.Contains("sin traduccion"))
				{
					return captionID;
				}
			}

			return output;
		}


		public void ActivarEvaluacion()
		{
			// Creo un box decisión manager con los parámetros dados
			MsgManager.CreateBoxMessageDecision(
				argTextMessage: Translate(_decisionMessage),
				argTextButtonCancel: Translate(_cancelarButtonLabel),
				argTextButtonAccept: Translate(_aceptarButtonLabel),
				argFunctionExeAccept: () => { _onEntrarDecision.WrapInvoke(); },
				argFunctionExeCancel: () => { _onCancelDecision.WrapInvoke(); }
			);
		}

	}
}
