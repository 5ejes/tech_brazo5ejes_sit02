﻿using NSContextosLocalesBrazo;
using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	[CreateAssetMenu(fileName = "Event ArmLocalContext", menuName = "NSScriptableEvent/ArmLocalContext", order = 0)]
	public class ScriptableEventArmLocalContext : AbstractScriptableEvent<ArmLocalContext>
	{
		[System.Serializable]
		public class ArmLocalContextEvent : UnityEvent<ArmLocalContext> { }
	}
}