﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Modular
{
	public class CopyPosition : MonoBehaviour, IManualUpdate
	{
		[Header("Settings")]
		[SerializeField] private Transform _target;
		[SerializeField] private Vector3 _offset;
		[SerializeField] private bool _copyX = true;
		[SerializeField] private bool _copyY = true;
		[SerializeField] private bool _copyZ = true;
		[Space]
		[SerializeField] private int _updateEach = 0;
		[SerializeField] private bool _runOnUpdate = true;

		public bool hasRigidbody => _rigidbody != null;

		public bool RunOnUpdate { get => _runOnUpdate; set => _runOnUpdate = value; }
		public bool AllowUpdate { get => enabled; set => enabled = value; }

		private Rigidbody _rigidbody = null;

		private void Awake()
		{
			// Obtengo el rigidbody
			_rigidbody = GetComponent<Rigidbody>();
		}

		private void OnEnable()
		{
			StartCoroutine(EndOfFrameUpdate());
			StartCoroutine(FixedUpdateRoutine());
		}

		private IEnumerator EndOfFrameUpdate()
		{
			while (true)
			{
				while (RunOnUpdate)
				{
					yield return null;

					// Si tiene un rigidbody, no haga ninguna operación
					if (hasRigidbody) yield break;

					// Si el target es nulo, aborto la operación
					if (_target == null)
					{
						Debug.LogError($"The target is null, can't continue {gameObject.name}");
						yield break;
					}

					yield return new WaitForEndOfFrame();

					CallUpdate();
				}
				yield return null;
			}
		}

		private IEnumerator FixedUpdateRoutine()
		{
			while (true)
			{
				while (RunOnUpdate)
				{
					yield return null;

					// Si tiene un RigidBody
					if (hasRigidbody)
					{

						// Si el target es nulo, aborto la operación
						if (_target == null)
						{
							Debug.LogError($"The target is null, can't continue {gameObject.name}");
							yield break;
						}

						yield return new WaitForFixedUpdate();

						CallUpdate();
					}
				}
				yield return null;
			}
		}

		private static void TrackPosition(Transform self, Transform target, Vector3 offset = new Vector3(), bool copyX = true, bool copyY = true, bool copyZ = true)
		{
			Vector3 finalPosition;

			// Efectuo la copia de las posiciones según el parametro de Copy
			finalPosition.x = copyX ? target.position.x : self.position.x;
			finalPosition.y = copyY ? target.position.y : self.position.y;
			finalPosition.z = copyZ ? target.position.z : self.position.z;

			// Hago el tracking de la posición
			self.position = finalPosition;
			self.position += offset;
		}

		private static void TrackPosition(Rigidbody self, Transform target, Vector3 offset = new Vector3(), bool copyX = true, bool copyY = true, bool copyZ = true)
		{

			Vector3 finalPosition;

			// Efectuo la copia de las posiciones según el parametro de Copy
			finalPosition.x = copyX ? target.position.x : self.position.x;
			finalPosition.y = copyY ? target.position.y : self.position.y;
			finalPosition.z = copyZ ? target.position.z : self.position.z;

			// Hago el tracking de la posición
			self.MovePosition(finalPosition + offset);
		}

		public void CallUpdate()
		{
			if (!hasRigidbody)
			{
				// Si updateEach tiene un valor no válido
				if (_updateEach <= 0)
				{
					// Cada frame
					TrackPosition(transform, _target, _offset, _copyX, _copyY, _copyZ);
				}
				else if (Time.frameCount % _updateEach == 0) // Si llego a un multiplo del frame definido
				{
					// Cada n frame
					TrackPosition(transform, _target, _offset, _copyX, _copyY, _copyZ);
				}
			}
			else
			{
				// Si updateEach tiene un valor no válido
				if (_updateEach <= 0)
				{
					// Cada frame
					TrackPosition(_rigidbody, _target, _offset, _copyX, _copyY, _copyZ);
				}
				else if (Time.frameCount % _updateEach == 0) // Si llego a un multiplo del frame definido
				{
					// Cada n frame
					TrackPosition(_rigidbody, _target, _offset, _copyX, _copyY, _copyZ);
				}
			}
		}
	}
}