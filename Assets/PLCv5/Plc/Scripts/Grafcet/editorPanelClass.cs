﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class editorPanelClass : MonoBehaviour {

	public GameObject _situation;

	public string _editorType;
	public string _editorParameter;
	public GameObject _actualElement;

	public Text _title;
	public Text _nameLabel;
	public InputField _nameInput;

	public Button _xBt;
	public Button _saveBt;

	public Button _bt1;
	public Button _bt2;
	public Button _bt3;
	public Button _bt4;
	public Button _bt5;
	public Button _bt6;

    public Button _btRob1;
    public Button _btRob2;
    public Button _btRob3;
    public Button _btRob4;
    public Button _btRob5;
    public Button _btRob6;
    public Button _btRob7;
    public Button _btRob8;
    public Button _btRob9;
    public Button _btRob10;
    public Button _btRob11;
    public Button _btRob12;
    public Button _btRob13;

    public GameObject _nameArea;
	public GameObject _buttonsArea;
    public GameObject _buttonsRoboticArea;

	public GameObject _area1Action;
	public GameObject _area2Action;
	public GameObject _area3Action;
	public GameObject _area4Action;
	public GameObject _area5Action;
	public GameObject _area6Action;

    public GameObject _area1ActionRobotics;
    public GameObject _area2ActionRobotics;
    public GameObject _area3ActionRobotics;
    public GameObject _area4ActionRobotics;
    public GameObject _area5ActionRobotics;
    public GameObject _area6ActionRobotics;

	public GameObject _areaOperation;
	public GameObject _area1Condition;
	public GameObject _area2Condition;
	public GameObject _area3Condition;

    public GameObject _areaOperationRobotics;
    public GameObject _area1ConditionRobotics;
    public GameObject _area2ConditionRobotics;
    public GameObject _area3ConditionRobotics;
    public GameObject _area4ConditionRobotics;
    public GameObject _area5ConditionRobotics;
    public GameObject _area6ConditionRobotics;

	public GameObject _areaDivergence;

    public GameObject CoordMenu;
    public Button coordMenuBgBt;

	[Header("AREA 1 - ACTION PANEL")]
	public Toggle _invArea1Action;
	public selectorComponent _selectorArea1Action; 
	[Space(10)]
	public Toggle _invArea2Action;
	public selectorComponent _selector1Area2Action;
	public Text _labelSelector2Area2Action; 
	public selectorComponent _selector2Area2Action;
	[Space(10)]
	public Toggle _resetArea3Action;
	public selectorComponent _selector1Area3Action;
	public Text _labelSelector2Area3Action;
	public selectorComponent _selector2Area3Action;
	public Text _labelTP3Area3Action;
	public InputField tpArea3Action;
	public Text _labelSelector3Area3Action;
	public selectorComponent _selector3Area3Action;
	[Space(10)]
	public Toggle _resetArea4Action;
	public selectorComponent _selector1Area4Action;
	public Text _labelSelector2Area4Action;
	public selectorComponent _selector2Area4Action;
	public Text _labelPV3Area4Action;
	public InputField pvArea4Action;
	public Text _labelSelector3Area4Action;
	public selectorComponent _selector3Area4Action;
	[Space(10)]
	public selectorComponent _selector1Area5Action;
	public Text _labelSelector2Area5Action;
	public selectorComponent _selector2Area5Action;
	public InputField _input2Area5Action;
	public Toggle _input2SelectorArea5Action;
	public Text _labelSelector3Area5Action;
	public selectorComponent _selector3Area5Action;
	public InputField _input3Area5Action;
	public Toggle _input3SelectorArea5Action;
	public Text _labelSelector4Area5Action;
	public selectorComponent _selector4Area5Action;
	[Space(10)]
	public selectorComponent _selector1Area6Action;
	public Text _labelSelector2Area6Action;
	public selectorComponent _selector2Area6Action;
	public InputField _input2Area6Action;
	public Toggle _input2SelectorArea6Action;
	public Text _labelSelector3Area6Action;
	public selectorComponent _selector3Area6Action;
	public InputField _input3Area6Action;
	public Toggle _input3SelectorArea6Action;
	public Text _labelSelector4Area6Action;
	public selectorComponent _selector4Area6Action;

	[Space(10)]
	public Text _labelSelector1Area1Transition;
	public selectorComponent _selector1Area1Operation;
	[Space(10)]
	public Toggle _invArea1Transition;
	public selectorComponent _selectorArea1Transition; 
	[Space(10)]
	public Toggle _invArea2Transition;
	public selectorComponent _selector1Area2Transition;
	public InputField _input1Area2Transition;
	public Toggle _input1SelectorArea2Transition;
	public selectorComponent _selector2Area2Transition;
	public selectorComponent _selector3Area2Transition;
	public InputField _input3Area2Transition;
	public Toggle _input3SelectorArea2Transition;
	[Space(10)]
	public Toggle _invArea3Transition;
	public selectorComponent _selector1Area3Transition;
	public Text _labelSelector2Area3Transition;
	public selectorComponent _selector2Area3Transition;
	[Space(10)]
	public Text _labelSelector1AreaDivergence;
	public selectorComponent _selector1AreaDivergence;
	public Text _labelSelector2AreaDivergence;
	public selectorComponent _selector2AreaDivergence;

    [Header("ROBOTIC - ACTION PANEL")]
    public Text _labelSelector1Area1ActionRob;
    public Text _labelSelector2Area1ActionRob;
    public selectorComponent _selector1Area1ActionRob;
    public selectorComponent _selector2Area1ActionRob;
    [Space(10)]
    public Toggle _invArea2ActionRob;
    public selectorComponent _selector1Area2ActionRob;
    public Text _labelSelector2Area2ActionRob;
    public Text _labelSelector3Area2ActionRob;
    public Text _labelSelector4Area2ActionRob;
    public selectorComponent _selector2Area2ActionRob;
    public InputField _input3Area2ActionRob;
    public selectorComponent _selector4Area2ActionRob;
    [Space(10)]
    public Toggle _invArea3ActionRob;
    public selectorComponent _selector1Area3ActionRob;
    public Text _labelSelector2Area3ActionRob;
    public Text _labelSelector3Area3ActionRob;
    public Text _labelSelector4Area3ActionRob;
    public selectorComponent _selector2Area3ActionRob;
    public InputField _input3Area3ActionRob;
    public selectorComponent _selector4Area3ActionRob;
    [Space(10)]
    public Text _labelSelector1Area4ActionRob;
    public selectorComponent _selector1Area4ActionRob;
    [Space(10)]
    public Text _labelSelector1Area5ActionRob;
    public Text _labelSelector2Area5ActionRob;
    public Text _labelSelector3Area5ActionRob;
    public selectorComponent _selector1Area5ActionRob;
    public selectorComponent _selector2Area5ActionRob;
    public selectorComponent _selector3Area5ActionRob;
    [Space(10)]
    public Text _labelSelector1Area6ActionRob;
    public Text _labelSelector2Area6ActionRob;
    public selectorComponent _selector1Area6ActionRob;
    public selectorComponent _selector2Area6ActionRob;
    [Space(10)]
    public Button CoordMenuCartBt;
    public Button CoordMenuPolBt;

    [Header("ROBOTIC - TRANSITION PANEL")]
    public Text operationRoboticLabel;
    public selectorComponent operationRoboticSelector;
    [Space(10)]
    public Toggle transitionRoboticArea1Toggle;
    public selectorComponent transitionRoboticArea1Selector;
    [Space(10)]
    public Toggle transitionRoboticArea2Toggle;
    public InputField transitionRoboticArea2Constant1;
    public selectorComponent transitionRoboticArea2Selector1;
    public Toggle transitionRoboticArea2ToggleConstant1;
    public selectorComponent transitionRoboticArea2Selector2;
    public InputField transitionRoboticArea2Constant3;
    public selectorComponent transitionRoboticArea2Selector3;
    public Toggle transitionRoboticArea2ToggleConstant3;
    [Space(10)]
    public Text transitionRoboticArea3Label1;
    public Text transitionRoboticArea3Label2;
    public Text transitionRoboticArea3Label3;
    public Button transitionRoboticArea3ButtonBit3;
    public Button transitionRoboticArea3ButtonBit2;
    public Button transitionRoboticArea3ButtonBit1;
    public Button transitionRoboticArea3ButtonBit0;
    public InputField transitionRoboticArea3Constant1;
    public selectorComponent transitionRoboticArea3Selector1;
    [Space(10)]
    public Image transitionRoboticArea4ActiveArea;
    public Button transitionRoboticArea4Color1;
    public Button transitionRoboticArea4Color2;
    public Button transitionRoboticArea4Color3;
    public Button transitionRoboticArea4Color4;
    public Button transitionRoboticArea4Color5;
    public Button transitionRoboticArea4Color6;
    public Button transitionRoboticArea4Color7;
    public Button transitionRoboticArea4Color8;
    [Space(10)]
    public Text transitionRoboticArea5Label1;
    public Text transitionRoboticArea5Label2;
    public selectorComponent transitionRoboticArea5Selector1;
    public selectorComponent transitionRoboticArea5Selector2;
    [Space(10)]
    public Text transitionRoboticArea6Label1;
    public selectorComponent transitionRoboticArea6Selector1;

    private int colorIndexSelected = 0;

	private int buttonActive = 1;

	private int scaleTPArea3 = 0;
    private int coordRob = 1; //1: cartesians, 2: polars

	// Use this for initialization
	void Start () {
		_xBt.onClick.AddListener (exitAction);
		_saveBt.onClick.AddListener (saveAction);

		_bt1.onClick.AddListener (bt1Action);
		_bt2.onClick.AddListener (bt2Action);
		_bt3.onClick.AddListener (bt3Action);
		_bt4.onClick.AddListener (bt4Action);
		_bt5.onClick.AddListener (bt5Action);
		_bt6.onClick.AddListener (bt6Action);

        _btRob1.onClick.AddListener(btRob1Action);
        _btRob2.onClick.AddListener(btRob2Action);
        _btRob3.onClick.AddListener(btRob3Action);
        _btRob4.onClick.AddListener(btRob4Action);
        _btRob5.onClick.AddListener(btRob5Action);
        _btRob6.onClick.AddListener(btRob6Action);
        _btRob7.onClick.AddListener(btRob7Action);
        _btRob8.onClick.AddListener(btRob8Action);
        _btRob9.onClick.AddListener(btRob9Action);
        _btRob10.onClick.AddListener(btRob10Action);

        _btRob11.onClick.AddListener(btRob11Action);
        _btRob12.onClick.AddListener(btRob12Action);
        _btRob13.onClick.AddListener(btRob13Action);

        _btRob1.GetComponentInChildren<Text>().fontSize = 25;
        _btRob2.GetComponentInChildren<Text>().fontSize = 25;
        _btRob3.GetComponentInChildren<Text>().fontSize = 25;
        _btRob4.GetComponentInChildren<Text>().fontSize = 25;
        _btRob5.GetComponentInChildren<Text>().fontSize = 25;
        _btRob6.GetComponentInChildren<Text>().fontSize = 25;
        _btRob7.GetComponentInChildren<Text>().fontSize = 25;
        _btRob8.GetComponentInChildren<Text>().fontSize = 25;
        _btRob9.GetComponentInChildren<Text>().fontSize = 25;
        _btRob10.GetComponentInChildren<Text>().fontSize = 25;
        _btRob11.GetComponentInChildren<Text>().fontSize = 25;
        _btRob12.GetComponentInChildren<Text>().fontSize = 25;
        _btRob13.GetComponentInChildren<Text>().fontSize = 25;

        CoordMenuCartBt.onClick.AddListener(btRob8RectAction);
        CoordMenuPolBt.onClick.AddListener(btRob8PolAction);

		_title.font = (Font)Resources.Load ("Fonts/ArialBold28");
		_saveBt.GetComponentInChildren<Text> ().font = (Font)Resources.Load ("Fonts/ArialBold28");

		tpArea3Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		pvArea4Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_input2Area5Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_input3Area5Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_input2Area6Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_input3Area6Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");

		_invArea1Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_invArea2Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_resetArea3Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_resetArea4Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_input2SelectorArea5Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_input3SelectorArea5Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_input2SelectorArea6Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_input3SelectorArea6Action.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");

		_invArea1Transition.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_invArea2Transition.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_invArea3Transition.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_input1SelectorArea2Transition.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_input3SelectorArea2Transition.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");

        transitionRoboticArea1Toggle.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        transitionRoboticArea2Toggle.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        transitionRoboticArea2ToggleConstant1.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        transitionRoboticArea2ToggleConstant3.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        transitionRoboticArea2Constant1.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        transitionRoboticArea2Constant3.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        transitionRoboticArea3Constant1.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");


		_resetArea3Action.onValueChanged.AddListener (changeResetArea3Action);
		_resetArea4Action.onValueChanged.AddListener (changeResetArea4Action);

        _invArea2ActionRob.onValueChanged.AddListener(changeResetArea2ActionRob);
        _invArea3ActionRob.onValueChanged.AddListener(changeResetArea3ActionRob);

		_input2SelectorArea5Action.onValueChanged.AddListener (changeConstantValue2Area5Action);
		_input3SelectorArea5Action.onValueChanged.AddListener (changeConstantValue3Area5Action);

		_input2SelectorArea6Action.onValueChanged.AddListener (changeConstantValue2Area6Action);
		_input3SelectorArea6Action.onValueChanged.AddListener (changeConstantValue3Area6Action);

		_input1SelectorArea2Transition.onValueChanged.AddListener (changeConstantValue1Area2Transition);
		_input3SelectorArea2Transition.onValueChanged.AddListener (changeConstantValue3Area2Transition);

        transitionRoboticArea2ToggleConstant1.onValueChanged.AddListener(changeConstantValue1Area2TransitionRobotic);
        transitionRoboticArea2ToggleConstant3.onValueChanged.AddListener(changeConstantValue3Area2TransitionRobotic);

		tpArea3Action.onValueChanged.AddListener (changeTextArea3Action);
        _input3Area2ActionRob.onValueChanged.AddListener(changeTextArea2ActionRob);

		_selector1Area6Action.setBtAction(checkLogicAction);

		_input2Area5Action.text = "0";
		_input3Area5Action.text = "0";
		_input2Area6Action.text = "0";
		_input3Area6Action.text = "0";

        transitionRoboticArea3Constant1.onValidateInput += ValidateArea3ConstantAction;

        transitionRoboticArea3Selector1.setBtAction(UpdateArea3RobValuesAction);
        transitionRoboticArea3Constant1.onValueChanged.AddListener(UpdateArea3RobFromConstantAction);
        transitionRoboticArea3ButtonBit0.onClick.AddListener(UpdateArea3RobFromBit0Action);
        transitionRoboticArea3ButtonBit1.onClick.AddListener(UpdateArea3RobFromBit1Action);
        transitionRoboticArea3ButtonBit2.onClick.AddListener(UpdateArea3RobFromBit2Action);
        transitionRoboticArea3ButtonBit3.onClick.AddListener(UpdateArea3RobFromBit3Action);

        transitionRoboticArea5Selector1.setBtAction(UpdateArea5Sel1RobValuesAction);
        transitionRoboticArea5Selector2.setBtAction(UpdateArea5Sel2RobValuesAction);

        transitionRoboticArea4Color1.onClick.AddListener(RoboticSelectColor1Action);
        transitionRoboticArea4Color2.onClick.AddListener(RoboticSelectColor2Action);
        transitionRoboticArea4Color3.onClick.AddListener(RoboticSelectColor3Action);
        transitionRoboticArea4Color4.onClick.AddListener(RoboticSelectColor4Action);
        transitionRoboticArea4Color5.onClick.AddListener(RoboticSelectColor5Action);
        transitionRoboticArea4Color6.onClick.AddListener(RoboticSelectColor6Action);
        transitionRoboticArea4Color7.onClick.AddListener(RoboticSelectColor7Action);
        transitionRoboticArea4Color8.onClick.AddListener(RoboticSelectColor8Action);

        coordMenuBgBt.onClick.AddListener(HideMenuBg);


    }

	public void configureEditor(GameObject _element, string _type, string _parameter = ""){
		_editorType = _type;
		_editorParameter = _parameter;
		buttonActive = 1;
		_actualElement = _element;

		_nameInput.text = "";

		_title.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='editor_title']").InnerText;
		_nameLabel.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='name']").InnerText + ":";
		_saveBt.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='save']").InnerText;

		_bt1.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_bt2.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_bt3.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_bt4.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_bt5.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
		_bt6.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");

        _btRob1.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        _btRob2.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        _btRob3.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        _btRob4.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        _btRob5.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        _btRob6.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        _btRob7.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        _btRob8.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        _btRob9.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        _btRob10.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");

        _btRob11.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        _btRob12.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");
        _btRob13.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold28");

        switch (_editorType){
		case "action":

			_nameArea.SetActive (true);

            if(Manager.Instance.globalRoboticsMode == false){
                _buttonsArea.SetActive(true);
                _buttonsRoboticArea.SetActive(false);
                
                _area1Condition.SetActive(false);
                _area2Condition.SetActive(false);
                _area3Condition.SetActive(false);

                _areaOperation.SetActive(false);
                _areaDivergence.SetActive(false);

                _bt1.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='outputs']").InnerText;
                _bt2.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='memory']").InnerText;
                _bt3.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='timers']").InnerText;
                _bt4.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='counters']").InnerText;
                _bt5.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='aritmetic_operations']").InnerText;
                _bt6.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='logic_operations']").InnerText;

                if (_actualElement.GetComponent<ActionClass>()._type != 0)
                {
                    buttonActive = _actualElement.GetComponent<ActionClass>()._type;
                }

                setTabAction(buttonActive);
                configureActionTab(buttonActive);
                setInitialActionValues();   
            } else {
                _buttonsRoboticArea.SetActive(true);
                _buttonsRoboticArea.GetComponent<RectTransform>().anchoredPosition = new Vector2(11, 104);
                _btRob10.gameObject.SetActive(false);
                _btRob11.gameObject.SetActive(true);
                _btRob12.gameObject.SetActive(true);
                _btRob13.gameObject.SetActive(false);

                _area1Condition.SetActive(false);
                _area2Condition.SetActive(false);
                _area3Condition.SetActive(false);

                _area1ConditionRobotics.SetActive(false);
                _area2ConditionRobotics.SetActive(false);
                _area3ConditionRobotics.SetActive(false);
                _area4ConditionRobotics.SetActive(false);
                _area5ConditionRobotics.SetActive(false);
                _area6ConditionRobotics.SetActive(false);

                _areaOperationRobotics.SetActive(false);

                _areaOperation.SetActive(false);
                _areaDivergence.SetActive(false);

                _btRob1.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_motor_a']").InnerText;
                _btRob2.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_motor_c']").InnerText;
                _btRob3.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='timers']").InnerText;
                _btRob4.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_motor_b']").InnerText;
                _btRob5.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_motor_d']").InnerText;
                _btRob6.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='counters']").InnerText;
                _btRob7.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_motor_band']").InnerText;
                _btRob8.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_coord']").InnerText;
                _btRob9.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_gripper']").InnerText;
                _btRob10.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_start']").InnerText;

                _btRob11.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_bastidor_z']").InnerText;
                _btRob12.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_eje_5']").InnerText;

                CoordMenuCartBt.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_cart']").InnerText;
                CoordMenuPolBt.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_pol']").InnerText;

                if (_actualElement.GetComponent<ActionClass>()._type != 0)
                {
                    switch(_actualElement.GetComponent<ActionClass>()._type){
                            case 1:
                                switch(_actualElement.GetComponent<ActionClass>()._typeRobotic){
                                    case 1:
                                        buttonActive = 10;
                                        break;
                                    case 2:
                                        buttonActive = 7;
                                        break;
                                    case 3:
                                        buttonActive = 11;
                                        break;
                                    case 4:
                                        buttonActive = 12;
                                        break;
                                }
                                break;
                            case 2:
                                switch (_actualElement.GetComponent<ActionClass>()._typeRobotic)
                                {
                                    case 3:
                                        buttonActive = 1;
                                        break;
                                    case 4:
                                        buttonActive = 4;
                                        break;
                                    case 5:
                                        buttonActive = 2;
                                        break;
                                    case 6:
                                        buttonActive = 5;
                                        break;
                                    case 7:
                                        buttonActive = 8;
                                        break;
                                    case 8:
                                        buttonActive = 9;
                                        break;
                                }
                                break;
                            case 3:
                                buttonActive = 3;
                                break;
                            case 4:
                                buttonActive = 6;
                                break;
                            default:
                                buttonActive = 0;
                                break;
                    }
                }

                setTabActionRobotics();
                configureActionRoboticTab();
                setInitialActionRoboticsValues();
            }

			break;
		case "transition":
            colorIndexSelected = 0;
			switch(_editorParameter){
			case "condition":
                if (Manager.Instance.globalRoboticsMode == false)
                { 
                    _nameArea.SetActive(false);
                    _buttonsArea.SetActive(true);
                    _buttonsRoboticArea.SetActive(false);

                    _area1Action.SetActive(false);
                    _area2Action.SetActive(false);
                    _area3Action.SetActive(false);
                    _area4Action.SetActive(false);
                    _area5Action.SetActive(false);
                    _area6Action.SetActive(false);

                    _areaOperation.SetActive(false);
                    _areaDivergence.SetActive(false);

                    _bt1.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='inputs']").InnerText;
                    _bt2.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='outputs']").InnerText;
                    _bt3.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='memory']").InnerText;
                    _bt4.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='timers']").InnerText;
                    _bt5.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='counters']").InnerText;
                    _bt6.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='comparators']").InnerText;

                    if (_actualElement.GetComponent<conditionElementClass>()._typeValue != 0)
                    {
                        buttonActive = _actualElement.GetComponent<conditionElementClass>()._typeValue;
                    }

                    setTabTransition(buttonActive);
                    configureTransitionTab(buttonActive);
                    setInitialTransitionValues();           
                }
                else {
                    _nameArea.SetActive(false);
                    _buttonsRoboticArea.SetActive(true);
                    _buttonsRoboticArea.GetComponent<RectTransform>().anchoredPosition = new Vector2(11, 90);
                    _btRob10.gameObject.SetActive(true);

                    _btRob11.gameObject.SetActive(true);
                    _btRob12.gameObject.SetActive(true);
                    _btRob13.gameObject.SetActive(true);

                    _area1ActionRobotics.SetActive(false);
                    _area2ActionRobotics.SetActive(false);
                    _area3ActionRobotics.SetActive(false);
                    _area4ActionRobotics.SetActive(false);
                    _area5ActionRobotics.SetActive(false);
                    _area6ActionRobotics.SetActive(false);

                    _areaOperationRobotics.SetActive(false);
                    
                    _areaDivergence.SetActive(false);

                    _btRob1.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_line_sensor']").InnerText;
                    _btRob2.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_right_ultrasonic']").InnerText;
                    _btRob3.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='timers']").InnerText;
                    _btRob4.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_band_sensor']").InnerText;
                    _btRob5.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_center_ultrasonic']").InnerText;
                    _btRob6.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='comparators']").InnerText;
                    _btRob7.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_color_sensor']").InnerText;
                    _btRob8.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_left_ultrasonic']").InnerText;
                    _btRob9.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='counters']").InnerText;
                    _btRob10.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_start']").InnerText;

                    _btRob11.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_tran_bt_11']").InnerText;
                    _btRob12.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_tran_bt_12']").InnerText;
                    _btRob13.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_tran_bt_13']").InnerText;

                    if (_actualElement.GetComponent<conditionElementClass>()._typeRobotics != 0)
                    {
                        buttonActive = _actualElement.GetComponent<conditionElementClass>()._typeRobotics;
                    }

                    setTabTransitionRobotic();
                    configureTransitionTabRobotic();
                    setInitialTransitionValuesRobotic();
                }
				break;
			case "operation":
                if (Manager.Instance.globalRoboticsMode == false)
                {
                    _nameArea.SetActive(false);
                    _buttonsArea.SetActive(false);
                    _buttonsRoboticArea.SetActive(false);
                    
                    _area1Action.SetActive(false);
                    _area2Action.SetActive(false);
                    _area3Action.SetActive(false);
                    _area4Action.SetActive(false);
                    _area5Action.SetActive(false);
                    _area6Action.SetActive(false);

                    _area1Condition.SetActive(false);
                    _area2Condition.SetActive(false);
                    _area3Condition.SetActive(false);

                    _areaOperation.SetActive(true);
                    _areaDivergence.SetActive(false);

                    _labelSelector1Area1Transition.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='type']").InnerText;
                    _selector1Area1Operation.initializeElement(_situation.GetComponent<GrafcetMainClass>().logicTransOperators);

                    setInitialTransitionValues();           
                }
                else {
                    _nameArea.SetActive(false);
                    _buttonsRoboticArea.SetActive(false);

                    _area1ActionRobotics.SetActive(false);
                    _area2ActionRobotics.SetActive(false);
                    _area3ActionRobotics.SetActive(false);
                    _area4ActionRobotics.SetActive(false);
                    _area5ActionRobotics.SetActive(false);
                    _area6ActionRobotics.SetActive(false);

                    _area1ConditionRobotics.SetActive(false);
                    _area2ConditionRobotics.SetActive(false);
                    _area3ConditionRobotics.SetActive(false);
                    _area4ConditionRobotics.SetActive(false);
                    _area5ConditionRobotics.SetActive(false);
                    _area6ConditionRobotics.SetActive(false);

                    _areaOperationRobotics.SetActive(true);
                    _areaDivergence.SetActive(false);

                    operationRoboticLabel.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='type']").InnerText;
                    operationRoboticSelector.initializeElement(_situation.GetComponent<GrafcetMainClass>().logicTransOperators);

                    setInitialTransitionValuesRobotic();  
                }
				break;
			}
			break;
		case "divergence":
		case "convergence":
			_nameArea.SetActive (false);
			_buttonsArea.SetActive (false);

			_area1Action.SetActive (false);
			_area2Action.SetActive (false);
			_area3Action.SetActive (false);
			_area4Action.SetActive (false);
			_area5Action.SetActive (false);
			_area6Action.SetActive (false);

            _buttonsRoboticArea.SetActive(false);

            _area1ActionRobotics.SetActive(false);
            _area2ActionRobotics.SetActive(false);
            _area3ActionRobotics.SetActive(false);
            _area4ActionRobotics.SetActive(false);
            _area5ActionRobotics.SetActive(false);
            _area6ActionRobotics.SetActive(false);

            _area1ConditionRobotics.SetActive(false);
            _area2ConditionRobotics.SetActive(false);
            _area3ConditionRobotics.SetActive(false);
            _area4ConditionRobotics.SetActive(false);
            _area5ConditionRobotics.SetActive(false);
            _area6ConditionRobotics.SetActive(false);

			_area1Condition.SetActive (false);
			_area2Condition.SetActive (false);
			_area3Condition.SetActive (false);

			_areaOperation.SetActive (false);
			_areaDivergence.SetActive (true);

			_labelSelector1AreaDivergence.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='type']").InnerText;
			_selector1AreaDivergence.initializeElement (_situation.GetComponent<GrafcetMainClass> ().logicTransOperators);
			_labelSelector2AreaDivergence.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='n_branches']").InnerText;

			List<string> _Numericlist = new List<string> ();
			for (int i = 2; i <= 64; i++) {
				_Numericlist.Add (i.ToString ());
			}
			_selector2AreaDivergence.initializeElement (_Numericlist);
			SetInitialDivergenceValues ();
			break;
		}
	}

	private void setTabAction(int _buttonActive){
		switch(_buttonActive){
		case 1:
			_area1Action.SetActive (true);
			_area2Action.SetActive (false);
			_area3Action.SetActive (false);
			_area4Action.SetActive (false);
			_area5Action.SetActive (false);
			_area6Action.SetActive (false);
			break;
		case 2:
			_area1Action.SetActive (false);
			_area2Action.SetActive (true);
			_area3Action.SetActive (false);
			_area4Action.SetActive (false);
			_area5Action.SetActive (false);
			_area6Action.SetActive (false);
			break;
		case 3:
			_area1Action.SetActive (false);
			_area2Action.SetActive (false);
			_area3Action.SetActive (true);
			_area4Action.SetActive (false);
			_area5Action.SetActive (false);
			_area6Action.SetActive (false);
			break;
		case 4:
			_area1Action.SetActive (false);
			_area2Action.SetActive (false);
			_area3Action.SetActive (false);
			_area4Action.SetActive (true);
			_area5Action.SetActive (false);
			_area6Action.SetActive (false);
			break;
		case 5:
			_area1Action.SetActive (false);
			_area2Action.SetActive (false);
			_area3Action.SetActive (false);
			_area4Action.SetActive (false);
			_area5Action.SetActive (true);
			_area6Action.SetActive (false);
			break;
		case 6:
			_area1Action.SetActive (false);
			_area2Action.SetActive (false);
			_area3Action.SetActive (false);
			_area4Action.SetActive (false);
			_area5Action.SetActive (false);
			_area6Action.SetActive (true);
			break;
		}
	}

	private void setTabTransition(int _buttonActive){
		switch(_buttonActive){
		case 1:
		case 2:
		case 4:
		case 5:
			_area1Condition.SetActive (true);
			_area2Condition.SetActive (false);
			_area3Condition.SetActive (false);
			break;
		case 3:
			_area1Condition.SetActive (false);
			_area2Condition.SetActive (false);
			_area3Condition.SetActive (true);
			break;
		case 6:
			_area1Condition.SetActive (false);
			_area2Condition.SetActive (true);
			_area3Condition.SetActive (false);
			break;
		}
	}

    private void setTabTransitionRobotic()
    {
        switch (buttonActive)
        {
            case 1:
                //line sensor
                _area1ConditionRobotics.SetActive(false);
                _area2ConditionRobotics.SetActive(false);
                _area3ConditionRobotics.SetActive(true);
                _area4ConditionRobotics.SetActive(false);
                _area5ConditionRobotics.SetActive(false);
                _area6ConditionRobotics.SetActive(false);
                break;
            case 2:
            case 5:
            case 8:
                //ultrasonic
                _area1ConditionRobotics.SetActive(false);
                _area2ConditionRobotics.SetActive(false);
                _area3ConditionRobotics.SetActive(false);
                _area4ConditionRobotics.SetActive(false);
                _area5ConditionRobotics.SetActive(true);
                _area6ConditionRobotics.SetActive(false);
                break;
            case 3:
            case 9:
                //temp
                //counters
                _area1ConditionRobotics.SetActive(true);
                _area2ConditionRobotics.SetActive(false);
                _area3ConditionRobotics.SetActive(false);
                _area4ConditionRobotics.SetActive(false);
                _area5ConditionRobotics.SetActive(false);
                _area6ConditionRobotics.SetActive(false);
                break;
            case 4:
            case 10:
            case 11:
            case 12:
            case 13:
                //band sensor
                _area1ConditionRobotics.SetActive(false);
                _area2ConditionRobotics.SetActive(false);
                _area3ConditionRobotics.SetActive(false);
                _area4ConditionRobotics.SetActive(false);
                _area5ConditionRobotics.SetActive(false);
                _area6ConditionRobotics.SetActive(true);
                break;
            case 6:
                //comparators
                _area1ConditionRobotics.SetActive(false);
                _area2ConditionRobotics.SetActive(true);
                _area3ConditionRobotics.SetActive(false);
                _area4ConditionRobotics.SetActive(false);
                _area5ConditionRobotics.SetActive(false);
                _area6ConditionRobotics.SetActive(false);
                break;
            case 7:
                //color sensor
                _area1ConditionRobotics.SetActive(false);
                _area2ConditionRobotics.SetActive(false);
                _area3ConditionRobotics.SetActive(false);
                _area4ConditionRobotics.SetActive(true);
                _area5ConditionRobotics.SetActive(false);
                _area6ConditionRobotics.SetActive(false);
                break;
        }
    }

    private void setTabActionRobotics(){
        switch (buttonActive)
        {
            case 1:
            case 2:
            case 4:
            case 5:
                _area1ActionRobotics.SetActive(true);
                _area2ActionRobotics.SetActive(false);
                _area3ActionRobotics.SetActive(false);
                _area4ActionRobotics.SetActive(false);
                _area5ActionRobotics.SetActive(false);
                _area6ActionRobotics.SetActive(false);
                break;
            case 3:
                _area1ActionRobotics.SetActive(false);
                _area2ActionRobotics.SetActive(true);
                _area3ActionRobotics.SetActive(false);
                _area4ActionRobotics.SetActive(false);
                _area5ActionRobotics.SetActive(false);
                _area6ActionRobotics.SetActive(false);
                break;
            case 6:
                _area1ActionRobotics.SetActive(false);
                _area2ActionRobotics.SetActive(false);
                _area3ActionRobotics.SetActive(true);
                _area4ActionRobotics.SetActive(false);
                _area5ActionRobotics.SetActive(false);
                _area6ActionRobotics.SetActive(false);
                break;
            case 7:
            case 10:
            case 11:
            case 12:
                _area1ActionRobotics.SetActive(false);
                _area2ActionRobotics.SetActive(false);
                _area3ActionRobotics.SetActive(false);
                _area4ActionRobotics.SetActive(true);
                _area5ActionRobotics.SetActive(false);
                _area6ActionRobotics.SetActive(false);
                break;
            case 8:
                _area1ActionRobotics.SetActive(false);
                _area2ActionRobotics.SetActive(false);
                _area3ActionRobotics.SetActive(false);
                _area4ActionRobotics.SetActive(false);
                _area5ActionRobotics.SetActive(true);
                _area6ActionRobotics.SetActive(false);
                break;
            case 9:
                _area1ActionRobotics.SetActive(false);
                _area2ActionRobotics.SetActive(false);
                _area3ActionRobotics.SetActive(false);
                _area4ActionRobotics.SetActive(false);
                _area5ActionRobotics.SetActive(false);
                _area6ActionRobotics.SetActive(true);
                break;
        }
    }

	public void exitAction(){
		switch (_editorType) {
		case "action":
			_actualElement.GetComponent<ActionClass> ().closeEditElements ();
			_situation.GetComponent<GrafcetMainClass> ().deactivateBackground ();
			break;
		case "transition":
			_situation.GetComponent<GrafcetMainClass> ()._editorTransPanel.GetComponent<editorTransPanel> ().hideOverElement ();
			switch(_editorParameter){
			case "condition":
				_actualElement.GetComponent<conditionElementClass> ().closeEditElements ();
				break;
			case "operation":
				_actualElement.GetComponent<operationElementClass> ().closeEditElements ();
				break;
			}
			break;
		case "divergence":
			_actualElement.GetComponent<DivergenceClass> ().closeEditElements ();
			_situation.GetComponent<GrafcetMainClass> ().deactivateBackground ();
			break;
		case "convergence":
			_actualElement.GetComponent<ConvergenceClass> ().closeEditElements ();
			_situation.GetComponent<GrafcetMainClass> ().deactivateBackground ();
			break;
		}

		iTween.MoveTo(
			_situation.GetComponent<GrafcetMainClass> ()._editorPanel,
			iTween.Hash(
				"position", new Vector3(0,1500,0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal",true
			)
		);

		Invoke ("goToInitialState", 1f);
	}

	private void bt1Action(){
		if(buttonActive != 1){
			buttonActive = 1;
			switch (_editorType) {
			case "action":
				configureActionTab (buttonActive);
				setTabAction (buttonActive);
				break;
			case "transition":
				configureTransitionTab (buttonActive);
				setTabTransition (buttonActive);
				break;
			}

		}
	}

	private void bt2Action(){
		if (buttonActive != 2) {
			buttonActive = 2;
			switch (_editorType) {
			case "action":
				configureActionTab (buttonActive);
				setTabAction (buttonActive);
				break;
			case "transition":
				configureTransitionTab (buttonActive);
				setTabTransition (buttonActive);
				break;
			}
		}
	}

	private void bt3Action(){
		if (buttonActive != 3) {
			buttonActive = 3;
			switch (_editorType) {
			case "action":
				configureActionTab (buttonActive);
				setTabAction (buttonActive);
				break;
			case "transition":
				configureTransitionTab (buttonActive);
				setTabTransition (buttonActive);
				break;
			}
		}
	}

	private void bt4Action(){
		if (buttonActive != 4) {
			buttonActive = 4;
			switch (_editorType) {
			case "action":
				configureActionTab (buttonActive);
				setTabAction (buttonActive);
				break;
			case "transition":
				configureTransitionTab (buttonActive);
				setTabTransition (buttonActive);
				break;
			}
		}
	}

	private void bt5Action(){
		if (buttonActive != 5) {
			buttonActive = 5;
			switch (_editorType) {
			case "action":
				configureActionTab (buttonActive);
				setTabAction (buttonActive);
				break;
			case "transition":
				configureTransitionTab (buttonActive);
				setTabTransition (buttonActive);
				break;
			}
		}
	}

	private void bt6Action(){
		if (buttonActive != 6) {
			buttonActive = 6;
			switch (_editorType) {
			case "action":
				configureActionTab (buttonActive);
				setTabAction (buttonActive);
				break;
			case "transition":
				configureTransitionTab (buttonActive);
				setTabTransition (buttonActive);
				break;
			}
		}
	}

    private void btRob1Action()
    {
        if (buttonActive != 1)
        {
            buttonActive = 1;
            switch (_editorType)
            {
                case "action":
                    configureActionRoboticTab();
                    setTabActionRobotics();
                    break;
                case "transition":
                    configureTransitionTabRobotic();
                    setTabTransitionRobotic();
                    break;
            }

        }
    }

    private void btRob2Action()
    {
        if (buttonActive != 2)
        {
            buttonActive = 2;
            switch (_editorType)
            {
                case "action":
                    configureActionRoboticTab();
                    setTabActionRobotics();
                    break;
                case "transition":
                    configureTransitionTabRobotic();
                    setTabTransitionRobotic();
                    break;
            }

        }
    }

    private void btRob3Action()
    {
        if (buttonActive != 3)
        {
            buttonActive = 3;
            switch (_editorType)
            {
                case "action":
                    configureActionRoboticTab();
                    setTabActionRobotics();
                    break;
                case "transition":
                    configureTransitionTabRobotic();
                    setTabTransitionRobotic();
                    break;
            }

        }
    }

    private void btRob4Action()
    {
        if (buttonActive != 4)
        {
            buttonActive = 4;
            switch (_editorType)
            {
                case "action":
                    configureActionRoboticTab();
                    setTabActionRobotics();
                    break;
                case "transition":
                    configureTransitionTabRobotic();
                    setTabTransitionRobotic();
                    break;
            }

        }
    }

    private void btRob5Action()
    {
        if (buttonActive != 5)
        {
            buttonActive = 5;
            switch (_editorType)
            {
                case "action":
                    configureActionRoboticTab();
                    setTabActionRobotics();
                    break;
                case "transition":
                    configureTransitionTabRobotic();
                    setTabTransitionRobotic();
                    break;
            }

        }
    }

    private void btRob6Action()
    {
        if (buttonActive != 6)
        {
            buttonActive = 6;
            switch (_editorType)
            {
                case "action":
                    configureActionRoboticTab();
                    setTabActionRobotics();
                    break;
                case "transition":
                    configureTransitionTabRobotic();
                    setTabTransitionRobotic();
                    break;
            }

        }
    }

    private void btRob7Action()
    {
        if (buttonActive != 7)
        {
            buttonActive = 7;
            switch (_editorType)
            {
                case "action":
                    configureActionRoboticTab();
                    setTabActionRobotics();
                    break;
                case "transition":
                    configureTransitionTabRobotic();
                    setTabTransitionRobotic();
                    break;
            }

        }
    }

    private void HideMenuBg()
    {
        CoordMenu.SetActive(false);
    }

    private void btRob8Action()
    {
        switch (_editorType)
        {
            case "action":
                CoordMenu.SetActive(true);
                //configureActionRoboticTab();
                //setTabActionRobotics();
                break;
            case "transition":
                if (buttonActive != 8)
                {
                    buttonActive = 8;
                    configureTransitionTabRobotic();
                    setTabTransitionRobotic();
                }

                break;
        }
    }

    private void btRob8RectAction()
    {
        CoordMenu.SetActive(false);
        buttonActive = 8;
        configureActionRoboticTab();
        setTabActionRobotics();
    }

    private void btRob8PolAction()
    {
        CoordMenu.SetActive(false);
        buttonActive = 9;
        configureActionRoboticTab();
        setTabActionRobotics();
    }

    private void btRob9Action()
    {
        switch (_editorType)
        {
            case "action":
                if (buttonActive != 10)
                {
                    buttonActive = 10;
                    configureActionRoboticTab();
                    setTabActionRobotics();
                }

                break;
            case "transition":
                if (buttonActive != 9)
                {
                    buttonActive = 9;
                    configureTransitionTabRobotic();
                    setTabTransitionRobotic();
                }

                break;
        }
    }

    private void btRob10Action()
    {
        switch (_editorType)
        {
            case "transition":
                if (buttonActive != 10)
                {
                    buttonActive = 10;
                    configureTransitionTabRobotic();
                    setTabTransitionRobotic();
                }

                break;
        }
    }

    private void btRob11Action()
    {
        switch (_editorType)
        {
            case "action":
                if (buttonActive != 11)
                {
                    buttonActive = 11;
                    configureActionRoboticTab();
                    setTabActionRobotics();
                }
                break;
            case "transition":
                if (buttonActive != 11)
                {
                    buttonActive = 11;
                    configureTransitionTabRobotic();
                    setTabTransitionRobotic();
                }
                break;
        }
    }

    private void btRob12Action()
    {
        switch (_editorType)
        {
            case "action":
                if (buttonActive != 12)
                {
                    buttonActive = 12;
                    configureActionRoboticTab();
                    setTabActionRobotics();
                }
                break;
            case "transition":
                if (buttonActive != 12)
                {
                    buttonActive = 12;
                    configureTransitionTabRobotic();
                    setTabTransitionRobotic();
                }
                break;
        }
    }

    private void btRob13Action()
    {
        switch (_editorType)
        {
            case "transition":
                if (buttonActive != 13)
                {
                    buttonActive = 13;
                    configureTransitionTabRobotic();
                    setTabTransitionRobotic();
                }
                break;
        }
    }

    private void configureActionTab(int _tab){
		switch(_tab){
		case 1:
			_invArea1Action.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='minus']").InnerText;
			List<string> _data = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().digitalOutputs; i++) {
				_data.Add ("O" + i.ToString());
			}
			_selectorArea1Action.initializeElement (_data);
			break;
		case 2:
			_invArea2Action.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='minus']").InnerText;
			List<string> _data2A = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().memoryElements; i++) {
				_data2A.Add ("M" + i.ToString());
			}
			_selector1Area2Action.initializeElement (_data2A);
			_labelSelector2Area2Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='bit']").InnerText;
			List<string> _data2B = new List<string> ();
			for (int i = 0; i < 8; i++) {
				_data2B.Add (i.ToString());
			}
			_selector2Area2Action.initializeElement (_data2B);
			break;
		case 3:
			_resetArea3Action.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='reset']").InnerText;
			List<string> _data3A = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().timerElements; i++) {
				_data3A.Add ("T" + i.ToString ());
			}
			_selector1Area3Action.initializeElement (_data3A);
			_labelSelector2Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='type']").InnerText;
			_selector2Area3Action.initializeElement (_situation.GetComponent<GrafcetMainClass> ().timerTypes);
			_labelTP3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='mseg_scale']").InnerText + ")";
			tpArea3Action.text = "0";
			_labelSelector3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ET']").InnerText;
			List<string> _data3C = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().memoryElements; i++) {
				_data3C.Add ("M" + i.ToString ());
			}
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().analogOutputs; i++) {
				_data3C.Add ("AO" + i.ToString ());
			}
			_selector3Area3Action.initializeElement (_data3C);
			_resetArea3Action.isOn = false;
			break;
		case 4:
			_resetArea4Action.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='reset']").InnerText;
			List<string> _data4A = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().counterElements; i++) {
				_data4A.Add ("C" + i.ToString ());
			}
			_selector1Area4Action.initializeElement (_data4A);
			_labelSelector2Area4Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='type']").InnerText;
			_selector2Area4Action.initializeElement (_situation.GetComponent<GrafcetMainClass> ().counterTypes);
			_labelPV3Area4Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='pv']").InnerText;
			pvArea4Action.text = "0";
			_labelSelector3Area4Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='cv']").InnerText;
			List<string> _data4C = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().memoryElements; i++) {
				_data4C.Add ("M" + i.ToString ());
			}
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().analogOutputs; i++) {
				_data4C.Add ("AO" + i.ToString ());
			}
			_selector3Area4Action.initializeElement (_data4C);
			_resetArea4Action.isOn = false;
			break;
		case 5:
			_selector1Area5Action.initializeElement (_situation.GetComponent<GrafcetMainClass> ().aritmethicOperators);
			_labelSelector2Area5Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='op1']").InnerText;
			List<string> _data5B = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().memoryElements; i++) {
				_data5B.Add ("M" + i.ToString ());
			}
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().counterElements; i++) {
				_data5B.Add ("C" + i.ToString ());
			}
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().timerElements; i++) {
				_data5B.Add ("T" + i.ToString ());
			}
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().analogInputs; i++) {
				_data5B.Add ("AI" + i.ToString ());
			}
			_selector2Area5Action.initializeElement (_data5B);
			_input2SelectorArea5Action.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='constant']").InnerText;
			_input2Area5Action.gameObject.SetActive (false);
			_labelSelector3Area5Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='op2']").InnerText;
			_selector3Area5Action.initializeElement (_data5B);
			_input3SelectorArea5Action.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='constant']").InnerText;
			_input3Area5Action.gameObject.SetActive (false);
			_labelSelector4Area5Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='dest']").InnerText;
			List<string> _data5D = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().memoryElements; i++) {
				_data5D.Add ("M" + i.ToString ());
			}
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().analogOutputs; i++) {
				_data5D.Add ("AO" + i.ToString ());
			}
			_selector4Area5Action.initializeElement (_data5D);

			_input2SelectorArea5Action.isOn = false;
			_input3SelectorArea5Action.isOn = false;
			break;
		case 6:
			_selector1Area6Action.initializeElement (_situation.GetComponent<GrafcetMainClass> ().logicOperators);
			_labelSelector2Area6Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='op1']").InnerText;
			List<string> _data6B = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().memoryElements; i++) {
				_data6B.Add ("M" + i.ToString ());
			}
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().counterElements; i++) {
				_data6B.Add ("C" + i.ToString ());
			}
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().timerElements; i++) {
				_data6B.Add ("T" + i.ToString ());
			}
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().analogInputs; i++) {
				_data6B.Add ("AI" + i.ToString ());
			}
			_selector2Area6Action.initializeElement (_data6B);
			_labelSelector3Area6Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='op2']").InnerText;
			_input2SelectorArea6Action.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='constant']").InnerText;
			_input2Area6Action.gameObject.SetActive (false);
			_selector3Area6Action.initializeElement (_data6B);
			_labelSelector4Area6Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='dest']").InnerText;
			_input3SelectorArea6Action.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='constant']").InnerText;
			_input3Area6Action.gameObject.SetActive (false);
			List<string> _data6D = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().memoryElements; i++) {
				_data6D.Add ("M" + i.ToString ());
			}
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().analogOutputs; i++) {
				_data6D.Add ("AO" + i.ToString ());
			}
			_selector4Area6Action.initializeElement (_data6D);

			_input2SelectorArea6Action.isOn = false;
			_input3SelectorArea6Action.isOn = false;

			setVisibilityArea6 (true);
			break;
		}
	}

    private void configureActionRoboticTab()
    {
        switch (buttonActive)
        {
            case 1:
            case 2:
            case 4:
            case 5:
                _labelSelector1Area1ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_dir']").InnerText;
                _labelSelector2Area1ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_vel']").InnerText;

                List<string> _dataArea1_1 = new List<string>();
                _dataArea1_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_up']").InnerText);
                _dataArea1_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_back']").InnerText);
                _dataArea1_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_stop']").InnerText);
                _selector1Area1ActionRob.initializeElement(_dataArea1_1);

                List<string> _dataArea1_2 = new List<string>();
                for (int i = 0; i <= Manager.Instance.globalEngineMaxVel; i++){
                    _dataArea1_2.Add(i.ToString());
                }
                _selector2Area1ActionRob.initializeElement(_dataArea1_2);
                break;
            case 7:
                _labelSelector1Area4ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_motor_band']").InnerText;
                List<string> _dataArea4_1 = new List<string>();
                _dataArea4_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_off']").InnerText);
                _dataArea4_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_on']").InnerText);
                _selector1Area4ActionRob.initializeElement(_dataArea4_1);
                break;
            case 10:
                _labelSelector1Area4ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_gripper']").InnerText;
                List<string> _dataArea4_1a = new List<string>();
                _dataArea4_1a.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_off']").InnerText);
                _dataArea4_1a.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_on']").InnerText);
                _selector1Area4ActionRob.initializeElement(_dataArea4_1a);
                break;
            case 11:
                _labelSelector1Area4ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_gripper']").InnerText;
                List<string> _dataArea4_1b = new List<string>();
                _dataArea4_1b.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_off']").InnerText);
                _dataArea4_1b.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_on']").InnerText);
                _selector1Area4ActionRob.initializeElement(_dataArea4_1b);
                break;
            case 12:
                _labelSelector1Area4ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_gripper']").InnerText;
                List<string> _dataArea4_1c = new List<string>();
                _dataArea4_1c.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_off']").InnerText);
                _dataArea4_1c.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_on']").InnerText);
                _selector1Area4ActionRob.initializeElement(_dataArea4_1c);
                break;
            case 3:
                _invArea2ActionRob.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='reset']").InnerText;
                List<string> _data3A = new List<string>();
                for (int i = 0; i < _situation.GetComponent<GrafcetMainClass>().timerElements; i++)
                {
                    _data3A.Add("T" + i.ToString());
                }
                _selector1Area2ActionRob.initializeElement(_data3A);
                _labelSelector2Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='type']").InnerText;
                _selector2Area2ActionRob.initializeElement(_situation.GetComponent<GrafcetMainClass>().timerTypes);
                _labelSelector3Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='mseg_scale']").InnerText + ")";
                _input3Area2ActionRob.text = "0";
                scaleTPArea3 = 0;
                _invArea2ActionRob.isOn = false;
                break;
            case 6:
                _invArea3ActionRob.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='reset']").InnerText;
                List<string> _data4A = new List<string>();
                for (int i = 0; i < _situation.GetComponent<GrafcetMainClass>().counterElements; i++)
                {
                    _data4A.Add("C" + i.ToString());
                }
                _selector1Area3ActionRob.initializeElement(_data4A);
                _labelSelector2Area3ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='type']").InnerText;
                _selector2Area3ActionRob.initializeElement(_situation.GetComponent<GrafcetMainClass>().counterTypes);
                _labelSelector3Area3ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='pv']").InnerText;
                _input3Area3ActionRob.text = "0";
                _invArea3ActionRob.isOn = false;
                break;
            case 8:
                SetCartesianValues();
                break;
            case 9:
                SetPolarValues();
                break;
                
        }
    }

    private void SetCartesianValues(){
        _area5ActionRobotics.SetActive(true);
        _area6ActionRobotics.SetActive(false);

        _labelSelector1Area5ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_x']").InnerText;
        _labelSelector2Area5ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_y']").InnerText;
        _labelSelector3Area5ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_z']").InnerText;

        List<string> _data5A = new List<string>();
        for (int i = 0; i < 100; i++)
        {
            _data5A.Add(i.ToString());
        }
       
        _selector1Area5ActionRob.initializeElement(_data5A);
        _selector2Area5ActionRob.initializeElement(_data5A);
        _selector3Area5ActionRob.initializeElement(_data5A);

    }

    private void SetPolarValues()
    {
        _area5ActionRobotics.SetActive(false);
        _area6ActionRobotics.SetActive(true);

        _labelSelector1Area6ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_r']").InnerText;
        _labelSelector2Area6ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_te']").InnerText;

        List<string> _data6A = new List<string>();
        for (int i = 0; i < 100; i++)
        {
            _data6A.Add(i.ToString());
        }

        List<string> _data6B = new List<string>();
        for (int i = 0; i < 91; i++)
        {
            _data6B.Add(i.ToString());
        }
        _selector1Area6ActionRob.initializeElement(_data6A);
        _selector2Area6ActionRob.initializeElement(_data6B);

    }

	private void configureTransitionTab(int _tab){
		switch(_tab){
		case 1:
			_invArea1Transition.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='minus']").InnerText;
			List<string> _data1T = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().digitalInputs; i++) {
				_data1T.Add ("I" + i.ToString ());
			}
			_selectorArea1Transition.initializeElement (_data1T);
			break;
		case 2:
			_invArea1Transition.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='minus']").InnerText;
			List<string> _data2T = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().digitalOutputs; i++) {
				_data2T.Add ("O" + i.ToString ());
			}
			_selectorArea1Transition.initializeElement (_data2T);
			break;
		case 3:
			_invArea3Transition.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='minus']").InnerText;
			List<string> _data3T = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().memoryElements; i++) {
				_data3T.Add ("M" + i.ToString ());
			}
			_selector1Area3Transition.initializeElement (_data3T);

			_labelSelector2Area3Transition.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='bit']").InnerText;
			List<string> _data3T_1 = new List<string> ();
			for (int i = 0; i < 8; i++) {
				_data3T_1.Add (i.ToString());
			}
			_selector2Area3Transition.initializeElement (_data3T_1);
			break;
		case 4:
			_invArea1Transition.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='minus']").InnerText;
			List<string> _data4T = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().timerElements; i++) {
				_data4T.Add ("T" + i.ToString ());
			}
			_selectorArea1Transition.initializeElement (_data4T);
			break;
		case 5:
			_invArea1Transition.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='minus']").InnerText;
			List<string> _data5T = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().counterElements; i++) {
				_data5T.Add ("C" + i.ToString ());
			}
			_selectorArea1Transition.initializeElement (_data5T);
			break;
		case 6:
			_invArea2Transition.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='minus']").InnerText;
			List<string> _data6T_1 = new List<string> ();
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().analogInputs; i++) {
				_data6T_1.Add ("AI" + i.ToString ());
			}
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().analogOutputs; i++) {
				_data6T_1.Add ("AO" + i.ToString ());
			}
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().memoryElements; i++) {
				_data6T_1.Add ("M" + i.ToString ());
			}
			_selector1Area2Transition.initializeElement (_data6T_1);
			_selector3Area2Transition.initializeElement (_data6T_1);
			_selector2Area2Transition.initializeElement (_situation.GetComponent<GrafcetMainClass>().logicComparators);

			_input1SelectorArea2Transition.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='constant']").InnerText;
			_input1Area2Transition.gameObject.SetActive (false);

			_input3SelectorArea2Transition.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='constant']").InnerText;
			_input3Area2Transition.gameObject.SetActive (false);

			_input1SelectorArea2Transition.isOn = false;
			_input3SelectorArea2Transition.isOn = false;
			break;
		}
	}

    private void configureTransitionTabRobotic(){
        Debug.Log(buttonActive);
        switch (buttonActive)
        {
            case 1:
                transitionRoboticArea3Label1.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_trans_rob_area3_label1']").InnerText;
                transitionRoboticArea3Label2.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_trans_rob_area3_label2']").InnerText;
                transitionRoboticArea3Label3.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_trans_rob_area3_label3']").InnerText;

                transitionRoboticArea3ButtonBit0.GetComponentInChildren<Text>().text = "0";
                transitionRoboticArea3ButtonBit1.GetComponentInChildren<Text>().text = "0";
                transitionRoboticArea3ButtonBit2.GetComponentInChildren<Text>().text = "0";
                transitionRoboticArea3ButtonBit3.GetComponentInChildren<Text>().text = "0";
                transitionRoboticArea3Constant1.text = "0";

                List<string> _data1T = new List<string>();
                for (int i = 0; i < 16; i++)
                {
                    _data1T.Add( i.ToString() );
                }
                transitionRoboticArea3Selector1.initializeElement(_data1T);
                break;
            case 2:
            case 5:
            case 8:
                transitionRoboticArea5Label1.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_trans_rob_area5_label1']").InnerText;
                transitionRoboticArea5Label2.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_trans_rob_area5_label2']").InnerText;

                List<string> _data2T_A = new List<string>();
                for (int i = 0; i < 256; i++)
                {
                    _data2T_A.Add(i.ToString());
                }
                transitionRoboticArea5Selector1.initializeElement(_data2T_A);

                List<string> _data2T_B = new List<string>();
                for (int i = 0; i < 101; i++)
                {
                    _data2T_B.Add(i.ToString());
                }
                transitionRoboticArea5Selector2.initializeElement(_data2T_B);
                break;
            case 3:
                transitionRoboticArea1Toggle.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='minus']").InnerText;
                List<string> _data3T = new List<string>();
                for (int i = 0; i < _situation.GetComponent<GrafcetMainClass>().timerElements; i++)
                {
                    _data3T.Add("T" + i.ToString());
                }
                transitionRoboticArea1Selector.initializeElement(_data3T);
                break;
            case 4:
            case 10:
            case 11:
            case 12:
            case 13:
                transitionRoboticArea6Label1.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_trans_rob_area6_label']").InnerText;
                List<string> _data4T = new List<string>();
                _data4T.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_trans_rob_area6_option2']").InnerText);
                _data4T.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_trans_rob_area6_option1']").InnerText);
                transitionRoboticArea6Selector1.initializeElement(_data4T);
                break;
            case 6:
                transitionRoboticArea2Toggle.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='minus']").InnerText;
                List<string> _data6T_1 = new List<string>();

                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_start']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_band_sensor']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_gripper']").InnerText);

                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_bastidor_z']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_eje_5']").InnerText);

                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_motor_band']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_vel_a']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_vel_b']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_vel_c']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_vel_d']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_dir_a']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_dir_b']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_dir_c']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_dir_d']").InnerText);

                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_coor_x']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_coor_y']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_coor_z']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_coor_r']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_coor_t']").InnerText);

                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='list_rob_coor_x']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='list_rob_coor_y']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='list_rob_coor_z']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='list_rob_coor_r']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='list_rob_coor_t']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_line_sensor']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_color_sensor']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_right_ultrasonic']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_center_ultrasonic']").InnerText);
                _data6T_1.Add(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_left_ultrasonic']").InnerText);

                for (int i = 0; i < _situation.GetComponent<GrafcetMainClass>().timerElements; i++)
                {
                    _data6T_1.Add("T" + i.ToString());
                }

                for (int i = 0; i < _situation.GetComponent<GrafcetMainClass>().counterElements; i++)
                {
                    _data6T_1.Add("C" + i.ToString());
                }

                transitionRoboticArea2Selector1.initializeElement(_data6T_1);
                transitionRoboticArea2Selector3.initializeElement(_data6T_1);
                transitionRoboticArea2Selector2.initializeElement(_situation.GetComponent<GrafcetMainClass>().logicComparators);

                transitionRoboticArea2ToggleConstant1.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='constant']").InnerText;
                transitionRoboticArea2Constant1.gameObject.SetActive(false);
                transitionRoboticArea2Constant1.text = "0";

                transitionRoboticArea2ToggleConstant3.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='constant']").InnerText;
                transitionRoboticArea2Constant3.gameObject.SetActive(false);
                transitionRoboticArea2Constant3.text = "0";

                transitionRoboticArea2ToggleConstant1.isOn = false;
                transitionRoboticArea2ToggleConstant3.isOn = false;
                break;
            case 7:
                break;
            case 9:
                transitionRoboticArea1Toggle.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='minus']").InnerText;
                List<string> _data9T = new List<string>();
                for (int i = 0; i < _situation.GetComponent<GrafcetMainClass>().counterElements; i++)
                {
                    _data9T.Add("C" + i.ToString());
                }
                transitionRoboticArea1Selector.initializeElement(_data9T);
                break;
        }
    }

	private void setInitialActionValues(){
		if (_actualElement.GetComponent<ActionClass> ()._titleElement.text == "?") {
			_nameInput.text = "";
		} else {
			_nameInput.text = _actualElement.GetComponent<ActionClass> ()._titleElement.text;
		}

		switch(buttonActive){
		case 1:
			_invArea1Action.isOn = _actualElement.GetComponent<ActionClass> ()._isInverse;
			_selectorArea1Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index);
			break;
		case 2:
			_invArea2Action.isOn = _actualElement.GetComponent<ActionClass> ()._isInverse;
			_selector1Area2Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index);
			_selector2Area2Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index2);
			break;
		case 3:
			_selector1Area3Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index);
			if (_actualElement.GetComponent<ActionClass> ()._isReset == true) {
				_resetArea3Action.isOn = true;	
			} else {
				_selector2Area3Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index2);
				_selector3Area3Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index3);
				scaleTPArea3 = _actualElement.GetComponent<ActionClass> ()._scale;
				float _valueScaled = 0;
				if (_actualElement.GetComponent<ActionClass> ()._value4 != "") {
					switch (scaleTPArea3) {
					case 0:
						_valueScaled = float.Parse(_actualElement.GetComponent<ActionClass> ()._value4);
						_labelTP3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='mseg_scale']").InnerText + ")";
						break;
					case 1:
						_valueScaled = float.Parse(_actualElement.GetComponent<ActionClass> ()._value4) / 1000f;
						_labelTP3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='seg_scale']").InnerText + ")";
						break;
					case 2:
						_valueScaled = float.Parse (_actualElement.GetComponent<ActionClass> ()._value4) / 1000f;
						_valueScaled = _valueScaled / 60f;
						_labelTP3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='min_scale']").InnerText + ")";
						break;
					case 3:
						_valueScaled = float.Parse (_actualElement.GetComponent<ActionClass> ()._value4) / 1000f;
						_valueScaled = _valueScaled / 60f;
						_valueScaled = _valueScaled / 60f;
						_labelTP3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='h_scale']").InnerText + ")";
						break;
					}
					tpArea3Action.text = _valueScaled.ToString ();
				} else {
					tpArea3Action.text = _actualElement.GetComponent<ActionClass> ()._value4;
				}


			}
			break;
		case 4:
			_selector1Area4Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index);
			if (_actualElement.GetComponent<ActionClass> ()._isReset == true) {
				_resetArea4Action.isOn = true;	
			} else {
				_selector2Area4Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index2);
				_selector3Area4Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index3);
				pvArea4Action.text = _actualElement.GetComponent<ActionClass> ()._value4;
			}
			break;
		case 5:
			_selector1Area5Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index);

			_input2SelectorArea5Action.isOn = _actualElement.GetComponent<ActionClass> ()._constant1;
			if (_actualElement.GetComponent<ActionClass> ()._constant1 == true) {
				_input2Area5Action.text = _actualElement.GetComponent<ActionClass> ()._value2;
			} else {
				_selector2Area5Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index2);
			}

			_input3SelectorArea5Action.isOn = _actualElement.GetComponent<ActionClass> ()._constant2;
			if (_actualElement.GetComponent<ActionClass> ()._constant2 == true) {
				_input3Area5Action.text = _actualElement.GetComponent<ActionClass> ()._value3;
			} else {
				_selector3Area5Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index3);
			}

			_selector4Area5Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index4);
			break;
		case 6:
			_selector1Area6Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index);

			_input2SelectorArea6Action.isOn = _actualElement.GetComponent<ActionClass> ()._constant1;
			if (_actualElement.GetComponent<ActionClass> ()._constant1 == true) {
				_input2Area6Action.text = _actualElement.GetComponent<ActionClass> ()._value2;
			} else {
				_selector2Area6Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index2);
			}

			_input3SelectorArea6Action.isOn = _actualElement.GetComponent<ActionClass> ()._constant2;
			if (_actualElement.GetComponent<ActionClass> ()._constant2 == true) {
				_input3Area6Action.text = _actualElement.GetComponent<ActionClass> ()._value3;
			} else {
				_selector3Area6Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index3);
			}
			_selector4Area6Action.setValuePerIndex (_actualElement.GetComponent<ActionClass> ()._index4);
			checkLogicAction ();
			break;
		}
	}

    private void setInitialActionRoboticsValues(){
        if (_actualElement.GetComponent<ActionClass>()._titleElement.text == "?")
        {
            _nameInput.text = "";
        }
        else
        {
            _nameInput.text = _actualElement.GetComponent<ActionClass>()._titleElement.text;
        }

        switch (buttonActive)
        {
            case 1:
            case 2:
            case 4:
            case 5:
                _selector1Area1ActionRob.setValuePerIndex(_actualElement.GetComponent<ActionClass>()._index);
                _selector2Area1ActionRob.setValuePerIndex(_actualElement.GetComponent<ActionClass>()._index2);
                break;
            case 3:
                _selector1Area2ActionRob.setValuePerIndex(_actualElement.GetComponent<ActionClass>()._index);
                if (_actualElement.GetComponent<ActionClass>()._isReset == true)
                {
                    _invArea2ActionRob.isOn = true;
                }
                else
                {
                    _selector2Area2ActionRob.setValuePerIndex(_actualElement.GetComponent<ActionClass>()._index2);

                    scaleTPArea3 = _actualElement.GetComponent<ActionClass>()._scale;
                    float _valueScaled = 0;
                    if (_actualElement.GetComponent<ActionClass>()._value4 != "")
                    {
                        switch (scaleTPArea3)
                        {
                            case 0:
                                _valueScaled = float.Parse(_actualElement.GetComponent<ActionClass>()._value4);
                                _labelSelector3Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='mseg_scale']").InnerText + ")";
                                break;
                            case 1:
                                _valueScaled = float.Parse(_actualElement.GetComponent<ActionClass>()._value4) / 1000f;
                                _labelSelector3Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='seg_scale']").InnerText + ")";
                                break;
                            case 2:
                                _valueScaled = float.Parse(_actualElement.GetComponent<ActionClass>()._value4) / 1000f;
                                _valueScaled = _valueScaled / 60f;
                                _labelSelector3Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='min_scale']").InnerText + ")";
                                break;
                            case 3:
                                _valueScaled = float.Parse(_actualElement.GetComponent<ActionClass>()._value4) / 1000f;
                                _valueScaled = _valueScaled / 60f;
                                _valueScaled = _valueScaled / 60f;
                                _labelSelector3Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='h_scale']").InnerText + ")";
                                break;
                        }
                        _input3Area2ActionRob.text = _valueScaled.ToString();
                    }
                    else
                    {
                        _input3Area2ActionRob.text = _actualElement.GetComponent<ActionClass>()._value4;
                    }
                }
                break;
            case 6:
                _selector1Area3ActionRob.setValuePerIndex(_actualElement.GetComponent<ActionClass>()._index);
                if (_actualElement.GetComponent<ActionClass>()._isReset == true)
                {
                    _invArea3ActionRob.isOn = true;
                }
                else
                {
                    _selector2Area3ActionRob.setValuePerIndex(_actualElement.GetComponent<ActionClass>()._index2);
                    _input3Area3ActionRob.text = _actualElement.GetComponent<ActionClass>()._value4;
                }
                break;
            case 7:
            case 10:
            case 11:
            case 12:
                _selector1Area4ActionRob.setValuePerIndex(_actualElement.GetComponent<ActionClass>()._index);
                break;
            case 8:
                _selector1Area5ActionRob.setValuePerIndex(_actualElement.GetComponent<ActionClass>()._index);
                _selector2Area5ActionRob.setValuePerIndex(_actualElement.GetComponent<ActionClass>()._index2);
                _selector3Area5ActionRob.setValuePerIndex(_actualElement.GetComponent<ActionClass>()._index3);
                break;
            case 9:
                _selector1Area6ActionRob.setValuePerIndex(_actualElement.GetComponent<ActionClass>()._index);
                _selector2Area6ActionRob.setValuePerIndex(_actualElement.GetComponent<ActionClass>()._index2);
                break;

        }
    }

	private void setInitialTransitionValues(){
		switch(_editorParameter){
		case "condition":
			switch(_editorParameter){
			case "condition":
				switch (buttonActive) {
				case 1:
				case 2:
				case 4:
				case 5:
					_selectorArea1Transition.setValuePerIndex (_actualElement.GetComponent<conditionElementClass> ()._index);
					_invArea1Transition.isOn = _actualElement.GetComponent<conditionElementClass> ()._isInverse;
					break;
				case 3:
					_selector1Area3Transition.setValuePerIndex (_actualElement.GetComponent<conditionElementClass> ()._index);
					_selector2Area3Transition.setValuePerIndex (_actualElement.GetComponent<conditionElementClass> ()._index2);
					_invArea3Transition.isOn = _actualElement.GetComponent<conditionElementClass> ()._isInverse;
					break;
				case 6:
					_selector2Area2Transition.setValuePerIndex (_actualElement.GetComponent<conditionElementClass> ()._index);
					_invArea2Transition.isOn = _actualElement.GetComponent<conditionElementClass> ()._isInverse;

					_input1SelectorArea2Transition.isOn = _actualElement.GetComponent<conditionElementClass> ()._constant1;
					_input3SelectorArea2Transition.isOn = _actualElement.GetComponent<conditionElementClass> ()._constant2;
						
					if (_input1SelectorArea2Transition.isOn) {
						_input1Area2Transition.text = _actualElement.GetComponent<conditionElementClass> ()._value2;
					} else {
						_selector1Area2Transition.setValuePerIndex (_actualElement.GetComponent<conditionElementClass> ()._index2);
						_input1Area2Transition.text = "0";
					}

					if (_input3SelectorArea2Transition.isOn) {
						_input3Area2Transition.text = _actualElement.GetComponent<conditionElementClass> ()._value3;
					} else {
						_selector3Area2Transition.setValuePerIndex (_actualElement.GetComponent<conditionElementClass> ()._index3);
						_input3Area2Transition.text = "0";
					}
					break;
				}
				break;
			case "operation":
				_actualElement.GetComponent<operationElementClass> ()._value = _selector1Area1Operation.getValue ();
				_actualElement.GetComponent<operationElementClass> ()._title.text = _selector1Area1Operation.getValue ();
				break;
			}
			break;
		case "operation":
			_selector1Area1Operation.setValue (_actualElement.GetComponent<operationElementClass> ()._value);
			break;
		}
	}

    private void setInitialTransitionValuesRobotic(){
        switch (_editorParameter)
        {
            case "condition":
                switch (_editorParameter)
                {
                    case "condition":
                        switch (buttonActive)
                        {
                            case 1:
                                transitionRoboticArea3Selector1.setValuePerIndex(_actualElement.GetComponent<conditionElementClass>()._index);
                                UpdateArea3RobValuesAction();
                                break;
                            case 2:
                            case 5:
                            case 8:
                                transitionRoboticArea5Selector1.setValuePerIndex(_actualElement.GetComponent<conditionElementClass>()._index);
                                transitionRoboticArea5Selector2.setValuePerIndex(_actualElement.GetComponent<conditionElementClass>()._index2);
                                break;
                            case 3:
                            case 9:
                                transitionRoboticArea1Selector.setValuePerIndex(_actualElement.GetComponent<conditionElementClass>()._index);
                                transitionRoboticArea1Toggle.isOn = _actualElement.GetComponent<conditionElementClass>()._isInverse;
                                break;
                            case 4:
                            case 10:
                            case 11:
                            case 12:
                            case 13:
                                transitionRoboticArea6Selector1.setValuePerIndex(_actualElement.GetComponent<conditionElementClass>()._index);
                                break;
                            case 6:
                                transitionRoboticArea2Selector2.setValuePerIndex(_actualElement.GetComponent<conditionElementClass>()._index);
                                transitionRoboticArea2Toggle.isOn = _actualElement.GetComponent<conditionElementClass>()._isInverse;

                                transitionRoboticArea2ToggleConstant1.isOn = _actualElement.GetComponent<conditionElementClass>()._constant1;
                                transitionRoboticArea2ToggleConstant3.isOn = _actualElement.GetComponent<conditionElementClass>()._constant2;

                                if (transitionRoboticArea2ToggleConstant1.isOn)
                                {
                                    transitionRoboticArea2Constant1.text = _actualElement.GetComponent<conditionElementClass>()._value2;
                                }
                                else
                                {
                                    transitionRoboticArea2Selector1.setValuePerIndex(_actualElement.GetComponent<conditionElementClass>()._index2);
                                    transitionRoboticArea2Constant1.text = "0";
                                }

                                if (transitionRoboticArea2ToggleConstant3.isOn)
                                {
                                    transitionRoboticArea2Constant3.text = _actualElement.GetComponent<conditionElementClass>()._value3;
                                }
                                else
                                {
                                    transitionRoboticArea2Selector3.setValuePerIndex(_actualElement.GetComponent<conditionElementClass>()._index3);
                                    transitionRoboticArea2Constant3.text = "0";
                                }
                                break;
                            case 7:
                                //load color element
                                colorIndexSelected = _actualElement.GetComponent<conditionElementClass>()._index;
                                switch(_actualElement.GetComponent<conditionElementClass>()._index){
                                    case 0:
                                        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color1.transform.localPosition;
                                        break;
                                    case 1:
                                        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color2.transform.localPosition;
                                        break;
                                    case 2:
                                        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color3.transform.localPosition;
                                        break;
                                    case 3:
                                        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color4.transform.localPosition;
                                        break;
                                    case 4:
                                        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color5.transform.localPosition;
                                        break;
                                    case 5:
                                        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color6.transform.localPosition;
                                        break;
                                    case 6:
                                        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color7.transform.localPosition;
                                        break;
                                    case 7:
                                        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color8.transform.localPosition;
                                        break;
                                }

                                break;
                        }
                        break;
                    case "operation":
                        _actualElement.GetComponent<operationElementClass>()._value = operationRoboticSelector.getValue();
                        _actualElement.GetComponent<operationElementClass>()._title.text = operationRoboticSelector.getValue();
                        break;
                }
                break;
            case "operation":
                operationRoboticSelector.setValue(_actualElement.GetComponent<operationElementClass>()._value);
                break;
        }
    }

	private void SetInitialDivergenceValues(){
		switch(_editorType){
		case "convergence":
			_selector1AreaDivergence.setValue (_actualElement.GetComponent<ConvergenceClass> ()._type);
			_selector2AreaDivergence.setValue (_actualElement.GetComponent<ConvergenceClass> ()._branches.ToString());
			break;
		case "divergence":
			_selector1AreaDivergence.setValue (_actualElement.GetComponent<DivergenceClass> ()._type);
			_selector2AreaDivergence.setValue (_actualElement.GetComponent<DivergenceClass> ()._branches.ToString());
			break;
		}
	}

	private void saveAction(){

		switch(_editorType){
		case "action":
			if (_nameInput.text != "") {
				_actualElement.GetComponent<ActionClass> ()._titleElement.text = _nameInput.text;
			} else {
				_actualElement.GetComponent<ActionClass> ()._titleElement.text = "?";
			}
			
            if(Manager.Instance.globalRoboticsMode == false){
                    _actualElement.GetComponent<ActionClass>()._type = buttonActive;
                    switch (buttonActive)
                    {
                        case 1:
                            _actualElement.GetComponent<ActionClass>()._value = _selectorArea1Action.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selectorArea1Action.getIndex();

                            _actualElement.GetComponent<ActionClass>()._isInverse = _invArea1Action.isOn;
                            _actualElement.GetComponent<ActionClass>()._value2 = "";
                            _actualElement.GetComponent<ActionClass>()._index2 = 0;
                            _actualElement.GetComponent<ActionClass>()._value3 = "";
                            _actualElement.GetComponent<ActionClass>()._index3 = 0;
                            _actualElement.GetComponent<ActionClass>()._value4 = "";
                            _actualElement.GetComponent<ActionClass>()._index4 = 0;
                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = false;
                            _actualElement.GetComponent<ActionClass>()._constant2 = false;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            if (_invArea1Action.isOn == true)
                            {
                                _actualElement.GetComponent<ActionClass>()._textElement.text = _selectorArea1Action.getValue() + " = 0";
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>()._textElement.text = _selectorArea1Action.getValue() + " = 1";
                            }
                            break;
                        case 2:
                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area2Action.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area2Action.getIndex();

                            _actualElement.GetComponent<ActionClass>()._isInverse = _invArea2Action.isOn;
                            _actualElement.GetComponent<ActionClass>()._value2 = _selector2Area2Action.getValue();
                            _actualElement.GetComponent<ActionClass>()._index2 = _selector2Area2Action.getIndex();
                            _actualElement.GetComponent<ActionClass>()._value3 = "";
                            _actualElement.GetComponent<ActionClass>()._index3 = 0;
                            _actualElement.GetComponent<ActionClass>()._value4 = "";
                            _actualElement.GetComponent<ActionClass>()._index4 = 0;
                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = false;
                            _actualElement.GetComponent<ActionClass>()._constant2 = false;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            if (_invArea2Action.isOn == true)
                            {
                                _actualElement.GetComponent<ActionClass>()._textElement.text = _selector1Area2Action.getValue() + "," + _selector2Area2Action.getValue() + " = 0";
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>()._textElement.text = _selector1Area2Action.getValue() + "," + _selector2Area2Action.getValue() + " = 1";
                            }
                            break;
                        case 3:
                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area3Action.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area3Action.getIndex();

                            if (_resetArea3Action.isOn == true)
                            {
                                _actualElement.GetComponent<ActionClass>()._isInverse = false;
                                _actualElement.GetComponent<ActionClass>()._value2 = "";
                                _actualElement.GetComponent<ActionClass>()._index2 = 0;
                                _actualElement.GetComponent<ActionClass>()._value3 = "";
                                _actualElement.GetComponent<ActionClass>()._index3 = 0;
                                _actualElement.GetComponent<ActionClass>()._value4 = "";
                                _actualElement.GetComponent<ActionClass>()._index4 = 0;
                                _actualElement.GetComponent<ActionClass>()._scale = 0;
                                _actualElement.GetComponent<ActionClass>()._constant1 = false;
                                _actualElement.GetComponent<ActionClass>()._constant2 = false;
                                _actualElement.GetComponent<ActionClass>()._options = "";
                                _actualElement.GetComponent<ActionClass>()._editorType = 1;
                                _actualElement.GetComponent<ActionClass>()._isReset = true;

                                _actualElement.GetComponent<ActionClass>()._textElement.text = "RES(" + _selector1Area3Action.getValue() + ")";
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>()._isInverse = false;
                                _actualElement.GetComponent<ActionClass>()._value2 = _selector2Area3Action.getValue();
                                _actualElement.GetComponent<ActionClass>()._index2 = _selector2Area3Action.getIndex();
                                _actualElement.GetComponent<ActionClass>()._value3 = _selector3Area3Action.getValue();
                                _actualElement.GetComponent<ActionClass>()._index3 = _selector3Area3Action.getIndex();

                                string _scaleTXT = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='mseg_scale']").InnerText;
                                if (tpArea3Action.text != "")
                                {
                                    if (scaleTPArea3 == 0)
                                    {
                                        _actualElement.GetComponent<ActionClass>()._value4 = tpArea3Action.text;
                                    }
                                    else if (scaleTPArea3 == 1)
                                    {
                                        _actualElement.GetComponent<ActionClass>()._value4 = (float.Parse(tpArea3Action.text) * 1000f).ToString();
                                        _scaleTXT = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='seg_scale']").InnerText;
                                    }
                                    else if (scaleTPArea3 == 2)
                                    {
                                        _actualElement.GetComponent<ActionClass>()._value4 = (float.Parse(tpArea3Action.text) * 1000f * 60f).ToString();
                                        _scaleTXT = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='min_scale']").InnerText;
                                    }
                                    else if (scaleTPArea3 == 3)
                                    {
                                        _actualElement.GetComponent<ActionClass>()._value4 = (float.Parse(tpArea3Action.text) * 1000f * 60f * 60f).ToString();
                                        _scaleTXT = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='h_scale']").InnerText;
                                    }
                                }
                                else
                                {
                                    _actualElement.GetComponent<ActionClass>()._value4 = tpArea3Action.text;
                                }

                                _actualElement.GetComponent<ActionClass>()._index4 = 0;
                                _actualElement.GetComponent<ActionClass>()._scale = scaleTPArea3;
                                _actualElement.GetComponent<ActionClass>()._constant1 = false;
                                _actualElement.GetComponent<ActionClass>()._constant2 = false;
                                _actualElement.GetComponent<ActionClass>()._options = "";
                                _actualElement.GetComponent<ActionClass>()._editorType = 1;
                                _actualElement.GetComponent<ActionClass>()._isReset = false;

                                _actualElement.GetComponent<ActionClass>()._textElement.text = _actualElement.GetComponent<ActionClass>()._value2 + " " + _actualElement.GetComponent<ActionClass>()._value + "=" + tpArea3Action.text + _scaleTXT;
                            }
                            break;
                        case 4:
                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area4Action.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area4Action.getIndex();

                            if (_resetArea4Action.isOn == true)
                            {
                                _actualElement.GetComponent<ActionClass>()._isInverse = false;
                                _actualElement.GetComponent<ActionClass>()._value2 = "";
                                _actualElement.GetComponent<ActionClass>()._index2 = 0;
                                _actualElement.GetComponent<ActionClass>()._value3 = "";
                                _actualElement.GetComponent<ActionClass>()._index3 = 0;
                                _actualElement.GetComponent<ActionClass>()._value4 = "";
                                _actualElement.GetComponent<ActionClass>()._index4 = 0;
                                _actualElement.GetComponent<ActionClass>()._scale = 0;
                                _actualElement.GetComponent<ActionClass>()._constant1 = false;
                                _actualElement.GetComponent<ActionClass>()._constant2 = false;
                                _actualElement.GetComponent<ActionClass>()._options = "";
                                _actualElement.GetComponent<ActionClass>()._editorType = 1;
                                _actualElement.GetComponent<ActionClass>()._isReset = true;

                                _actualElement.GetComponent<ActionClass>()._textElement.text = "RES(" + _selector1Area4Action.getValue() + ")";
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>()._isInverse = false;
                                _actualElement.GetComponent<ActionClass>()._value2 = _selector2Area4Action.getValue();
                                _actualElement.GetComponent<ActionClass>()._index2 = _selector2Area4Action.getIndex();
                                _actualElement.GetComponent<ActionClass>()._value3 = _selector3Area4Action.getValue();
                                _actualElement.GetComponent<ActionClass>()._index3 = _selector3Area4Action.getIndex();

                                _actualElement.GetComponent<ActionClass>()._value4 = pvArea4Action.text;

                                _actualElement.GetComponent<ActionClass>()._index4 = 0;
                                _actualElement.GetComponent<ActionClass>()._scale = 0;
                                _actualElement.GetComponent<ActionClass>()._constant1 = false;
                                _actualElement.GetComponent<ActionClass>()._constant2 = false;
                                _actualElement.GetComponent<ActionClass>()._options = "";
                                _actualElement.GetComponent<ActionClass>()._editorType = 1;
                                _actualElement.GetComponent<ActionClass>()._isReset = false;

                                _actualElement.GetComponent<ActionClass>()._textElement.text = _actualElement.GetComponent<ActionClass>()._value2 + " " + _actualElement.GetComponent<ActionClass>()._value + "=" + _actualElement.GetComponent<ActionClass>()._value4;
                            }
                            break;
                        case 5:
                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area5Action.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area5Action.getIndex();
                            _actualElement.GetComponent<ActionClass>()._isInverse = false;

                            if (_input2SelectorArea5Action.isOn == true)
                            {
                                _actualElement.GetComponent<ActionClass>()._value2 = _input2Area5Action.text;
                                _actualElement.GetComponent<ActionClass>()._index2 = 0;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>()._value2 = _selector2Area5Action.getValue();
                                _actualElement.GetComponent<ActionClass>()._index2 = _selector2Area5Action.getIndex();
                            }

                            if (_input3SelectorArea5Action.isOn == true)
                            {
                                _actualElement.GetComponent<ActionClass>()._value3 = _input3Area5Action.text;
                                _actualElement.GetComponent<ActionClass>()._index3 = 0;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>()._value3 = _selector3Area5Action.getValue();
                                _actualElement.GetComponent<ActionClass>()._index3 = _selector3Area5Action.getIndex();
                            }

                            _actualElement.GetComponent<ActionClass>()._value4 = _selector4Area5Action.getValue();
                            _actualElement.GetComponent<ActionClass>()._index4 = _selector4Area5Action.getIndex();

                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = _input2SelectorArea5Action.isOn;
                            _actualElement.GetComponent<ActionClass>()._constant2 = _input3SelectorArea5Action.isOn;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            _actualElement.GetComponent<ActionClass>()._textElement.text = _actualElement.GetComponent<ActionClass>()._value2 + " " + _actualElement.GetComponent<ActionClass>()._value + " " + _actualElement.GetComponent<ActionClass>()._value3;
                            break;
                        case 6:
                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area6Action.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area6Action.getIndex();
                            _actualElement.GetComponent<ActionClass>()._isInverse = false;

                            if (_input2SelectorArea6Action.isOn == true)
                            {
                                _actualElement.GetComponent<ActionClass>()._value2 = _input2Area6Action.text;
                                _actualElement.GetComponent<ActionClass>()._index2 = 0;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>()._value2 = _selector2Area6Action.getValue();
                                _actualElement.GetComponent<ActionClass>()._index2 = _selector2Area6Action.getIndex();
                            }

                            if (_input3SelectorArea6Action.isOn == true)
                            {
                                _actualElement.GetComponent<ActionClass>()._value3 = _input3Area6Action.text;
                                _actualElement.GetComponent<ActionClass>()._index3 = 0;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>()._value3 = _selector3Area6Action.getValue();
                                _actualElement.GetComponent<ActionClass>()._index3 = _selector3Area6Action.getIndex();
                            }

                            _actualElement.GetComponent<ActionClass>()._value4 = _selector4Area6Action.getValue();
                            _actualElement.GetComponent<ActionClass>()._index4 = _selector4Area6Action.getIndex();

                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = _input2SelectorArea6Action.isOn;
                            _actualElement.GetComponent<ActionClass>()._constant2 = _input3SelectorArea6Action.isOn;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            if (_actualElement.GetComponent<ActionClass>()._index >= 3)
                            {
                                _actualElement.GetComponent<ActionClass>()._textElement.text = _actualElement.GetComponent<ActionClass>()._value2 + " " + _actualElement.GetComponent<ActionClass>()._value + " " + _actualElement.GetComponent<ActionClass>()._value4;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>()._textElement.text = _actualElement.GetComponent<ActionClass>()._value2 + " " + _actualElement.GetComponent<ActionClass>()._value + " " + _actualElement.GetComponent<ActionClass>()._value3;
                            }
                            break;
                    }

            }
            else
            {
                    switch (buttonActive){
                        case 7:
                            //ouputs
                            _actualElement.GetComponent<ActionClass>()._type = 1;
                            _actualElement.GetComponent<ActionClass>()._typeRobotic = 2;

                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area4ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area4ActionRob.getIndex();

                            _actualElement.GetComponent<ActionClass>()._isInverse = false;
                            _actualElement.GetComponent<ActionClass>()._value2 = "";
                            _actualElement.GetComponent<ActionClass>()._index2 = 0;
                            _actualElement.GetComponent<ActionClass>()._value3 = "";
                            _actualElement.GetComponent<ActionClass>()._index3 = 0;
                            _actualElement.GetComponent<ActionClass>()._value4 = "";
                            _actualElement.GetComponent<ActionClass>()._index4 = 0;
                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = false;
                            _actualElement.GetComponent<ActionClass>()._constant2 = false;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            _actualElement.GetComponent<ActionClass>().childActions = new ActionClass[1];
                            _actualElement.GetComponent<ActionClass>().childActions[0] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[0]._type = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[0].childName = _actualElement.GetComponent<ActionClass>().name;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._value = "O1";
                            _actualElement.GetComponent<ActionClass>().childActions[0]._index = 1;

                            if (_selector1Area4ActionRob.getIndex() == 0)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = true;
                            } else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = false;
                            }

                            _actualElement.GetComponent<ActionClass>()._textElement.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_motor_band']").InnerText;
                            break;
                        case 10:
                            _actualElement.GetComponent<ActionClass>()._type = 1;
                            _actualElement.GetComponent<ActionClass>()._typeRobotic = 1;

                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area4ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area4ActionRob.getIndex();

                            _actualElement.GetComponent<ActionClass>()._isInverse = false;
                            _actualElement.GetComponent<ActionClass>()._value2 = "";
                            _actualElement.GetComponent<ActionClass>()._index2 = 0;
                            _actualElement.GetComponent<ActionClass>()._value3 = "";
                            _actualElement.GetComponent<ActionClass>()._index3 = 0;
                            _actualElement.GetComponent<ActionClass>()._value4 = "";
                            _actualElement.GetComponent<ActionClass>()._index4 = 0;
                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = false;
                            _actualElement.GetComponent<ActionClass>()._constant2 = false;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            _actualElement.GetComponent<ActionClass>().childActions = new ActionClass[1];
                            _actualElement.GetComponent<ActionClass>().childActions[0] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[0]._type = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[0].childName = _actualElement.GetComponent<ActionClass>().name;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._value = "O2";
                            _actualElement.GetComponent<ActionClass>().childActions[0]._index = 2;

                            if (_selector1Area4ActionRob.getIndex() == 0)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = true;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = false;
                            }
                            _actualElement.GetComponent<ActionClass>()._textElement.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_gripper']").InnerText;
                            break;
                        case 11:
                            _actualElement.GetComponent<ActionClass>()._type = 1;
                            _actualElement.GetComponent<ActionClass>()._typeRobotic = 3;

                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area4ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area4ActionRob.getIndex();

                            _actualElement.GetComponent<ActionClass>()._isInverse = false;
                            _actualElement.GetComponent<ActionClass>()._value2 = "";
                            _actualElement.GetComponent<ActionClass>()._index2 = 0;
                            _actualElement.GetComponent<ActionClass>()._value3 = "";
                            _actualElement.GetComponent<ActionClass>()._index3 = 0;
                            _actualElement.GetComponent<ActionClass>()._value4 = "";
                            _actualElement.GetComponent<ActionClass>()._index4 = 0;
                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = false;
                            _actualElement.GetComponent<ActionClass>()._constant2 = false;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            _actualElement.GetComponent<ActionClass>().childActions = new ActionClass[1];
                            _actualElement.GetComponent<ActionClass>().childActions[0] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[0]._type = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[0].childName = _actualElement.GetComponent<ActionClass>().name;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._value = "O3";
                            _actualElement.GetComponent<ActionClass>().childActions[0]._index = 3;

                            if (_selector1Area4ActionRob.getIndex() == 0)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = true;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = false;
                            }
                            _actualElement.GetComponent<ActionClass>()._textElement.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_bastidor_z']").InnerText;
                            break;
                        case 12:
                            _actualElement.GetComponent<ActionClass>()._type = 1;
                            _actualElement.GetComponent<ActionClass>()._typeRobotic = 4;

                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area4ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area4ActionRob.getIndex();

                            _actualElement.GetComponent<ActionClass>()._isInverse = false;
                            _actualElement.GetComponent<ActionClass>()._value2 = "";
                            _actualElement.GetComponent<ActionClass>()._index2 = 0;
                            _actualElement.GetComponent<ActionClass>()._value3 = "";
                            _actualElement.GetComponent<ActionClass>()._index3 = 0;
                            _actualElement.GetComponent<ActionClass>()._value4 = "";
                            _actualElement.GetComponent<ActionClass>()._index4 = 0;
                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = false;
                            _actualElement.GetComponent<ActionClass>()._constant2 = false;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            _actualElement.GetComponent<ActionClass>().childActions = new ActionClass[1];
                            _actualElement.GetComponent<ActionClass>().childActions[0] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[0]._type = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[0].childName = _actualElement.GetComponent<ActionClass>().name;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._value = "O4";
                            _actualElement.GetComponent<ActionClass>().childActions[0]._index = 4;

                            if (_selector1Area4ActionRob.getIndex() == 0)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = true;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = false;
                            }
                            _actualElement.GetComponent<ActionClass>()._textElement.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_eje_5']").InnerText;
                            break;
                        case 1:
                            //memory
                            _actualElement.GetComponent<ActionClass>()._type = 2;
                            _actualElement.GetComponent<ActionClass>()._typeRobotic = 3;

                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area1ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area1ActionRob.getIndex();

                            _actualElement.GetComponent<ActionClass>()._isInverse = false;
                            _actualElement.GetComponent<ActionClass>()._value2 = _selector2Area1ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index2 = _selector2Area1ActionRob.getIndex();
                            _actualElement.GetComponent<ActionClass>()._value3 = "";
                            _actualElement.GetComponent<ActionClass>()._index3 = 0;
                            _actualElement.GetComponent<ActionClass>()._value4 = "";
                            _actualElement.GetComponent<ActionClass>()._index4 = 0;
                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = false;
                            _actualElement.GetComponent<ActionClass>()._constant2 = false;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            _actualElement.GetComponent<ActionClass>()._textElement.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_motor_a']").InnerText;

                            //Robotic module:
                            //A part:
                            _actualElement.GetComponent<ActionClass>().childActions = new ActionClass[16];

                            _actualElement.GetComponent<ActionClass>().childActions[0] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[1] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[2] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[3] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[4] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[5] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[6] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[7] = new ActionClass();

                            _actualElement.GetComponent<ActionClass>().childActions[0]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._type = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[0].childName = _actualElement.GetComponent<ActionClass>().name;
                            _actualElement.GetComponent<ActionClass>().childActions[1].childName = _actualElement.GetComponent<ActionClass>().name + "_A";
                            _actualElement.GetComponent<ActionClass>().childActions[2].childName = _actualElement.GetComponent<ActionClass>().name + "_B";
                            _actualElement.GetComponent<ActionClass>().childActions[3].childName = _actualElement.GetComponent<ActionClass>().name + "_C";
                            _actualElement.GetComponent<ActionClass>().childActions[4].childName = _actualElement.GetComponent<ActionClass>().name + "_D";
                            _actualElement.GetComponent<ActionClass>().childActions[5].childName = _actualElement.GetComponent<ActionClass>().name + "_E";
                            _actualElement.GetComponent<ActionClass>().childActions[6].childName = _actualElement.GetComponent<ActionClass>().name + "_F";
                            _actualElement.GetComponent<ActionClass>().childActions[7].childName = _actualElement.GetComponent<ActionClass>().name + "_G";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._value = "M4";
                            _actualElement.GetComponent<ActionClass>().childActions[1]._value = "M4";
                            _actualElement.GetComponent<ActionClass>().childActions[2]._value = "M4";
                            _actualElement.GetComponent<ActionClass>().childActions[3]._value = "M4";
                            _actualElement.GetComponent<ActionClass>().childActions[4]._value = "M4";
                            _actualElement.GetComponent<ActionClass>().childActions[5]._value = "M4";
                            _actualElement.GetComponent<ActionClass>().childActions[6]._value = "M4";
                            _actualElement.GetComponent<ActionClass>().childActions[7]._value = "M4";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._index = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._index = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._index = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._index = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._index = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._index = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._index = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._index = 4;

                            _actualElement.GetComponent<ActionClass>().childActions[0]._value2 = "0";
                            _actualElement.GetComponent<ActionClass>().childActions[1]._value2 = "1";
                            _actualElement.GetComponent<ActionClass>().childActions[2]._value2 = "2";
                            _actualElement.GetComponent<ActionClass>().childActions[3]._value2 = "3";
                            _actualElement.GetComponent<ActionClass>().childActions[4]._value2 = "4";
                            _actualElement.GetComponent<ActionClass>().childActions[5]._value2 = "5";
                            _actualElement.GetComponent<ActionClass>().childActions[6]._value2 = "6";
                            _actualElement.GetComponent<ActionClass>().childActions[7]._value2 = "7";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._index2 = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._index2 = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._index2 = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._index2 = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._index2 = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._index2 = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._index2 = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._index2 = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[0].nextAction = _actualElement.GetComponent<ActionClass>().name + "_A";
                            _actualElement.GetComponent<ActionClass>().childActions[1].nextAction = _actualElement.GetComponent<ActionClass>().name + "_B";
                            _actualElement.GetComponent<ActionClass>().childActions[2].nextAction = _actualElement.GetComponent<ActionClass>().name + "_C";
                            _actualElement.GetComponent<ActionClass>().childActions[3].nextAction = _actualElement.GetComponent<ActionClass>().name + "_D";
                            _actualElement.GetComponent<ActionClass>().childActions[4].nextAction = _actualElement.GetComponent<ActionClass>().name + "_E";
                            _actualElement.GetComponent<ActionClass>().childActions[5].nextAction = _actualElement.GetComponent<ActionClass>().name + "_F";
                            _actualElement.GetComponent<ActionClass>().childActions[6].nextAction = _actualElement.GetComponent<ActionClass>().name + "_G";
                            _actualElement.GetComponent<ActionClass>().childActions[7].nextAction = _actualElement.GetComponent<ActionClass>().name + "_H";

                            BitArray valueEngADirBits = new BitArray(new int[] { _selector1Area1ActionRob.getIndex() });

                            if (valueEngADirBits[0] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = true;
                            }

                            if (valueEngADirBits[1] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[1]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[1]._isInverse = true;
                            }

                            _actualElement.GetComponent<ActionClass>().childActions[2]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._isInverse = true;
                                                        
                            //Part B
                            _actualElement.GetComponent<ActionClass>().childActions[8] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[9] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[10] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[11] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[12] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[13] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[14] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[15] = new ActionClass();

                            _actualElement.GetComponent<ActionClass>().childActions[8]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._type = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[8].childName = _actualElement.GetComponent<ActionClass>().name + "_H";
                            _actualElement.GetComponent<ActionClass>().childActions[9].childName = _actualElement.GetComponent<ActionClass>().name + "_I";
                            _actualElement.GetComponent<ActionClass>().childActions[10].childName = _actualElement.GetComponent<ActionClass>().name + "_J";
                            _actualElement.GetComponent<ActionClass>().childActions[11].childName = _actualElement.GetComponent<ActionClass>().name + "_K";
                            _actualElement.GetComponent<ActionClass>().childActions[12].childName = _actualElement.GetComponent<ActionClass>().name + "_L";
                            _actualElement.GetComponent<ActionClass>().childActions[13].childName = _actualElement.GetComponent<ActionClass>().name + "_M";
                            _actualElement.GetComponent<ActionClass>().childActions[14].childName = _actualElement.GetComponent<ActionClass>().name + "_N";
                            _actualElement.GetComponent<ActionClass>().childActions[15].childName = _actualElement.GetComponent<ActionClass>().name + "_O";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._value = "M0";
                            _actualElement.GetComponent<ActionClass>().childActions[9]._value = "M0";
                            _actualElement.GetComponent<ActionClass>().childActions[10]._value = "M0";
                            _actualElement.GetComponent<ActionClass>().childActions[11]._value = "M0";
                            _actualElement.GetComponent<ActionClass>().childActions[12]._value = "M0";
                            _actualElement.GetComponent<ActionClass>().childActions[13]._value = "M0";
                            _actualElement.GetComponent<ActionClass>().childActions[14]._value = "M0";
                            _actualElement.GetComponent<ActionClass>().childActions[15]._value = "M0";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._index = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._index = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._index = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._index = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._index = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._index = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._index = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._index = 0;

                            _actualElement.GetComponent<ActionClass>().childActions[8]._value2 = "0";
                            _actualElement.GetComponent<ActionClass>().childActions[9]._value2 = "1";
                            _actualElement.GetComponent<ActionClass>().childActions[10]._value2 = "2";
                            _actualElement.GetComponent<ActionClass>().childActions[11]._value2 = "3";
                            _actualElement.GetComponent<ActionClass>().childActions[12]._value2 = "4";
                            _actualElement.GetComponent<ActionClass>().childActions[13]._value2 = "5";
                            _actualElement.GetComponent<ActionClass>().childActions[14]._value2 = "6";
                            _actualElement.GetComponent<ActionClass>().childActions[15]._value2 = "7";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._index2 = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._index2 = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._index2 = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._index2 = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._index2 = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._index2 = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._index2 = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._index2 = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[8].nextAction = _actualElement.GetComponent<ActionClass>().name + "_I";
                            _actualElement.GetComponent<ActionClass>().childActions[9].nextAction = _actualElement.GetComponent<ActionClass>().name + "_J";
                            _actualElement.GetComponent<ActionClass>().childActions[10].nextAction = _actualElement.GetComponent<ActionClass>().name + "_K";
                            _actualElement.GetComponent<ActionClass>().childActions[11].nextAction = _actualElement.GetComponent<ActionClass>().name + "_L";
                            _actualElement.GetComponent<ActionClass>().childActions[12].nextAction = _actualElement.GetComponent<ActionClass>().name + "_M";
                            _actualElement.GetComponent<ActionClass>().childActions[13].nextAction = _actualElement.GetComponent<ActionClass>().name + "_N";
                            _actualElement.GetComponent<ActionClass>().childActions[14].nextAction = _actualElement.GetComponent<ActionClass>().name + "_O";
                            //LAST ELEMENT TO NEXT ACTION

                            BitArray valueEngAVelBits = new BitArray(new int[] { _selector2Area1ActionRob.getIndex() });

                            if (valueEngAVelBits[0] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[8]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[8]._isInverse = true;
                            }
                            if (valueEngAVelBits[1] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[9]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[9]._isInverse = true;
                            }
                            if (valueEngAVelBits[2] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[10]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[10]._isInverse = true;
                            }
                            if (valueEngAVelBits[3] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[11]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[11]._isInverse = true;
                            }
                            _actualElement.GetComponent<ActionClass>().childActions[12]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._isInverse = true;

                            break;
                        case 2:
                            //memory
                            _actualElement.GetComponent<ActionClass>()._type = 2;
                            _actualElement.GetComponent<ActionClass>()._typeRobotic = 5;

                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area1ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area1ActionRob.getIndex();

                            _actualElement.GetComponent<ActionClass>()._isInverse = false;
                            _actualElement.GetComponent<ActionClass>()._value2 = _selector2Area1ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index2 = _selector2Area1ActionRob.getIndex();
                            _actualElement.GetComponent<ActionClass>()._value3 = "";
                            _actualElement.GetComponent<ActionClass>()._index3 = 0;
                            _actualElement.GetComponent<ActionClass>()._value4 = "";
                            _actualElement.GetComponent<ActionClass>()._index4 = 0;
                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = false;
                            _actualElement.GetComponent<ActionClass>()._constant2 = false;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            _actualElement.GetComponent<ActionClass>()._textElement.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_motor_c']").InnerText;

                            //Robotic module:
                            //A part:
                            _actualElement.GetComponent<ActionClass>().childActions = new ActionClass[16];

                            _actualElement.GetComponent<ActionClass>().childActions[0] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[1] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[2] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[3] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[4] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[5] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[6] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[7] = new ActionClass();

                            _actualElement.GetComponent<ActionClass>().childActions[0]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._type = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[0].childName = _actualElement.GetComponent<ActionClass>().name;
                            _actualElement.GetComponent<ActionClass>().childActions[1].childName = _actualElement.GetComponent<ActionClass>().name + "_A";
                            _actualElement.GetComponent<ActionClass>().childActions[2].childName = _actualElement.GetComponent<ActionClass>().name + "_B";
                            _actualElement.GetComponent<ActionClass>().childActions[3].childName = _actualElement.GetComponent<ActionClass>().name + "_C";
                            _actualElement.GetComponent<ActionClass>().childActions[4].childName = _actualElement.GetComponent<ActionClass>().name + "_D";
                            _actualElement.GetComponent<ActionClass>().childActions[5].childName = _actualElement.GetComponent<ActionClass>().name + "_E";
                            _actualElement.GetComponent<ActionClass>().childActions[6].childName = _actualElement.GetComponent<ActionClass>().name + "_F";
                            _actualElement.GetComponent<ActionClass>().childActions[7].childName = _actualElement.GetComponent<ActionClass>().name + "_G";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._value = "M6";
                            _actualElement.GetComponent<ActionClass>().childActions[1]._value = "M6";
                            _actualElement.GetComponent<ActionClass>().childActions[2]._value = "M6";
                            _actualElement.GetComponent<ActionClass>().childActions[3]._value = "M6";
                            _actualElement.GetComponent<ActionClass>().childActions[4]._value = "M6";
                            _actualElement.GetComponent<ActionClass>().childActions[5]._value = "M6";
                            _actualElement.GetComponent<ActionClass>().childActions[6]._value = "M6";
                            _actualElement.GetComponent<ActionClass>().childActions[7]._value = "M6";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._index = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._index = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._index = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._index = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._index = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._index = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._index = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._index = 6;

                            _actualElement.GetComponent<ActionClass>().childActions[0]._value2 = "0";
                            _actualElement.GetComponent<ActionClass>().childActions[1]._value2 = "1";
                            _actualElement.GetComponent<ActionClass>().childActions[2]._value2 = "2";
                            _actualElement.GetComponent<ActionClass>().childActions[3]._value2 = "3";
                            _actualElement.GetComponent<ActionClass>().childActions[4]._value2 = "4";
                            _actualElement.GetComponent<ActionClass>().childActions[5]._value2 = "5";
                            _actualElement.GetComponent<ActionClass>().childActions[6]._value2 = "6";
                            _actualElement.GetComponent<ActionClass>().childActions[7]._value2 = "7";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._index2 = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._index2 = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._index2 = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._index2 = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._index2 = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._index2 = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._index2 = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._index2 = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[0].nextAction = _actualElement.GetComponent<ActionClass>().name + "_A";
                            _actualElement.GetComponent<ActionClass>().childActions[1].nextAction = _actualElement.GetComponent<ActionClass>().name + "_B";
                            _actualElement.GetComponent<ActionClass>().childActions[2].nextAction = _actualElement.GetComponent<ActionClass>().name + "_C";
                            _actualElement.GetComponent<ActionClass>().childActions[3].nextAction = _actualElement.GetComponent<ActionClass>().name + "_D";
                            _actualElement.GetComponent<ActionClass>().childActions[4].nextAction = _actualElement.GetComponent<ActionClass>().name + "_E";
                            _actualElement.GetComponent<ActionClass>().childActions[5].nextAction = _actualElement.GetComponent<ActionClass>().name + "_F";
                            _actualElement.GetComponent<ActionClass>().childActions[6].nextAction = _actualElement.GetComponent<ActionClass>().name + "_G";
                            _actualElement.GetComponent<ActionClass>().childActions[7].nextAction = _actualElement.GetComponent<ActionClass>().name + "_H";

                            BitArray valueEngCDirBits = new BitArray(new int[] { _selector1Area1ActionRob.getIndex() });

                            if (valueEngCDirBits[0] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = true;
                            }

                            if (valueEngCDirBits[1] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[1]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[1]._isInverse = true;
                            }

                            _actualElement.GetComponent<ActionClass>().childActions[2]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._isInverse = true;

                            //Part B
                            _actualElement.GetComponent<ActionClass>().childActions[8] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[9] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[10] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[11] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[12] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[13] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[14] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[15] = new ActionClass();

                            _actualElement.GetComponent<ActionClass>().childActions[8]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._type = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[8].childName = _actualElement.GetComponent<ActionClass>().name + "_H";
                            _actualElement.GetComponent<ActionClass>().childActions[9].childName = _actualElement.GetComponent<ActionClass>().name + "_I";
                            _actualElement.GetComponent<ActionClass>().childActions[10].childName = _actualElement.GetComponent<ActionClass>().name + "_J";
                            _actualElement.GetComponent<ActionClass>().childActions[11].childName = _actualElement.GetComponent<ActionClass>().name + "_K";
                            _actualElement.GetComponent<ActionClass>().childActions[12].childName = _actualElement.GetComponent<ActionClass>().name + "_L";
                            _actualElement.GetComponent<ActionClass>().childActions[13].childName = _actualElement.GetComponent<ActionClass>().name + "_M";
                            _actualElement.GetComponent<ActionClass>().childActions[14].childName = _actualElement.GetComponent<ActionClass>().name + "_N";
                            _actualElement.GetComponent<ActionClass>().childActions[15].childName = _actualElement.GetComponent<ActionClass>().name + "_O";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._value = "M2";
                            _actualElement.GetComponent<ActionClass>().childActions[9]._value = "M2";
                            _actualElement.GetComponent<ActionClass>().childActions[10]._value = "M2";
                            _actualElement.GetComponent<ActionClass>().childActions[11]._value = "M2";
                            _actualElement.GetComponent<ActionClass>().childActions[12]._value = "M2";
                            _actualElement.GetComponent<ActionClass>().childActions[13]._value = "M2";
                            _actualElement.GetComponent<ActionClass>().childActions[14]._value = "M2";
                            _actualElement.GetComponent<ActionClass>().childActions[15]._value = "M2";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._index = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._index = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._index = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._index = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._index = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._index = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._index = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._index = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[8]._value2 = "0";
                            _actualElement.GetComponent<ActionClass>().childActions[9]._value2 = "1";
                            _actualElement.GetComponent<ActionClass>().childActions[10]._value2 = "2";
                            _actualElement.GetComponent<ActionClass>().childActions[11]._value2 = "3";
                            _actualElement.GetComponent<ActionClass>().childActions[12]._value2 = "4";
                            _actualElement.GetComponent<ActionClass>().childActions[13]._value2 = "5";
                            _actualElement.GetComponent<ActionClass>().childActions[14]._value2 = "6";
                            _actualElement.GetComponent<ActionClass>().childActions[15]._value2 = "7";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._index2 = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._index2 = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._index2 = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._index2 = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._index2 = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._index2 = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._index2 = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._index2 = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[8].nextAction = _actualElement.GetComponent<ActionClass>().name + "_I";
                            _actualElement.GetComponent<ActionClass>().childActions[9].nextAction = _actualElement.GetComponent<ActionClass>().name + "_J";
                            _actualElement.GetComponent<ActionClass>().childActions[10].nextAction = _actualElement.GetComponent<ActionClass>().name + "_K";
                            _actualElement.GetComponent<ActionClass>().childActions[11].nextAction = _actualElement.GetComponent<ActionClass>().name + "_L";
                            _actualElement.GetComponent<ActionClass>().childActions[12].nextAction = _actualElement.GetComponent<ActionClass>().name + "_M";
                            _actualElement.GetComponent<ActionClass>().childActions[13].nextAction = _actualElement.GetComponent<ActionClass>().name + "_N";
                            _actualElement.GetComponent<ActionClass>().childActions[14].nextAction = _actualElement.GetComponent<ActionClass>().name + "_O";
                            //LAST ELEMENT TO NEXT ACTION

                            BitArray valueEngCVelBits = new BitArray(new int[] { _selector2Area1ActionRob.getIndex() });

                            if (valueEngCVelBits[0] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[8]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[8]._isInverse = true;
                            }
                            if (valueEngCVelBits[1] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[9]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[9]._isInverse = true;
                            }
                            if (valueEngCVelBits[2] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[10]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[10]._isInverse = true;
                            }
                            if (valueEngCVelBits[3] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[11]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[11]._isInverse = true;
                            }
                            _actualElement.GetComponent<ActionClass>().childActions[12]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._isInverse = true;
                            break;
                        case 4:
                            //memory
                            _actualElement.GetComponent<ActionClass>()._type = 2;
                            _actualElement.GetComponent<ActionClass>()._typeRobotic = 4;

                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area1ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area1ActionRob.getIndex();

                            _actualElement.GetComponent<ActionClass>()._isInverse = false;
                            _actualElement.GetComponent<ActionClass>()._value2 = _selector2Area1ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index2 = _selector2Area1ActionRob.getIndex();
                            _actualElement.GetComponent<ActionClass>()._value3 = "";
                            _actualElement.GetComponent<ActionClass>()._index3 = 0;
                            _actualElement.GetComponent<ActionClass>()._value4 = "";
                            _actualElement.GetComponent<ActionClass>()._index4 = 0;
                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = false;
                            _actualElement.GetComponent<ActionClass>()._constant2 = false;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            _actualElement.GetComponent<ActionClass>()._textElement.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_motor_b']").InnerText;

                            //Robotic module:
                            //A part:
                            _actualElement.GetComponent<ActionClass>().childActions = new ActionClass[16];

                            _actualElement.GetComponent<ActionClass>().childActions[0] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[1] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[2] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[3] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[4] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[5] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[6] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[7] = new ActionClass();

                            _actualElement.GetComponent<ActionClass>().childActions[0]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._type = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[0].childName = _actualElement.GetComponent<ActionClass>().name;
                            _actualElement.GetComponent<ActionClass>().childActions[1].childName = _actualElement.GetComponent<ActionClass>().name + "_A";
                            _actualElement.GetComponent<ActionClass>().childActions[2].childName = _actualElement.GetComponent<ActionClass>().name + "_B";
                            _actualElement.GetComponent<ActionClass>().childActions[3].childName = _actualElement.GetComponent<ActionClass>().name + "_C";
                            _actualElement.GetComponent<ActionClass>().childActions[4].childName = _actualElement.GetComponent<ActionClass>().name + "_D";
                            _actualElement.GetComponent<ActionClass>().childActions[5].childName = _actualElement.GetComponent<ActionClass>().name + "_E";
                            _actualElement.GetComponent<ActionClass>().childActions[6].childName = _actualElement.GetComponent<ActionClass>().name + "_F";
                            _actualElement.GetComponent<ActionClass>().childActions[7].childName = _actualElement.GetComponent<ActionClass>().name + "_G";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._value = "M5";
                            _actualElement.GetComponent<ActionClass>().childActions[1]._value = "M5";
                            _actualElement.GetComponent<ActionClass>().childActions[2]._value = "M5";
                            _actualElement.GetComponent<ActionClass>().childActions[3]._value = "M5";
                            _actualElement.GetComponent<ActionClass>().childActions[4]._value = "M5";
                            _actualElement.GetComponent<ActionClass>().childActions[5]._value = "M5";
                            _actualElement.GetComponent<ActionClass>().childActions[6]._value = "M5";
                            _actualElement.GetComponent<ActionClass>().childActions[7]._value = "M5";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._index = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._index = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._index = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._index = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._index = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._index = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._index = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._index = 5;

                            _actualElement.GetComponent<ActionClass>().childActions[0]._value2 = "0";
                            _actualElement.GetComponent<ActionClass>().childActions[1]._value2 = "1";
                            _actualElement.GetComponent<ActionClass>().childActions[2]._value2 = "2";
                            _actualElement.GetComponent<ActionClass>().childActions[3]._value2 = "3";
                            _actualElement.GetComponent<ActionClass>().childActions[4]._value2 = "4";
                            _actualElement.GetComponent<ActionClass>().childActions[5]._value2 = "5";
                            _actualElement.GetComponent<ActionClass>().childActions[6]._value2 = "6";
                            _actualElement.GetComponent<ActionClass>().childActions[7]._value2 = "7";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._index2 = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._index2 = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._index2 = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._index2 = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._index2 = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._index2 = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._index2 = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._index2 = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[0].nextAction = _actualElement.GetComponent<ActionClass>().name + "_A";
                            _actualElement.GetComponent<ActionClass>().childActions[1].nextAction = _actualElement.GetComponent<ActionClass>().name + "_B";
                            _actualElement.GetComponent<ActionClass>().childActions[2].nextAction = _actualElement.GetComponent<ActionClass>().name + "_C";
                            _actualElement.GetComponent<ActionClass>().childActions[3].nextAction = _actualElement.GetComponent<ActionClass>().name + "_D";
                            _actualElement.GetComponent<ActionClass>().childActions[4].nextAction = _actualElement.GetComponent<ActionClass>().name + "_E";
                            _actualElement.GetComponent<ActionClass>().childActions[5].nextAction = _actualElement.GetComponent<ActionClass>().name + "_F";
                            _actualElement.GetComponent<ActionClass>().childActions[6].nextAction = _actualElement.GetComponent<ActionClass>().name + "_G";
                            _actualElement.GetComponent<ActionClass>().childActions[7].nextAction = _actualElement.GetComponent<ActionClass>().name + "_H";

                            BitArray valueEngBDirBits = new BitArray(new int[] { _selector1Area1ActionRob.getIndex() });

                            if (valueEngBDirBits[0] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = true;
                            }

                            if (valueEngBDirBits[1] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[1]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[1]._isInverse = true;
                            }

                            _actualElement.GetComponent<ActionClass>().childActions[2]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._isInverse = true;

                            //Part B
                            _actualElement.GetComponent<ActionClass>().childActions[8] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[9] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[10] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[11] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[12] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[13] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[14] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[15] = new ActionClass();

                            _actualElement.GetComponent<ActionClass>().childActions[8]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._type = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[8].childName = _actualElement.GetComponent<ActionClass>().name + "_H";
                            _actualElement.GetComponent<ActionClass>().childActions[9].childName = _actualElement.GetComponent<ActionClass>().name + "_I";
                            _actualElement.GetComponent<ActionClass>().childActions[10].childName = _actualElement.GetComponent<ActionClass>().name + "_J";
                            _actualElement.GetComponent<ActionClass>().childActions[11].childName = _actualElement.GetComponent<ActionClass>().name + "_K";
                            _actualElement.GetComponent<ActionClass>().childActions[12].childName = _actualElement.GetComponent<ActionClass>().name + "_L";
                            _actualElement.GetComponent<ActionClass>().childActions[13].childName = _actualElement.GetComponent<ActionClass>().name + "_M";
                            _actualElement.GetComponent<ActionClass>().childActions[14].childName = _actualElement.GetComponent<ActionClass>().name + "_N";
                            _actualElement.GetComponent<ActionClass>().childActions[15].childName = _actualElement.GetComponent<ActionClass>().name + "_O";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._value = "M1";
                            _actualElement.GetComponent<ActionClass>().childActions[9]._value = "M1";
                            _actualElement.GetComponent<ActionClass>().childActions[10]._value = "M1";
                            _actualElement.GetComponent<ActionClass>().childActions[11]._value = "M1";
                            _actualElement.GetComponent<ActionClass>().childActions[12]._value = "M1";
                            _actualElement.GetComponent<ActionClass>().childActions[13]._value = "M1";
                            _actualElement.GetComponent<ActionClass>().childActions[14]._value = "M1";
                            _actualElement.GetComponent<ActionClass>().childActions[15]._value = "M1";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._index = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._index = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._index = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._index = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._index = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._index = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._index = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._index = 1;

                            _actualElement.GetComponent<ActionClass>().childActions[8]._value2 = "0";
                            _actualElement.GetComponent<ActionClass>().childActions[9]._value2 = "1";
                            _actualElement.GetComponent<ActionClass>().childActions[10]._value2 = "2";
                            _actualElement.GetComponent<ActionClass>().childActions[11]._value2 = "3";
                            _actualElement.GetComponent<ActionClass>().childActions[12]._value2 = "4";
                            _actualElement.GetComponent<ActionClass>().childActions[13]._value2 = "5";
                            _actualElement.GetComponent<ActionClass>().childActions[14]._value2 = "6";
                            _actualElement.GetComponent<ActionClass>().childActions[15]._value2 = "7";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._index2 = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._index2 = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._index2 = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._index2 = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._index2 = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._index2 = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._index2 = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._index2 = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[8].nextAction = _actualElement.GetComponent<ActionClass>().name + "_I";
                            _actualElement.GetComponent<ActionClass>().childActions[9].nextAction = _actualElement.GetComponent<ActionClass>().name + "_J";
                            _actualElement.GetComponent<ActionClass>().childActions[10].nextAction = _actualElement.GetComponent<ActionClass>().name + "_K";
                            _actualElement.GetComponent<ActionClass>().childActions[11].nextAction = _actualElement.GetComponent<ActionClass>().name + "_L";
                            _actualElement.GetComponent<ActionClass>().childActions[12].nextAction = _actualElement.GetComponent<ActionClass>().name + "_M";
                            _actualElement.GetComponent<ActionClass>().childActions[13].nextAction = _actualElement.GetComponent<ActionClass>().name + "_N";
                            _actualElement.GetComponent<ActionClass>().childActions[14].nextAction = _actualElement.GetComponent<ActionClass>().name + "_O";
                            //LAST ELEMENT TO NEXT ACTION

                            BitArray valueEngBVelBits = new BitArray(new int[] { _selector2Area1ActionRob.getIndex() });

                            if (valueEngBVelBits[0] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[8]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[8]._isInverse = true;
                            }
                            if (valueEngBVelBits[1] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[9]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[9]._isInverse = true;
                            }
                            if (valueEngBVelBits[2] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[10]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[10]._isInverse = true;
                            }
                            if (valueEngBVelBits[3] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[11]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[11]._isInverse = true;
                            }
                            _actualElement.GetComponent<ActionClass>().childActions[12]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._isInverse = true;

                            break;
                        case 5:
                            //memory
                            _actualElement.GetComponent<ActionClass>()._type = 2;
                            _actualElement.GetComponent<ActionClass>()._typeRobotic = 6;

                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area1ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area1ActionRob.getIndex();

                            _actualElement.GetComponent<ActionClass>()._isInverse = false;
                            _actualElement.GetComponent<ActionClass>()._value2 = _selector2Area1ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index2 = _selector2Area1ActionRob.getIndex();
                            _actualElement.GetComponent<ActionClass>()._value3 = "";
                            _actualElement.GetComponent<ActionClass>()._index3 = 0;
                            _actualElement.GetComponent<ActionClass>()._value4 = "";
                            _actualElement.GetComponent<ActionClass>()._index4 = 0;
                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = false;
                            _actualElement.GetComponent<ActionClass>()._constant2 = false;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            _actualElement.GetComponent<ActionClass>()._textElement.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_motor_d']").InnerText;
                            //Robotic module:
                            //A part:
                            _actualElement.GetComponent<ActionClass>().childActions = new ActionClass[16];

                            _actualElement.GetComponent<ActionClass>().childActions[0] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[1] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[2] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[3] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[4] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[5] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[6] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[7] = new ActionClass();

                            _actualElement.GetComponent<ActionClass>().childActions[0]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._type = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[0].childName = _actualElement.GetComponent<ActionClass>().name;
                            _actualElement.GetComponent<ActionClass>().childActions[1].childName = _actualElement.GetComponent<ActionClass>().name + "_A";
                            _actualElement.GetComponent<ActionClass>().childActions[2].childName = _actualElement.GetComponent<ActionClass>().name + "_B";
                            _actualElement.GetComponent<ActionClass>().childActions[3].childName = _actualElement.GetComponent<ActionClass>().name + "_C";
                            _actualElement.GetComponent<ActionClass>().childActions[4].childName = _actualElement.GetComponent<ActionClass>().name + "_D";
                            _actualElement.GetComponent<ActionClass>().childActions[5].childName = _actualElement.GetComponent<ActionClass>().name + "_E";
                            _actualElement.GetComponent<ActionClass>().childActions[6].childName = _actualElement.GetComponent<ActionClass>().name + "_F";
                            _actualElement.GetComponent<ActionClass>().childActions[7].childName = _actualElement.GetComponent<ActionClass>().name + "_G";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._value = "M7";
                            _actualElement.GetComponent<ActionClass>().childActions[1]._value = "M7";
                            _actualElement.GetComponent<ActionClass>().childActions[2]._value = "M7";
                            _actualElement.GetComponent<ActionClass>().childActions[3]._value = "M7";
                            _actualElement.GetComponent<ActionClass>().childActions[4]._value = "M7";
                            _actualElement.GetComponent<ActionClass>().childActions[5]._value = "M7";
                            _actualElement.GetComponent<ActionClass>().childActions[6]._value = "M7";
                            _actualElement.GetComponent<ActionClass>().childActions[7]._value = "M7";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._index = 7;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._index = 7;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._index = 7;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._index = 7;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._index = 7;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._index = 7;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._index = 7;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._index = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[0]._value2 = "0";
                            _actualElement.GetComponent<ActionClass>().childActions[1]._value2 = "1";
                            _actualElement.GetComponent<ActionClass>().childActions[2]._value2 = "2";
                            _actualElement.GetComponent<ActionClass>().childActions[3]._value2 = "3";
                            _actualElement.GetComponent<ActionClass>().childActions[4]._value2 = "4";
                            _actualElement.GetComponent<ActionClass>().childActions[5]._value2 = "5";
                            _actualElement.GetComponent<ActionClass>().childActions[6]._value2 = "6";
                            _actualElement.GetComponent<ActionClass>().childActions[7]._value2 = "7";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._index2 = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._index2 = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._index2 = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._index2 = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._index2 = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._index2 = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._index2 = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._index2 = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[0].nextAction = _actualElement.GetComponent<ActionClass>().name + "_A";
                            _actualElement.GetComponent<ActionClass>().childActions[1].nextAction = _actualElement.GetComponent<ActionClass>().name + "_B";
                            _actualElement.GetComponent<ActionClass>().childActions[2].nextAction = _actualElement.GetComponent<ActionClass>().name + "_C";
                            _actualElement.GetComponent<ActionClass>().childActions[3].nextAction = _actualElement.GetComponent<ActionClass>().name + "_D";
                            _actualElement.GetComponent<ActionClass>().childActions[4].nextAction = _actualElement.GetComponent<ActionClass>().name + "_E";
                            _actualElement.GetComponent<ActionClass>().childActions[5].nextAction = _actualElement.GetComponent<ActionClass>().name + "_F";
                            _actualElement.GetComponent<ActionClass>().childActions[6].nextAction = _actualElement.GetComponent<ActionClass>().name + "_G";
                            _actualElement.GetComponent<ActionClass>().childActions[7].nextAction = _actualElement.GetComponent<ActionClass>().name + "_H";

                            BitArray valueEngDDirBits = new BitArray(new int[] { _selector1Area1ActionRob.getIndex() });

                            if (valueEngDDirBits[0] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = true;
                            }

                            if (valueEngDDirBits[1] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[1]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[1]._isInverse = true;
                            }

                            _actualElement.GetComponent<ActionClass>().childActions[2]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._isInverse = true;

                            //Part B
                            _actualElement.GetComponent<ActionClass>().childActions[8] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[9] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[10] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[11] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[12] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[13] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[14] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[15] = new ActionClass();

                            _actualElement.GetComponent<ActionClass>().childActions[8]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._type = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[8].childName = _actualElement.GetComponent<ActionClass>().name + "_H";
                            _actualElement.GetComponent<ActionClass>().childActions[9].childName = _actualElement.GetComponent<ActionClass>().name + "_I";
                            _actualElement.GetComponent<ActionClass>().childActions[10].childName = _actualElement.GetComponent<ActionClass>().name + "_J";
                            _actualElement.GetComponent<ActionClass>().childActions[11].childName = _actualElement.GetComponent<ActionClass>().name + "_K";
                            _actualElement.GetComponent<ActionClass>().childActions[12].childName = _actualElement.GetComponent<ActionClass>().name + "_L";
                            _actualElement.GetComponent<ActionClass>().childActions[13].childName = _actualElement.GetComponent<ActionClass>().name + "_M";
                            _actualElement.GetComponent<ActionClass>().childActions[14].childName = _actualElement.GetComponent<ActionClass>().name + "_N";
                            _actualElement.GetComponent<ActionClass>().childActions[15].childName = _actualElement.GetComponent<ActionClass>().name + "_O";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._value = "M3";
                            _actualElement.GetComponent<ActionClass>().childActions[9]._value = "M3";
                            _actualElement.GetComponent<ActionClass>().childActions[10]._value = "M3";
                            _actualElement.GetComponent<ActionClass>().childActions[11]._value = "M3";
                            _actualElement.GetComponent<ActionClass>().childActions[12]._value = "M3";
                            _actualElement.GetComponent<ActionClass>().childActions[13]._value = "M3";
                            _actualElement.GetComponent<ActionClass>().childActions[14]._value = "M3";
                            _actualElement.GetComponent<ActionClass>().childActions[15]._value = "M3";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._index = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._index = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._index = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._index = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._index = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._index = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._index = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._index = 3;

                            _actualElement.GetComponent<ActionClass>().childActions[8]._value2 = "0";
                            _actualElement.GetComponent<ActionClass>().childActions[9]._value2 = "1";
                            _actualElement.GetComponent<ActionClass>().childActions[10]._value2 = "2";
                            _actualElement.GetComponent<ActionClass>().childActions[11]._value2 = "3";
                            _actualElement.GetComponent<ActionClass>().childActions[12]._value2 = "4";
                            _actualElement.GetComponent<ActionClass>().childActions[13]._value2 = "5";
                            _actualElement.GetComponent<ActionClass>().childActions[14]._value2 = "6";
                            _actualElement.GetComponent<ActionClass>().childActions[15]._value2 = "7";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._index2 = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._index2 = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._index2 = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._index2 = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._index2 = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._index2 = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._index2 = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._index2 = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[8].nextAction = _actualElement.GetComponent<ActionClass>().name + "_I";
                            _actualElement.GetComponent<ActionClass>().childActions[9].nextAction = _actualElement.GetComponent<ActionClass>().name + "_J";
                            _actualElement.GetComponent<ActionClass>().childActions[10].nextAction = _actualElement.GetComponent<ActionClass>().name + "_K";
                            _actualElement.GetComponent<ActionClass>().childActions[11].nextAction = _actualElement.GetComponent<ActionClass>().name + "_L";
                            _actualElement.GetComponent<ActionClass>().childActions[12].nextAction = _actualElement.GetComponent<ActionClass>().name + "_M";
                            _actualElement.GetComponent<ActionClass>().childActions[13].nextAction = _actualElement.GetComponent<ActionClass>().name + "_N";
                            _actualElement.GetComponent<ActionClass>().childActions[14].nextAction = _actualElement.GetComponent<ActionClass>().name + "_O";
                            //LAST ELEMENT TO NEXT ACTION

                            BitArray valueEngDVelBits = new BitArray(new int[] { _selector2Area1ActionRob.getIndex() });

                            if (valueEngDVelBits[0] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[8]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[8]._isInverse = true;
                            }
                            if (valueEngDVelBits[1] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[9]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[9]._isInverse = true;
                            }
                            if (valueEngDVelBits[2] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[10]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[10]._isInverse = true;
                            }
                            if (valueEngDVelBits[3] == true)
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[11]._isInverse = false;
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>().childActions[11]._isInverse = true;
                            }
                            _actualElement.GetComponent<ActionClass>().childActions[12]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._isInverse = true;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._isInverse = true;
                            break;
                        case 8:
                            //memory
                            _actualElement.GetComponent<ActionClass>()._type = 2;
                            _actualElement.GetComponent<ActionClass>()._typeRobotic = 7;

                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area5ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area5ActionRob.getIndex();

                            _actualElement.GetComponent<ActionClass>()._isInverse = false;
                            _actualElement.GetComponent<ActionClass>()._value2 = _selector2Area5ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index2 = _selector2Area5ActionRob.getIndex();
                            _actualElement.GetComponent<ActionClass>()._value3 = _selector3Area5ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index3 = _selector3Area5ActionRob.getIndex();
                            _actualElement.GetComponent<ActionClass>()._value4 = "";
                            _actualElement.GetComponent<ActionClass>()._index4 = 0;
                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = false;
                            _actualElement.GetComponent<ActionClass>()._constant2 = false;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            _actualElement.GetComponent<ActionClass>()._textElement.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_coord_cart']").InnerText;

                            _actualElement.GetComponent<ActionClass>().childActions = new ActionClass[24];
                            _actualElement.GetComponent<ActionClass>().childActions[0] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[1] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[2] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[3] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[4] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[5] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[6] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[7] = new ActionClass();

                            _actualElement.GetComponent<ActionClass>().childActions[8] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[9] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[10] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[11] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[12] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[13] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[14] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[15] = new ActionClass();

                            _actualElement.GetComponent<ActionClass>().childActions[16] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[17] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[18] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[19] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[20] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[21] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[22] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[23] = new ActionClass();

                            _actualElement.GetComponent<ActionClass>().childActions[0]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._type = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[8]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._type = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[16]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[17]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[18]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[19]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[20]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[21]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[22]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[23]._type = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[0].childName = _actualElement.GetComponent<ActionClass>().name;
                            _actualElement.GetComponent<ActionClass>().childActions[1].childName = _actualElement.GetComponent<ActionClass>().name + "_A";
                            _actualElement.GetComponent<ActionClass>().childActions[2].childName = _actualElement.GetComponent<ActionClass>().name + "_B";
                            _actualElement.GetComponent<ActionClass>().childActions[3].childName = _actualElement.GetComponent<ActionClass>().name + "_C";
                            _actualElement.GetComponent<ActionClass>().childActions[4].childName = _actualElement.GetComponent<ActionClass>().name + "_D";
                            _actualElement.GetComponent<ActionClass>().childActions[5].childName = _actualElement.GetComponent<ActionClass>().name + "_E";
                            _actualElement.GetComponent<ActionClass>().childActions[6].childName = _actualElement.GetComponent<ActionClass>().name + "_F";
                            _actualElement.GetComponent<ActionClass>().childActions[7].childName = _actualElement.GetComponent<ActionClass>().name + "_G";

                            _actualElement.GetComponent<ActionClass>().childActions[8].childName = _actualElement.GetComponent<ActionClass>().name + "_H";
                            _actualElement.GetComponent<ActionClass>().childActions[9].childName = _actualElement.GetComponent<ActionClass>().name + "_I";
                            _actualElement.GetComponent<ActionClass>().childActions[10].childName = _actualElement.GetComponent<ActionClass>().name + "_J";
                            _actualElement.GetComponent<ActionClass>().childActions[11].childName = _actualElement.GetComponent<ActionClass>().name + "_K";
                            _actualElement.GetComponent<ActionClass>().childActions[12].childName = _actualElement.GetComponent<ActionClass>().name + "_L";
                            _actualElement.GetComponent<ActionClass>().childActions[13].childName = _actualElement.GetComponent<ActionClass>().name + "_M";
                            _actualElement.GetComponent<ActionClass>().childActions[14].childName = _actualElement.GetComponent<ActionClass>().name + "_N";
                            _actualElement.GetComponent<ActionClass>().childActions[15].childName = _actualElement.GetComponent<ActionClass>().name + "_O";

                            _actualElement.GetComponent<ActionClass>().childActions[16].childName = _actualElement.GetComponent<ActionClass>().name + "_P";
                            _actualElement.GetComponent<ActionClass>().childActions[17].childName = _actualElement.GetComponent<ActionClass>().name + "_Q";
                            _actualElement.GetComponent<ActionClass>().childActions[18].childName = _actualElement.GetComponent<ActionClass>().name + "_R";
                            _actualElement.GetComponent<ActionClass>().childActions[19].childName = _actualElement.GetComponent<ActionClass>().name + "_S";
                            _actualElement.GetComponent<ActionClass>().childActions[20].childName = _actualElement.GetComponent<ActionClass>().name + "_T";
                            _actualElement.GetComponent<ActionClass>().childActions[21].childName = _actualElement.GetComponent<ActionClass>().name + "_U";
                            _actualElement.GetComponent<ActionClass>().childActions[22].childName = _actualElement.GetComponent<ActionClass>().name + "_V";
                            _actualElement.GetComponent<ActionClass>().childActions[23].childName = _actualElement.GetComponent<ActionClass>().name + "_W";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._value = "M8";
                            _actualElement.GetComponent<ActionClass>().childActions[1]._value = "M8";
                            _actualElement.GetComponent<ActionClass>().childActions[2]._value = "M8";
                            _actualElement.GetComponent<ActionClass>().childActions[3]._value = "M8";
                            _actualElement.GetComponent<ActionClass>().childActions[4]._value = "M8";
                            _actualElement.GetComponent<ActionClass>().childActions[5]._value = "M8";
                            _actualElement.GetComponent<ActionClass>().childActions[6]._value = "M8";
                            _actualElement.GetComponent<ActionClass>().childActions[7]._value = "M8";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._value = "M9";
                            _actualElement.GetComponent<ActionClass>().childActions[9]._value = "M9";
                            _actualElement.GetComponent<ActionClass>().childActions[10]._value = "M9";
                            _actualElement.GetComponent<ActionClass>().childActions[11]._value = "M9";
                            _actualElement.GetComponent<ActionClass>().childActions[12]._value = "M9";
                            _actualElement.GetComponent<ActionClass>().childActions[13]._value = "M9";
                            _actualElement.GetComponent<ActionClass>().childActions[14]._value = "M9";
                            _actualElement.GetComponent<ActionClass>().childActions[15]._value = "M9";

                            _actualElement.GetComponent<ActionClass>().childActions[16]._value = "M10";
                            _actualElement.GetComponent<ActionClass>().childActions[17]._value = "M10";
                            _actualElement.GetComponent<ActionClass>().childActions[18]._value = "M10";
                            _actualElement.GetComponent<ActionClass>().childActions[19]._value = "M10";
                            _actualElement.GetComponent<ActionClass>().childActions[20]._value = "M10";
                            _actualElement.GetComponent<ActionClass>().childActions[21]._value = "M10";
                            _actualElement.GetComponent<ActionClass>().childActions[22]._value = "M10";
                            _actualElement.GetComponent<ActionClass>().childActions[23]._value = "M10";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._index = 8;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._index = 8;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._index = 8;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._index = 8;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._index = 8;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._index = 8;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._index = 8;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._index = 8;

                            _actualElement.GetComponent<ActionClass>().childActions[8]._index = 9;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._index = 9;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._index = 9;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._index = 9;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._index = 9;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._index = 9;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._index = 9;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._index = 9;

                            _actualElement.GetComponent<ActionClass>().childActions[16]._index = 10;
                            _actualElement.GetComponent<ActionClass>().childActions[17]._index = 10;
                            _actualElement.GetComponent<ActionClass>().childActions[18]._index = 10;
                            _actualElement.GetComponent<ActionClass>().childActions[19]._index = 10;
                            _actualElement.GetComponent<ActionClass>().childActions[20]._index = 10;
                            _actualElement.GetComponent<ActionClass>().childActions[21]._index = 10;
                            _actualElement.GetComponent<ActionClass>().childActions[22]._index = 10;
                            _actualElement.GetComponent<ActionClass>().childActions[23]._index = 10;

                            _actualElement.GetComponent<ActionClass>().childActions[0]._value2 = "0";
                            _actualElement.GetComponent<ActionClass>().childActions[1]._value2 = "1";
                            _actualElement.GetComponent<ActionClass>().childActions[2]._value2 = "2";
                            _actualElement.GetComponent<ActionClass>().childActions[3]._value2 = "3";
                            _actualElement.GetComponent<ActionClass>().childActions[4]._value2 = "4";
                            _actualElement.GetComponent<ActionClass>().childActions[5]._value2 = "5";
                            _actualElement.GetComponent<ActionClass>().childActions[6]._value2 = "6";
                            _actualElement.GetComponent<ActionClass>().childActions[7]._value2 = "7";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._value2 = "0";
                            _actualElement.GetComponent<ActionClass>().childActions[9]._value2 = "1";
                            _actualElement.GetComponent<ActionClass>().childActions[10]._value2 = "2";
                            _actualElement.GetComponent<ActionClass>().childActions[11]._value2 = "3";
                            _actualElement.GetComponent<ActionClass>().childActions[12]._value2 = "4";
                            _actualElement.GetComponent<ActionClass>().childActions[13]._value2 = "5";
                            _actualElement.GetComponent<ActionClass>().childActions[14]._value2 = "6";
                            _actualElement.GetComponent<ActionClass>().childActions[15]._value2 = "7";

                            _actualElement.GetComponent<ActionClass>().childActions[16]._value2 = "0";
                            _actualElement.GetComponent<ActionClass>().childActions[17]._value2 = "1";
                            _actualElement.GetComponent<ActionClass>().childActions[18]._value2 = "2";
                            _actualElement.GetComponent<ActionClass>().childActions[19]._value2 = "3";
                            _actualElement.GetComponent<ActionClass>().childActions[20]._value2 = "4";
                            _actualElement.GetComponent<ActionClass>().childActions[21]._value2 = "5";
                            _actualElement.GetComponent<ActionClass>().childActions[22]._value2 = "6";
                            _actualElement.GetComponent<ActionClass>().childActions[23]._value2 = "7";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._index2 = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._index2 = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._index2 = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._index2 = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._index2 = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._index2 = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._index2 = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._index2 = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[8]._index2 = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._index2 = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._index2 = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._index2 = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._index2 = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._index2 = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._index2 = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._index2 = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[16]._index2 = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[17]._index2 = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[18]._index2 = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[19]._index2 = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[20]._index2 = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[21]._index2 = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[22]._index2 = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[23]._index2 = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[0].nextAction = _actualElement.GetComponent<ActionClass>().name + "_A";
                            _actualElement.GetComponent<ActionClass>().childActions[1].nextAction = _actualElement.GetComponent<ActionClass>().name + "_B";
                            _actualElement.GetComponent<ActionClass>().childActions[2].nextAction = _actualElement.GetComponent<ActionClass>().name + "_C";
                            _actualElement.GetComponent<ActionClass>().childActions[3].nextAction = _actualElement.GetComponent<ActionClass>().name + "_D";
                            _actualElement.GetComponent<ActionClass>().childActions[4].nextAction = _actualElement.GetComponent<ActionClass>().name + "_E";
                            _actualElement.GetComponent<ActionClass>().childActions[5].nextAction = _actualElement.GetComponent<ActionClass>().name + "_F";
                            _actualElement.GetComponent<ActionClass>().childActions[6].nextAction = _actualElement.GetComponent<ActionClass>().name + "_G";
                            _actualElement.GetComponent<ActionClass>().childActions[7].nextAction = _actualElement.GetComponent<ActionClass>().name + "_H";

                            _actualElement.GetComponent<ActionClass>().childActions[8].nextAction = _actualElement.GetComponent<ActionClass>().name + "_I";
                            _actualElement.GetComponent<ActionClass>().childActions[9].nextAction = _actualElement.GetComponent<ActionClass>().name + "_J";
                            _actualElement.GetComponent<ActionClass>().childActions[10].nextAction = _actualElement.GetComponent<ActionClass>().name + "_K";
                            _actualElement.GetComponent<ActionClass>().childActions[11].nextAction = _actualElement.GetComponent<ActionClass>().name + "_L";
                            _actualElement.GetComponent<ActionClass>().childActions[12].nextAction = _actualElement.GetComponent<ActionClass>().name + "_M";
                            _actualElement.GetComponent<ActionClass>().childActions[13].nextAction = _actualElement.GetComponent<ActionClass>().name + "_N";
                            _actualElement.GetComponent<ActionClass>().childActions[14].nextAction = _actualElement.GetComponent<ActionClass>().name + "_O";
                            _actualElement.GetComponent<ActionClass>().childActions[15].nextAction = _actualElement.GetComponent<ActionClass>().name + "_P";

                            _actualElement.GetComponent<ActionClass>().childActions[16].nextAction = _actualElement.GetComponent<ActionClass>().name + "_Q";
                            _actualElement.GetComponent<ActionClass>().childActions[17].nextAction = _actualElement.GetComponent<ActionClass>().name + "_R";
                            _actualElement.GetComponent<ActionClass>().childActions[18].nextAction = _actualElement.GetComponent<ActionClass>().name + "_S";
                            _actualElement.GetComponent<ActionClass>().childActions[19].nextAction = _actualElement.GetComponent<ActionClass>().name + "_T";
                            _actualElement.GetComponent<ActionClass>().childActions[20].nextAction = _actualElement.GetComponent<ActionClass>().name + "_U";
                            _actualElement.GetComponent<ActionClass>().childActions[21].nextAction = _actualElement.GetComponent<ActionClass>().name + "_V";
                            _actualElement.GetComponent<ActionClass>().childActions[22].nextAction = _actualElement.GetComponent<ActionClass>().name + "_W";

                            BitArray valuePosXBits = new BitArray(new int[] { _selector1Area5ActionRob.getIndex() });
                            BitArray valuePosYBits = new BitArray(new int[] { _selector2Area5ActionRob.getIndex() });
                            BitArray valuePosZBits = new BitArray(new int[] { _selector3Area5ActionRob.getIndex() });

                            _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = !valuePosXBits[0];
                            _actualElement.GetComponent<ActionClass>().childActions[1]._isInverse = !valuePosXBits[1];
                            _actualElement.GetComponent<ActionClass>().childActions[2]._isInverse = !valuePosXBits[2];
                            _actualElement.GetComponent<ActionClass>().childActions[3]._isInverse = !valuePosXBits[3];
                            _actualElement.GetComponent<ActionClass>().childActions[4]._isInverse = !valuePosXBits[4];
                            _actualElement.GetComponent<ActionClass>().childActions[5]._isInverse = !valuePosXBits[5];
                            _actualElement.GetComponent<ActionClass>().childActions[6]._isInverse = !valuePosXBits[6];
                            _actualElement.GetComponent<ActionClass>().childActions[7]._isInverse = !valuePosXBits[7];

                            _actualElement.GetComponent<ActionClass>().childActions[8]._isInverse = !valuePosYBits[0];
                            _actualElement.GetComponent<ActionClass>().childActions[9]._isInverse = !valuePosYBits[1];
                            _actualElement.GetComponent<ActionClass>().childActions[10]._isInverse = !valuePosYBits[2];
                            _actualElement.GetComponent<ActionClass>().childActions[11]._isInverse = !valuePosYBits[3];
                            _actualElement.GetComponent<ActionClass>().childActions[12]._isInverse = !valuePosYBits[4];
                            _actualElement.GetComponent<ActionClass>().childActions[13]._isInverse = !valuePosYBits[5];
                            _actualElement.GetComponent<ActionClass>().childActions[14]._isInverse = !valuePosYBits[6];
                            _actualElement.GetComponent<ActionClass>().childActions[15]._isInverse = !valuePosYBits[7];

                            _actualElement.GetComponent<ActionClass>().childActions[16]._isInverse = !valuePosZBits[0];
                            _actualElement.GetComponent<ActionClass>().childActions[17]._isInverse = !valuePosZBits[1];
                            _actualElement.GetComponent<ActionClass>().childActions[18]._isInverse = !valuePosZBits[2];
                            _actualElement.GetComponent<ActionClass>().childActions[19]._isInverse = !valuePosZBits[3];
                            _actualElement.GetComponent<ActionClass>().childActions[20]._isInverse = !valuePosZBits[4];
                            _actualElement.GetComponent<ActionClass>().childActions[21]._isInverse = !valuePosZBits[5];
                            _actualElement.GetComponent<ActionClass>().childActions[22]._isInverse = !valuePosZBits[6];
                            _actualElement.GetComponent<ActionClass>().childActions[23]._isInverse = !valuePosZBits[7];
                            break;
                        case 9:
                            //memory
                            _actualElement.GetComponent<ActionClass>()._type = 2;
                            _actualElement.GetComponent<ActionClass>()._typeRobotic = 8;

                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area6ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area6ActionRob.getIndex();

                            _actualElement.GetComponent<ActionClass>()._isInverse = false;
                            _actualElement.GetComponent<ActionClass>()._value2 = _selector2Area6ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index2 = _selector2Area6ActionRob.getIndex();
                            _actualElement.GetComponent<ActionClass>()._value3 = "";
                            _actualElement.GetComponent<ActionClass>()._index3 = 0;
                            _actualElement.GetComponent<ActionClass>()._value4 = "";
                            _actualElement.GetComponent<ActionClass>()._index4 = 0;
                            _actualElement.GetComponent<ActionClass>()._scale = 0;
                            _actualElement.GetComponent<ActionClass>()._constant1 = false;
                            _actualElement.GetComponent<ActionClass>()._constant2 = false;
                            _actualElement.GetComponent<ActionClass>()._options = "";
                            _actualElement.GetComponent<ActionClass>()._editorType = 1;
                            _actualElement.GetComponent<ActionClass>()._isReset = false;

                            _actualElement.GetComponent<ActionClass>()._textElement.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_coord_pol']").InnerText;

                            _actualElement.GetComponent<ActionClass>().childActions = new ActionClass[16];
                            _actualElement.GetComponent<ActionClass>().childActions[0] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[1] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[2] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[3] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[4] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[5] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[6] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[7] = new ActionClass();

                            _actualElement.GetComponent<ActionClass>().childActions[8] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[9] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[10] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[11] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[12] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[13] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[14] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[15] = new ActionClass();

                            _actualElement.GetComponent<ActionClass>().childActions[0]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._type = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[8]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._type = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._type = 2;

                            _actualElement.GetComponent<ActionClass>().childActions[0].childName = _actualElement.GetComponent<ActionClass>().name;
                            _actualElement.GetComponent<ActionClass>().childActions[1].childName = _actualElement.GetComponent<ActionClass>().name + "_A";
                            _actualElement.GetComponent<ActionClass>().childActions[2].childName = _actualElement.GetComponent<ActionClass>().name + "_B";
                            _actualElement.GetComponent<ActionClass>().childActions[3].childName = _actualElement.GetComponent<ActionClass>().name + "_C";
                            _actualElement.GetComponent<ActionClass>().childActions[4].childName = _actualElement.GetComponent<ActionClass>().name + "_D";
                            _actualElement.GetComponent<ActionClass>().childActions[5].childName = _actualElement.GetComponent<ActionClass>().name + "_E";
                            _actualElement.GetComponent<ActionClass>().childActions[6].childName = _actualElement.GetComponent<ActionClass>().name + "_F";
                            _actualElement.GetComponent<ActionClass>().childActions[7].childName = _actualElement.GetComponent<ActionClass>().name + "_G";

                            _actualElement.GetComponent<ActionClass>().childActions[8].childName = _actualElement.GetComponent<ActionClass>().name + "_H";
                            _actualElement.GetComponent<ActionClass>().childActions[9].childName = _actualElement.GetComponent<ActionClass>().name + "_I";
                            _actualElement.GetComponent<ActionClass>().childActions[10].childName = _actualElement.GetComponent<ActionClass>().name + "_J";
                            _actualElement.GetComponent<ActionClass>().childActions[11].childName = _actualElement.GetComponent<ActionClass>().name + "_K";
                            _actualElement.GetComponent<ActionClass>().childActions[12].childName = _actualElement.GetComponent<ActionClass>().name + "_L";
                            _actualElement.GetComponent<ActionClass>().childActions[13].childName = _actualElement.GetComponent<ActionClass>().name + "_M";
                            _actualElement.GetComponent<ActionClass>().childActions[14].childName = _actualElement.GetComponent<ActionClass>().name + "_N";
                            _actualElement.GetComponent<ActionClass>().childActions[15].childName = _actualElement.GetComponent<ActionClass>().name + "_O";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._value = "M11";
                            _actualElement.GetComponent<ActionClass>().childActions[1]._value = "M11";
                            _actualElement.GetComponent<ActionClass>().childActions[2]._value = "M11";
                            _actualElement.GetComponent<ActionClass>().childActions[3]._value = "M11";
                            _actualElement.GetComponent<ActionClass>().childActions[4]._value = "M11";
                            _actualElement.GetComponent<ActionClass>().childActions[5]._value = "M11";
                            _actualElement.GetComponent<ActionClass>().childActions[6]._value = "M11";
                            _actualElement.GetComponent<ActionClass>().childActions[7]._value = "M11";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._value = "M12";
                            _actualElement.GetComponent<ActionClass>().childActions[9]._value = "M12";
                            _actualElement.GetComponent<ActionClass>().childActions[10]._value = "M12";
                            _actualElement.GetComponent<ActionClass>().childActions[11]._value = "M12";
                            _actualElement.GetComponent<ActionClass>().childActions[12]._value = "M12";
                            _actualElement.GetComponent<ActionClass>().childActions[13]._value = "M12";
                            _actualElement.GetComponent<ActionClass>().childActions[14]._value = "M12";
                            _actualElement.GetComponent<ActionClass>().childActions[15]._value = "M12";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._index = 11;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._index = 11;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._index = 11;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._index = 11;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._index = 11;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._index = 11;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._index = 11;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._index = 11;

                            _actualElement.GetComponent<ActionClass>().childActions[8]._index = 12;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._index = 12;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._index = 12;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._index = 12;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._index = 12;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._index = 12;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._index = 12;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._index = 12;

                            _actualElement.GetComponent<ActionClass>().childActions[0]._value2 = "0";
                            _actualElement.GetComponent<ActionClass>().childActions[1]._value2 = "1";
                            _actualElement.GetComponent<ActionClass>().childActions[2]._value2 = "2";
                            _actualElement.GetComponent<ActionClass>().childActions[3]._value2 = "3";
                            _actualElement.GetComponent<ActionClass>().childActions[4]._value2 = "4";
                            _actualElement.GetComponent<ActionClass>().childActions[5]._value2 = "5";
                            _actualElement.GetComponent<ActionClass>().childActions[6]._value2 = "6";
                            _actualElement.GetComponent<ActionClass>().childActions[7]._value2 = "7";

                            _actualElement.GetComponent<ActionClass>().childActions[8]._value2 = "0";
                            _actualElement.GetComponent<ActionClass>().childActions[9]._value2 = "1";
                            _actualElement.GetComponent<ActionClass>().childActions[10]._value2 = "2";
                            _actualElement.GetComponent<ActionClass>().childActions[11]._value2 = "3";
                            _actualElement.GetComponent<ActionClass>().childActions[12]._value2 = "4";
                            _actualElement.GetComponent<ActionClass>().childActions[13]._value2 = "5";
                            _actualElement.GetComponent<ActionClass>().childActions[14]._value2 = "6";
                            _actualElement.GetComponent<ActionClass>().childActions[15]._value2 = "7";

                            _actualElement.GetComponent<ActionClass>().childActions[0]._index2 = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[1]._index2 = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[2]._index2 = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[3]._index2 = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[4]._index2 = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[5]._index2 = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[6]._index2 = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[7]._index2 = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[8]._index2 = 0;
                            _actualElement.GetComponent<ActionClass>().childActions[9]._index2 = 1;
                            _actualElement.GetComponent<ActionClass>().childActions[10]._index2 = 2;
                            _actualElement.GetComponent<ActionClass>().childActions[11]._index2 = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[12]._index2 = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[13]._index2 = 5;
                            _actualElement.GetComponent<ActionClass>().childActions[14]._index2 = 6;
                            _actualElement.GetComponent<ActionClass>().childActions[15]._index2 = 7;

                            _actualElement.GetComponent<ActionClass>().childActions[0].nextAction = _actualElement.GetComponent<ActionClass>().name + "_A";
                            _actualElement.GetComponent<ActionClass>().childActions[1].nextAction = _actualElement.GetComponent<ActionClass>().name + "_B";
                            _actualElement.GetComponent<ActionClass>().childActions[2].nextAction = _actualElement.GetComponent<ActionClass>().name + "_C";
                            _actualElement.GetComponent<ActionClass>().childActions[3].nextAction = _actualElement.GetComponent<ActionClass>().name + "_D";
                            _actualElement.GetComponent<ActionClass>().childActions[4].nextAction = _actualElement.GetComponent<ActionClass>().name + "_E";
                            _actualElement.GetComponent<ActionClass>().childActions[5].nextAction = _actualElement.GetComponent<ActionClass>().name + "_F";
                            _actualElement.GetComponent<ActionClass>().childActions[6].nextAction = _actualElement.GetComponent<ActionClass>().name + "_G";
                            _actualElement.GetComponent<ActionClass>().childActions[7].nextAction = _actualElement.GetComponent<ActionClass>().name + "_H";

                            _actualElement.GetComponent<ActionClass>().childActions[8].nextAction = _actualElement.GetComponent<ActionClass>().name + "_I";
                            _actualElement.GetComponent<ActionClass>().childActions[9].nextAction = _actualElement.GetComponent<ActionClass>().name + "_J";
                            _actualElement.GetComponent<ActionClass>().childActions[10].nextAction = _actualElement.GetComponent<ActionClass>().name + "_K";
                            _actualElement.GetComponent<ActionClass>().childActions[11].nextAction = _actualElement.GetComponent<ActionClass>().name + "_L";
                            _actualElement.GetComponent<ActionClass>().childActions[12].nextAction = _actualElement.GetComponent<ActionClass>().name + "_M";
                            _actualElement.GetComponent<ActionClass>().childActions[13].nextAction = _actualElement.GetComponent<ActionClass>().name + "_N";
                            _actualElement.GetComponent<ActionClass>().childActions[14].nextAction = _actualElement.GetComponent<ActionClass>().name + "_O";
                            
                            BitArray valuePosRBits = new BitArray(new int[] { _selector1Area6ActionRob.getIndex() });
                            BitArray valuePosTBits = new BitArray(new int[] { _selector2Area6ActionRob.getIndex() });
                           
                            _actualElement.GetComponent<ActionClass>().childActions[0]._isInverse = !valuePosRBits[0];
                            _actualElement.GetComponent<ActionClass>().childActions[1]._isInverse = !valuePosRBits[1];
                            _actualElement.GetComponent<ActionClass>().childActions[2]._isInverse = !valuePosRBits[2];
                            _actualElement.GetComponent<ActionClass>().childActions[3]._isInverse = !valuePosRBits[3];
                            _actualElement.GetComponent<ActionClass>().childActions[4]._isInverse = !valuePosRBits[4];
                            _actualElement.GetComponent<ActionClass>().childActions[5]._isInverse = !valuePosRBits[5];
                            _actualElement.GetComponent<ActionClass>().childActions[6]._isInverse = !valuePosRBits[6];
                            _actualElement.GetComponent<ActionClass>().childActions[7]._isInverse = !valuePosRBits[7];

                            _actualElement.GetComponent<ActionClass>().childActions[8]._isInverse = !valuePosTBits[0];
                            _actualElement.GetComponent<ActionClass>().childActions[9]._isInverse = !valuePosTBits[1];
                            _actualElement.GetComponent<ActionClass>().childActions[10]._isInverse = !valuePosTBits[2];
                            _actualElement.GetComponent<ActionClass>().childActions[11]._isInverse = !valuePosTBits[3];
                            _actualElement.GetComponent<ActionClass>().childActions[12]._isInverse = !valuePosTBits[4];
                            _actualElement.GetComponent<ActionClass>().childActions[13]._isInverse = !valuePosTBits[5];
                            _actualElement.GetComponent<ActionClass>().childActions[14]._isInverse = !valuePosTBits[6];
                            _actualElement.GetComponent<ActionClass>().childActions[15]._isInverse = !valuePosTBits[7];

                            break;
                        case 3:
                            //tmp
                            _actualElement.GetComponent<ActionClass>()._type = 3;

                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area2ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area2ActionRob.getIndex();

                            if (_invArea2ActionRob.isOn == true)
                            {
                                _actualElement.GetComponent<ActionClass>()._isInverse = false;
                                _actualElement.GetComponent<ActionClass>()._value2 = "";
                                _actualElement.GetComponent<ActionClass>()._index2 = 0;
                                _actualElement.GetComponent<ActionClass>()._value3 = "";
                                _actualElement.GetComponent<ActionClass>()._index3 = 0;
                                _actualElement.GetComponent<ActionClass>()._value4 = "";
                                _actualElement.GetComponent<ActionClass>()._index4 = 0;
                                _actualElement.GetComponent<ActionClass>()._scale = 0;
                                _actualElement.GetComponent<ActionClass>()._constant1 = false;
                                _actualElement.GetComponent<ActionClass>()._constant2 = false;
                                _actualElement.GetComponent<ActionClass>()._options = "";
                                _actualElement.GetComponent<ActionClass>()._editorType = 1;
                                _actualElement.GetComponent<ActionClass>()._isReset = true;

                                _actualElement.GetComponent<ActionClass>()._textElement.text = "RES(" + _selector1Area2ActionRob.getValue() + ")";
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>()._isInverse = false;
                                _actualElement.GetComponent<ActionClass>()._value2 = _selector2Area2ActionRob.getValue();
                                _actualElement.GetComponent<ActionClass>()._index2 = _selector2Area2ActionRob.getIndex();

                                string _scaleTXT = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='mseg_scale']").InnerText;
                                if (_input3Area2ActionRob.text != "")
                                {
                                    if (scaleTPArea3 == 0)
                                    {
                                        _actualElement.GetComponent<ActionClass>()._value4 = _input3Area2ActionRob.text;
                                    }
                                    else if (scaleTPArea3 == 1)
                                    {
                                        _actualElement.GetComponent<ActionClass>()._value4 = (float.Parse(_input3Area2ActionRob.text) * 1000f).ToString();
                                        _scaleTXT = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='seg_scale']").InnerText;
                                    }
                                    else if (scaleTPArea3 == 2)
                                    {
                                        _actualElement.GetComponent<ActionClass>()._value4 = (float.Parse(_input3Area2ActionRob.text) * 1000f * 60f).ToString();
                                        _scaleTXT = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='min_scale']").InnerText;
                                    }
                                    else if (scaleTPArea3 == 3)
                                    {
                                        _actualElement.GetComponent<ActionClass>()._value4 = (float.Parse(_input3Area2ActionRob.text) * 1000f * 60f * 60f).ToString();
                                        _scaleTXT = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='h_scale']").InnerText;
                                    }
                                }
                                else
                                {
                                    _actualElement.GetComponent<ActionClass>()._value4 = _input3Area2ActionRob.text;
                                }

                                _actualElement.GetComponent<ActionClass>()._index4 = 0;
                                _actualElement.GetComponent<ActionClass>()._scale = scaleTPArea3;
                                _actualElement.GetComponent<ActionClass>()._constant1 = false;
                                _actualElement.GetComponent<ActionClass>()._constant2 = false;
                                _actualElement.GetComponent<ActionClass>()._options = "";
                                _actualElement.GetComponent<ActionClass>()._editorType = 1;
                                _actualElement.GetComponent<ActionClass>()._isReset = false;

                                _actualElement.GetComponent<ActionClass>()._textElement.text = _actualElement.GetComponent<ActionClass>()._value2 + " " + _actualElement.GetComponent<ActionClass>()._value + "=" + _input3Area2ActionRob.text + _scaleTXT;
                            }

                            _actualElement.GetComponent<ActionClass>().childActions = new ActionClass[1];
                            _actualElement.GetComponent<ActionClass>().childActions[0] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[0]._type = 3;
                            _actualElement.GetComponent<ActionClass>().childActions[0].childName = _actualElement.GetComponent<ActionClass>().name;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._value = _actualElement.GetComponent<ActionClass>()._value;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._index = _actualElement.GetComponent<ActionClass>()._index;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._value2 = _actualElement.GetComponent<ActionClass>()._value2;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._index2 = _actualElement.GetComponent<ActionClass>()._index2;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._value3 = _actualElement.GetComponent<ActionClass>()._value3;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._index3 = _actualElement.GetComponent<ActionClass>()._index3;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._value4 = _actualElement.GetComponent<ActionClass>()._value4;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._index4 = _actualElement.GetComponent<ActionClass>()._index4;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._scale = _actualElement.GetComponent<ActionClass>()._scale;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._isReset = _actualElement.GetComponent<ActionClass>()._isReset;

                            break;
                        case 6:
                            //count
                            _actualElement.GetComponent<ActionClass>()._type = 4;

                            _actualElement.GetComponent<ActionClass>()._value = _selector1Area3ActionRob.getValue();
                            _actualElement.GetComponent<ActionClass>()._index = _selector1Area3ActionRob.getIndex();

                            if (_invArea3ActionRob.isOn == true)
                            {
                                _actualElement.GetComponent<ActionClass>()._isInverse = false;
                                _actualElement.GetComponent<ActionClass>()._value2 = "";
                                _actualElement.GetComponent<ActionClass>()._index2 = 0;
                                _actualElement.GetComponent<ActionClass>()._value3 = "";
                                _actualElement.GetComponent<ActionClass>()._index3 = 0;
                                _actualElement.GetComponent<ActionClass>()._value4 = "";
                                _actualElement.GetComponent<ActionClass>()._index4 = 0;
                                _actualElement.GetComponent<ActionClass>()._scale = 0;
                                _actualElement.GetComponent<ActionClass>()._constant1 = false;
                                _actualElement.GetComponent<ActionClass>()._constant2 = false;
                                _actualElement.GetComponent<ActionClass>()._options = "";
                                _actualElement.GetComponent<ActionClass>()._editorType = 1;
                                _actualElement.GetComponent<ActionClass>()._isReset = true;

                                _actualElement.GetComponent<ActionClass>()._textElement.text = "RES(" + _selector1Area3ActionRob.getValue() + ")";
                            }
                            else
                            {
                                _actualElement.GetComponent<ActionClass>()._isInverse = false;
                                _actualElement.GetComponent<ActionClass>()._value2 = _selector2Area3ActionRob.getValue();
                                _actualElement.GetComponent<ActionClass>()._index2 = _selector2Area3ActionRob.getIndex();

                                _actualElement.GetComponent<ActionClass>()._value4 = _input3Area3ActionRob.text;

                                _actualElement.GetComponent<ActionClass>()._index4 = 0;
                                _actualElement.GetComponent<ActionClass>()._scale = 0;
                                _actualElement.GetComponent<ActionClass>()._constant1 = false;
                                _actualElement.GetComponent<ActionClass>()._constant2 = false;
                                _actualElement.GetComponent<ActionClass>()._options = "";
                                _actualElement.GetComponent<ActionClass>()._editorType = 1;
                                _actualElement.GetComponent<ActionClass>()._isReset = false;

                                _actualElement.GetComponent<ActionClass>()._textElement.text = _actualElement.GetComponent<ActionClass>()._value2 + " " + _actualElement.GetComponent<ActionClass>()._value + "=" + _actualElement.GetComponent<ActionClass>()._value4;
                            }

                            _actualElement.GetComponent<ActionClass>().childActions = new ActionClass[1];
                            _actualElement.GetComponent<ActionClass>().childActions[0] = new ActionClass();
                            _actualElement.GetComponent<ActionClass>().childActions[0]._type = 4;
                            _actualElement.GetComponent<ActionClass>().childActions[0].childName = _actualElement.GetComponent<ActionClass>().name;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._value = _actualElement.GetComponent<ActionClass>()._value;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._index = _actualElement.GetComponent<ActionClass>()._index;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._value2 = _actualElement.GetComponent<ActionClass>()._value2;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._index2 = _actualElement.GetComponent<ActionClass>()._index2;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._value3 = _actualElement.GetComponent<ActionClass>()._value3;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._index3 = _actualElement.GetComponent<ActionClass>()._index3;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._value4 = _actualElement.GetComponent<ActionClass>()._value4;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._index4 = _actualElement.GetComponent<ActionClass>()._index4;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._scale = _actualElement.GetComponent<ActionClass>()._scale;
                            _actualElement.GetComponent<ActionClass>().childActions[0]._isReset = _actualElement.GetComponent<ActionClass>()._isReset;

                            break;
                    }
            }
			break;
		case "transition":
			switch(_editorParameter){
			case "condition":
                
                if (Manager.Instance.globalRoboticsMode == false)
                {
                    _actualElement.GetComponent<conditionElementClass>()._typeValue = buttonActive;

                    switch (buttonActive)
                    {
                        case 1:
                        case 2:
                        case 4:
                        case 5:
                            _actualElement.GetComponent<conditionElementClass>()._value = _selectorArea1Transition.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._index = _selectorArea1Transition.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = _invArea1Transition.isOn;

                            _actualElement.GetComponent<conditionElementClass>()._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = false;

                            _actualElement.GetComponent<conditionElementClass>()._title.text = _selectorArea1Transition.getValue();
                            _actualElement.GetComponent<conditionElementClass>().setInv(_invArea1Transition.isOn);
                            break;
                        case 3:
                            _actualElement.GetComponent<conditionElementClass>()._value = _selector1Area3Transition.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._index = _selector1Area3Transition.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = _invArea3Transition.isOn;

                            _actualElement.GetComponent<conditionElementClass>()._value2 = _selector2Area3Transition.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._index2 = _selector2Area3Transition.getIndex();

                            _actualElement.GetComponent<conditionElementClass>()._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = false;

                            _actualElement.GetComponent<conditionElementClass>()._title.text = _selector1Area3Transition.getValue() + "," + _selector2Area3Transition.getValue();
                            _actualElement.GetComponent<conditionElementClass>().setInv(_invArea3Transition.isOn);
                            break;
                        case 6:
                            _actualElement.GetComponent<conditionElementClass>()._value = _selector2Area2Transition.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._index = _selector2Area2Transition.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = _invArea2Transition.isOn;

                            if (_input1SelectorArea2Transition.isOn == true)
                            {
                                _actualElement.GetComponent<conditionElementClass>()._value2 = _input1Area2Transition.text;
                                _actualElement.GetComponent<conditionElementClass>()._index2 = 0;
                            }
                            else
                            {
                                _actualElement.GetComponent<conditionElementClass>()._value2 = _selector1Area2Transition.getValue();
                                _actualElement.GetComponent<conditionElementClass>()._index2 = _selector1Area2Transition.getIndex();
                            }

                            if (_input3SelectorArea2Transition.isOn == true)
                            {
                                _actualElement.GetComponent<conditionElementClass>()._value3 = _input3Area2Transition.text;
                                _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            }
                            else
                            {
                                _actualElement.GetComponent<conditionElementClass>()._value3 = _selector3Area2Transition.getValue();
                                _actualElement.GetComponent<conditionElementClass>()._index3 = _selector3Area2Transition.getIndex();
                            }

                            _actualElement.GetComponent<conditionElementClass>()._constant1 = _input1SelectorArea2Transition.isOn;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = _input3SelectorArea2Transition.isOn;

                            _actualElement.GetComponent<conditionElementClass>()._title.text = _actualElement.GetComponent<conditionElementClass>()._value;
                            _actualElement.GetComponent<conditionElementClass>().setInv(_invArea2Transition.isOn);
                            break;
                    }
                } else {

                    _actualElement.GetComponent<conditionElementClass>()._typeRobotics = buttonActive;

                    switch (buttonActive)
                    {
                        case 1:
                            _actualElement.GetComponent<conditionElementClass>()._typeValue = 3;
                            _actualElement.GetComponent<conditionElementClass>()._index = transitionRoboticArea3Selector1.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._value = transitionRoboticArea3Selector1.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>()._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>()._title.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_line_sensor']").InnerText;
                            _actualElement.GetComponent<conditionElementClass>()._title.fontSize = 22;
                            _actualElement.GetComponent<conditionElementClass>().setInv(false);

                            _actualElement.GetComponent<conditionElementClass>().childCondition = new conditionElementClass();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value = "M13";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index = 13;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value2 = "-1";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index2 = -1;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value3 = transitionRoboticArea3Selector1.getValue();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index3 = transitionRoboticArea3Selector1.getIndex();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._typeValue = _actualElement.GetComponent<conditionElementClass>()._typeValue;
                            break;
                        case 2:
                            _actualElement.GetComponent<conditionElementClass>()._typeValue = 3;
                            _actualElement.GetComponent<conditionElementClass>()._index = transitionRoboticArea5Selector1.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._value = transitionRoboticArea5Selector1.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._index2 = transitionRoboticArea5Selector2.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._value2 = transitionRoboticArea5Selector2.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>()._title.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_right_ultrasonic']").InnerText;
                                    _actualElement.GetComponent<conditionElementClass>()._title.fontSize = 22;
                                    _actualElement.GetComponent<conditionElementClass>().setInv(false);

                            _actualElement.GetComponent<conditionElementClass>().childCondition = new conditionElementClass();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value = "M15";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index = 15;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value2 = "-1";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index2 = -1;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value3 = transitionRoboticArea5Selector1.getValue();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index3 = transitionRoboticArea5Selector1.getIndex();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._typeValue = _actualElement.GetComponent<conditionElementClass>()._typeValue;
                            break;
                        case 3:
                        case 9:
                            if (buttonActive == 3)
                            {
                                _actualElement.GetComponent<conditionElementClass>()._typeValue = 4;
                            } else
                            {
                                _actualElement.GetComponent<conditionElementClass>()._typeValue = 5;
                            }
                           
                            _actualElement.GetComponent<conditionElementClass>()._value = transitionRoboticArea1Selector.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._index = transitionRoboticArea1Selector.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = transitionRoboticArea1Toggle.isOn;

                            _actualElement.GetComponent<conditionElementClass>()._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = false;

                            _actualElement.GetComponent<conditionElementClass>()._title.text = transitionRoboticArea1Selector.getValue();
                            _actualElement.GetComponent<conditionElementClass>().setInv(transitionRoboticArea1Toggle.isOn);

                            _actualElement.GetComponent<conditionElementClass>().childCondition = new conditionElementClass();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value = transitionRoboticArea1Selector.getValue();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index = transitionRoboticArea1Selector.getIndex();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = transitionRoboticArea1Toggle.isOn;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant2 = false;

                            _actualElement.GetComponent<conditionElementClass>().childCondition._typeValue = _actualElement.GetComponent<conditionElementClass>()._typeValue;
                            break;
                        case 4:
                            _actualElement.GetComponent<conditionElementClass>()._typeValue = 1;
                            _actualElement.GetComponent<conditionElementClass>()._index = transitionRoboticArea6Selector1.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._value = transitionRoboticArea6Selector1.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>()._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>()._title.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_band_sensor']").InnerText;
                                    _actualElement.GetComponent<conditionElementClass>()._title.fontSize = 22;
                                    _actualElement.GetComponent<conditionElementClass>().setInv(false);

                            _actualElement.GetComponent<conditionElementClass>().childCondition = new conditionElementClass();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value = "I1";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index = 1;
                            if (transitionRoboticArea6Selector1.getIndex() == 0)
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = true;
                            } else
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = false;
                            }
                          
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._typeValue = _actualElement.GetComponent<conditionElementClass>()._typeValue;
                            break;
                        case 5:
                            _actualElement.GetComponent<conditionElementClass>()._typeValue = 3;
                            _actualElement.GetComponent<conditionElementClass>()._index = transitionRoboticArea5Selector1.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._value = transitionRoboticArea5Selector1.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._index2 = transitionRoboticArea5Selector2.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._value2 = transitionRoboticArea5Selector2.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>()._title.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_center_ultrasonic']").InnerText;
                                    _actualElement.GetComponent<conditionElementClass>()._title.fontSize = 22;
                                    _actualElement.GetComponent<conditionElementClass>().setInv(false);

                            _actualElement.GetComponent<conditionElementClass>().childCondition = new conditionElementClass();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value = "M16";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index = 16;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value2 = "-1";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index2 = -1;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value3 = transitionRoboticArea5Selector1.getValue();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index3 = transitionRoboticArea5Selector1.getIndex();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant2 = false;

                            _actualElement.GetComponent<conditionElementClass>().childCondition._typeValue = _actualElement.GetComponent<conditionElementClass>()._typeValue;

                            break;
                        case 6:
                            _actualElement.GetComponent<conditionElementClass>()._typeValue = 6;
                            _actualElement.GetComponent<conditionElementClass>()._value = transitionRoboticArea2Selector2.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._index = transitionRoboticArea2Selector2.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = transitionRoboticArea2Toggle.isOn;

                            if (transitionRoboticArea2ToggleConstant1.isOn == true)
                            {
                                _actualElement.GetComponent<conditionElementClass>()._value2 = transitionRoboticArea2Constant1.text;
                                _actualElement.GetComponent<conditionElementClass>()._index2 = 0;
                            }
                            else
                            {
                                _actualElement.GetComponent<conditionElementClass>()._value2 = transitionRoboticArea2Selector1.getValue();
                                _actualElement.GetComponent<conditionElementClass>()._index2 = transitionRoboticArea2Selector1.getIndex();
                            }

                            if (transitionRoboticArea2ToggleConstant3.isOn == true)
                            {
                                _actualElement.GetComponent<conditionElementClass>()._value3 = transitionRoboticArea2Constant3.text;
                                _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            }
                            else
                            {
                                _actualElement.GetComponent<conditionElementClass>()._value3 = transitionRoboticArea2Selector3.getValue();
                                _actualElement.GetComponent<conditionElementClass>()._index3 = transitionRoboticArea2Selector3.getIndex();
                            }

                            _actualElement.GetComponent<conditionElementClass>()._constant1 = transitionRoboticArea2ToggleConstant1.isOn;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = transitionRoboticArea2ToggleConstant3.isOn;

                            _actualElement.GetComponent<conditionElementClass>()._title.text = _actualElement.GetComponent<conditionElementClass>()._value;
                            _actualElement.GetComponent<conditionElementClass>().setInv(transitionRoboticArea2Toggle.isOn);

                            _actualElement.GetComponent<conditionElementClass>().childCondition = new conditionElementClass();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value = transitionRoboticArea2Selector2.getValue();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index = transitionRoboticArea2Selector2.getIndex();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = transitionRoboticArea2Toggle.isOn;
                            
                            if (transitionRoboticArea2ToggleConstant1.isOn == true)
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._value2 = _actualElement.GetComponent<conditionElementClass>()._value2;
                                _actualElement.GetComponent<conditionElementClass>().childCondition._index2 = _actualElement.GetComponent<conditionElementClass>()._index2;

                            }
                            else
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._value2 = GetRoboticsComparatorValue(_actualElement.GetComponent<conditionElementClass>()._index2);
                                _actualElement.GetComponent<conditionElementClass>().childCondition._index2 = GetRoboticsComparatorIndex(_actualElement.GetComponent<conditionElementClass>()._index2);

                            }

                            if (transitionRoboticArea2ToggleConstant3.isOn == true)
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._value3 = _actualElement.GetComponent<conditionElementClass>()._value3;
                                _actualElement.GetComponent<conditionElementClass>().childCondition._index3 = _actualElement.GetComponent<conditionElementClass>()._index3;

                            }
                            else
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._value3 = GetRoboticsComparatorValue(_actualElement.GetComponent<conditionElementClass>()._index3);
                                _actualElement.GetComponent<conditionElementClass>().childCondition._index3 = GetRoboticsComparatorIndex(_actualElement.GetComponent<conditionElementClass>()._index3);

                            }

                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant1 = _actualElement.GetComponent<conditionElementClass>()._constant1;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant2 = _actualElement.GetComponent<conditionElementClass>()._constant2;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._typeValue = _actualElement.GetComponent<conditionElementClass>()._typeValue;
                            break;
                        case 7:
                            _actualElement.GetComponent<conditionElementClass>()._typeValue = 3;
                            _actualElement.GetComponent<conditionElementClass>()._index = colorIndexSelected;
                            _actualElement.GetComponent<conditionElementClass>()._value = colorIndexSelected.ToString();
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>()._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>()._title.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_color_sensor']").InnerText;
                                    _actualElement.GetComponent<conditionElementClass>()._title.fontSize = 22;
                                    _actualElement.GetComponent<conditionElementClass>().setInv(false);

                            _actualElement.GetComponent<conditionElementClass>().childCondition = new conditionElementClass();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value = "M14";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index = 14;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value2 = "-1";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index2 = -1;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value3 = colorIndexSelected.ToString();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index3 = colorIndexSelected;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._typeValue = _actualElement.GetComponent<conditionElementClass>()._typeValue;
                            break;
                        case 8:
                            _actualElement.GetComponent<conditionElementClass>()._typeValue = 3;
                            _actualElement.GetComponent<conditionElementClass>()._index = transitionRoboticArea5Selector1.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._value = transitionRoboticArea5Selector1.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._index2 = transitionRoboticArea5Selector2.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._value2 = transitionRoboticArea5Selector2.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>()._title.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_left_ultrasonic']").InnerText;
                            
                            _actualElement.GetComponent<conditionElementClass>()._title.fontSize = 22;
                            _actualElement.GetComponent<conditionElementClass>().setInv(false);

                            _actualElement.GetComponent<conditionElementClass>().childCondition = new conditionElementClass();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value = "M17";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index = 17;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value2 = "-1";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index2 = -1;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value3 = transitionRoboticArea5Selector1.getValue();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index3 = transitionRoboticArea5Selector1.getIndex();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._typeValue = _actualElement.GetComponent<conditionElementClass>()._typeValue;
                            break;
                        case 10:
                            _actualElement.GetComponent<conditionElementClass>()._typeValue = 1;
                            _actualElement.GetComponent<conditionElementClass>()._index = transitionRoboticArea6Selector1.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._value = transitionRoboticArea6Selector1.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>()._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>()._title.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_start']").InnerText;
                            _actualElement.GetComponent<conditionElementClass>()._title.fontSize = 22;
                            _actualElement.GetComponent<conditionElementClass>().setInv(false);

                            _actualElement.GetComponent<conditionElementClass>().childCondition = new conditionElementClass();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value = "I0";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index = 0;
                            if (transitionRoboticArea6Selector1.getIndex() == 0)
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = true;
                            }
                            else
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = false;
                            }

                            _actualElement.GetComponent<conditionElementClass>().childCondition._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._typeValue = _actualElement.GetComponent<conditionElementClass>()._typeValue;
                            break;
                        case 11:
                            _actualElement.GetComponent<conditionElementClass>()._typeValue = 1;
                            _actualElement.GetComponent<conditionElementClass>()._index = transitionRoboticArea6Selector1.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._value = transitionRoboticArea6Selector1.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>()._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>()._title.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_tran_bt_11']").InnerText;
                            _actualElement.GetComponent<conditionElementClass>()._title.fontSize = 22;
                            _actualElement.GetComponent<conditionElementClass>().setInv(false);

                            _actualElement.GetComponent<conditionElementClass>().childCondition = new conditionElementClass();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value = "I2";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index = 2;
                            if (transitionRoboticArea6Selector1.getIndex() == 0)
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = true;
                            }
                            else
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = false;
                            }

                            _actualElement.GetComponent<conditionElementClass>().childCondition._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._typeValue = _actualElement.GetComponent<conditionElementClass>()._typeValue;
                            break;
                        case 12:
                            _actualElement.GetComponent<conditionElementClass>()._typeValue = 1;
                            _actualElement.GetComponent<conditionElementClass>()._index = transitionRoboticArea6Selector1.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._value = transitionRoboticArea6Selector1.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>()._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>()._title.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_tran_bt_12']").InnerText;
                            _actualElement.GetComponent<conditionElementClass>()._title.fontSize = 22;
                            _actualElement.GetComponent<conditionElementClass>().setInv(false);

                            _actualElement.GetComponent<conditionElementClass>().childCondition = new conditionElementClass();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value = "I3";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index = 3;
                            if (transitionRoboticArea6Selector1.getIndex() == 0)
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = true;
                            }
                            else
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = false;
                            }

                            _actualElement.GetComponent<conditionElementClass>().childCondition._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._typeValue = _actualElement.GetComponent<conditionElementClass>()._typeValue;
                            break;
                        case 13:
                            _actualElement.GetComponent<conditionElementClass>()._typeValue = 1;
                            _actualElement.GetComponent<conditionElementClass>()._index = transitionRoboticArea6Selector1.getIndex();
                            _actualElement.GetComponent<conditionElementClass>()._value = transitionRoboticArea6Selector1.getValue();
                            _actualElement.GetComponent<conditionElementClass>()._isInverse = false;
                            _actualElement.GetComponent<conditionElementClass>()._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>()._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>()._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>()._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>()._title.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='editor_rob_tran_bt_13']").InnerText;
                            _actualElement.GetComponent<conditionElementClass>()._title.fontSize = 22;
                            _actualElement.GetComponent<conditionElementClass>().setInv(false);

                            _actualElement.GetComponent<conditionElementClass>().childCondition = new conditionElementClass();
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value = "I4";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index = 4;
                            if (transitionRoboticArea6Selector1.getIndex() == 0)
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = true;
                            }
                            else
                            {
                                _actualElement.GetComponent<conditionElementClass>().childCondition._isInverse = false;
                            }

                            _actualElement.GetComponent<conditionElementClass>().childCondition._value2 = "";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index2 = 0;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._value3 = "";
                            _actualElement.GetComponent<conditionElementClass>().childCondition._index3 = 0;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant1 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._constant2 = false;
                            _actualElement.GetComponent<conditionElementClass>().childCondition._typeValue = _actualElement.GetComponent<conditionElementClass>()._typeValue;
                            break;

                            }
                }
				break;
			case "operation":
                if (Manager.Instance.globalRoboticsMode == false)
                {
                    _actualElement.GetComponent<operationElementClass>()._value = _selector1Area1Operation.getValue();
                    _actualElement.GetComponent<operationElementClass>()._index = _selector1Area1Operation.getIndex();
                    _actualElement.GetComponent<operationElementClass>()._title.text = _actualElement.GetComponent<operationElementClass>()._value;
                } else {
                    _actualElement.GetComponent<operationElementClass>()._value = operationRoboticSelector.getValue();
                    _actualElement.GetComponent<operationElementClass>()._index = operationRoboticSelector.getIndex();
                    _actualElement.GetComponent<operationElementClass>()._title.text = _actualElement.GetComponent<operationElementClass>()._value;
                }
				break;
			}
			break;
		case "divergence":
            _actualElement.GetComponent<DivergenceClass> ()._type = _selector1AreaDivergence.getValue ();
			_actualElement.GetComponent<DivergenceClass> ()._branches = int.Parse (_selector2AreaDivergence.getValue ());
			_actualElement.GetComponent<DivergenceClass> ().updateConnections ();
            break;
		case "convergence":
            _actualElement.GetComponent<ConvergenceClass> ()._type = _selector1AreaDivergence.getValue ();
			_actualElement.GetComponent<ConvergenceClass> ()._branches = int.Parse (_selector2AreaDivergence.getValue ());
			_actualElement.GetComponent<ConvergenceClass> ().updateConnections ();
            break;
		}

		exitAction ();
	}


    private int GetRoboticsComparatorIndex(int index)
    {
        int result = 0;

        switch (index)
        {
            case 0:
                result = 0;
                break;
            case 1:
                result = 1;
                break;
            case 2:
                result = 2;
                break;
            case 3:
                result = 3;
                break;
            case 4:
                result = 4;
                break;
            case 5:
                result = 1;
                break;
            case 6:
                result = 0;
                break;
            case 7:
                result = 1;
                break;
            case 8:
                result = 2;
                break;
            case 9:
                result = 3;
                break;
            case 10:
                result = 4;
                break;
            case 11:
                result = 5;
                break;
            case 12:
                result = 6;
                break;
            case 13:
                result = 7;
                break;

            case 14:
                result = 8;
                break;
            case 15:
                result = 9;
                break;
            case 16:
                result = 10;
                break;
            case 17:
                result = 11;
                break;
            case 18:
                result = 12;
                break;

            case 19:
                result = 0;
                break;
            case 20:
                result = 1;
                break;
            case 21:
                result = 2;
                break;
            case 22:
                result = 3;
                break;
            case 23:
                result = 4;
                break;
            case 24:
                result = 13;
                break;
            case 25:
                result = 14;
                break;
            case 26:
                result = 15;
                break;
            case 27:
                result = 16;
                break;
            case 28:
                result = 17;
                break;
            case 29:
                result = 0;
                break;
            case 30:
                result = 1;
                break;
            case 31:
                result = 2;
                break;
            case 32:
                result = 3;
                break;
            case 33:
                result = 4;
                break;
            case 34:
                result = 5;
                break;
            case 35:
                result = 6;
                break;
            case 36:
                result = 7;
                break;
            case 37:
                result = 8;
                break;
            case 38:
                result = 9;
                break;
            case 39:
                result = 10;
                break;
            case 40:
                result = 11;
                break;
            case 41:
                result = 12;
                break;
            case 42:
                result = 13;
                break;
            case 43:
                result = 14;
                break;
            case 44:
                result = 15;
                break;
            case 45:
                result = 16;
                break;
            case 46:
                result = 17;
                break;
            case 47:
                result = 18;
                break;
            case 48:
                result = 19;
                break;
            case 49:
                result = 0;
                break;
            case 50:
                result = 1;
                break;
            case 51:
                result = 2;
                break;
            case 52:
                result = 3;
                break;
            case 53:
                result = 4;
                break;
            case 54:
                result = 5;
                break;
            case 55:
                result = 6;
                break;
            case 56:
                result = 7;
                break;
            case 57:
                result = 8;
                break;
            case 58:
                result = 9;
                break;
        }

        return result;
    }

    private string GetRoboticsComparatorValue(int index)
    {
        string result = "";

        switch (index)
        {
            case 0:
                result = "I0";
                break;
            case 1:
                result = "I1";
                break;
            case 2:
                result = "I2";
                break;
            case 3:
                result = "I3";
                break;
            case 4:
                result = "I4";
                break;
            case 5:
                result = "O1";
                break;
            case 6:
                result = "M0";
                break;
            case 7:
                result = "M1";
                break;
            case 8:
                result = "M2";
                break;
            case 9:
                result = "M3";
                break;
            case 10:
                result = "M4";
                break;
            case 11:
                result = "M5";
                break;
            case 12:
                result = "M6";
                break;
            case 13:
                result = "M7";
                break;

            case 14:
                result = "M8";
                break;
            case 15:
                result = "M9";
                break;
            case 16:
                result = "M10";
                break;
            case 17:
                result = "M11";
                break;
            case 18:
                result = "M12";
                break;

            case 19:
                result = "AI0";
                break;
            case 20:
                result = "AI1";
                break;
            case 21:
                result = "AI2";
                break;
            case 22:
                result = "AI3";
                break;
            case 23:
                result = "AI4";
                break;

            case 24:
                result = "M13";
                break;
            case 25:
                result = "M14";
                break;
            case 26:
                result = "M15";
                break;
            case 27:
                result = "M16";
                break;
            case 28:
                result = "M17";
                break;
            case 29:
                result = "U0";
                break;
            case 30:
                result = "U1";
                break;
            case 31:
                result = "U2";
                break;
            case 32:
                result = "U3";
                break;
            case 33:
                result = "U4";
                break;
            case 34:
                result = "U5";
                break;
            case 35:
                result = "U6";
                break;
            case 36:
                result = "U7";
                break;
            case 37:
                result = "U8";
                break;
            case 38:
                result = "U9";
                break;
            case 39:
                result = "U10";
                break;
            case 40:
                result = "U11";
                break;
            case 41:
                result = "U12";
                break;
            case 42:
                result = "U13";
                break;
            case 43:
                result = "U14";
                break;
            case 44:
                result = "U15";
                break;
            case 45:
                result = "U16";
                break;
            case 46:
                result = "U17";
                break;
            case 47:
                result = "U18";
                break;
            case 48:
                result = "U19";
                break;
            case 49:
                result = "D0";
                break;
            case 50:
                result = "D1";
                break;
            case 51:
                result = "D2";
                break;
            case 52:
                result = "D3";
                break;
            case 53:
                result = "D4";
                break;
            case 54:
                result = "D5";
                break;
            case 55:
                result = "D6";
                break;
            case 56:
                result = "D7";
                break;
            case 57:
                result = "D8";
                break;
            case 58:
                result = "D9";
                break;
        }
        return result;
    }

    private void changeResetArea3Action(bool _value){
		if (_value == true) {
			setArea3Visibility (false);
		} else {
			setArea3Visibility (true);
		}
	}

	public void setArea3Visibility(bool _visible){
		_labelSelector2Area3Action.enabled = _visible;
		_selector2Area3Action.gameObject.SetActive(_visible);
		_labelTP3Area3Action.enabled = _visible;
		tpArea3Action.gameObject.SetActive (_visible);
		_labelSelector3Area3Action.enabled = _visible;
		_selector3Area3Action.gameObject.SetActive(_visible);
	}

	private void changeResetArea4Action(bool _value){
		if (_value == true) {
			setArea4Visibility (false);
		} else {
			setArea4Visibility (true);
		}
	}

	public void setArea4Visibility(bool _visible){
		_labelSelector2Area4Action.enabled = _visible;
		_selector2Area4Action.gameObject.SetActive(_visible);
		_labelPV3Area4Action.enabled = _visible;
		pvArea4Action.gameObject.SetActive (_visible);
		_labelSelector3Area4Action.enabled = _visible;
		_selector3Area4Action.gameObject.SetActive(_visible);
	}

	private void changeConstantValue2Area5Action(bool _value){
		if (_value == true) {
			setConstantValue2Area5 (true);
		} else {
			setConstantValue2Area5 (false);
		}
	}

	public void setConstantValue2Area5(bool _visible){
		_selector2Area5Action.gameObject.SetActive(!_visible);
		_input2Area5Action.gameObject.SetActive(_visible);
	}

	private void changeConstantValue3Area5Action(bool _value){
		if (_value == true) {
			setConstantValue3Area5 (true);
		} else {
			setConstantValue3Area5 (false);
		}
	}

	public void setConstantValue3Area5(bool _visible){
		_selector3Area5Action.gameObject.SetActive(!_visible);
		_input3Area5Action.gameObject.SetActive(_visible);
	}

	private void changeConstantValue2Area6Action(bool _value){
		if (_value == true) {
			setConstantValue2Area6 (true);
		} else {
			setConstantValue2Area6 (false);
		}
	}

	public void setConstantValue2Area6(bool _visible){
		_selector2Area6Action.gameObject.SetActive(!_visible);
		_input2Area6Action.gameObject.SetActive(_visible);
	}

	private void changeConstantValue3Area6Action(bool _value){
		if (_value == true) {
			setConstantValue3Area6 (true);
		} else {
			setConstantValue3Area6 (false);
		}
	}

	public void setConstantValue3Area6(bool _visible){
		_selector3Area6Action.gameObject.SetActive(!_visible);
		_input3Area6Action.gameObject.SetActive(_visible);
	}

	private void changeConstantValue1Area2Transition(bool _value){
		if (_value == true) {
			setConstantValue1Area2Transition (true);
		} else {
			setConstantValue1Area2Transition (false);
		}
	}

	public void setConstantValue1Area2Transition(bool _visible){
		_selector1Area2Transition.gameObject.SetActive(!_visible);
		_input1Area2Transition.gameObject.SetActive(_visible);
	}

	private void changeConstantValue3Area2Transition(bool _value){
		if (_value == true) {
			setConstantValue3Area2Transition (true);
		} else {
			setConstantValue3Area2Transition (false);
		}
	}

    public void setConstantValue3Area2Transition(bool _visible)
    {
        _selector3Area2Transition.gameObject.SetActive(!_visible);
        _input3Area2Transition.gameObject.SetActive(_visible);
    }

    private void changeConstantValue1Area2TransitionRobotic(bool _value)
    {
        if (_value == true)
        {
            setConstantValue1Area2TransitionRobotic(true);
        }
        else
        {
            setConstantValue1Area2TransitionRobotic(false);
        }
    }

    public void setConstantValue1Area2TransitionRobotic(bool _visible)
    {
        transitionRoboticArea2Selector1.gameObject.SetActive(!_visible);
        transitionRoboticArea2Constant1.gameObject.SetActive(_visible);
    }


    private void changeConstantValue3Area2TransitionRobotic(bool _value)
    {
        if (_value == true)
        {
            setConstantValue3Area2TransitionRobotic(true);
        }
        else
        {
            setConstantValue3Area2TransitionRobotic(false);
        }
    }

    public void setConstantValue3Area2TransitionRobotic(bool _visible)
    {
        transitionRoboticArea2Selector3.gameObject.SetActive(!_visible);
        transitionRoboticArea2Constant3.gameObject.SetActive(_visible);
    }

	public void checkLogicAction(){
		if (_selector1Area6Action.getIndex () >= 3) {
			setVisibilityArea6 (false);
		} else {
			setVisibilityArea6 (true);
		}
	}

	public void setVisibilityArea6(bool _visible){
		_labelSelector3Area6Action.enabled = _visible;
		_selector3Area6Action.gameObject.SetActive (_visible);
		_input3Area6Action.gameObject.SetActive (_visible);
		_input3SelectorArea6Action.gameObject.SetActive (_visible);

		if(_visible == true){
			if (_input3SelectorArea6Action.isOn == true) {
				setConstantValue3Area6 (true);
			} else {
				setConstantValue3Area6 (false);
			}
		}
	}

	private void changeTextArea3Action(string _value){
		if (tpArea3Action.text != "") {
			if (scaleTPArea3 == 0) {
				_labelTP3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='mseg_scale']").InnerText + ")";
				if (float.Parse (tpArea3Action.text) >= 1000) {
					scaleTPArea3 = 1;
					tpArea3Action.text = (float.Parse (tpArea3Action.text) / 1000f).ToString ();
					_labelTP3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='seg_scale']").InnerText + ")";
				}
			} else if (scaleTPArea3 == 1) {
				_labelTP3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='seg_scale']").InnerText + ")";
				if (float.Parse (tpArea3Action.text) >= 60) {
					scaleTPArea3 = 2;
					tpArea3Action.text = (Mathf.RoundToInt ((float.Parse (tpArea3Action.text) / 60f) * 1000f) / 1000f).ToString ();
					_labelTP3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='min_scale']").InnerText + ")";
				}
			} else if (scaleTPArea3 == 2) {
				_labelTP3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='min_scale']").InnerText + ")";
				if (float.Parse (tpArea3Action.text) >= 60) {
					scaleTPArea3 = 3;
					tpArea3Action.text = (Mathf.RoundToInt ((float.Parse (tpArea3Action.text) / 60f) * 1000f) / 1000f).ToString ();
					_labelTP3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='h_scale']").InnerText + ")";
				}
			} else if(scaleTPArea3 == 3){
				_labelTP3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='h_scale']").InnerText + ")";
			}	
		} else {
			scaleTPArea3 = 0;
			_labelTP3Area3Action.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='mseg_scale']").InnerText + ")";
		}
	}

    private void changeTextArea2ActionRob(string _value)
    {
        if (_input3Area2ActionRob.text != "")
        {
            if (scaleTPArea3 == 0)
            {
                _labelSelector3Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='mseg_scale']").InnerText + ")";
                if (float.Parse(_input3Area2ActionRob.text) >= 1000)
                {
                    scaleTPArea3 = 1;
                    _input3Area2ActionRob.text = (float.Parse(_input3Area2ActionRob.text) / 1000f).ToString();
                    _labelSelector3Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='seg_scale']").InnerText + ")";
                }
            }
            else if (scaleTPArea3 == 1)
            {
                _labelSelector3Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='seg_scale']").InnerText + ")";
                if (float.Parse(_input3Area2ActionRob.text) >= 60)
                {
                    scaleTPArea3 = 2;
                    _input3Area2ActionRob.text = (Mathf.RoundToInt((float.Parse(_input3Area2ActionRob.text) / 60f) * 1000f) / 1000f).ToString();
                    _labelSelector3Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='min_scale']").InnerText + ")";
                }
            }
            else if (scaleTPArea3 == 2)
            {
                _labelSelector3Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='min_scale']").InnerText + ")";
                if (float.Parse(_input3Area2ActionRob.text) >= 60)
                {
                    scaleTPArea3 = 3;
                    _input3Area2ActionRob.text = (Mathf.RoundToInt((float.Parse(_input3Area2ActionRob.text) / 60f) * 1000f) / 1000f).ToString();
                    _labelSelector3Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='h_scale']").InnerText + ")";
                }
            }
            else if (scaleTPArea3 == 3)
            {
                _labelSelector3Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='h_scale']").InnerText + ")";
            }
        }
        else
        {
            scaleTPArea3 = 0;
            _labelSelector3Area2ActionRob.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='mseg_scale']").InnerText + ")";
        }
    }

    private void changeResetArea2ActionRob(bool _value){
        if (_value == true)
        {
            setArea2RobVisibility(false);
        }
        else
        {
            setArea2RobVisibility(true);
        }
    }

    public void setArea2RobVisibility(bool _visible)
    {
        _labelSelector2Area2ActionRob.enabled = _visible;
        _selector2Area2ActionRob.gameObject.SetActive(_visible);
        _labelSelector3Area2ActionRob.enabled = _visible;
        _input3Area2ActionRob.gameObject.SetActive(_visible);
    }

    private void changeResetArea3ActionRob(bool _value)
    {
        if (_value == true)
        {
            setArea3RobVisibility(false);
        }
        else
        {
            setArea3RobVisibility(true);
        }
    }

    public void setArea3RobVisibility(bool _visible)
    {
        _labelSelector2Area3ActionRob.enabled = _visible;
        _selector2Area3ActionRob.gameObject.SetActive(_visible);
        _labelSelector3Area3ActionRob.enabled = _visible;
        _input3Area3ActionRob.gameObject.SetActive(_visible);
    }

    private char ValidateArea3ConstantAction(string text, int charIndex, char addedChar){
        string output = Regex.Replace(addedChar + "", @"[^0-1]", "\0");
        return output.ToCharArray()[0];
    }

    private string convertBitArrayToString(BitArray value){
        string result = "";

        for (int i = 3; i >= 0; i--){
            Debug.Log(value[i]);
            if(value[i] == true){
                result = result + "1";
            } else {
                result = result + "0";
            }
        }

        return int.Parse(result).ToString();
    }

    private void ConfigureButton(Button button, Text innerText){
        ColorBlock cb = button.colors;
        if(button.GetComponentInChildren<Text>().text == "1"){
            cb.normalColor = Color.white;
            cb.highlightedColor = Color.white;
            innerText.color = new Color(9f / 255f, 112f / 255f, 180f / 255f);
        } else {
            cb.normalColor = Color.white;
            cb.highlightedColor = Color.white;
            innerText.color = new Color(50f / 255f, 50f / 255f, 50f / 255f);
        }
        button.colors = cb;
    }

    private void UpdateArea3RobValuesAction(){
        //check value
        int value = transitionRoboticArea3Selector1.getIndex();

        BitArray valueBits = new BitArray(new int[] { value });

        transitionRoboticArea3ButtonBit0.GetComponentInChildren<Text>().text = (valueBits[0] == true)? "1" : "0";
        transitionRoboticArea3ButtonBit1.GetComponentInChildren<Text>().text = (valueBits[1] == true) ? "1" : "0";
        transitionRoboticArea3ButtonBit2.GetComponentInChildren<Text>().text = (valueBits[2] == true) ? "1" : "0";
        transitionRoboticArea3ButtonBit3.GetComponentInChildren<Text>().text = (valueBits[3] == true) ? "1" : "0";

        ConfigureButton(transitionRoboticArea3ButtonBit0, transitionRoboticArea3ButtonBit0.GetComponentInChildren<Text>());
        ConfigureButton(transitionRoboticArea3ButtonBit1, transitionRoboticArea3ButtonBit1.GetComponentInChildren<Text>());
        ConfigureButton(transitionRoboticArea3ButtonBit2, transitionRoboticArea3ButtonBit2.GetComponentInChildren<Text>());
        ConfigureButton(transitionRoboticArea3ButtonBit3, transitionRoboticArea3ButtonBit3.GetComponentInChildren<Text>());

        transitionRoboticArea3Constant1.text = convertBitArrayToString(valueBits);

    }

    private void UpdateArea3RobFromConstantAction(string value){
        int decimalValue = 0;
        //convert to decimal
        for (int i = 0; i < value.Length; i++){
            decimalValue = decimalValue + int.Parse(value[i].ToString()) * (int)Mathf.Pow(2, value.Length - 1 - i);
        }

        transitionRoboticArea3Selector1.setValuePerIndex(decimalValue);

        BitArray valueBits = new BitArray(new int[] { decimalValue });

        transitionRoboticArea3ButtonBit0.GetComponentInChildren<Text>().text = (valueBits[0] == true) ? "1" : "0";
        transitionRoboticArea3ButtonBit1.GetComponentInChildren<Text>().text = (valueBits[1] == true) ? "1" : "0";
        transitionRoboticArea3ButtonBit2.GetComponentInChildren<Text>().text = (valueBits[2] == true) ? "1" : "0";
        transitionRoboticArea3ButtonBit3.GetComponentInChildren<Text>().text = (valueBits[3] == true) ? "1" : "0";

        ConfigureButton(transitionRoboticArea3ButtonBit0, transitionRoboticArea3ButtonBit0.GetComponentInChildren<Text>());
        ConfigureButton(transitionRoboticArea3ButtonBit1, transitionRoboticArea3ButtonBit1.GetComponentInChildren<Text>());
        ConfigureButton(transitionRoboticArea3ButtonBit2, transitionRoboticArea3ButtonBit2.GetComponentInChildren<Text>());
        ConfigureButton(transitionRoboticArea3ButtonBit3, transitionRoboticArea3ButtonBit3.GetComponentInChildren<Text>());
    }

    private void UpdateArea3RobFromBit0Action(){
        int valueToAdd = 0;
        if(transitionRoboticArea3ButtonBit0.GetComponentInChildren<Text>().text == "0"){
            transitionRoboticArea3ButtonBit0.GetComponentInChildren<Text>().text = "1";
            valueToAdd = 1;
        }
        else {
            transitionRoboticArea3ButtonBit0.GetComponentInChildren<Text>().text = "0";
            valueToAdd = -1;
        }

        ConfigureButton(transitionRoboticArea3ButtonBit0, transitionRoboticArea3ButtonBit0.GetComponentInChildren<Text>());

        //change values in selector
        int value = transitionRoboticArea3Selector1.getIndex();
        value = value + valueToAdd * (int)Mathf.Pow(2, 0);

        transitionRoboticArea3Selector1.setValuePerIndex(value);

        BitArray valueBits = new BitArray(new int[] { value });
        transitionRoboticArea3Constant1.text = convertBitArrayToString(valueBits);
    }

    private void UpdateArea3RobFromBit1Action()
    {
        int valueToAdd = 0;
        if (transitionRoboticArea3ButtonBit1.GetComponentInChildren<Text>().text == "0")
        {
            transitionRoboticArea3ButtonBit1.GetComponentInChildren<Text>().text = "1";
            valueToAdd = 1;
        }
        else
        {
            transitionRoboticArea3ButtonBit1.GetComponentInChildren<Text>().text = "0";
            valueToAdd = -1;
        }

        ConfigureButton(transitionRoboticArea3ButtonBit1, transitionRoboticArea3ButtonBit1.GetComponentInChildren<Text>());

        //change values in selector
        int value = transitionRoboticArea3Selector1.getIndex();
        value = value + valueToAdd * (int)Mathf.Pow(2, 1);

        transitionRoboticArea3Selector1.setValuePerIndex(value);

        BitArray valueBits = new BitArray(new int[] { value });
        transitionRoboticArea3Constant1.text = convertBitArrayToString(valueBits);
    }

    private void UpdateArea3RobFromBit2Action()
    {
        int valueToAdd = 0;
        if (transitionRoboticArea3ButtonBit2.GetComponentInChildren<Text>().text == "0")
        {
            transitionRoboticArea3ButtonBit2.GetComponentInChildren<Text>().text = "1";
            valueToAdd = 1;
        }
        else
        {
            transitionRoboticArea3ButtonBit2.GetComponentInChildren<Text>().text = "0";
            valueToAdd = -1;
        }

        ConfigureButton(transitionRoboticArea3ButtonBit2, transitionRoboticArea3ButtonBit2.GetComponentInChildren<Text>());

        //change values in selector
        int value = transitionRoboticArea3Selector1.getIndex();
        value = value + valueToAdd * (int)Mathf.Pow(2, 2);

        transitionRoboticArea3Selector1.setValuePerIndex(value);

        BitArray valueBits = new BitArray(new int[] { value });
        transitionRoboticArea3Constant1.text = convertBitArrayToString(valueBits);
    }

    private void UpdateArea3RobFromBit3Action()
    {
        int valueToAdd = 0;
        if (transitionRoboticArea3ButtonBit3.GetComponentInChildren<Text>().text == "0")
        {
            transitionRoboticArea3ButtonBit3.GetComponentInChildren<Text>().text = "1";
            valueToAdd = 1;
        }
        else
        {
            transitionRoboticArea3ButtonBit3.GetComponentInChildren<Text>().text = "0";
            valueToAdd = -1;
        }

        ConfigureButton(transitionRoboticArea3ButtonBit3, transitionRoboticArea3ButtonBit3.GetComponentInChildren<Text>());

        //change values in selector
        int value = transitionRoboticArea3Selector1.getIndex();
        value = value + valueToAdd * (int)Mathf.Pow(2, 3);

        transitionRoboticArea3Selector1.setValuePerIndex(value);

        BitArray valueBits = new BitArray(new int[] { value });
        transitionRoboticArea3Constant1.text = convertBitArrayToString(valueBits);
    }

    private void UpdateArea5Sel1RobValuesAction()
    {
        //integer 0-255
        int intValue = int.Parse(transitionRoboticArea5Selector1.getValue());
        int centValue = Mathf.RoundToInt((float)intValue / 2.55f);

        transitionRoboticArea5Selector2.setValue(centValue.ToString());
    }

    private void UpdateArea5Sel2RobValuesAction()
    {
        //cent 0-100
        int centValue = int.Parse(transitionRoboticArea5Selector2.getValue());
        int intValue = Mathf.RoundToInt((float)centValue * 2.55f);

        transitionRoboticArea5Selector1.setValue(intValue.ToString());

    }

    private void RoboticSelectColor1Action(){
        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color1.transform.localPosition;
        colorIndexSelected = 0;
    }

    private void RoboticSelectColor2Action()
    {
        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color2.transform.localPosition;
        colorIndexSelected = 1;
    }

    private void RoboticSelectColor3Action()
    {
        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color3.transform.localPosition;
        colorIndexSelected = 2;
    }

    private void RoboticSelectColor4Action()
    {
        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color4.transform.localPosition;
        colorIndexSelected = 3;
    }

    private void RoboticSelectColor5Action()
    {
        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color5.transform.localPosition;
        colorIndexSelected = 4;
    }

    private void RoboticSelectColor6Action()
    {
        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color6.transform.localPosition;
        colorIndexSelected = 5;
    }

    private void RoboticSelectColor7Action()
    {
        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color7.transform.localPosition;
        colorIndexSelected = 6;
    }

    private void RoboticSelectColor8Action()
    {
        transitionRoboticArea4ActiveArea.transform.localPosition = transitionRoboticArea4Color8.transform.localPosition;
        colorIndexSelected = 7;
    }

	private void goToInitialState(){
		gameObject.transform.SetParent (GameObject.Find ("Canvas/Grafcet").transform, true);
	}
	
	// Update is called once per frame
	void Update () {
        if (Manager.Instance.globalRoboticsMode == false)
        {
            switch (buttonActive)
            {
                case 1:
                    _bt1.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _bt2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt6.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 2:
                    _bt1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt2.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _bt3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt6.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 3:
                    _bt1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt3.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _bt4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt6.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 4:
                    _bt1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt4.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _bt5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt6.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 5:
                    _bt1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt5.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _bt6.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 6:
                    _bt1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _bt6.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    break;

            }
        } else {
            switch (buttonActive)
            {
                case 1:
                    _btRob1.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob8.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob9.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 2:
                    _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob2.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob8.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob9.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 3:
                    _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob3.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob8.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob9.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 4:
                    _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob4.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob8.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob9.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 5:
                    _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob5.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob8.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob9.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 6:
                    _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob6.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob8.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob9.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 7:
                    _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob7.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _btRob8.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob9.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 8:
                    _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob8.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _btRob9.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 9:
                    if(_editorType == "action"){
                        _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob8.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                        _btRob9.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    } else {
                        _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob8.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob9.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                        _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    }
                    break;
                case 10:
                    if (_editorType == "action")
                    {
                        _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob8.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob9.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                        _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    }
                    else
                    {
                        _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob8.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob9.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob10.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                        _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                        _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    }
                    break;
                case 11:
                    _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob8.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob9.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob11.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 12:
                    _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob8.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob9.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob12.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    _btRob13.GetComponent<Image>().color = new Color(1, 1, 1);
                    break;
                case 13:
                    _btRob1.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob2.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob3.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob4.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob5.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob6.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob7.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob8.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob9.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob10.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob11.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob12.GetComponent<Image>().color = new Color(1, 1, 1);
                    _btRob13.GetComponent<Image>().color = new Color(215f / 255f, 215f / 255f, 215f / 255f);
                    break;
            }
        }
	}
}
