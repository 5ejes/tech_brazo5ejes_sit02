using UnityEngine;

namespace NSEditorAcciones
{

	public class GripOperation : Operation
	{

		public GripOperation()
		{
			// Defino el ID
			BlockID = IDTable.GRIP_OPERATION_ID;
		}

		private bool _closed = false;

		public bool Closed
		{
			get => _closed;
			set => _closed = value;
		}
	}

}