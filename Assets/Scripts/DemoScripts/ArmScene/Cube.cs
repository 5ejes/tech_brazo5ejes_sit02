﻿using System;
using System.Collections;
using System.Collections.Generic;
using NSArm3Axis;
using NSScriptableValues;
using UnityEngine;

public class Cube : AbstractCollectableObject
{
	private class ColorMaterialReference
	{
		public Material Material { get; set; }
		public int ColorID { get; set; }
	}
	[Header("Referencias")]
	[SerializeField] private ValueColorArray _colorInfo = null;
	[SerializeField] private Transform _overrideTransform = null;
	[SerializeField] private Transform _cubeEnviroCollider;
	[Header("Valid Catch Checks")]
	[SerializeField] private Transform[] _referenceCatchInvalidPoints;
	[Header("Settings")]
	[SerializeField] private bool _useOverride = false;
	[SerializeField] private CubeState state;
	[SerializeField] private int colorID;
	[SerializeField] private MeshRenderer[] _renderers;

	[Header("Special Settings")]
	[SerializeField] private bool _useStartValuesOnReset = false;
	[Header("Top Horizontal Cube Check")]
	[SerializeField] private float _checkDistanceHorizontal = 0.08f;
	[SerializeField] private LayerMask _topCubeCheckLayer = Physics.AllLayers;

	public delegate void CollisionInfoDelegate(Collision collisionInfo);
	public event CollisionInfoDelegate CollisionEnter;

	private bool _useGravityStartValue;
	private bool _isKinematicStartValue;
	private static readonly float PUSH_FORCE = 25f;
	private int _rollingDirection = 0;
	private static List<ColorMaterialReference> _materialReferences = new List<ColorMaterialReference>();

	private RaycastHit _horizontalCheckHitInfo;

	private Rigidbody _rigidbody;
	public Rigidbody Rigidbody
	{
		get
		{
			if (_rigidbody == null) _rigidbody = GetComponent<Rigidbody>();
			return _rigidbody;
		}
		private set => _rigidbody = value;
	}
	public CubeState State
	{
		get => state;
		set
		{

			// Si el estado pasa a Final
			if (value.Equals(CubeState.Final))
			{
				
				// Si el objeto está activo en la escena
				if (gameObject.activeInHierarchy)
				{
					// Ejecuto una corutina
					StartCoroutine(WaitOneFrame(() =>
					{
					// Defino el estado del cubo a inicial
					state = CubeState.Inicial;
					}));
				}
				else
				{
					// El objeto no está activo en la escena

					// Defino el estado inicial sin ejecutar la corutina
					state = CubeState.Inicial;
				}
			}
			state = value;
		}
	}

	/// <summary>
	/// Identificador entero que me define el color del cubo
	/// </summary>
	/// <value></value>
	public int ColorID { get => colorID; set => colorID = value; }

	/// <summary>
	/// Transform que sobreescribe el transform del MonoBehaviour
	/// </summary>
	/// <value></value>
	public new Transform OverrideTransform
	{
		get => _overrideTransform;
		set
		{
			_overrideTransform = value;
			UpdateBaseOverrides();
		}
	}
	/// <summary>
	/// ¿Usar el Override Transform?
	/// </summary>
	/// <value></value>
	public new bool UseOverrideTransform
	{
		get => _useOverride;
		set
		{
			_useOverride = value;
			UpdateBaseOverrides();
		}
	}

	public bool EnableRollingEffect { get; set; } = true;

	private static Material _sharedMaterial;
	private Vector3 _lastPosition;

	public void UpdateBaseOverrides()
	{
		base.OverrideTransform = OverrideTransform;
		base.UseOverrideTransform = UseOverrideTransform;
	}

	private IEnumerator WaitOneFrame(System.Action callback)
	{
		yield return null;

		if (callback != null)
			callback.Invoke();
	}

	private void Start()
	{
		// Inicializo el color del cubo
		InitializeColor();

		// Si uso los valores de inicio
		if (_useStartValuesOnReset)
		{
			// Almaceno los valores iniciales
			_isKinematicStartValue = Rigidbody.isKinematic;
			_useGravityStartValue = Rigidbody.useGravity;
		}

		// Reseteo la configuración del cubo
		Reset();

		_transform = transform;

		// Calculo la dirección a rodar
		UnityEngine.Random.InitState(Time.frameCount);

		// Obtengo el signo del valor aleatorio
		_rollingDirection = (int)Mathf.Sign(UnityEngine.Random.Range(-1f, 1f));

		// Si la dirección queda en 0
		if (_rollingDirection == 0)
			// La convierto a -1
			_rollingDirection = -1;
	}

	private void FixedUpdate()
	{
		// Si el efecto de rodarse no está activo, retorno
		if (!EnableRollingEffect) return;

		// Si el cilindro se encuentra horizontal
		if (IsHorizontal(_transform))
		{

			// Lanzo un rayo verticalmente hacia abajo para detectar otro cubo
			if (Physics.Raycast(_transform.position, -Vector3.up, out _horizontalCheckHitInfo, _checkDistanceHorizontal, _topCubeCheckLayer, QueryTriggerInteraction.Ignore))
			{
				// Compruebo que el otro elemento esté tambien horizontal
				Transform otherTransform = _horizontalCheckHitInfo.transform;

				if (IsHorizontal(otherTransform))
				{
					// Ambos cilindros son horizontales

					// Si ambos cilindros son paralelos horizontalmente
					bool isParallel = Mathf.Abs(Vector3.Dot(otherTransform.up, _transform.up)) > 0.9f;
					Debug.DrawRay(_transform.position, -Vector3.up * _checkDistanceHorizontal, Color.blue);

					// Creo la magnitud del vector de fuerza
					float forceMagnitude = Time.fixedDeltaTime * PUSH_FORCE;

					// Si los cilindros no son paralelos
					if (!isParallel)
					{
						// Obtengo el lado derecho del vector
						Vector3 cross = Vector3.Cross(otherTransform.up, Vector3.up);

						// Obtengo un vector de Unidad (dirección)
						cross.Normalize();

						// Aplico la dirección de rotación (De impulso en este caso)
						cross *= _rollingDirection;

						// Aplico el impulso 
						Rigidbody.AddForce(cross * forceMagnitude, ForceMode.Impulse);
					}
					else
					{
						// Los cilindros son paralelos

						// Calculo la fuerza usando un vector hacia la derecha más la dirección de rodamiento actual
						// Y un vector hacia arriba, generando una diagonal
						// A este vector le aplico una magnitud considerando la delta
						Vector3 force = _transform.rotation * Vector3.up * _rollingDirection * forceMagnitude;

						Rigidbody Rigidbody = this.Rigidbody;
						ApplyCilinderTorque(force, Rigidbody);

						// Aplico la misma fuerza pero en sentido contrario al cubo que detecté
						force = otherTransform.rotation * Vector3.up * -_rollingDirection * forceMagnitude;
						ApplyCilinderTorque(force, _horizontalCheckHitInfo.rigidbody);
					}

				}

			}
		}
	}

	private void ApplyCilinderTorque(Vector3 force, Rigidbody Rigidbody)
	{
		Rigidbody.AddTorque(force, ForceMode.Impulse);
	}

	private bool IsHorizontal(Transform target)
	{
		return Mathf.Abs(Vector3.Dot(target.rotation * Vector3.up, Vector3.up)) < 0.05f;
	}

	/// <summary>
	/// Inicializo el color del cubo según su ID
	/// </summary>
	private void InitializeColor()
	{

		// Si la lista de renderers está definida
		if (_renderers != null)
		{
			// Obtengo el material

			int length = _renderers.Length;
			for (int i = 0; i < length; i++)
			{
				var renderer = _renderers[i];

				// NOTE: Esto puede causar problemas porque toma el primer objeto inicializado, extrae y asigna su material
				// al campo estático. Una solución sería asignar una referencia 
				// a través del inspector o de una entidad global y asignarla al campo estático para asegurarse
				// que el material correcto es aplicado

				// Si la referencia al material es nula
				if (_sharedMaterial == null)
					// Obtengo el material y asigno la referencia estática
					_sharedMaterial = renderer.material;

				// Obtengo un material a partir del id de color
				var renderMaterial = GetMaterial(ColorID);

				// Asigno el nuevo material
				renderer.material = renderMaterial;
			}
		}


	}

	private Material GetMaterial(int colorID)
	{
		// Si la información de color no existe
		if (_colorInfo == null)
		{
			// Retorno una copia al material original
			return new Material(_sharedMaterial);
		}

		// Obtengo la referencia al material
		var materialReference = _materialReferences.Find(materialRef => materialRef.ColorID.Equals(colorID));

		// Si la referencia al material no existe
		if (materialReference == null)
		{
			// Creo una referencia
			ColorMaterialReference reference = new ColorMaterialReference();

			// Obtengo el color por el id
			var color = _colorInfo.Value[colorID];
			// Creo un nuevo material
			Material material = new Material(_sharedMaterial);

			// Asigno el color al material
			material.color = color;

			// Asigno el id y el material
			reference.ColorID = colorID;
			reference.Material = material;

			// Agrego la referencia al registro
			_materialReferences.Add(reference);

			// Retorno el material recién creado
			return material;
		}
		else
		{
			//Si el material existe

			// Retorno el material
			return materialReference.Material;
		}
	}

	public void Reset()
	{

		// Defino el estado del cubo como Inicial;
		state = CubeState.Inicial;

		if (_useStartValuesOnReset)
		{
			// Restauro los valores iniciales
			Rigidbody.isKinematic = _isKinematicStartValue;
			Rigidbody.useGravity = _useGravityStartValue;
		}
		else
		{
			// Defino el rigidbody como no quinemático
			Rigidbody.isKinematic = false;
		}

		// Desactivo el colisionador del cubo
		SetEnvironmentColliderStatus(false);

		// Reinicio la velocidad del cubo
		Rigidbody.velocity = Vector3.zero;
	}

	private void OnCollisionEnter(Collision collisionInfo)
	{
		// Invoco el evento
		CollisionEnter?.Invoke(collisionInfo);
	}

	public void SetEnvironmentColliderStatus(bool value)
	{
		// Si el colisionador del entorno no está referenciado, retorno
		if (_cubeEnviroCollider == null) return;

		// Defino el estado de activación
		_cubeEnviroCollider.gameObject.SetActive(value);
	}

	public bool ValidCatch(Vector3 position, float errorMargin)
	{
		// Si por alguna razón, el cilindro está tumbado retorno true

		var absDot = Mathf.Abs(Vector3.Dot(Target.up, Vector3.up));

		Debug.DrawRay(Target.position, Target.up, Color.green, 5f);
		Debug.DrawRay(Target.position, Vector3.up, Color.red, 5f);

		// Si el cilindro está tumbado, retorno true
		if (absDot < 0.7f) return true;

		// Si el cilindro está normal, continuo con la operación

		// Hallo la referencia que se encuentra más cercana al punto dado y el margen
		var invalidPoint = System.Array.Find(_referenceCatchInvalidPoints,
		(point) =>
		{
			// Hallo la distancia al cuadrado
			var heightDifference = (point.position - position).y;

			// Retorno si esta distancia es menor al margen de error al cuadrado
			return (Mathf.Abs(heightDifference) < errorMargin);
		});

		// Retorno si la referencia no es nula
		return invalidPoint == null;
	}
}