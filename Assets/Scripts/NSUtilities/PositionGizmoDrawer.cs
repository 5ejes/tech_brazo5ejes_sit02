﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NSUtilities
{

	public class PositionGizmoDrawer : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private float _radius = 0.1f;
		[SerializeField] private Color _wireColor = Color.white;

		[Header("Events")]
		[SerializeField] private UnityEvent _onPositionRegistered;

		private List<Vector3> _registeredPositions = new List<Vector3>();

		public float Radius { get => _radius; set => _radius = value; }
		public Color WireColor { get => _wireColor; set => _wireColor = value; }

		public void RegisterPosition(Vector3 position)
		{
			// Si la lista no contiene la información del parámetro
			if (!_registeredPositions.Contains(position))
			{
				// Registro la posición
				_registeredPositions.Add(position);
			}
		}

		private void OnDrawGizmos()
		{
			// Si la referencia a la lista no es nula
			if (_registeredPositions != null)
			{
				// Si hay al menos un elemento
				if (_registeredPositions.Count > 0)
				{
					// Dibujo los gizmos

					// Defino el color
					Gizmos.color = WireColor;

					// Recorro cada posición
					foreach (var position in _registeredPositions)
					{
						// Dibujo la posición con el radio dado
						Gizmos.DrawWireSphere(position, Radius);
					}
				}
			}
		}
	}
}
