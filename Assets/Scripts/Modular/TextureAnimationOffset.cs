using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSUtilities
{
	public class TextureAnimationOffset : MonoBehaviour
	{
		[Header("Material")]
		[SerializeField] private Material materialObjective;
		[Header("Target Settings")]
		[SerializeField] private MeshRenderer _targetRenderer;
		[SerializeField] private int _materialIndex;
		[Header("Settings")]
		[SerializeField] private bool animateAxisX;

		[SerializeField] private float animationVelocityX = 1f;

		[Space]
		[SerializeField] private bool animateAxisY;

		[SerializeField] private float animationVelocityY = 1f;

		private Material _renderMaterialCopy;


		private float offsetX;

		private float offsetY;

		private void Awake()
		{
			_renderMaterialCopy = new Material(materialObjective);
			_renderMaterialCopy.name = "TransportBeltRenderInstanceMaterial";

			SetMaterialToRenderer(_targetRenderer, _materialIndex, _renderMaterialCopy);
		}


		private void Start()
		{
			StartCoroutine(CouAnimationOffset());
		}

		private void SetMaterialToRenderer(MeshRenderer targetRenderer, int materialIndex, Material renderMaterialCopy)
		{
			if (targetRenderer == null) return;
			if (materialIndex < 0) return;
			if (renderMaterialCopy == null) return;

			// Asigno el material en los parámetros
			Material[] targetMats = targetRenderer.materials;
			targetMats[materialIndex] = renderMaterialCopy;
			targetRenderer.materials = targetMats;
		}

		public void AnimateXBand(bool argAnimate)
		{
			animateAxisX = argAnimate;
		}

		public void AnimateYBand(bool argAnimate)
		{
			animateAxisY = argAnimate;
		}

		private IEnumerator CouAnimationOffset()
		{
			while (true)
			{
				// Si se anima cualquiera de los ejes
				if (animateAxisX || animateAxisY)
				{

					if (animateAxisX)
						offsetX += animationVelocityX * Time.deltaTime;

					if (animateAxisY)
						offsetY += animationVelocityY * Time.deltaTime;

					// Defino un wrapAround
					offsetX %= 1;
					offsetY %= 1;

					// Defino el offset de la textura
					SetTextureOffset(offsetX, offsetY);

				}
				
				// Espero un frame
				yield return null;
			}
		}

		private void SetTextureOffset(float offsetX, float offsetY, string tex = "_MainTex")
		{
			_renderMaterialCopy.SetTextureOffset(tex, new Vector2(offsetX, offsetY));
		}

		private void OnDisable()
		{
			// Reinicio el offset de las texturas
			SetTextureOffset(1, 1);
		}

	}
}