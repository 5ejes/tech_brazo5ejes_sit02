﻿using System.Collections;
using System.Collections.Generic;
using NSBrazo5Ejes.NSDraggingBehaviour;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace NSBrazo5Ejes.NSDraggingBehaviour
{
	public class DragComponentPositionGenerator : MonoBehaviour
	{
		[Header("References")]
		[SerializeField] private LocalPositionParser _parser;
		[SerializeField] private DragMoveComponent _moveComponent;
		[Space]
		[SerializeField] private List<DraggableComponent> _draggables = new List<DraggableComponent>();
		[SerializeField] private Transform[] _bounds;
		[SerializeField] private Vector3[] _rotationArray;
		[SerializeField] private Vector3[] _positionArray;
		[Header("Settings")]
		[SerializeField] private bool _evaluateInside = false;
		[Header("Events")]
		[SerializeField] private UnityEvent _onPositionGeneratedFailure;
		[SerializeField] private UnityEvent _onPositionGeneratedSuccess;

		private TransformBound[] _transformBounds;
		private Vector3 _minPosition = new Vector3(0f, 0f, 0f);
		private Vector3 _maxPosition = new Vector3(0.8f, 0f, 0.8f);

		private void Awake()
		{
			// Si existe una referencia a los límites
			if (_bounds != null)
			{
				// Si hay almenos un límite
				if (_bounds.Length > 0)
				{

					// Creo una variable temporal
					var tempList = new List<TransformBound>();

					// Por cada transform
					for (int i = 0; i < _bounds.Length; i++)
					{
						// Lo añado a la lista
						tempList.Add(_bounds[i]);
					}

					// Almaceno los resultados en el arreglo
					_transformBounds = tempList.ToArray();
				}
			}
		}

		public void OrganizeRegisteredDraggables()
		{
			// Organizo los draggables registrados
			OrganizeDraggables(_draggables.ToArray());
		}

		public void OrganizeDraggables(DraggableComponent[] _draggables)
		{
			StartCoroutine(OrganizeDraggableCo(_draggables));
		}

		private IEnumerator OrganizeDraggableCo(DraggableComponent[] _draggables)
		{
			// Si la lista existe, el LocalPositionParser y el DragMoveComponent
			if (_draggables != null && _parser && _moveComponent)
			{
				// Si hay al menos un elemento en la lista
				if (_draggables.Length > 0)
				{
					// Recorro cada draggable
					for (int draggableIndex = 0; draggableIndex < _draggables.Length; draggableIndex++)
					{
						// Obtengo el draggable actual
						var currentDraggable = _draggables[draggableIndex];

						// Si existe el draggable actual
						if (currentDraggable != null)
						{

							// Intento generar una posición válida N veces
							for (int tries = 0; tries < 1000; tries++)
							{

								// Genero una posición en 0
								Vector3 localPosition = Vector3.zero;

								int length = _positionArray.Length;
								if (length > 0)
								{
									// Genero un índice aleatorio
									int positionIndex = Random.Range(0, length);

									// Obtengo la posición
									localPosition = _positionArray[positionIndex];
								}
								else
								{
									// Genero una posición local aleatoria
									localPosition = GenerateRandomPosition(_minPosition, _maxPosition);
								}

								Quaternion rotation = Quaternion.identity;

								length = _rotationArray.Length;
								if (length > 0)
								{
									// Genero un índice aleatorio
									int rotationIndex = Random.Range(0, length);

									// Obteno la rotación
									rotation = Quaternion.Euler(_rotationArray[rotationIndex]);	
								}
								else
								{
									// Genero un ángulo de rotación
									float snap = 90f;
									var generatedteAngle = Random.Range(0f, 270f);
									generatedteAngle = Mathf.Round(generatedteAngle / snap) * snap;
									rotation = Quaternion.AngleAxis(generatedteAngle, Vector3.up);
								}

								// var localPosition = new Vector3(0.75f, 0, 0.45f);
								// var rotation = Quaternion.identity;

								// Convierto la posición a coordenadas globales
								var worldPosition = _parser.ParseVector(localPosition);

								// Trato de mover el draggable actual a la posición
								if (_moveComponent.TryMoveDraggable(currentDraggable, worldPosition, Vector3.zero, rotation, _transformBounds, _evaluateInside))
								{
									// Actualizo la rotación del draggable
									currentDraggable.transform.rotation = rotation;

									// Espero un frame
									yield return null;

									// Ejecuto el evento
									_onPositionGeneratedSuccess.WrapInvoke();

									// Salgo del bucle
									break;
								}
								else
								{
									// Ejecuto el evento
									_onPositionGeneratedFailure.WrapInvoke();
								}
							}
						}
					}
				}
			}
		}
		private Vector3 GenerateRandomPosition(Vector3 min, Vector3 max)
		{
			// Genero una posición aleatoria
			var output = new Vector3();

			output.x = Random.Range(min.x, max.x);
			output.y = Random.Range(min.y, max.y);
			output.z = Random.Range(min.z, max.z);

			// Retorno la posición
			return output;
		}
	}
}