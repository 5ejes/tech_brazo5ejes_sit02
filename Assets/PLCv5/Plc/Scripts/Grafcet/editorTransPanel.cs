﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class editorTransPanel : MonoBehaviour {

	public GameObject _situation;

	public GameObject _actualElement;

	public Text _title;
	public Text _nameLabel;
	public InputField _nameInput;

	public Image _overAddImage;

	public Button _xBt;
	public Button _saveBt;

	public Button _addBt;
	public GameObject _contextualMenu;

	public ScrollRect _scrollContainer;
	public GameObject _scrollContent;

	public Button _opButton;
	public Button _condButton;
	public Button _openButton;
	public Button _closeButton;
	public Button _exitButton;

	public int operatorsCount = 0;

	// Use this for initialization
	void Start () {
		_xBt.onClick.AddListener (exitAction);
		_saveBt.onClick.AddListener (saveAction);

		_title.font = (Font)Resources.Load ("Fonts/ArialBold28");
		_saveBt.GetComponentInChildren<Text> ().font = (Font)Resources.Load ("Fonts/ArialBold28");

		_opButton.GetComponentInChildren<Text> ().font = (Font)Resources.Load ("Fonts/ArialBold28");
		_condButton.GetComponentInChildren<Text> ().font = (Font)Resources.Load ("Fonts/ArialBold28");
		_openButton.GetComponentInChildren<Text> ().font = (Font)Resources.Load ("Fonts/ArialBold28");
		_closeButton.GetComponentInChildren<Text> ().font = (Font)Resources.Load ("Fonts/ArialBold28");
		_exitButton.GetComponentInChildren<Text> ().font = (Font)Resources.Load ("Fonts/ArialBold28");

		_contextualMenu.transform.localScale = new Vector3 (0,0,0);

		_addBt.onClick.AddListener (showAddBtAction);

		_opButton.onClick.AddListener (opBtAction);
		_condButton.onClick.AddListener (condBtAction);
		_openButton.onClick.AddListener (openBtAction);
		_closeButton.onClick.AddListener (closeBtAction);

		_exitButton.onClick.AddListener (hideAddBtClickAction);

		_overAddImage.CrossFadeAlpha (0f, 0.01f, false);
		_overAddImage.raycastTarget = false;
	}

	public void configureEditor(GameObject _element){
		_actualElement = _element;

		_nameInput.text = "";

		_title.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='editor_title']").InnerText;
		_nameLabel.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='name']").InnerText + ":";
		_saveBt.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='save']").InnerText;

		_opButton.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='operation']").InnerText;
		_condButton.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='value']").InnerText;
		_openButton.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='agr_left']").InnerText;
		_closeButton.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='agr_right']").InnerText;
		_exitButton.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='cancel_lower']").InnerText;


		if (_actualElement.GetComponent<TransitionClass> ()._titleElement.text != "?") {
			_nameInput.text = _actualElement.GetComponent<TransitionClass> ()._titleElement.text;
		} else {
			_nameInput.text = "";
		}

		if(operatorsCount > 0){
			for(int i = 0; i < operatorsCount; i++){
				GameObject _opElement = GameObject.Find("operator_" + i.ToString());
				Destroy (_opElement);
			}
			operatorsCount = 0;
		}

        string _dataTest = JsonUtility.ToJson(_actualElement.GetComponent<TransitionClass>().getJSONData());
        //Debug.Log(_dataTest);
       
        if (_actualElement.GetComponent<TransitionClass> ()._elements != null){
			for(int i = 0; i < _actualElement.GetComponent<TransitionClass> ()._elements.Count; i++){

				switch(_actualElement.GetComponent<TransitionClass> ()._elements[i].editorType){
				case 2:
					GameObject _clone2 = Instantiate (_situation.GetComponent<GrafcetMainClass> ().conditionElementPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
					_clone2.transform.SetParent (_scrollContent.transform, false);
					_clone2.name = _actualElement.GetComponent<TransitionClass> ()._elements [i].name;

                        _clone2.GetComponent<conditionElementClass>()._title.text = _actualElement.GetComponent<TransitionClass>()._elements[i].title;
                        _clone2.GetComponent<conditionElementClass>()._type = _actualElement.GetComponent<TransitionClass>()._elements[i].type;
                        _clone2.GetComponent<conditionElementClass>()._id = _actualElement.GetComponent<TransitionClass>()._elements[i].id;
                        _clone2.GetComponent<conditionElementClass>()._editorType = _actualElement.GetComponent<TransitionClass>()._elements[i].editorType;
                        _clone2.GetComponent<conditionElementClass>()._typeValue = _actualElement.GetComponent<TransitionClass>()._elements[i].typeValue;
                        _clone2.GetComponent<conditionElementClass>()._index = _actualElement.GetComponent<TransitionClass>()._elements[i].index;
                        _clone2.GetComponent<conditionElementClass>()._value = _actualElement.GetComponent<TransitionClass>()._elements[i].value;
                        _clone2.GetComponent<conditionElementClass>()._index2 = _actualElement.GetComponent<TransitionClass>()._elements[i].index2;
                        _clone2.GetComponent<conditionElementClass>()._value2 = _actualElement.GetComponent<TransitionClass>()._elements[i].value2;
                        _clone2.GetComponent<conditionElementClass>()._index3 = _actualElement.GetComponent<TransitionClass>()._elements[i].index3;
                        _clone2.GetComponent<conditionElementClass>()._value3 = _actualElement.GetComponent<TransitionClass>()._elements[i].value3;
                        _clone2.GetComponent<conditionElementClass>()._isInverse = _actualElement.GetComponent<TransitionClass>()._elements[i].isInverse;
                        _clone2.GetComponent<conditionElementClass>()._constant1 = _actualElement.GetComponent<TransitionClass>()._elements[i].constant1;
                        _clone2.GetComponent<conditionElementClass>()._constant2 = _actualElement.GetComponent<TransitionClass>()._elements[i].constant2;
                        _clone2.GetComponent<conditionElementClass>()._typeRobotics = _actualElement.GetComponent<TransitionClass>()._elements[i].typeRobotics;

                        _clone2.GetComponent<conditionElementClass>().setInv(_clone2.GetComponent<conditionElementClass>()._isInverse);

                        if (Manager.Instance.globalRoboticsMode == true){

                            switch (_clone2.GetComponent<conditionElementClass>()._typeRobotics)
                            {
                                case 1:
                                case 2:
                                case 4:
                                case 5:
                                case 7:
                                case 8:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                    _clone2.GetComponent<conditionElementClass>()._title.fontSize = 22;
                                    break;
                            }


                            _clone2.GetComponent<conditionElementClass>().childCondition = new conditionElementClass();
                            _clone2.GetComponent<conditionElementClass>().childCondition._type = 0;
                            _clone2.GetComponent<conditionElementClass>().childCondition._editorType = 2;
                            _clone2.GetComponent<conditionElementClass>().childCondition._typeValue = _actualElement.GetComponent<TransitionClass>()._elements[i].childOperators[0].typeValue;

                            _clone2.GetComponent<conditionElementClass>().childCondition._index = _actualElement.GetComponent<TransitionClass>()._elements[i].childOperators[0].index;
                            _clone2.GetComponent<conditionElementClass>().childCondition._value = _actualElement.GetComponent<TransitionClass>()._elements[i].childOperators[0].value;
                            _clone2.GetComponent<conditionElementClass>().childCondition._index2 = _actualElement.GetComponent<TransitionClass>()._elements[i].childOperators[0].index2;
                            _clone2.GetComponent<conditionElementClass>().childCondition._value2 = _actualElement.GetComponent<TransitionClass>()._elements[i].childOperators[0].value2;
                            _clone2.GetComponent<conditionElementClass>().childCondition._index3 = _actualElement.GetComponent<TransitionClass>()._elements[i].childOperators[0].index3;
                            _clone2.GetComponent<conditionElementClass>().childCondition._value3 = _actualElement.GetComponent<TransitionClass>()._elements[i].childOperators[0].value3;
                            _clone2.GetComponent<conditionElementClass>().childCondition._isInverse = _actualElement.GetComponent<TransitionClass>()._elements[i].childOperators[0].isInverse;
                            _clone2.GetComponent<conditionElementClass>().childCondition._constant1 = _actualElement.GetComponent<TransitionClass>()._elements[i].childOperators[0].constant1;
                            _clone2.GetComponent<conditionElementClass>().childCondition._constant2 = _actualElement.GetComponent<TransitionClass>()._elements[i].childOperators[0].constant2;
                            _clone2.GetComponent<conditionElementClass>().childCondition._typeRobotics = _actualElement.GetComponent<TransitionClass>()._elements[i].childOperators[0].typeRobotics;

                    }

					
					break;
				case 4:
					GameObject _clone3 = Instantiate (_situation.GetComponent<GrafcetMainClass> ().operationElementPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
					_clone3.transform.SetParent (_scrollContent.transform, false);
					_clone3.name = _actualElement.GetComponent<TransitionClass> ()._elements [i].name;
					_clone3.GetComponent<operationElementClass> ()._title.text = _actualElement.GetComponent<TransitionClass> ()._elements [i].title;
					_clone3.GetComponent<operationElementClass> ()._type = _actualElement.GetComponent<TransitionClass> ()._elements [i].type;
					_clone3.GetComponent<operationElementClass> ()._id = _actualElement.GetComponent<TransitionClass> ()._elements [i].id;
					_clone3.GetComponent<operationElementClass> ()._editorType = _actualElement.GetComponent<TransitionClass> ()._elements [i].editorType;
					_clone3.GetComponent<operationElementClass> ()._typeValue = _actualElement.GetComponent<TransitionClass> ()._elements [i].typeValue;
					_clone3.GetComponent<operationElementClass> ()._index = _actualElement.GetComponent<TransitionClass> ()._elements [i].index;
					_clone3.GetComponent<operationElementClass> ()._value = _actualElement.GetComponent<TransitionClass> ()._elements [i].value;
					_clone3.GetComponent<operationElementClass> ()._index2 = _actualElement.GetComponent<TransitionClass> ()._elements [i].index2;
					_clone3.GetComponent<operationElementClass> ()._value2 = _actualElement.GetComponent<TransitionClass> ()._elements [i].value2;
					_clone3.GetComponent<operationElementClass> ()._index3 = _actualElement.GetComponent<TransitionClass> ()._elements [i].index3;
					_clone3.GetComponent<operationElementClass> ()._value3 = _actualElement.GetComponent<TransitionClass> ()._elements [i].value3;
					_clone3.GetComponent<operationElementClass> ()._isInverse = _actualElement.GetComponent<TransitionClass> ()._elements [i].isInverse;
					_clone3.GetComponent<operationElementClass> ()._constant1 = _actualElement.GetComponent<TransitionClass> ()._elements [i].constant1;
					_clone3.GetComponent<operationElementClass> ()._constant2 = _actualElement.GetComponent<TransitionClass> ()._elements [i].constant2;
					break;
				case 5:
					GameObject _clone5 = Instantiate (_situation.GetComponent<GrafcetMainClass> ().groupElementPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
					_clone5.transform.SetParent (_scrollContent.transform, false);
					_clone5.name = _actualElement.GetComponent<TransitionClass> ()._elements [i].name;
					_clone5.GetComponent<groupElementClass> ()._title.text = _actualElement.GetComponent<TransitionClass> ()._elements [i].title;
					_clone5.GetComponent<groupElementClass> ()._type = _actualElement.GetComponent<TransitionClass> ()._elements [i].type;
					_clone5.GetComponent<groupElementClass> ()._id = _actualElement.GetComponent<TransitionClass> ()._elements [i].id;
					_clone5.GetComponent<groupElementClass> ()._editorType = _actualElement.GetComponent<TransitionClass> ()._elements [i].editorType;
					_clone5.GetComponent<groupElementClass> ()._typeValue = _actualElement.GetComponent<TransitionClass> ()._elements [i].typeValue;
					_clone5.GetComponent<groupElementClass> ()._index = _actualElement.GetComponent<TransitionClass> ()._elements [i].index;
					_clone5.GetComponent<groupElementClass> ()._value = _actualElement.GetComponent<TransitionClass> ()._elements [i].value;
					_clone5.GetComponent<groupElementClass> ()._index2 = _actualElement.GetComponent<TransitionClass> ()._elements [i].index2;
					_clone5.GetComponent<groupElementClass> ()._value2 = _actualElement.GetComponent<TransitionClass> ()._elements [i].value2;
					_clone5.GetComponent<groupElementClass> ()._index3 = _actualElement.GetComponent<TransitionClass> ()._elements [i].index3;
					_clone5.GetComponent<groupElementClass> ()._value3 = _actualElement.GetComponent<TransitionClass> ()._elements [i].value3;
					_clone5.GetComponent<groupElementClass> ()._isInverse = _actualElement.GetComponent<TransitionClass> ()._elements [i].isInverse;
					_clone5.GetComponent<groupElementClass> ()._constant1 = _actualElement.GetComponent<TransitionClass> ()._elements [i].constant1;
					_clone5.GetComponent<groupElementClass> ()._constant2 = _actualElement.GetComponent<TransitionClass> ()._elements [i].constant2;

					_clone5.GetComponent<groupElementClass> ().configureElement (groupElementClass.typesOfGroup.Open);
					break;
				case 6:
					GameObject _clone6 = Instantiate (_situation.GetComponent<GrafcetMainClass> ().groupElementPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
					_clone6.transform.SetParent (_scrollContent.transform, false);
					_clone6.name = _actualElement.GetComponent<TransitionClass> ()._elements [i].name;
					_clone6.GetComponent<groupElementClass> ()._title.text = _actualElement.GetComponent<TransitionClass> ()._elements [i].title;
					_clone6.GetComponent<groupElementClass> ()._type = _actualElement.GetComponent<TransitionClass> ()._elements [i].type;
					_clone6.GetComponent<groupElementClass> ()._id = _actualElement.GetComponent<TransitionClass> ()._elements [i].id;
					_clone6.GetComponent<groupElementClass> ()._editorType = _actualElement.GetComponent<TransitionClass> ()._elements [i].editorType;
					_clone6.GetComponent<groupElementClass> ()._typeValue = _actualElement.GetComponent<TransitionClass> ()._elements [i].typeValue;
					_clone6.GetComponent<groupElementClass> ()._index = _actualElement.GetComponent<TransitionClass> ()._elements [i].index;
					_clone6.GetComponent<groupElementClass> ()._value = _actualElement.GetComponent<TransitionClass> ()._elements [i].value;
					_clone6.GetComponent<groupElementClass> ()._index2 = _actualElement.GetComponent<TransitionClass> ()._elements [i].index2;
					_clone6.GetComponent<groupElementClass> ()._value2 = _actualElement.GetComponent<TransitionClass> ()._elements [i].value2;
					_clone6.GetComponent<groupElementClass> ()._index3 = _actualElement.GetComponent<TransitionClass> ()._elements [i].index3;
					_clone6.GetComponent<groupElementClass> ()._value3 = _actualElement.GetComponent<TransitionClass> ()._elements [i].value3;
					_clone6.GetComponent<groupElementClass> ()._isInverse = _actualElement.GetComponent<TransitionClass> ()._elements [i].isInverse;
					_clone6.GetComponent<groupElementClass> ()._constant1 = _actualElement.GetComponent<TransitionClass> ()._elements [i].constant1;
					_clone6.GetComponent<groupElementClass> ()._constant2 = _actualElement.GetComponent<TransitionClass> ()._elements [i].constant2;

					_clone6.GetComponent<groupElementClass> ().configureElement (groupElementClass.typesOfGroup.Close);
					break;
				}
				operatorsCount++;
			}

			_addBt.transform.SetAsLastSibling ();
			Invoke ("goToEndInScroll", 0.1f);
		}
	}

	public void exitAction(){

		_actualElement.GetComponent<TransitionClass> ().closeEditElements ();

		iTween.MoveTo(
			_situation.GetComponent<GrafcetMainClass> ()._editorTransPanel,
			iTween.Hash(
				"position", new Vector3(0,1500,0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal",true
			)
		);

		_situation.GetComponent<GrafcetMainClass> ().deactivateBackground ();

		Invoke ("goToInitialState", 1f);
	}

	private void saveAction(){

		if(this._nameInput.text != ""){
			_actualElement.GetComponent<TransitionClass> ()._titleElement.text = this._nameInput.text;
		}
		else {
			_actualElement.GetComponent<TransitionClass> ()._titleElement.text = "?";
		}

		_actualElement.GetComponent<TransitionClass> ()._elements = new List<operatorStructure> ();

		for(int i = 0; i < this.operatorsCount; i++){
			GameObject _element = GameObject.Find ("operator_" + i.ToString ());

			operatorStructure _strOperator = new operatorStructure ();

			if (_element.GetComponent<operationElementClass> () != null) {
				//is operation
				_strOperator.name = _element.name;
				_strOperator.title = _element.GetComponent<operationElementClass> ()._title.text;
				_strOperator.type = 0;
				_strOperator.id = _element.GetComponent<operationElementClass> ()._id;
				_strOperator.editorType = 4;
				_strOperator.typeValue = _element.GetComponent<operationElementClass> ()._typeValue;
				_strOperator.index = _element.GetComponent<operationElementClass> ()._index;
				_strOperator.value = _element.GetComponent<operationElementClass> ()._value;
				_strOperator.index2 = _element.GetComponent<operationElementClass> ()._index2;
				_strOperator.value2 = _element.GetComponent<operationElementClass> ()._value2;
				_strOperator.index3 = _element.GetComponent<operationElementClass> ()._index3;
				_strOperator.value3 = _element.GetComponent<operationElementClass> ()._value3;
				_strOperator.isInverse = _element.GetComponent<operationElementClass> ()._isInverse;
				_strOperator.constant1 = _element.GetComponent<operationElementClass> ()._constant1;
				_strOperator.constant2 = _element.GetComponent<operationElementClass> ()._constant2;

			} else if(_element.GetComponent<conditionElementClass> () != null){
				//is condition
				_strOperator.name = _element.name;
				_strOperator.title = _element.GetComponent<conditionElementClass> ()._title.text;
				_strOperator.type = 0;
				_strOperator.id = _element.GetComponent<conditionElementClass> ()._id;
				_strOperator.editorType = 2;
				_strOperator.typeValue = _element.GetComponent<conditionElementClass> ()._typeValue;
				_strOperator.index = _element.GetComponent<conditionElementClass> ()._index;
				_strOperator.value = _element.GetComponent<conditionElementClass> ()._value;
				_strOperator.index2 = _element.GetComponent<conditionElementClass> ()._index2;
				_strOperator.value2 = _element.GetComponent<conditionElementClass> ()._value2;
				_strOperator.index3 = _element.GetComponent<conditionElementClass> ()._index3;
				_strOperator.value3 = _element.GetComponent<conditionElementClass> ()._value3;
				_strOperator.isInverse = _element.GetComponent<conditionElementClass> ()._isInverse;
				_strOperator.constant1 = _element.GetComponent<conditionElementClass> ()._constant1;
				_strOperator.constant2 = _element.GetComponent<conditionElementClass> ()._constant2;
                _strOperator.typeRobotics = _element.GetComponent<conditionElementClass>()._typeRobotics;

                if (Manager.Instance.globalRoboticsMode == true)
                {
                    _strOperator.childOperators = new operatorStructure[1];
                    _strOperator.childOperators[0].type = 0;
                    _strOperator.childOperators[0].editorType = 2;
                    _strOperator.childOperators[0].typeValue = _element.GetComponent<conditionElementClass>().childCondition._typeValue;
                    _strOperator.childOperators[0].index = _element.GetComponent<conditionElementClass>().childCondition._index;
                    _strOperator.childOperators[0].value = _element.GetComponent<conditionElementClass>().childCondition._value;
                    _strOperator.childOperators[0].index2 = _element.GetComponent<conditionElementClass>().childCondition._index2;
                    _strOperator.childOperators[0].value2 = _element.GetComponent<conditionElementClass>().childCondition._value2;
                    _strOperator.childOperators[0].index3 = _element.GetComponent<conditionElementClass>().childCondition._index3;
                    _strOperator.childOperators[0].value3 = _element.GetComponent<conditionElementClass>().childCondition._value3;
                    _strOperator.childOperators[0].isInverse = _element.GetComponent<conditionElementClass>().childCondition._isInverse;
                    _strOperator.childOperators[0].constant1 = _element.GetComponent<conditionElementClass>().childCondition._constant1;
                    _strOperator.childOperators[0].constant2 = _element.GetComponent<conditionElementClass>().childCondition._constant2;
                    _strOperator.childOperators[0].typeRobotics = _element.GetComponent<conditionElementClass>().childCondition._typeRobotics;
                }
			} else if(_element.GetComponent<groupElementClass> () != null){
				//is group
				_strOperator.name = _element.name;
				_strOperator.title = _element.GetComponent<groupElementClass> ()._title.text;
				_strOperator.type = 0;
				_strOperator.id = _element.GetComponent<groupElementClass> ()._id;
				if (_element.GetComponent<groupElementClass> ().typeElement == groupElementClass.typesOfGroup.Open) {
					_strOperator.editorType = 5;
				} else {
					_strOperator.editorType = 6;
				}
				_strOperator.typeValue = _element.GetComponent<groupElementClass> ()._typeValue;
				_strOperator.index = _element.GetComponent<groupElementClass> ()._index;
				_strOperator.value = _element.GetComponent<groupElementClass> ()._value;
				_strOperator.index2 = _element.GetComponent<groupElementClass> ()._index2;
				_strOperator.value2 = _element.GetComponent<groupElementClass> ()._value2;
				_strOperator.index3 = _element.GetComponent<groupElementClass> ()._index3;
				_strOperator.value3 = _element.GetComponent<groupElementClass> ()._value3;
				_strOperator.isInverse = _element.GetComponent<groupElementClass> ()._isInverse;
				_strOperator.constant1 = _element.GetComponent<groupElementClass> ()._constant1;
				_strOperator.constant2 = _element.GetComponent<groupElementClass> ()._constant2;
			}
			_actualElement.GetComponent<TransitionClass> ()._elements.Add (_strOperator);
		}

		exitAction ();
	}

	private void goToInitialState(){
		gameObject.transform.SetParent (GameObject.Find ("Canvas/Grafcet").transform, true);
	}

	private void showAddBtAction(){
		Vector3 _goPosition = new Vector3 (_addBt.transform.position.x, _contextualMenu.transform.position.y, _contextualMenu.transform.position.z);

		_contextualMenu.transform.position = _goPosition;
		_contextualMenu.transform.localPosition = new Vector3 (_contextualMenu.transform.localPosition.x + 126f, _contextualMenu.transform.localPosition.y, _contextualMenu.transform.localPosition.x);

		iTween.ScaleTo (_contextualMenu, new Vector3 (1f, 1f, 1f), 0.3f);
		_overAddImage.CrossFadeAlpha (1f, 0.3f, false);
		_overAddImage.raycastTarget = true;
	}

	private void opBtAction(){
		hideAddBtAction (false);

		GameObject _clone = Instantiate(_situation.GetComponent<GrafcetMainClass>().operationElementPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_clone.transform.SetParent (_scrollContent.transform, false);

		_clone.GetComponent<operationElementClass> ()._id = operatorsCount;
		_clone.name = "operator_" + operatorsCount.ToString();
		operatorsCount++;

		_clone.GetComponent<operationElementClass> ().openEditPostAction ();

		_addBt.transform.SetAsLastSibling ();
		Invoke ("goToEndInScroll", 0.1f);
	}

	private void condBtAction(){
		hideAddBtAction (false);

		GameObject _clone = Instantiate(_situation.GetComponent<GrafcetMainClass>().conditionElementPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_clone.transform.SetParent (_scrollContent.transform, false);

		_clone.GetComponent<conditionElementClass> ()._id = operatorsCount;
		_clone.name = "operator_" + operatorsCount.ToString();
		operatorsCount++;

		_clone.GetComponent<conditionElementClass> ().openEditPostAction ();

		_addBt.transform.SetAsLastSibling ();
		Invoke ("goToEndInScroll", 0.1f);
	}

	private void openBtAction(){
		hideAddBtAction ();

		GameObject _clone = Instantiate(_situation.GetComponent<GrafcetMainClass>().groupElementPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_clone.transform.SetParent (_scrollContent.transform, false);

		_clone.GetComponent<groupElementClass> ()._id = operatorsCount;
		_clone.name = "operator_" + operatorsCount.ToString();
		operatorsCount++;

		_clone.GetComponent<groupElementClass> ().configureElement (groupElementClass.typesOfGroup.Open);

		_addBt.transform.SetAsLastSibling ();
		Invoke ("goToEndInScroll", 0.1f);
	}

	private void closeBtAction(){
		hideAddBtAction ();

		GameObject _clone = Instantiate(_situation.GetComponent<GrafcetMainClass>().groupElementPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_clone.transform.SetParent (_scrollContent.transform, false);

		_clone.GetComponent<groupElementClass> ()._id = operatorsCount;
		_clone.name = "operator_" + operatorsCount.ToString();
		operatorsCount++;

		_clone.GetComponent<groupElementClass> ().configureElement (groupElementClass.typesOfGroup.Close);

		_addBt.transform.SetAsLastSibling ();
		Invoke ("goToEndInScroll", 0.1f);
	}

	public void showOverElement(){
		_overAddImage.CrossFadeAlpha (1f, 0.3f, false);
		_overAddImage.raycastTarget = true;
	}

	public void hideOverElement(){
		_overAddImage.CrossFadeAlpha (0f, 0.3f, false);
		Invoke ("deactivateRayCastBgAction", 0.4f);
	}

	private void goToEndInScroll(){
		_scrollContainer.horizontalNormalizedPosition = 1;
	}

	private void hideAddBtAction(bool hideBackgroud = true){
		if(hideBackgroud == true){
			_overAddImage.CrossFadeAlpha (0f, 0.3f, false);
			Invoke ("deactivateRayCastBgAction", 0.4f);
		}
		iTween.ScaleTo (_contextualMenu, new Vector3 (0f, 0f, 0f), 0.3f);
	}

	private void hideAddBtClickAction(){
		hideAddBtAction (true);
	}

	private void deactivateRayCastBgAction(){
		_overAddImage.raycastTarget = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
