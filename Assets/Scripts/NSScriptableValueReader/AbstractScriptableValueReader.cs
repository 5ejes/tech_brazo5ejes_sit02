﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSScriptableValueReader
{
	public abstract class AbstractScriptableValueReader : MonoBehaviour
	{
		public abstract void Read();
	}
}
