﻿#pragma warning disable 0649
using UnityEngine;

namespace NSUtilities
{
    public class FollowMouse : MonoBehaviour
    {
        #region members

        [SerializeField] private bool followMouse;

        private RectTransform meRectTransform;

        private Vector2 initPosition;

        #endregion

        #region accesors

        public bool _followMouse
        {
            get { return followMouse; }
            set { followMouse = value; }
        }

        #endregion

        #region monoBehaviur

        private void Awake()
        {
            meRectTransform = GetComponent<RectTransform>();
            initPosition = meRectTransform.position;
        }

        private void Update()
        {
            FollowMouseExecute();
        }

        #endregion

        #region private methods

        private void FollowMouseExecute()
        {
            if (followMouse)
                meRectTransform.position = Vector2.Lerp(meRectTransform.position, Input.mousePosition, 0.85f);
            else
                meRectTransform.position = Vector2.Lerp(meRectTransform.position, initPosition, 0.85f);
        }

        #endregion

        #region public methods

        public void ResetPosition()
        {
            //followMouse = false;
            transform.position = initPosition;
        }

        #endregion
    }
}