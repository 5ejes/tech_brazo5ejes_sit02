﻿using System.Collections;
using System.Collections.Generic;
using NSBrazo5Ejes.NSTransportBelt;
using NSUtilities;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
  public class AreaExerciseGenerator : AbstractExerciseGenerator
  {

    [Header("Settings")]
    [Header("Positions")]
    [SerializeField] private Transform[] _areaTokenReferences;

    [Header("Events")]
    [SerializeField] private AreaExerciseEvent _onExerciseGenerated;

    [Header("Debug")]
    [SerializeField] private bool _drawDebug = false;
    [SerializeField] private Color _gizmosColor = Color.blue;

		public Transform[] AreaTokenReferences { get => _areaTokenReferences; set => _areaTokenReferences = value; }

		public override void GenerateRoutine()
    {
      // Genero una variable con las posiciones
      Vector3[] generatedPositions = null;

      // Genero un arreglo con todas las posiciones
      generatedPositions = GenerateCubePositions(GeneratedPositionCount);

      // Creo una lista de tokens
      var tokens = new List<AreaEvaluationToken>();

      // Creo por cada referencia un token
      foreach (var tokenReference in AreaTokenReferences)
      {
        // Creo el token de evaluación
        AreaEvaluationToken evaluationToken = new AreaEvaluationToken();

        evaluationToken.PositionInfo = tokenReference;

        evaluationToken.CheckLayerMask = TokenCheckLayer;
        evaluationToken.TargetEnumID = TargetEnumId;

        evaluationToken.CheckedElementGoal = 3;

        // Lo agrego a la lista
        tokens.Add(evaluationToken);
      }

      // Creo la rutina de ejercicio
      AreaExerciseRoutine routine = new AreaExerciseRoutine(tokens.ToArray());

      // Anexo las posiciones
      routine.Positions.AddRange(generatedPositions);

      // Ejecuto el evento
      _onExerciseGenerated.WrapInvoke(routine);
    }

    private void OnDrawGizmos()
    {
      // Si puedo dibujar las guías visuales
      if (_drawDebug && AreaTokenReferences != null)
      {
        // Si hay más de un elemento en la lista
        if (AreaTokenReferences.Length > 0)
        {
          // Por cada referencia para el token
          foreach (var tokenReference in AreaTokenReferences)
          {
						
						// Si el token actual no existe, continúo
						if(tokenReference == null) continue;

            // Defino el color del dibujado
            Gizmos.color = _gizmosColor;

            var rotationMatrix = Matrix4x4.TRS((Vector3)tokenReference.position, (Quaternion)tokenReference.rotation, (Vector3)tokenReference.localScale);
            Gizmos.matrix = rotationMatrix;

            // Dibujo el cubo
            Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
          }
        }
      }
    }
  }
}
