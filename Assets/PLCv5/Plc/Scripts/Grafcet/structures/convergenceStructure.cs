﻿using System;

[Serializable]
public struct convergenceStructure
{
	public float x;
	public float y;
	public int _id;
	public string name;
	public string _type;
	public int _editorType;
	public int _branches;
	public connModuleStructure _d_conn;
	public connModuleStructure[] conns;
}