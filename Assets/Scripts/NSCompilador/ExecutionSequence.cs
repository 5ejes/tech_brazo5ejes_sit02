﻿using System.Collections;
using System.Collections.Generic;
using NSEditorAcciones;
using UnityEngine;

namespace NSCompilador
{
	public class ExecutionSequence
	{
		private LogicBlock[] _sequence;
		public LogicBlock[] Sequence => _sequence;

		public ExecutionSequence(LogicBlock[] argBlocks)
		{
			// Defino la secuencia de datos
			_sequence = argBlocks;
		}
	}
}