﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gridDelete : MonoBehaviour {

	public GameObject _situation;

	public Button _cancelBt;

	public Button input_1_1;
	public Button input_1_2;
	public Button input_1_3;
	public Button input_1_4;

	public Button input_2_1;
	public Button input_2_2;
	public Button input_2_3;
	public Button input_2_4;

	public Button input_3_1;
	public Button input_3_2;
	public Button input_3_3;
	public Button input_3_4;

	public Button input_4_1;
	public Button input_4_2;
	public Button input_4_3;
	public Button input_4_4;

	public Button output_1_1;
	public Button output_1_2;
	public Button output_1_3;
	public Button output_1_4;
	public Button output_1_5;
	public Button output_1_6;

	public Button output_2_1;
	public Button output_2_2;
	public Button output_2_3;
	public Button output_2_4;
	public Button output_2_5;
	public Button output_2_6;

	public Button output_3_1;
	public Button output_3_2;
	public Button output_3_3;
	public Button output_3_4;
	public Button output_3_5;
	public Button output_3_6;

	public Button output_4_1;
	public Button output_4_2;
	public Button output_4_3;
	public Button output_4_4;
	public Button output_4_5;
	public Button output_4_6;

	private Vector3 button_initial_position;
	private Vector3 button_hide_position;

	// Use this for initialization
	void Start () {

		button_initial_position = _cancelBt.gameObject.transform.localPosition;
		button_hide_position = new Vector3 (button_initial_position.x, button_initial_position.y - 200f, button_initial_position.z);

		_cancelBt.gameObject.transform.localPosition = button_hide_position;

		_cancelBt.gameObject.SetActive (false);
		hideButtons ();

		input_1_1.transform.localScale = new Vector3 (0, 0, 0);
		input_1_2.transform.localScale = new Vector3 (0, 0, 0);
		if(input_1_3 != null){
			input_1_3.transform.localScale = new Vector3 (0, 0, 0);
			input_1_3.onClick.AddListener(() => { deleteInputElement(1,3); });
		}
		if (input_1_4 != null) {
			input_1_4.transform.localScale = new Vector3 (0, 0, 0);
			input_1_4.onClick.AddListener(() => { deleteInputElement(1,4); });
		}
		input_2_1.transform.localScale = new Vector3 (0, 0, 0);
		input_2_2.transform.localScale = new Vector3 (0, 0, 0);
		if (input_2_3 != null) {
			input_2_3.transform.localScale = new Vector3 (0, 0, 0);
			input_2_3.onClick.AddListener(() => { deleteInputElement(2,3); });
		}
		if (input_2_4 != null) {
			input_2_4.transform.localScale = new Vector3 (0, 0, 0);
			input_2_4.onClick.AddListener(() => { deleteInputElement(2,4); });
		}
		input_3_1.transform.localScale = new Vector3 (0, 0, 0);
		input_3_2.transform.localScale = new Vector3 (0, 0, 0);
		if (input_3_3 != null) {
			input_3_3.transform.localScale = new Vector3 (0, 0, 0);
			input_3_3.onClick.AddListener(() => { deleteInputElement(3,3); });
		}
		if (input_3_4 != null) {
			input_3_4.transform.localScale = new Vector3 (0, 0, 0);
			input_3_4.onClick.AddListener(() => { deleteInputElement(3,4); });
		}
		input_4_1.transform.localScale = new Vector3 (0, 0, 0);
		input_4_2.transform.localScale = new Vector3 (0, 0, 0);
		if (input_4_3 != null) {
			input_4_3.transform.localScale = new Vector3 (0, 0, 0);
			input_4_3.onClick.AddListener(() => { deleteInputElement(4,3); });
		}
		if (input_4_4 != null) {
			input_4_4.transform.localScale = new Vector3 (0, 0, 0);
			input_4_4.onClick.AddListener(() => { deleteInputElement(4,4); });
		}

		output_1_1.transform.localScale = new Vector3 (0, 0, 0);
		output_1_2.transform.localScale = new Vector3 (0, 0, 0);
		output_1_3.transform.localScale = new Vector3 (0, 0, 0);
		output_1_4.transform.localScale = new Vector3 (0, 0, 0);

		output_2_1.transform.localScale = new Vector3 (0, 0, 0);
		output_2_2.transform.localScale = new Vector3 (0, 0, 0);
		output_2_3.transform.localScale = new Vector3 (0, 0, 0);
		output_2_4.transform.localScale = new Vector3 (0, 0, 0);

		output_3_1.transform.localScale = new Vector3 (0, 0, 0);
		output_3_2.transform.localScale = new Vector3 (0, 0, 0);
		output_3_3.transform.localScale = new Vector3 (0, 0, 0);
		output_3_4.transform.localScale = new Vector3 (0, 0, 0);

		output_4_1.transform.localScale = new Vector3 (0, 0, 0);
		output_4_2.transform.localScale = new Vector3 (0, 0, 0);
		output_4_3.transform.localScale = new Vector3 (0, 0, 0);
		output_4_4.transform.localScale = new Vector3 (0, 0, 0);

		if (output_1_5 != null) {
			output_1_5.transform.localScale = new Vector3 (0, 0, 0);
			output_1_5.onClick.AddListener(() => { deleteOutputElement(1,5); });
		}

		if (output_1_6 != null) {
			output_1_6.transform.localScale = new Vector3 (0, 0, 0);
			output_1_6.onClick.AddListener(() => { deleteOutputElement(1,6); });
		}

		if (output_2_5 != null) {
			output_2_5.transform.localScale = new Vector3 (0, 0, 0);
			output_2_5.onClick.AddListener(() => { deleteOutputElement(2,5); });
		}

		if (output_2_6 != null) {
			output_2_6.transform.localScale = new Vector3 (0, 0, 0);
			output_2_6.onClick.AddListener(() => { deleteOutputElement(2,6); });
		}

		if (output_3_5 != null) {
			output_3_5.transform.localScale = new Vector3 (0, 0, 0);
			output_3_5.onClick.AddListener(() => { deleteOutputElement(3,5); });
		}

		if (output_3_6 != null) {
			output_3_6.transform.localScale = new Vector3 (0, 0, 0);
			output_3_6.onClick.AddListener(() => { deleteOutputElement(3,6); });
		}

		if (output_4_5 != null) {
			output_4_5.transform.localScale = new Vector3 (0, 0, 0);
			output_4_5.onClick.AddListener(() => { deleteOutputElement(4,5); });
		}

		if (output_4_6 != null) {
			output_4_6.transform.localScale = new Vector3 (0, 0, 0);
			output_4_6.onClick.AddListener(() => { deleteOutputElement(4,6); });
		}


		input_1_1.onClick.AddListener(() => { deleteInputElement(1,1); });
		input_1_2.onClick.AddListener(() => { deleteInputElement(1,2); });

		input_2_1.onClick.AddListener(() => { deleteInputElement(2,1); });
		input_2_2.onClick.AddListener(() => { deleteInputElement(2,2); });

		input_3_1.onClick.AddListener(() => { deleteInputElement(3,1); });
		input_3_2.onClick.AddListener(() => { deleteInputElement(3,2); });

		input_4_1.onClick.AddListener(() => { deleteInputElement(4,1); });
		input_4_2.onClick.AddListener(() => { deleteInputElement(4,2); });

		output_1_1.onClick.AddListener(() => { deleteOutputElement(1,1); });
		output_1_2.onClick.AddListener(() => { deleteOutputElement(1,2); });
		output_1_3.onClick.AddListener(() => { deleteOutputElement(1,3); });
		output_1_4.onClick.AddListener(() => { deleteOutputElement(1,4); });

		output_2_1.onClick.AddListener(() => { deleteOutputElement(2,1); });
		output_2_2.onClick.AddListener(() => { deleteOutputElement(2,2); });
		output_2_3.onClick.AddListener(() => { deleteOutputElement(2,3); });
		output_2_4.onClick.AddListener(() => { deleteOutputElement(2,4); });

		output_3_1.onClick.AddListener(() => { deleteOutputElement(3,1); });
		output_3_2.onClick.AddListener(() => { deleteOutputElement(3,2); });
		output_3_3.onClick.AddListener(() => { deleteOutputElement(3,3); });
		output_3_4.onClick.AddListener(() => { deleteOutputElement(3,4); });

		output_4_1.onClick.AddListener(() => { deleteOutputElement(4,1); });
		output_4_2.onClick.AddListener(() => { deleteOutputElement(4,2); });
		output_4_3.onClick.AddListener(() => { deleteOutputElement(4,3); });
		output_4_4.onClick.AddListener(() => { deleteOutputElement(4,4); });

	}

	public void showGrid(){

		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			if (_situation.GetComponent<ElectricalTestBenchMainClass> ().usedInputs == 0 && _situation.GetComponent<ElectricalTestBenchMainClass> ().usedOutputs == 0) {
				_situation.GetComponent<ElectricalTestBenchMainClass> ()._alert = Instantiate(_situation.GetComponent<ElectricalTestBenchMainClass> ()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
				_situation.GetComponent<ElectricalTestBenchMainClass> ()._alert.transform.SetParent (GameObject.Find("Canvas").transform, false);
				_situation.GetComponent<ElectricalTestBenchMainClass> ()._alert.transform.localPosition = new Vector3(-700,700,0);
				_situation.GetComponent<ElectricalTestBenchMainClass> ()._alert.GetComponent<Alert> ().showAlert (1, "", "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='delete_empty']").InnerText, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText);	
			} else {

				iTween.MoveTo(
					_cancelBt.gameObject,
					iTween.Hash(
						"position", button_initial_position,
						"looktarget", Camera.main,
						"easeType", iTween.EaseType.easeOutExpo,
						"time", 1f,
						"islocal",true
					)
				);

				_situation.GetComponent<ElectricalTestBenchMainClass> ().activateBackground ();
				_cancelBt.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='delete_grid_button']").InnerText;

				gameObject.transform.SetAsLastSibling ();
				_cancelBt.gameObject.SetActive (true);

				for(int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass>().inputElements.Length; i++){
					if(_situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i]){
						int _row = _situation.GetComponent<ElectricalTestBenchMainClass> ().inputElements [i].GetComponent<dragElement> ().rowUsed;
						int _column = _situation.GetComponent<ElectricalTestBenchMainClass> ().inputElements [i].GetComponent<dragElement> ().columnUsed;
						showDeleteInputElement (_row, _column);
					}
				}

				for(int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements.Length; i++){
					if(_situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i]){
						int _row = _situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements [i].GetComponent<dragElement> ().rowUsed;
						int _column = _situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements [i].GetComponent<dragElement> ().columnUsed;
						showDeleteOutputElement (_row, _column);
					}
				}
			}
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			if (_situation.GetComponent<PnematicTestBenchClass> ().usedInputs == 0 && _situation.GetComponent<PnematicTestBenchClass> ().usedOutputs == 0) {
				_situation.GetComponent<PnematicTestBenchClass> ()._alert = Instantiate(_situation.GetComponent<PnematicTestBenchClass> ()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
				_situation.GetComponent<PnematicTestBenchClass> ()._alert.transform.SetParent (GameObject.Find("Canvas").transform, false);
				_situation.GetComponent<PnematicTestBenchClass> ()._alert.transform.localPosition = new Vector3(-700,700,0);
				_situation.GetComponent<PnematicTestBenchClass> ()._alert.GetComponent<Alert> ().showAlert (1, "", "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='delete_empty']").InnerText, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText);	
			} else {

				iTween.MoveTo(
					_cancelBt.gameObject,
					iTween.Hash(
						"position", button_initial_position,
						"looktarget", Camera.main,
						"easeType", iTween.EaseType.easeOutExpo,
						"time", 1f,
						"islocal",true
					)
				);

				_situation.GetComponent<PnematicTestBenchClass> ().activateBackground ();
				_cancelBt.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='delete_grid_button']").InnerText;

				gameObject.transform.SetAsLastSibling ();
				_cancelBt.gameObject.SetActive (true);

				for(int i = 0; i < _situation.GetComponent<PnematicTestBenchClass>().inputElements.Length; i++){
					if(_situation.GetComponent<PnematicTestBenchClass>().inputElements[i]){
						if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._typeInput == dragElementPneumatic.typesInputs.Left) {
							int _row = _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().rowUsed;
							int _column = _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().columnUsed;
							showDeleteInputElement (_row, _column);
						} else {
							if(_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> () != null){
								int _row = _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().rowUsed;
								int _column = _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().columnUsed;
								showDeleteOutputElement (_row, _column);	
							}
						}
					}
				}

				for(int i = 0; i < _situation.GetComponent<PnematicTestBenchClass>().outputsElements.Length; i++){
					if(_situation.GetComponent<PnematicTestBenchClass>().outputsElements[i]){
						if(_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> () != null){
							int _row = _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ().rowUsed;
							int _column = _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ().columnUsed;
							showDeleteOutputElement (_row, _column);
						}
						else if(_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> () != null){
							int _row = _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ().getUsedRow();
							int _column = _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ().getUsedColumn();
							showDeleteOutputElement (_row, _column);
						}

					}
				}
			}
			break;
		}
	}

	public void hideGrid(){

		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			iTween.ScaleTo (input_1_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_1_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_1_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_1_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (input_2_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_2_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_2_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_2_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (input_3_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_3_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_3_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_3_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (input_4_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_4_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_4_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_4_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (output_1_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_1_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_1_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_1_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (output_2_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_2_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_2_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_2_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (output_3_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_3_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_3_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_3_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (output_4_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_4_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_4_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_4_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.MoveTo(
				_cancelBt.gameObject,
				iTween.Hash(
					"position", button_hide_position,
					"looktarget", Camera.main,
					"easeType", iTween.EaseType.easeOutExpo,
					"time", 1f,
					"islocal",true
				)
			);

			_situation.GetComponent<ElectricalTestBenchMainClass> ().deactivateBackground ();
			Invoke ("closeGrid", 0.5f);
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			iTween.ScaleTo (input_1_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_1_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (input_2_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_2_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (input_3_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_3_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (input_4_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (input_4_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (output_1_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_1_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_1_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_1_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_1_5.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_1_6.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (output_2_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_2_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_2_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_2_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_2_5.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_2_6.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (output_3_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_3_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_3_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_3_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_3_5.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_3_6.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.ScaleTo (output_4_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_4_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_4_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_4_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_4_5.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
			iTween.ScaleTo (output_4_6.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);

			iTween.MoveTo(
				_cancelBt.gameObject,
				iTween.Hash(
					"position", button_hide_position,
					"looktarget", Camera.main,
					"easeType", iTween.EaseType.easeOutExpo,
					"time", 1f,
					"islocal",true
				)
			);

			_situation.GetComponent<PnematicTestBenchClass> ().deactivateBackground ();
			Invoke ("closeGrid", 0.5f);
			break;
		}

	}

	private void closeGrid(){
		_cancelBt.gameObject.SetActive (false);
		hideButtons ();
	}

	public void showDeleteInputElement(int _row, int _column){
		switch(_row){
		case 1:
			switch(_column){
			case 1:
				input_1_1.gameObject.SetActive (true);
				iTween.ScaleTo (input_1_1.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 2:
				input_1_2.gameObject.SetActive (true);
				iTween.ScaleTo (input_1_2.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 3:
				input_1_3.gameObject.SetActive (true);
				iTween.ScaleTo (input_1_3.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 4:
				input_1_4.gameObject.SetActive (true);
				iTween.ScaleTo (input_1_4.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			}
			break;
		case 2:
			switch(_column){
			case 1:
				input_2_1.gameObject.SetActive (true);
				iTween.ScaleTo (input_2_1.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 2:
				input_2_2.gameObject.SetActive (true);
				iTween.ScaleTo (input_2_2.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 3:
				input_2_3.gameObject.SetActive (true);
				iTween.ScaleTo (input_2_3.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 4:
				input_2_4.gameObject.SetActive (true);
				iTween.ScaleTo (input_2_4.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			}
			break;
		case 3:
			switch(_column){
			case 1:
				input_3_1.gameObject.SetActive (true);
				iTween.ScaleTo (input_3_1.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 2:
				input_3_2.gameObject.SetActive (true);
				iTween.ScaleTo (input_3_2.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 3:
				input_3_3.gameObject.SetActive (true);
				iTween.ScaleTo (input_3_3.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 4:
				input_3_4.gameObject.SetActive (true);
				iTween.ScaleTo (input_3_4.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			}
			break;
		case 4:
			switch(_column){
			case 1:
				input_4_1.gameObject.SetActive (true);
				iTween.ScaleTo (input_4_1.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 2:
				input_4_2.gameObject.SetActive (true);
				iTween.ScaleTo (input_4_2.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 3:
				input_4_3.gameObject.SetActive (true);
				iTween.ScaleTo (input_4_3.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 4:
				input_4_4.gameObject.SetActive (true);
				iTween.ScaleTo (input_4_4.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			}
			break;
		}
	}

	public void hideDeleteInputElement(int _row, int _column){
		switch(_row){
		case 1:
			switch(_column){
			case 1:
				iTween.ScaleTo (input_1_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 2:
				iTween.ScaleTo (input_1_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 3:
				iTween.ScaleTo (input_1_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 4:
				iTween.ScaleTo (input_1_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			}
			break;
		case 2:
			switch(_column){
			case 1:
				iTween.ScaleTo (input_2_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 2:
				iTween.ScaleTo (input_2_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 3:
				iTween.ScaleTo (input_2_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 4:
				iTween.ScaleTo (input_2_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			}
			break;
		case 3:
			switch(_column){
			case 1:
				iTween.ScaleTo (input_3_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 2:
				iTween.ScaleTo (input_3_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 3:
				iTween.ScaleTo (input_3_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 4:
				iTween.ScaleTo (input_3_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			}
			break;
		case 4:
			switch(_column){
			case 1:
				iTween.ScaleTo (input_4_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 2:
				iTween.ScaleTo (input_4_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 3:
				iTween.ScaleTo (input_4_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 4:
				iTween.ScaleTo (input_4_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			}
			break;
		}
	}

	public void showDeleteOutputElement(int _row, int _column){
		switch(_row){
		case 1:
			switch(_column){
			case 1:
				output_1_1.gameObject.SetActive (true);
				iTween.ScaleTo (output_1_1.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 2:
				output_1_2.gameObject.SetActive (true);
				iTween.ScaleTo (output_1_2.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 3:
				output_1_3.gameObject.SetActive (true);
				iTween.ScaleTo (output_1_3.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 4:
				output_1_4.gameObject.SetActive (true);
				iTween.ScaleTo (output_1_4.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 5:
				output_1_5.gameObject.SetActive (true);
				iTween.ScaleTo (output_1_5.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 6:
				output_1_6.gameObject.SetActive (true);
				iTween.ScaleTo (output_1_6.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			}
			break;
		case 2:
			switch(_column){
			case 1:
				output_2_1.gameObject.SetActive (true);
				iTween.ScaleTo (output_2_1.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 2:
				output_2_2.gameObject.SetActive (true);
				iTween.ScaleTo (output_2_2.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 3:
				output_2_3.gameObject.SetActive (true);
				iTween.ScaleTo (output_2_3.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 4:
				output_2_4.gameObject.SetActive (true);
				iTween.ScaleTo (output_2_4.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 5:
				output_2_5.gameObject.SetActive (true);
				iTween.ScaleTo (output_2_5.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 6:
				output_2_6.gameObject.SetActive (true);
				iTween.ScaleTo (output_2_6.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			}
			break;
		case 3:
			switch(_column){
			case 1:
				output_3_1.gameObject.SetActive (true);
				iTween.ScaleTo (output_3_1.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 2:
				output_3_2.gameObject.SetActive (true);
				iTween.ScaleTo (output_3_2.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 3:
				output_3_3.gameObject.SetActive (true);
				iTween.ScaleTo (output_3_3.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 4:
				output_3_4.gameObject.SetActive (true);
				iTween.ScaleTo (output_3_4.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 5:
				output_3_5.gameObject.SetActive (true);
				iTween.ScaleTo (output_3_5.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 6:
				output_3_6.gameObject.SetActive (true);
				iTween.ScaleTo (output_3_6.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			}
			break;
		case 4:
			switch(_column){
			case 1:
				output_4_1.gameObject.SetActive (true);
				iTween.ScaleTo (output_4_1.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 2:
				output_4_2.gameObject.SetActive (true);
				iTween.ScaleTo (output_4_2.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 3:
				output_4_3.gameObject.SetActive (true);
				iTween.ScaleTo (output_4_3.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 4:
				output_4_4.gameObject.SetActive (true);
				iTween.ScaleTo (output_4_4.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 5:
				output_4_5.gameObject.SetActive (true);
				iTween.ScaleTo (output_4_5.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			case 6:
				output_4_6.gameObject.SetActive (true);
				iTween.ScaleTo (output_4_6.gameObject, new Vector3 (0.5f, 0.5f, 0.5f), 0.5f);
				break;
			}
			break;
		}
	}

	public void hideDeleteOutputElement(int _row, int _column){
		switch(_row){
		case 1:
			switch(_column){
			case 1:
				iTween.ScaleTo (output_1_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 2:
				iTween.ScaleTo (output_1_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 3:
				iTween.ScaleTo (output_1_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 4:
				iTween.ScaleTo (output_1_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 5:
				iTween.ScaleTo (output_1_5.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 6:
				iTween.ScaleTo (output_1_6.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			}
			break;
		case 2:
			switch(_column){
			case 1:
				iTween.ScaleTo (output_2_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 2:
				iTween.ScaleTo (output_2_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 3:
				iTween.ScaleTo (output_2_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 4:
				iTween.ScaleTo (output_2_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 5:
				iTween.ScaleTo (output_2_5.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 6:
				iTween.ScaleTo (output_2_6.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			}
			break;
		case 3:
			switch(_column){
			case 1:
				iTween.ScaleTo (output_3_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 2:
				iTween.ScaleTo (output_3_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 3:
				iTween.ScaleTo (output_3_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 4:
				iTween.ScaleTo (output_3_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 5:
				iTween.ScaleTo (output_3_5.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 6:
				iTween.ScaleTo (output_3_6.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			}
			break;
		case 4:
			switch(_column){
			case 1:
				iTween.ScaleTo (output_4_1.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 2:
				iTween.ScaleTo (output_4_2.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 3:
				iTween.ScaleTo (output_4_3.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 4:
				iTween.ScaleTo (output_4_4.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 5:
				iTween.ScaleTo (output_4_5.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			case 6:
				iTween.ScaleTo (output_4_6.gameObject, new Vector3 (0f, 0f, 0f), 0.5f);
				break;
			}
			break;
		}
	}

	private void deleteInputElement(int _row, int _column){
		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			for(int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass>().inputElements.Length; i++){
				if(_situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i]){
					if(_situation.GetComponent<ElectricalTestBenchMainClass> ().inputElements [i].GetComponent<dragElement> ().rowUsed == _row && _situation.GetComponent<ElectricalTestBenchMainClass> ().inputElements [i].GetComponent<dragElement> ().columnUsed == _column){
						//delete element
						if(_situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i].GetComponent<dragElement>()._leftCable){
							Destroy (_situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i].GetComponent<dragElement>()._leftCable);
						}
						if(_situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i].GetComponent<dragElement>()._rightCable){
							Destroy (_situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i].GetComponent<dragElement>()._rightCable);
						}
						if(_situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i].GetComponent<dragElement>()._topCable){
							Destroy (_situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i].GetComponent<dragElement>()._topCable);
						}

						Destroy(_situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i]);
						_situation.GetComponent<ElectricalTestBenchMainClass> ().inputElements [i] = null;
						_situation.GetComponent<ElectricalTestBenchMainClass> ().usedInputs--;
						_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().updateInputsDropdown ();
						_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();

						_situation.GetComponent<ElectricalTestBenchMainClass> ()._leftOvers.GetComponent<benchAvailability> ().setAreaAvailable (_row, _column);

						hideDeleteInputElement (_row, _column);

						if(_situation.GetComponent<ElectricalTestBenchMainClass> ().usedInputs == 0 && _situation.GetComponent<ElectricalTestBenchMainClass> ().usedOutputs == 0){
							hideGrid ();
						}
						break;
					}
				}
			}
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			for(int i = 0; i < _situation.GetComponent<PnematicTestBenchClass>().inputElements.Length; i++){
				if(_situation.GetComponent<PnematicTestBenchClass>().inputElements[i]){
					if(_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().rowUsed == _row && _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().columnUsed == _column && _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._typeInput == dragElementPneumatic.typesInputs.Left){
						//delete element
						if(_situation.GetComponent<PnematicTestBenchClass>().inputElements[i].GetComponent<dragElementPneumatic>()._leftCable){
							Destroy (_situation.GetComponent<PnematicTestBenchClass>().inputElements[i].GetComponent<dragElementPneumatic>()._leftCable);
						}
						if(_situation.GetComponent<PnematicTestBenchClass>().inputElements[i].GetComponent<dragElementPneumatic>()._rightCable){
							Destroy (_situation.GetComponent<PnematicTestBenchClass>().inputElements[i].GetComponent<dragElementPneumatic>()._rightCable);
						}
						if(_situation.GetComponent<PnematicTestBenchClass>().inputElements[i].GetComponent<dragElementPneumatic>()._topCable){
							Destroy (_situation.GetComponent<PnematicTestBenchClass>().inputElements[i].GetComponent<dragElementPneumatic>()._topCable);
						}

						Destroy(_situation.GetComponent<PnematicTestBenchClass>().inputElements[i]);
						_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i] = null;
						_situation.GetComponent<PnematicTestBenchClass> ().usedInputs--;
						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().updateInputsDropdown ();
						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();

						_situation.GetComponent<PnematicTestBenchClass> ()._leftOvers.GetComponent<benchAvailability> ().setAreaAvailable (_row, _column);

						hideDeleteInputElement (_row, _column);

						if(_situation.GetComponent<PnematicTestBenchClass> ().usedInputs == 0 && _situation.GetComponent<PnematicTestBenchClass> ().usedOutputs == 0){
							hideGrid ();
						}
						break;
					}
				}
			}
			break;
		}
	}

	private void deleteOutputElement(int _row, int _column){
		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			for(int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements.Length; i++){
				if(_situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i]){
					if(_situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements [i].GetComponent<dragElement> ().rowUsed == _row && _situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements [i].GetComponent<dragElement> ().columnUsed == _column){
						//delete element
						if(_situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i].GetComponent<dragElement>()._leftCable){
							Destroy (_situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i].GetComponent<dragElement>()._leftCable);
						}
						if(_situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i].GetComponent<dragElement>()._rightCable){
							Destroy (_situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i].GetComponent<dragElement>()._rightCable);
						}
						if(_situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i].GetComponent<dragElement>()._topCable){
							Destroy (_situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i].GetComponent<dragElement>()._topCable);
						}

						Destroy(_situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i]);
						_situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements [i] = null;
						_situation.GetComponent<ElectricalTestBenchMainClass> ().usedOutputs--;
						_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperBars.GetComponent<upperBars> ().updateOutputsDropdown ();

						_situation.GetComponent<ElectricalTestBenchMainClass> ()._rightOvers.GetComponent<benchAvailability> ().setAreaAvailable (_row, _column);

						hideDeleteOutputElement (_row, _column);

						if(_situation.GetComponent<ElectricalTestBenchMainClass> ().usedInputs == 0 && _situation.GetComponent<ElectricalTestBenchMainClass> ().usedOutputs == 0){
							hideGrid ();
						}
						break;
					}
				}
			}
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			bool _elementDeleted = false;
			for (int i = 0; i < _situation.GetComponent<PnematicTestBenchClass> ().outputsElements.Length; i++) {
				if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i]) {
					if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> () != null) {
						if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ().rowUsed == _row && _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ().columnUsed == _column) {
							//delete element
							_elementDeleted = true;
							if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ()._leftCable) {
								Destroy (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ()._leftCable);
							}
							if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ()._rightCable) {
								Destroy (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ()._rightCable);
							}
							if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ()._topCable) {
								Destroy (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ()._topCable);
							}

							Destroy (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i]);
							_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i] = null;
							_situation.GetComponent<PnematicTestBenchClass> ().usedOutputs--;
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().updateOutputsDropdown ();

							_situation.GetComponent<PnematicTestBenchClass> ()._rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaAvailable (_row, _column);

							hideDeleteOutputElement (_row, _column);

							if (_situation.GetComponent<PnematicTestBenchClass> ().usedInputs == 0 && _situation.GetComponent<PnematicTestBenchClass> ().usedOutputs == 0) {
								hideGrid ();
							}
							break;
						}	
					} else if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> () != null) {
						if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent != null && _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> () != null) {
							if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ().rowsColumnsUsed [0].x == _row && _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ().rowsColumnsUsed [0].y == _column) {
								_elementDeleted = true;

								//activate availability in elements
								for(int j = 0; j < _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ().rowsColumnsUsed.Length; j++){
									int _rowN = (int)_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ().rowsColumnsUsed [j].x;
									int _columnN = (int)_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ().rowsColumnsUsed [j].y;
									_situation.GetComponent<PnematicTestBenchClass> ()._rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaAvailable (_rowN, _columnN);
								}

								//check inputs
								if(_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ()._sensorAttachedA != null){
									for(int k = 0; k < _situation.GetComponent<PnematicTestBenchClass> ().inputElements.Length; k++){
										if(_situation.GetComponent<PnematicTestBenchClass> ().inputElements[k] != null && _situation.GetComponent<PnematicTestBenchClass> ().inputElements[k].name == _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ()._sensorAttachedA.name){
											_situation.GetComponent<PnematicTestBenchClass> ().inputElements [k] = null;
											_situation.GetComponent<PnematicTestBenchClass> ().usedInputs--;
										}
									}
								}

								for (int k = 0; k < _situation.GetComponent<PnematicTestBenchClass> ().inputElements.Length; k++) {
									if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [k] != null && _situation.GetComponent<PnematicTestBenchClass> ().inputElements [k].GetComponent<dragElementPneumatic> ()._typeComponent == dragElementPneumatic.typesComponents.inductive_sensor) {
										if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [k].GetComponent<dragElementPneumatic> ().rowUsed == (_row) && _situation.GetComponent<PnematicTestBenchClass> ().inputElements [k].GetComponent<dragElementPneumatic> ().columnUsed == (_column + 3)) {
											_situation.GetComponent<PnematicTestBenchClass> ().inputElements [k].GetComponent<dragElementPneumatic> ().setValue (0);
										}
									} else if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [k] != null && _situation.GetComponent<PnematicTestBenchClass> ().inputElements [k].GetComponent<dragElementPneumatic> ()._typeComponent == dragElementPneumatic.typesComponents.limit_switch_sensor) {
										if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [k].GetComponent<dragElementPneumatic> ().rowUsed == (_row + 1) && _situation.GetComponent<PnematicTestBenchClass> ().inputElements [k].GetComponent<dragElementPneumatic> ().columnUsed == (_column + 2)) {
											_situation.GetComponent<PnematicTestBenchClass> ().inputElements [k].GetComponent<dragElementPneumatic> ().setValue (0);
										}
									}
								}

								if(_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ()._sensorAttachedB != null){
									for(int k = 0; k < _situation.GetComponent<PnematicTestBenchClass> ().inputElements.Length; k++){
										if(_situation.GetComponent<PnematicTestBenchClass> ().inputElements[k] != null && _situation.GetComponent<PnematicTestBenchClass> ().inputElements[k].name == _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ()._sensorAttachedB.name){
											_situation.GetComponent<PnematicTestBenchClass> ().inputElements [k] = null;
											_situation.GetComponent<PnematicTestBenchClass> ().usedInputs--;
										}
									}
								}

								//delete parent element
								Destroy(_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent);

								//delete element
								Destroy (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i]);
								_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i] = null;
								_situation.GetComponent<PnematicTestBenchClass> ().usedOutputs--;
							}
						} else if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent != null && _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<motorActuatorElement> () != null) {
							if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<motorActuatorElement> ().rowsColumnsUsed [0].x == _row && _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<motorActuatorElement> ().rowsColumnsUsed [0].y == _column) {
								_elementDeleted = true;

								//activate availability in elements
								for(int j = 0; j < _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<motorActuatorElement> ().rowsColumnsUsed.Length; j++){
									int _rowN = (int)_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<motorActuatorElement> ().rowsColumnsUsed [j].x;
									int _columnN = (int)_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<motorActuatorElement> ().rowsColumnsUsed [j].y;
									_situation.GetComponent<PnematicTestBenchClass> ()._rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaAvailable (_rowN, _columnN);
								}

								//delete parent element
								Destroy(_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent);

								//delete element
								Destroy (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i]);
								_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i] = null;
								_situation.GetComponent<PnematicTestBenchClass> ().usedOutputs--;
							}
						} else if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent == null) {
							_elementDeleted = true;
							Destroy (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i]);
							_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i] = null;
							_situation.GetComponent<PnematicTestBenchClass> ().usedOutputs--;
						}
					}
				}
			}
			if (_elementDeleted == false) {
				for (int i = 0; i < _situation.GetComponent<PnematicTestBenchClass> ().inputElements.Length; i++) {
					if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i]) {
						if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().rowUsed == _row && _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().columnUsed == _column && _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._typeInput == dragElementPneumatic.typesInputs.Right) {
							//delete element
							if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._leftCable) {
								Destroy (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._leftCable);
							}
							if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._rightCable) {
								Destroy (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._rightCable);
							}
							if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._topCable) {
								Destroy (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._topCable);
							}

							Destroy (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i]);
							_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i] = null;
							_situation.GetComponent<PnematicTestBenchClass> ().usedInputs--;
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().updateInputsDropdown ();
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();

							_situation.GetComponent<PnematicTestBenchClass> ()._rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaAvailable (_row, _column);

							hideDeleteOutputElement (_row, _column);

							if (_situation.GetComponent<PnematicTestBenchClass> ().usedInputs == 0 && _situation.GetComponent<PnematicTestBenchClass> ().usedOutputs == 0) {
								hideGrid ();
							}
							break;
						}
					}
				}
			} else {
				hideDeleteOutputElement (_row, _column);
				_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().updateOutputsDropdown ();
				_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().updateInputsDropdown ();
				_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
				if (_situation.GetComponent<PnematicTestBenchClass> ().usedInputs == 0 && _situation.GetComponent<PnematicTestBenchClass> ().usedOutputs == 0) {
					hideGrid ();
				}
			}
			break;
		}
	}

	public void hideButtons(){
		input_1_1.gameObject.SetActive(false);
		input_1_2.gameObject.SetActive(false);
		if(input_1_3 != null){
			input_1_3.gameObject.SetActive(false);
		}
		if(input_1_4 != null){
			input_1_4.gameObject.SetActive(false);
		}
		input_2_1.gameObject.SetActive(false);
		input_2_2.gameObject.SetActive(false);
		if(input_2_3 != null){
			input_2_3.gameObject.SetActive(false);
		}
		if (input_2_4 != null) {
			input_2_4.gameObject.SetActive(false);
		}
		input_3_1.gameObject.SetActive(false);
		input_3_2.gameObject.SetActive(false);
		if (input_3_3 != null) {
			input_3_3.gameObject.SetActive(false);
		}
		if (input_3_4 != null) {
			input_3_4.gameObject.SetActive(false);
		}
		input_4_1.gameObject.SetActive(false);
		input_4_2.gameObject.SetActive(false);
		if (input_3_4 != null) {
			input_3_4.gameObject.SetActive(false);
		}
		if (input_4_4 != null) {
			input_4_4.gameObject.SetActive(false);
		}

		output_1_1.gameObject.SetActive(false);
		output_1_2.gameObject.SetActive(false);
		output_1_3.gameObject.SetActive(false);
		output_1_4.gameObject.SetActive(false);
		output_2_1.gameObject.SetActive(false);
		output_2_2.gameObject.SetActive(false);
		output_2_3.gameObject.SetActive(false);
		output_2_4.gameObject.SetActive(false);
		output_3_1.gameObject.SetActive(false);
		output_3_2.gameObject.SetActive(false);
		output_3_3.gameObject.SetActive(false);
		output_3_4.gameObject.SetActive(false);
		output_4_1.gameObject.SetActive(false);
		output_4_2.gameObject.SetActive(false);
		output_4_3.gameObject.SetActive(false);
		output_4_4.gameObject.SetActive(false);

		if (output_1_5 != null) {
			output_1_5.gameObject.SetActive(false);
		}
		if (output_1_6 != null) {
			output_1_6.gameObject.SetActive(false);
		}
		if (output_2_5 != null) {
			output_2_5.gameObject.SetActive(false);
		}
		if (output_2_6 != null) {
			output_2_6.gameObject.SetActive(false);
		}
		if (output_3_5 != null) {
			output_3_5.gameObject.SetActive(false);
		}
		if (output_3_6 != null) {
			output_3_6.gameObject.SetActive(false);
		}
		if (output_4_5 != null) {
			output_4_5.gameObject.SetActive(false);
		}
		if (output_4_6 != null) {
			output_4_6.gameObject.SetActive(false);
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
