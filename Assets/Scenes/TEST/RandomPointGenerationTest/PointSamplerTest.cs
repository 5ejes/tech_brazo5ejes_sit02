﻿using System.Collections;
using System.Collections.Generic;
using NSBrazo5Ejes.NSDraggingBehaviour;
using UnityEngine;

public class PointSamplerTest : MonoBehaviour
{
	[SerializeField] private DraggableComponent _component;
	[SerializeField] private int pointCount = 10;
	[SerializeField] private DrawPointsInspector _inspectorPoints;
	[SerializeField] private float _minDistance = 1f;

	public void GeneratePoints()
	{
		// Limpio la lista de puntos
		_inspectorPoints.ClearPointList();

		// Genero los halfExtents
		var halfExtents = (_component.MaxLimit.position - _component.MinLimit.position) / 2f;

		// Genero los puntos
		var generatedPoints = PointSampler.GenerateInsideXZ(pointCount, _component.Target.position, halfExtents, _minDistance);

		// Registro los puntos
		_inspectorPoints.AddPoints(generatedPoints);
	}
}
