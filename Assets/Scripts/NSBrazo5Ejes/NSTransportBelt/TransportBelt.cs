﻿using System;
using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace NSBrazo5Ejes.NSTransportBelt
{

	/// <summary>
	/// Controla la banda transportadora
	/// </summary>
	public class TransportBelt : MonoBehaviour
	{
		private static readonly float _distanceThresholdSqr = float.Epsilon * float.Epsilon;

#pragma warning disable 0649
		[Header("Settings")]
		[SerializeField] private Transform _cubeSpawnPosition;
		[SerializeField] private float _beltSpeed = 0.1f;
		[SerializeField] private bool _beltDetection = false;

		[Header("Other Settings")]
		[SerializeField] private float _emptyBeltDelayExecute = 1f;
		[SerializeField] private Transform _readyCubeCheck = null;
		[SerializeField] private LayerMask _layerMask = Physics.AllLayers;

		[Header("Stopping Settings")]
		[SerializeField] private bool _stopBandCall = false;
		[SerializeField] private float _snapValue = 1f;
		[SerializeField] private Transform _positionLocalTransform = null;

		[Header("Belt Led Communication")]

		[SerializeField] private BeltLedController _ledController;

		[Header("Belt Ray")]
		[SerializeField] private Transform _beltRayForward = null;
		[SerializeField] private float _beltRayLength = 0.75f;
		[SerializeField] private LayerMask _beltRayLayerMask = Physics.AllLayers;
		[SerializeField] private int _beltRayFrameCall = 0;
		[Header("Exit Belt Ray Settings")]
		[SerializeField] private Transform _exitBeltRayTransform = null;
		[SerializeField] private float _exitBeltRayRadius = 0.035f;
		[SerializeField] private LayerMask _exitBeltLayerMask = Physics.AllLayers;
		[SerializeField] private int _exitBeltFrameCall = 0;

		[Header("Events")]
		[SerializeField] private UnityEvent _onActivate;
		[SerializeField] private UnityEvent _onDeactivate;

		[Space]
		[SerializeField] private UnityEvent _onEmptyBand;
		[Space]
		[SerializeField] private NSScriptableEvent.ScriptableEventCube.CubeEvent _onBeltDectectedCubes;
#pragma warning restore 0649

		[Header("Debug")]
		[SerializeField] private bool _drawDebug = false;
		[SerializeField] private Color _debugColor = Color.red;

		public static readonly Color offBeltLedColor = Color.white;
		public static readonly Color onBeltLedColor = Color.red;
		public static readonly Color onSensorDataCheck = Color.green;
		private int _frameCallOnEmpty = 120;
		private float _elapsedEmptyBeltSeconds = 0f;
		private int _frameCallCubeObstacleCheck = 4;
		private int _lastCastCount = -1;
		private bool MovingToStoppingPoint { get; set; } = false;

		/// <summary>
		/// Objetos rígidos que pueden ser movidos
		/// </summary>
		/// <typeparam name="Rigidbody">El objeto registrado movible</typeparam>
		/// <returns>La lista de objetos movibles</returns>
		private readonly List<Rigidbody> _beltObjects = new List<Rigidbody>(10);
		public List<Rigidbody> BeltObjects => _beltObjects;

		/// <summary>
		/// Vector de dirección hacia donde la banda se mueve y empuja los objetos
		/// </summary>
		private Vector3 _worldCoordinatesPushDirection = -Vector3.right;

		/// <summary>
		/// Componentes Transform que se pueden mover en la banda
		/// </summary>
		public Transform[] RegisteredMovableCubes { get; set; } = null;


		/// <summary>
		/// Posición en donde se originan los objetos
		/// </summary>
		public Vector3 SpawnPosition => _cubeSpawnPosition?.position ?? Vector3.zero;

		/// <summary>
		/// Estado de activación de la banda transportadora, definir este valor significa encender o apagar la banda
		/// </summary>
		/// <value>Valor de encendido</value>
		public bool Active { get; private set; }
		public bool StopBandCall { get => _stopBandCall; set => _stopBandCall = value; }
		public bool BeltDetection { get => _beltDetection; set => _beltDetection = value; }

		private void Update()
		{
			// Si la banda no está activa
			if (!Active)
			{
				if (BeltDetection)
					_ledController.SetLedColor(onSensorDataCheck);
				else
					_ledController.SetLedColor(offBeltLedColor);
					
				return;
			}

			// Si hay detección en la banda
			if (BeltDetection)
			{
				_ledController.SetLedColor(onSensorDataCheck);
			}
			else
			{
				// Si no hay detección

				// Si hay una llamada para detener la banda
				if (StopBandCall)
				{
					_ledController.SetLedColor(offBeltLedColor);
				}
				else
				{
					// Si la banda sigue ejecutando movimiento
					_ledController.SetLedColor(onBeltLedColor);
				}
			}


			// Si la información de posición y dirección está definida
			// Y está en el frame de actualización 
			if (_beltRayForward && IsAtFrameUpdate(_beltRayFrameCall))
				// Detecto los objetos en la banda
				CastBeltDetectionRay(_beltRayForward.position, _beltRayForward.forward, _beltRayLength, _beltRayLayerMask);

			// Obtengo la cuenta de objetos activos
			int count = _beltObjects.Count;

			// Si al menos un elemento en la banda
			if (count > 0)
			{

				// Si existe la posición del rayo de salida definida
				// y si el frame de actualización es menor o igual a 1
				// O si estamos en el frame de actualización
				if (_exitBeltRayTransform && IsAtFrameUpdate(_exitBeltFrameCall))
					// Lanzo el rayo que verifica la salida de los elementos de la banda
					CastExitRay(_exitBeltRayTransform.position, _exitBeltRayRadius, _exitBeltLayerMask);

				// Recorro la lista de índices activos
				for (int i = count - 1; i >= 0; i--)
				{
					// Si el índice está por fuera de los límites, continúo
					if (i < 0 || i >= _beltObjects.Count) continue;

					// Obtengo el Rigidbody actual
					var currentRigidbody = _beltObjects[i];

					// Si el rigidbody existe
					if (currentRigidbody)
					{

						// Obtengo el componente cubo del rigidbody
						Cube currentCube = currentRigidbody.GetComponent<Cube>();
						if (currentCube)
						{
							// Invoco el evento de detección de cubos
							_onBeltDectectedCubes?.Invoke(currentCube);
						}

						// Si hay una llamada para detener la banda activa
						if (StopBandCall)
						{

							// Detengo la banda
							TurnOff();

							// Cancelo la detención de la banda
							StopBandCall = false;

							// Inicializo una corutina de posicionamiento
							StartCoroutine(PositionObject(
								currentRigidbody.transform, _snapValue));

						}

						// Solo realiza la operación cada n frames
						if (Time.frameCount % _frameCallCubeObstacleCheck != 0) return;

						// Si el rigidbody actual no es quinemático
						if (!currentRigidbody.isKinematic)
						{
							// Realizo una comprobación por seguridad
							if (currentRigidbody == null) continue;

							// Lo empujo
							currentRigidbody.MovePosition(currentRigidbody.position + GetPushDirection() * _beltSpeed * Time.deltaTime);
						}
					}
				}
			}
			else
			{
				// Si la banda está vacía

				// Si hay una llamada para detener la banda, la detengo
				if (StopBandCall)
				{
					TurnOff();
				}

				// Comienzo la cuenta
				_elapsedEmptyBeltSeconds += Time.deltaTime;

				// Si la banda supera el delay de banda vacía
				if (_elapsedEmptyBeltSeconds > _emptyBeltDelayExecute)
				{

					// Creo el rayo que me comprueba si la banda puede desplegar otro cubo
					var emptyBandRay = new Ray(_readyCubeCheck.position, Vector3.up);

					var results = Physics.SphereCastAll(emptyBandRay, 0.05f, 0.25f, _layerMask, QueryTriggerInteraction.Ignore);

					// Compruebo si el EmptyBeltCheck no está vacío
					if (results.Length > 0) return;

					// Si los frames de llamado son diferentes de 0
					if (_frameCallOnEmpty != 0)
					{
						// Cada n frames
						if (Time.frameCount % Mathf.Abs(_frameCallOnEmpty) == 0)
						{
							// Ejecuto el evento
							_onEmptyBand.WrapInvoke();

							// Reinicio de la cuenta
							_elapsedEmptyBeltSeconds = 0f;
						}
					}
					else
					{
						// Si el frameCallOnEmpty no está configurado

						// Ejecuto el evento
						_onEmptyBand.WrapInvoke();

						// Reinicio de la cuenta
						_elapsedEmptyBeltSeconds = 0f;
					}
				}
			}
		}

		private RaycastHit[] CastBeltDetectionRay(Vector3 position, Vector3 forward, float rayLength, LayerMask layerMask, int maxCastOutputSize = 4)
		{

			// Defino un rayo con la información
			Ray directionRay = new Ray(position, forward);

			// Defino el arreglo de los resultados
			RaycastHit[] output = new RaycastHit[maxCastOutputSize];

			// Lanzo una comprobación con el rayo
			int castCount = Physics.RaycastNonAlloc(directionRay, output, rayLength, layerMask, QueryTriggerInteraction.Collide);

			// Si la cuenta de elementos ha cambiado desde que la registré
			if (!castCount.Equals(_lastCastCount))
			{
				// Registro la nueva cuenta de elemntos de la banda
				_lastCastCount = castCount;

				// Borro el registro de la banda
				ClearBeltRegister();
			}

			for (int i = 0; i < castCount; i++)
			{
				// Obtengo la información del hit
				RaycastHit hitInfo = output[i];

				// Compruebo el rigidbody del objeto
				if (!hitInfo.rigidbody) continue;

				// Añado el elemento a la banda
				RegisterOnBelt(hitInfo.rigidbody);
			}

			// Retorno la salida de datos
			return output;
		}
		private void InitializeValues()
		{
			// Reinicio la cuenta
			_lastCastCount = -1;

			// Limpio la banda
			ClearBeltRegister();
		}

		private bool IsAtFrameUpdate(int frameCycle)
		{
			return frameCycle <= 1 || Time.frameCount % frameCycle == 0;
		}

		private Collider[] CastExitRay(Vector3 position, float radius, LayerMask layerMask, int maxCastOutputSize = 4)
		{
			if (maxCastOutputSize <= 0)
			{
				Debug.LogWarning("The max output size can't be 0 or less: Changing to 1...");
				maxCastOutputSize = 1;
			}
			// Genero la variable de salida
			Collider[] output = new Collider[maxCastOutputSize];

			// Genero una comprobación en la posición dada
			int castCount = Physics.OverlapSphereNonAlloc(position, radius, output, layerMask, QueryTriggerInteraction.Collide);

			for (int i = 0; i < castCount; i++)
			{
				// Obtengo el colisionador de la lista
				Collider currentCollider = output[i];

				// Verifico los datos del objeto con el que colisioné
				Cube cube = currentCollider.GetComponent<Cube>();

				// Si no es un cubo, continúo
				if (!cube) continue;

				// Elimino el elemento de la banda
				RemoveFromBelt(cube.transform);
			}

			// Retorno el resultado
			return output;
		}

		/// <summary>
		/// Obtengo la posición hacia donde se empujan los objetos de la banda
		/// </summary>
		/// <returns></returns>
		private Vector3 GetPushDirection()
		{
			// Retorno la dirección local hacia donde se empujan los cubos
			return transform.rotation * _worldCoordinatesPushDirection;
		}

		private IEnumerator PositionObject(Transform beltElement, float snap)
		{
			// Si alguno de los dos parámetros es null
			if (!beltElement || snap <= 0f)
			{
				yield break;
			}

			// Marco el objeto como en movimiento al punto de detenimiento
			MovingToStoppingPoint = true;

			// Convierto la coordenada del objeto en local

			// Ajusto el valor local del elemento de la banda al snap indicado
			Vector3 beltLocalPosition = _positionLocalTransform.InverseTransformPoint(beltElement.position);
			beltLocalPosition.SnapVector(snap);

			// Defino la posición de detenimiento
			Vector3 stoppingPointTargetPosition = _positionLocalTransform.TransformPoint(beltLocalPosition);

			// Defino el threshold de detención
			float distanceToTargetPointSqr = float.MaxValue;

			// Mientras la distancia entre el objetivo y el punto sea mayor al límite
			while (distanceToTargetPointSqr > _distanceThresholdSqr)
			{
				// Si el elemento al que estamos operando es nulo, rompemos el ciclo
				if (beltElement == null)
				{
					MovingToStoppingPoint = false;
					yield break;
				}

				// Defino nuevamente la distancia
				distanceToTargetPointSqr = (beltElement.position - stoppingPointTargetPosition).sqrMagnitude;

				// Ubico el punto un poco más cerca del punto final
				beltElement.position = Vector3.MoveTowards(beltElement.position, stoppingPointTargetPosition, (_beltSpeed / 4f) * Time.deltaTime);

				yield return null;
			}

			// Defino los parametros finales, de salida
			beltElement.position = stoppingPointTargetPosition;

			// Marco el movimiento como finalizado
			MovingToStoppingPoint = false;
		}

		private void OnDisable()
		{
			ClearBeltRegister();
		}

		public void RegisterOnBelt(Transform transform)
		{
			// Si la lista de objetos de la banda no está definida, retorno
			if (_beltObjects == null) return;
			if (transform == null) return;

			// Obtengo el Rigidbody del transform
			Rigidbody transformRigidbody = transform.GetComponent<Rigidbody>();

			// Si el rigidbody ha sido definido
			RegisterOnBelt(transformRigidbody);
		}

		public void RegisterOnBelt(Rigidbody rigidbody)
		{
			// Si el rigidbody existe y no está ya contenido en la lista
			if (rigidbody != null && !_beltObjects.Contains(rigidbody))
				_beltObjects.Add(rigidbody);
		}

		public void RemoveFromBelt(Transform transform)
		{
			// Si la lista de objetos de la banda no está definida, retorno
			if (_beltObjects == null) return;
			if (transform == null) return;

			// Obtengo la referencia al rigidbody
			Rigidbody transformRigidbody = transform.GetComponent<Rigidbody>();

			// Remuevo el objeto de la banda
			RemoveFromBelt(transformRigidbody);

		}

		public void RemoveFromBelt(Rigidbody rigidbody)
		{
			// Si el rigidbody está definido
			if (rigidbody != null)
			{
				// Si está contenido entre los objetos de la banda
				if (_beltObjects.Contains(rigidbody))
				{
					// Remuevo el transform actual de la banda
					_beltObjects.Remove(rigidbody);
				}
			}
		}

		public void ClearBeltRegister()
		{
			Debug.Log($"Belt Cleared!");
			// Limpio los objetos residuales de la banda
			_beltObjects.Clear();
		}

		/// <summary>
		/// Enciende la banda transportadora
		/// </summary>
		public void TurnOn()
		{
			// Defino el estado de la banda a encendido
			SetOn(true);
		}

		/// <summary>
		/// Apaga la banda transportadora
		/// </summary>
		public void TurnOff()
		{
			// Defino el estado de la banda a apagado
			SetOn(false);
		}

		/// <summary>
		/// Define el valor de encendido de la banda transportadora
		/// </summary>
		/// <param name="value"></param>
		public void SetOn(bool value)
		{
			// Defino el valor de encendido según el parámetro dado

			// Defino el valor activo
			Active = value;

			// Según el valor ejecuto el evento
			if (value)
			{
				// Cada vez que activo la banda, inicializo los valores
				InitializeValues();
				_onActivate.WrapInvoke();
			}
			else
			{
				_onDeactivate.WrapInvoke();
			}
		}

		/// <summary>
		/// Define el valor de encendido de la banda transportadora, y apaga la banda cuando la evaluación sea correcta
		/// </summary>
		/// <param name="value"></param>
		public void TryStopCallSetOn(bool value)
		{
			// Defino el valor de encendido según el parámetro dado

			// Detengo la llamada a detener la banda por defecto
			StopBandCall = false;

			// Según el valor ejecuto el evento
			if (value)
			{
				// Realizo la llamada al evento de activación
				_onActivate.WrapInvoke();

				// Activo la banda
				Active = true;
			}
			else StopBandCall = true;

			// En este caso, al apagar la banda se realiza una 
			// activación del booleano para procesar el requerimiento 
			// con el objeto o objetos que se encuentren en la banda
		}

		/// <summary>
		/// Alterna entre encendido y apagado según el estado de la banda
		/// </summary>
		public void Toggle()
		{
			// Defino el valor de encendido a su valor alterno
			SetOn(!Active);
		}

		/// <summary>
		/// Procesa los elementos que entran a la banda
		/// </summary>
		/// <param name="collidingObject">Objeto que colisiona contra la banda</param>
		public void OnBeltEnter(Collider collidingObject)
		{
			// Capa del objeto
			int layer = collidingObject.gameObject.layer;

			// Si no hace parte de la capa
			if (!IsInLayerMask(_layerMask, layer)) return;

			// Si la referencia a los cubos registrados está definida
			if (RegisteredMovableCubes != null)
			{
				// Si el objeto se encuentra previamente registrado
				var registeredObject = System.Array.Find(RegisteredMovableCubes, cube => cube.Equals(collidingObject.transform));


				// No se ha encontrado registrado el objeto
				// Retorno
				if (registeredObject == null) return;

				// Obtengo el Rigidbody del elemento registrado
				Rigidbody rigidbodyFromObject = registeredObject.GetComponent<Rigidbody>();

				// Si este componente existe
				if (rigidbodyFromObject != null)
				{
					// Si el elemento registrado ya se encuentra en la lista de índices
					if (_beltObjects.Contains(rigidbodyFromObject)) Debug.LogError($"El objeto ya se encuentra registrado y activo : {gameObject}");

					// Añado el índice a la lista de los objetos en la banda activos
					_beltObjects.Add(rigidbodyFromObject);
				}
			}
		}

		private bool IsInLayerMask(LayerMask layerMask, int layer)
		{
			// Compruebo si la capa está en la máscara de layer
			return (layerMask == (layerMask | (1 << layer)));
		}

		/// <summary>
		/// Procesa los elementos que salen de la banda
		/// </summary>
		/// <param name="exitingObject"></param>
		public void OnBeltExit(Collider exitingObject)
		{
			// Capa del objeto
			int layer = exitingObject.gameObject.layer;


			// Si no hace parte de la capa
			if (!IsInLayerMask(_layerMask, layer)) return;

			// Si la referencia al arreglo existe
			if (RegisteredMovableCubes != null)
			{

				// Busco su índice en la lista
				var registeredObject = System.Array.Find(RegisteredMovableCubes, cube => cube.Equals(exitingObject.transform));

				// Si el objeto no se encuentra, retorno
				if (registeredObject == null) return;

				// Obtengo el Componente Rigidbody
				var rigidbodyFromObject = registeredObject.GetComponent<Rigidbody>();

				// Si el componente existe
				if (rigidbodyFromObject)
				{
					// Remuevo el elemento registrado
					_beltObjects.Remove(rigidbodyFromObject);
				}
			}

		}

		private void OnDrawGizmos()
		{
			if (!_drawDebug) return;

			Gizmos.color = Color.white;

			if (_exitBeltRayTransform)
				Gizmos.DrawWireSphere(_exitBeltRayTransform.position, _exitBeltRayRadius);

			if (_beltRayForward)
				Gizmos.DrawRay(_beltRayForward.position, _beltRayForward.forward * _beltRayLength);
		}
	}
}
