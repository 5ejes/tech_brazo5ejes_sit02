﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Crosstales.FB;
using System.IO;
using System;
using System.Xml;

public class LadderMainClass : MonoBehaviour {

	public GameObject _situation;

	public Text infoText;
	public Text titleLeftMenu;

	public Button _infoBt;
	public Button _openBt;
	public Button _saveBt;
	public Button _editBt;
	public Button _compileBt;
	public Button _helpBt;
	public Button _reportBt;
	public Button _trashBt;
	public Button _goToBt;
	public Button _languageBt;
	public Button _debugBt;

	public Button _basicBt;
	public Button _aritmeticBt;
	public Button _logicBt;
	public Button _countersBt;
	public Button _tempBt;
	public Button _specialsBt;

	public Button addLineBt;
	public Button removeLineBt;

	public GameObject _buttonsArea1;
	public GameObject _buttonsArea2;
	public GameObject _buttonsArea3;
	public GameObject _buttonsArea4;
	public GameObject _buttonsArea5;
	public GameObject _buttonsArea6;

	public GameObject _leftMenuArea;
	public GameObject _workbenchArea;
	public GameObject _workbenchContainer;
	public GameObject editMenu;
	public GameObject editorBox;

	public List<GameObject> lineGroupObjects;

	public GameObject lineGroupPrefab;
	public GameObject _backgroundPref;

	public bool simulationRunning = false;
	public bool debugMode = false;

	[Header("CONFIG VALUES")]
	public int digitalOutputs = 16;
	public int digitalInputs = 16;
	public int analogInputs = 4;
	public int analogOutputs = 4;
	public int memoryElements = 20;
	public int memoryNElements = 4;
	public int timerElements = 20;
	public int counterElements = 10;

	private bool overBtActive = false;
	public bool bgEditorActive = false;
	public bool editorActive = false;
	private string path;

	private float _initialAreaHeight;

	[HideInInspector]
	public GameObject _background;

	[HideInInspector]
	public List<int> countersUsed;
	[HideInInspector]
	public List<int> tempsUsed;

	public XmlDocument copiedLineXML;

	public Color lineNormalColor;
	public Color lineActiveColor;

	public PLC ladderEngine;
	private bool[] _inputsBool;
	private byte[] _inputsByte;
	private bool[] _outputsBool;
	private byte[] _outputsByte;

	private string _ladderCompiledStr;

	// Use this for initialization
	void Start () {
		infoText.text = "";

		_infoBt.onClick.AddListener (infoAction);
		_openBt.onClick.AddListener (openAction);
		_saveBt.onClick.AddListener (saveActionPrev);
		_editBt.onClick.AddListener (editAction);
		_compileBt.onClick.AddListener (compileAction);
		_helpBt.onClick.AddListener (helpAction);
		_reportBt.onClick.AddListener (reportAction);
		_trashBt.onClick.AddListener (trashAction);
		_goToBt.onClick.AddListener (goToAction);
		_debugBt.onClick.AddListener (debugAction);
		_languageBt.onClick.AddListener (languageAction);

		addLineBt.onClick.AddListener (addLineAction);
		removeLineBt.onClick.AddListener (removeLineAction);

		_infoBt.gameObject.AddComponent<EventTrigger> ();
		_openBt.gameObject.AddComponent<EventTrigger> ();
		_saveBt.gameObject.AddComponent<EventTrigger> ();
		_compileBt.gameObject.AddComponent<EventTrigger> ();
		_helpBt.gameObject.AddComponent<EventTrigger> ();
		_reportBt.gameObject.AddComponent<EventTrigger> ();
		_trashBt.gameObject.AddComponent<EventTrigger> ();
		_goToBt.gameObject.AddComponent<EventTrigger> ();
		_debugBt.gameObject.AddComponent<EventTrigger> ();
		_languageBt.gameObject.AddComponent<EventTrigger> ();
		_editBt.gameObject.AddComponent<EventTrigger> ();
		addLineBt.gameObject.AddComponent<EventTrigger> ();
		removeLineBt.gameObject.AddComponent<EventTrigger> ();

		EventTrigger.Entry entry_1_0 = new EventTrigger.Entry();
		entry_1_0.eventID = EventTriggerType.PointerEnter;
		entry_1_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "info"); });

		EventTrigger.Entry entry_1_1 = new EventTrigger.Entry();
		entry_1_1.eventID = EventTriggerType.PointerExit;
		entry_1_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "info"); });

		EventTrigger _infoTrigger1 = _infoBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger1.triggers.Add(entry_1_0);
		_infoTrigger1.triggers.Add(entry_1_1);

		EventTrigger.Entry entry_2_0 = new EventTrigger.Entry();
		entry_2_0.eventID = EventTriggerType.PointerEnter;
		entry_2_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "open"); });

		EventTrigger.Entry entry_2_1 = new EventTrigger.Entry();
		entry_2_1.eventID = EventTriggerType.PointerExit;
		entry_2_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "open"); });

		EventTrigger.Entry entry_2_2 = new EventTrigger.Entry();
		entry_2_2.eventID = EventTriggerType.PointerDown;
		entry_2_2.callback.AddListener((data) => { openDownAction((PointerEventData)data); });

		EventTrigger _infoTrigger2 = _openBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger2.triggers.Add(entry_2_0);
		_infoTrigger2.triggers.Add(entry_2_1);
		_infoTrigger2.triggers.Add(entry_2_2);

		EventTrigger.Entry entry_3_0 = new EventTrigger.Entry();
		entry_3_0.eventID = EventTriggerType.PointerEnter;
		entry_3_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "save"); });

		EventTrigger.Entry entry_3_1 = new EventTrigger.Entry();
		entry_3_1.eventID = EventTriggerType.PointerExit;
		entry_3_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "save"); });

		EventTrigger _infoTrigger3 = _saveBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger3.triggers.Add(entry_3_0);
		_infoTrigger3.triggers.Add(entry_3_1);

		EventTrigger.Entry entry_4_0 = new EventTrigger.Entry();
		entry_4_0.eventID = EventTriggerType.PointerEnter;
		entry_4_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "compile"); });

		EventTrigger.Entry entry_4_1 = new EventTrigger.Entry();
		entry_4_1.eventID = EventTriggerType.PointerExit;
		entry_4_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "compile"); });

		EventTrigger _infoTrigger4 = _compileBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger4.triggers.Add(entry_4_0);
		_infoTrigger4.triggers.Add(entry_4_1);

		EventTrigger.Entry entry_5_0 = new EventTrigger.Entry();
		entry_5_0.eventID = EventTriggerType.PointerEnter;
		entry_5_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "help"); });

		EventTrigger.Entry entry_5_1 = new EventTrigger.Entry();
		entry_5_1.eventID = EventTriggerType.PointerExit;
		entry_5_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "help"); });

		EventTrigger _infoTrigger5 = _helpBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger5.triggers.Add(entry_5_0);
		_infoTrigger5.triggers.Add(entry_5_1);

		EventTrigger.Entry entry_6_0 = new EventTrigger.Entry();
		entry_6_0.eventID = EventTriggerType.PointerEnter;
		entry_6_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "report"); });

		EventTrigger.Entry entry_6_1 = new EventTrigger.Entry();
		entry_6_1.eventID = EventTriggerType.PointerExit;
		entry_6_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "report"); });

		EventTrigger _infoTrigger6 = _reportBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger6.triggers.Add(entry_6_0);
		_infoTrigger6.triggers.Add(entry_6_1);

		EventTrigger.Entry entry_7_0 = new EventTrigger.Entry();
		entry_7_0.eventID = EventTriggerType.PointerEnter;
		entry_7_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "trash"); });

		EventTrigger.Entry entry_7_1 = new EventTrigger.Entry();
		entry_7_1.eventID = EventTriggerType.PointerExit;
		entry_7_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "trash"); });

		EventTrigger _infoTrigger7 = _trashBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger7.triggers.Add(entry_7_0);
		_infoTrigger7.triggers.Add(entry_7_1);

		EventTrigger.Entry entry_8_0 = new EventTrigger.Entry();
		entry_8_0.eventID = EventTriggerType.PointerEnter;
		entry_8_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "goto"); });

		EventTrigger.Entry entry_8_1 = new EventTrigger.Entry();
		entry_8_1.eventID = EventTriggerType.PointerExit;
		entry_8_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "goto"); });

		EventTrigger _infoTrigger8 = _goToBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger8.triggers.Add(entry_8_0);
		_infoTrigger8.triggers.Add(entry_8_1);

		EventTrigger.Entry entry_9_0 = new EventTrigger.Entry();
		entry_9_0.eventID = EventTriggerType.PointerEnter;
		entry_9_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "lang"); });

		EventTrigger.Entry entry_9_1 = new EventTrigger.Entry();
		entry_9_1.eventID = EventTriggerType.PointerExit;
		entry_9_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "lang"); });

		EventTrigger _infoTrigger9 = _languageBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger9.triggers.Add(entry_9_0);
		_infoTrigger9.triggers.Add(entry_9_1);

		EventTrigger.Entry entry_10_0 = new EventTrigger.Entry();
		entry_10_0.eventID = EventTriggerType.PointerEnter;
		entry_10_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "edit"); });

		EventTrigger.Entry entry_10_1 = new EventTrigger.Entry();
		entry_10_1.eventID = EventTriggerType.PointerExit;
		entry_10_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "edit"); });

		EventTrigger _infoTrigger10 = _editBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger10.triggers.Add(entry_10_0);
		_infoTrigger10.triggers.Add(entry_10_1);

		EventTrigger.Entry entry_11_0 = new EventTrigger.Entry();
		entry_11_0.eventID = EventTriggerType.PointerEnter;
		entry_11_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "add"); });

		EventTrigger.Entry entry_11_1 = new EventTrigger.Entry();
		entry_11_1.eventID = EventTriggerType.PointerExit;
		entry_11_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "add"); });

		EventTrigger _infoTrigger11 = addLineBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger11.triggers.Add(entry_11_0);
		_infoTrigger11.triggers.Add(entry_11_1);

		EventTrigger.Entry entry_12_0 = new EventTrigger.Entry();
		entry_12_0.eventID = EventTriggerType.PointerEnter;
		entry_12_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "remove"); });

		EventTrigger.Entry entry_12_1 = new EventTrigger.Entry();
		entry_12_1.eventID = EventTriggerType.PointerExit;
		entry_12_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "remove"); });

		EventTrigger _infoTrigger12 = removeLineBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger12.triggers.Add(entry_12_0);
		_infoTrigger12.triggers.Add(entry_12_1);

		EventTrigger.Entry entry_13_0 = new EventTrigger.Entry();
		entry_13_0.eventID = EventTriggerType.PointerEnter;
		entry_13_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "debug"); });

		EventTrigger.Entry entry_13_1 = new EventTrigger.Entry();
		entry_13_1.eventID = EventTriggerType.PointerExit;
		entry_13_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "debug"); });

		EventTrigger _infoTrigger13 = _debugBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger13.triggers.Add(entry_13_0);
		_infoTrigger13.triggers.Add(entry_13_1);

		_buttonsArea1.SetActive (true);
		_buttonsArea2.SetActive (false);
		_buttonsArea3.SetActive (false);
		_buttonsArea4.SetActive (false);
		_buttonsArea5.SetActive (false);
		_buttonsArea6.SetActive (false);

		_basicBt.GetComponent<Image> ().color = new Color (41f / 255f, 171f / 255f, 226f / 255f);
		_aritmeticBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
		_logicBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
		_countersBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
		_tempBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
		_specialsBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);

		_basicBt.onClick.AddListener (delegate {
			left_bt_action (1);
		});
		_aritmeticBt.onClick.AddListener (delegate {
			left_bt_action (2);
		});
		_logicBt.onClick.AddListener (delegate {
			left_bt_action (3);
		});
		_countersBt.onClick.AddListener (delegate {
			left_bt_action (4);
		});
		_tempBt.onClick.AddListener (delegate {
			left_bt_action (5);
		});
		_specialsBt.onClick.AddListener (delegate {
			left_bt_action(6);
		});
			
		_inputsBool = new bool[digitalInputs];
		_inputsByte = new byte[analogInputs];
		_outputsBool = new bool[digitalOutputs];
		_outputsByte = new byte[analogOutputs];

		if(analogInputs == 0){
			_specialsBt.gameObject.SetActive (false);
			_leftMenuArea.GetComponent<RectTransform> ().offsetMax = new Vector2 (_leftMenuArea.GetComponent<RectTransform> ().offsetMax.x, _leftMenuArea.GetComponent<RectTransform> ().offsetMax.y + 50);
		}

		countersUsed = new List<int> ();
		tempsUsed = new List<int> ();
		//initial linegroup
		createInitialGroupLine();

		lineNormalColor = new Color (77f / 255f, 77f / 255f, 77f / 255f);
		lineActiveColor = new Color (255f / 255f, 0f / 255f, 0f / 255f);
	}

	public void initialAction(){
		float _aspectRatio = 0;
		switch(Manager.Instance.globalGraphicInterface){
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation = GameObject.Find ("Canvas/ElectricalBench");
			_aspectRatio = (float)_situation.GetComponent<ElectricalTestBenchMainClass> ()._cam1.pixelWidth / (float)_situation.GetComponent<ElectricalTestBenchMainClass> ()._cam1.pixelHeight;
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation = GameObject.Find ("Canvas/PneumaticBench");
			_aspectRatio = (float)_situation.GetComponent<PnematicTestBenchClass> ()._cam1.pixelWidth / (float)_situation.GetComponent<PnematicTestBenchClass> ()._cam1.pixelHeight;
			break;
		case baseSystem.graphicInterfaces.none:
			GameObject _langBt = GameObject.Find ("lang_bt_ladder");
			_langBt.transform.localPosition = new Vector3 (_langBt.transform.localPosition.x - 105f, _langBt.transform.localPosition.y, _langBt.transform.localPosition.z);
			_langBt.GetComponent<languageBt> ().setInitialPosition ();
			_goToBt.gameObject.SetActive (false);
			_debugBt.gameObject.SetActive (false);

			_trashBt.transform.localPosition = new Vector3 (_trashBt.transform.localPosition.x - 105f, _trashBt.transform.localPosition.y, _trashBt.transform.localPosition.z);
			_helpBt.transform.localPosition = new Vector3 (_helpBt.transform.localPosition.x - 105f, _helpBt.transform.localPosition.y, _helpBt.transform.localPosition.z);
			_langBt.transform.localPosition = new Vector3 (_langBt.transform.localPosition.x - 105f, _langBt.transform.localPosition.y, _langBt.transform.localPosition.z);

			_situation = GameObject.Find ("Canvas/OnlyControllerMain");
			_aspectRatio = (float)_situation.GetComponent<onlyControllerMainClass> ()._cam1.pixelWidth / (float)_situation.GetComponent<onlyControllerMainClass> ()._cam1.pixelHeight;
			break;
		}

		if (Manager.Instance.globalLanguageEnabled == false) {
			GameObject _langBtToHide = GameObject.Find ("lang_bt_ladder");
			_langBtToHide.SetActive (false);
		}

		float _heightTotal = 1080f * ((1920f / 1080f) / _aspectRatio);
		_initialAreaHeight = _heightTotal - 210f;


		#if UNITY_WEBGL
		Application.ExternalEval(
			@"
document.addEventListener('click', function() {

    var fileuploader = document.getElementById('fileuploader');
    if (!fileuploader) {
        fileuploader = document.createElement('input');
        fileuploader.setAttribute('style','display:none;');
        fileuploader.setAttribute('type', 'file');
		fileuploader.setAttribute('accept', '.vplc');
        fileuploader.setAttribute('id', 'fileuploader');
        fileuploader.setAttribute('class', '');
        document.getElementsByTagName('body')[0].appendChild(fileuploader);

        fileuploader.onchange = function(e) {
        var files = e.target.files;
            for (var i = 0, f; f = files[i]; i++) {
				var reader = new FileReader();
			    reader.readAsText(f,'UTF-8');
			    reader.onload = readerEvent => {
			       	var content = readerEvent.target.result; // this is the content!
					SendMessage('" + gameObject.name +@"', 'FileDialogResult', content);
			    }
            }
        };
    }
    if (fileuploader.getAttribute('class') == 'focused') {
        fileuploader.setAttribute('class', '');
        fileuploader.click();
    }
});
            ");

		#endif
	}

	private void left_bt_action(int _id){
		_leftMenuArea.GetComponent<ScrollRect> ().verticalNormalizedPosition = 1;
		switch(_id){
		case 1:
			_buttonsArea1.SetActive (true);
			_buttonsArea2.SetActive (false);
			_buttonsArea3.SetActive (false);
			_buttonsArea4.SetActive (false);
			_buttonsArea5.SetActive (false);
			_buttonsArea6.SetActive (false);
			_basicBt.GetComponent<Image> ().color = new Color (41f / 255f, 171f / 255f, 226f / 255f);
			_aritmeticBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_logicBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_countersBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_tempBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_specialsBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			break;
		case 2:
			_buttonsArea1.SetActive (false);
			_buttonsArea2.SetActive (true);
			_buttonsArea3.SetActive (false);
			_buttonsArea4.SetActive (false);
			_buttonsArea5.SetActive (false);
			_buttonsArea6.SetActive (false);
			_aritmeticBt.GetComponent<Image> ().color = new Color (41f / 255f, 171f / 255f, 226f / 255f);
			_basicBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_logicBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_countersBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_tempBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_specialsBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			break;
		case 3:
			_buttonsArea1.SetActive (false);
			_buttonsArea2.SetActive (false);
			_buttonsArea3.SetActive (true);
			_buttonsArea4.SetActive (false);
			_buttonsArea5.SetActive (false);
			_buttonsArea6.SetActive (false);
			_logicBt.GetComponent<Image> ().color = new Color (41f / 255f, 171f / 255f, 226f / 255f);
			_basicBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_aritmeticBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_countersBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_tempBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_specialsBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			break;
		case 4:
			_buttonsArea1.SetActive (false);
			_buttonsArea2.SetActive (false);
			_buttonsArea3.SetActive (false);
			_buttonsArea4.SetActive (true);
			_buttonsArea5.SetActive (false);
			_buttonsArea6.SetActive (false);
			_countersBt.GetComponent<Image> ().color = new Color (41f / 255f, 171f / 255f, 226f / 255f);
			_basicBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_aritmeticBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_logicBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_tempBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_specialsBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			break;
		case 5:
			_buttonsArea1.SetActive (false);
			_buttonsArea2.SetActive (false);
			_buttonsArea3.SetActive (false);
			_buttonsArea4.SetActive (false);
			_buttonsArea5.SetActive (true);
			_buttonsArea6.SetActive (false);
			_tempBt.GetComponent<Image> ().color = new Color (41f / 255f, 171f / 255f, 226f / 255f);
			_basicBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_aritmeticBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_logicBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_countersBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_specialsBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			break;
		case 6:
			_buttonsArea1.SetActive (false);
			_buttonsArea2.SetActive (false);
			_buttonsArea3.SetActive (false);
			_buttonsArea4.SetActive (false);
			_buttonsArea5.SetActive (false);
			_buttonsArea6.SetActive (true);
			_specialsBt.GetComponent<Image> ().color = new Color (41f / 255f, 171f / 255f, 226f / 255f);
			_basicBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_aritmeticBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_logicBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_countersBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			_tempBt.GetComponent<Image> ().color = new Color (0f / 255f, 113f / 255f, 183f / 255f);
			break;
		}
	}

	public void overButtonAction_(PointerEventData data, string _element)
	{
		overBtActive = true;
		string _text = "";
		switch(_element){
		case "info": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='info_over']").InnerText;
			break;
		case "open": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='open_over']").InnerText;
			break;
		case "save": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='save_over']").InnerText;
			break;
		case "compile": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='compile_over']").InnerText;
			break;
		case "help": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='help_over']").InnerText;
			break;
		case "report": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='report_over']").InnerText;
			break;
		case "trash": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='trash_over']").InnerText;
			break;
		case "goto": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='bench_over']").InnerText;
			break;
		case "lang": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='lang_over']").InnerText;
			break;
		case "edit": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='edit_over']").InnerText;
			break;
		case "add": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_add_line']").InnerText;
			break;
		case "remove": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_remove_line']").InnerText;
			break;
		case "deleteLine": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_delete_line']").InnerText;
			break;
		case "moveDownLine": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_move_down_line']").InnerText;
			break;
		case "moveUpLine": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_move_up_line']").InnerText;
			break;
		case "copyLine": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_copy_line']").InnerText;
			break;
		case "pasteLine": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_paste_line']").InnerText;
			break;
		case "addPrevLine": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_add_prev_line']").InnerText;
			break;
		case "debug": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_debug']").InnerText;
			break;
		}

		CancelInvoke ("hideInfoText");
		infoText.text = _text;

	}

	public void outButtonAction_(PointerEventData data, string _element)
	{
		if(overBtActive == true){
			overBtActive = false;
			if(infoText.text != Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='successful_comp']").InnerText){
				infoText.text = "";
			}
		}
	}

	public void setTexts(){
		titleLeftMenu.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='grafcet_functions']").InnerText;
		_basicBt.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='basics_bt']").InnerText;
		_aritmeticBt.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='aritmetic_bt']").InnerText;
		_logicBt.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='logic_bt']").InnerText;
		_countersBt.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='counters_bt']").InnerText;
		_tempBt.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='temps_bt']").InnerText;
		_specialsBt.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_specials']").InnerText;
	}

	private void infoAction(){
		string DescriptionPractice = "";
		switch(Manager.Instance.globalGraphicInterface){
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert = Instantiate(_situation.GetComponent<ElectricalTestBenchMainClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			DescriptionPractice = Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<ElectricalTestBenchMainClass>().situationTag + "/description_practice").InnerText;
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.GetComponent<Alert>().showAlert(7, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='situation']").InnerText + "|" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='procedure']").InnerText, Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<ElectricalTestBenchMainClass>().situationTag + "/name_practice").InnerText, DescriptionPractice + "|" + Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<ElectricalTestBenchMainClass>().situationTag + "/procedure").InnerText);	
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation.GetComponent<PnematicTestBenchClass>()._alert = Instantiate(_situation.GetComponent<PnematicTestBenchClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<PnematicTestBenchClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<PnematicTestBenchClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			DescriptionPractice = Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<PnematicTestBenchClass>().situationTag + "/description_practice").InnerText;
			_situation.GetComponent<PnematicTestBenchClass>()._alert.GetComponent<Alert>().showAlert(7, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='situation']").InnerText + "|" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='procedure']").InnerText, Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<PnematicTestBenchClass>().situationTag + "/name_practice").InnerText, DescriptionPractice + "|" + Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<PnematicTestBenchClass>().situationTag + "/procedure").InnerText);	
			break;
		case baseSystem.graphicInterfaces.none:
			_situation.GetComponent<onlyControllerMainClass>()._alert = Instantiate(_situation.GetComponent<onlyControllerMainClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<onlyControllerMainClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<onlyControllerMainClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			DescriptionPractice = Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<onlyControllerMainClass>().situationTag + "/description_practice").InnerText;
			_situation.GetComponent<onlyControllerMainClass>()._alert.GetComponent<Alert>().showAlert(7, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='situation']").InnerText + "|" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='procedure']").InnerText, Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<onlyControllerMainClass>().situationTag + "/name_practice").InnerText, DescriptionPractice + "|" + Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<onlyControllerMainClass>().situationTag + "/procedure").InnerText);	
			break;
		}
	}

	private void openAction(){
		debugMode = false;
		string extensions = "vplc";
		string _dataJSON = "";

		#if !UNITY_WEBGL
		if(Application.platform == RuntimePlatform.Android){
			GameObject openWindow = GameObject.Find ("openWindow");
			if(openWindow != null){
				openWindow.GetComponent<OpenWindow> ().showWindow (gameObject);
			}
		} else {
			string _path = FileBrowser.OpenSingleFile (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='openFileDialog']").InnerText, "", extensions);
			if(_path != ""){
				StreamReader reader = new StreamReader(_path);
				_dataJSON = reader.ReadToEnd ();
				reader.Close();

				resetScenario ();

				XmlDocument xmlProgram = new XmlDocument ();
				xmlProgram.LoadXml (_dataJSON);

				StartCoroutine (loadElements (xmlProgram));

			}
		}
		#endif
	}

	public void openFromAndroid(string pathFile){
		string _dataJSON = "";
		if(pathFile != ""){
			StreamReader reader = new StreamReader(pathFile);
			_dataJSON = reader.ReadToEnd ();
			reader.Close();

			resetScenario ();

			XmlDocument xmlProgram = new XmlDocument ();
			xmlProgram.LoadXml (_dataJSON);

			StartCoroutine (loadElements (xmlProgram));

		}
	}

	private void openDownAction(PointerEventData data){
		#if UNITY_WEBGL
		Application.ExternalEval(
			@"
var fileuploader = document.getElementById('fileuploader');
if (fileuploader) {
    fileuploader.setAttribute('class', 'focused');
}
            ");
		#endif
	}

	public void FileDialogResult(string fileData) {
		//restart scenario
		resetScenario();
		//load new elements.
		XmlDocument xmlProgram = new XmlDocument ();
		xmlProgram.LoadXml (fileData);

		StartCoroutine (loadElements (xmlProgram));
	}

	private IEnumerator loadElements(XmlDocument ladderXML){
		XmlNode root = ladderXML.DocumentElement;
		XmlNodeList nodeList;
		nodeList = root.SelectNodes( "programa" );
		nodeList = nodeList[0].ChildNodes;

		yield return new WaitForSeconds (0.5f);

		for (int i = 0; i < nodeList.Count; i++) {
			XmlNodeList _inputList;
			_inputList = nodeList[i].SelectNodes("entrada");
			for (int j = 0; j < _inputList.Count; j++){
				yield return StartCoroutine (openProcessInput (_inputList[j], i));
			}

			XmlNodeList _outputList;
			_outputList = nodeList[i].SelectNodes("salida");

			if (_outputList.Count == 1) {
				yield return StartCoroutine (openProcessOutput (_outputList [0], i));
			}
		}
	}

	public IEnumerator openProcessInput(XmlNode inputXML, int lineIndex, bool isForPaste = false){

		if (inputXML.ChildNodes[0].ChildNodes.Count > 0) {
			if (lineGroupObjects.Count < (lineIndex + 1)) {
				addLineAction ();
				yield return new WaitForSeconds (0.1f);
			}
			yield return StartCoroutine (processInputBranch (inputXML.ChildNodes[0], lineIndex, 0));

		} else {
			if (lineGroupObjects.Count < (lineIndex + 1)) {
				addLineAction ();
				yield return new WaitForSeconds (0.1f);
			}
			int newlineNumber = int.Parse (inputXML.Attributes ["numero"].Value);

			int newBrachLevel = 0;

			lineGroupObjects [lineIndex].GetComponent<lineGroupObject> ().addLineAtLineNumberAndBranch (newlineNumber, newBrachLevel, inputXML.ChildNodes[0].Value, dragElementLadder._typesIO.Input);
		}

		yield return new WaitForSeconds (0.01f);

	}

	private IEnumerator processInputBranch(XmlNode inputXML, int lineIndex, int branchOffset){
		XmlNodeList branchNodes = inputXML.ChildNodes;
		int branchLevel = branchOffset;

		for(int i = 0; i < branchNodes.Count; i++){
			for (int j = 0; j < branchNodes [i].ChildNodes.Count; j++) {
				if (branchNodes [i].ChildNodes [j].SelectNodes("div").Count > 0) {
					//new branch
					yield return StartCoroutine (processInputBranch (branchNodes [i].ChildNodes [j].ChildNodes[0], lineIndex, branchLevel));
				} else {
					int newlineNumber = int.Parse (branchNodes [i].ChildNodes [j].Attributes ["numero"].Value);
					int newBrachLevel = branchLevel;
					if (i == 0) {
						GameObject branchObject = lineGroupObjects [lineIndex].GetComponent<lineGroupObject> ().getElementPerLineAndLevel (newlineNumber, newBrachLevel);
						branchObject.GetComponent<lineObject> ().addBranch ();
						yield return new WaitForSeconds (0.05f);
					}
					lineGroupObjects [lineIndex].GetComponent<lineGroupObject> ().addLineAtLineNumberAndBranch (newlineNumber, newBrachLevel, branchNodes [i].ChildNodes [j].ChildNodes [0].Value, dragElementLadder._typesIO.Input);
				}
			}
			branchLevel++;
		}

		yield return new WaitForSeconds (0.01f);
	}

	public IEnumerator openProcessOutput(XmlNode outputXML, int lineIndex){
		XmlNodeList _outputChilds;

		if (outputXML.SelectNodes ("sal").Count > 0) {
			//Debug.Log (outputXML.SelectNodes ("sal").Count);
			if (lineGroupObjects.Count < (lineIndex + 1)) {
				addLineAction ();
				yield return new WaitForSeconds (0.1f);
			}

			for(int i = 0; i < outputXML.SelectNodes ("sal").Count; i++){
				//Debug.Log (outputXML.SelectNodes ("sal")[i].OuterXml);
				XmlNode outputDivNode = outputXML.SelectNodes ("sal") [i];
				if (i > 0) {
					GameObject outObject = lineGroupObjects [lineIndex].GetComponent<lineGroupObject> ().getElementPerLineAndLevel (12, i - 1);
					outObject.GetComponent<lineObject> ().addBranch ();
					yield return new WaitForSeconds (0.05f);
				}

				int newlineNumber = 12;
				int newBrachLevel = i;
				int outputLineName = -1;
				if(outputDivNode.Attributes ["name"] != null){
					outputLineName = int.Parse (outputDivNode.Attributes ["name"].Value);
				}
				lineGroupObjects [lineIndex].GetComponent<lineGroupObject> ().addLineAtLineNumberAndBranch (newlineNumber, newBrachLevel, outputDivNode.ChildNodes[0].Value, dragElementLadder._typesIO.Output, outputLineName);
			}

			//branch
		} else {
			//Debug.Log (outputXML.ChildNodes [0].Value);
			if (lineGroupObjects.Count < (lineIndex + 1)) {
				addLineAction ();
				yield return new WaitForSeconds (0.1f);
			}
			int newlineNumber = 12;
			int newBrachLevel = 0;
			int outputLineName = -1;
			if(outputXML.Attributes ["name"] != null){
				outputLineName = int.Parse (outputXML.Attributes ["name"].Value);
			}
			lineGroupObjects [lineIndex].GetComponent<lineGroupObject> ().addLineAtLineNumberAndBranch (newlineNumber, newBrachLevel, outputXML.ChildNodes[0].Value, dragElementLadder._typesIO.Output, outputLineName);
		}

		yield return new WaitForSeconds (0.01f);
	}

	private void saveActionPrev(){
		StartCoroutine (saveAction ());
	}

	public void editAction(){
		if (editorActive == false) {
			editorActive = true;
			for (int i = 0; i < lineGroupObjects.Count; i++) {
				lineGroupObjects [i].GetComponent<lineGroupObject> ().showMainEditor ();
			}
		} else {
			editorActive = false;
			for (int i = 0; i < lineGroupObjects.Count; i++) {
				lineGroupObjects [i].GetComponent<lineGroupObject> ().hideMainEditor ();
			}
		}
	}

	private IEnumerator saveAction(){
		string _data = generateExportData();

		if ( Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer) {
			string extensions = "vplc";

			path = FileBrowser.SaveFile (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='fileDialog']").InnerText, "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='fileDefaultName']").InnerText, extensions);

			//Debug.Log("Save file: " + path);

			if (path == "") {
				yield break;
			}

		}
		else if( Application.platform == RuntimePlatform.Android){

			GameObject saveAsWindow = GameObject.Find ("saveWindow");
			if(saveAsWindow != null){
				saveAsWindow.GetComponent<SaveWindow> ().showWindow (gameObject, _data);
			}
		}
		else if(Application.platform == RuntimePlatform.IPhonePlayer){
			path = Application.persistentDataPath + "/" + Manager.Instance.currentPDFName + ".vplc";
		}

		byte[] bytes = System.Text.Encoding.UTF8.GetBytes (_data);

		if (Application.platform == RuntimePlatform.WebGLPlayer) {
			saveFiletoWebGL (_data);
		} else if (Application.platform != RuntimePlatform.Android) {
			File.WriteAllBytes (path, bytes);
			closeFile ();
		}

	}

	void closeFile(){
		if (Application.platform != RuntimePlatform.WebGLPlayer) {
			if( Application.platform == RuntimePlatform.Android ){
				
			}
		}
	}

	private void saveFiletoWebGL(string dataToSave){
		Application.ExternalCall ("saveToFile", dataToSave, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='fileDefaultName']").InnerText + ".vplc");
	}

	private void compileAction(){
		_ladderCompiledStr = generateExportData();

		ladderEngine = new PLC(_inputsBool, _inputsByte, _outputsBool, _outputsByte, _ladderCompiledStr);

		simulationRunning = true;

		showInfoText (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='successful_comp']").InnerText);
	}

	private string generateExportData(){
		string _resultLadderData = "";

		XmlDocument doc = new XmlDocument ();
		XmlElement el = (XmlElement)doc.AppendChild(doc.CreateElement("inicio"));
		XmlNode configNode = el.AppendChild(doc.CreateNode(XmlNodeType.Element,"config",""));
		configNode.AppendChild (configNode.OwnerDocument.CreateTextNode (""));

		XmlNode ProgramNode = el.AppendChild(doc.CreateNode(XmlNodeType.Element,"programa",""));

		int lineOffset = 0;
		for(int i = 0; i < lineGroupObjects.Count; i++){
			ProgramNode.AppendChild (lineGroupObjects [i].GetComponent<lineGroupObject> ().getXMLLineNode (doc, i, lineOffset));

			lineOffset = lineOffset + lineGroupObjects [i].GetComponent<lineGroupObject> ().getMaxBranchOffset ();
		}

		_resultLadderData = doc.OuterXml;
		return _resultLadderData;
	}

	private void helpAction(){

		switch(Manager.Instance.globalGraphicInterface){
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert = Instantiate(_situation.GetComponent<ElectricalTestBenchMainClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.GetComponent<Alert>().showAlert(4, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='help']").InnerText, "", Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<ElectricalTestBenchMainClass>().situationTag + "/tips").InnerText);
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation.GetComponent<PnematicTestBenchClass>()._alert = Instantiate(_situation.GetComponent<PnematicTestBenchClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<PnematicTestBenchClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<PnematicTestBenchClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			_situation.GetComponent<PnematicTestBenchClass>()._alert.GetComponent<Alert>().showAlert(4, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='help']").InnerText, "", Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<PnematicTestBenchClass>().situationTag + "/tips").InnerText);
			break;
		case baseSystem.graphicInterfaces.none:
			_situation.GetComponent<onlyControllerMainClass>()._alert = Instantiate(_situation.GetComponent<onlyControllerMainClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<onlyControllerMainClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<onlyControllerMainClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			_situation.GetComponent<onlyControllerMainClass>()._alert.GetComponent<Alert>().showAlert(4, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='help']").InnerText, "", Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<onlyControllerMainClass>().situationTag + "/tips").InnerText);
			break;
		}
	}

	private void reportAction(){

		List<reportImgElement> _list = new List<reportImgElement>();
		List<reportExtraPageStructure> _extraPagesData = null;

		//check if extra pages is required
		float _widthMaxSize = 0;
		//float _heightMaxSize = _initialAreaHeight;
		float _heightMaxSize = _workbenchContainer.GetComponent<RectTransform>().rect.height;

		_widthMaxSize = 1630;

		if (_workbenchArea.GetComponent<RectTransform> ().rect.height > (_heightMaxSize)) {
			_extraPagesData = new List<reportExtraPageStructure> ();

			//decide number of pages to take
			int widthDivisions = 1;
			int heightDivisions = Mathf.CeilToInt (_workbenchArea.GetComponent<RectTransform> ().rect.height / _heightMaxSize);

			Vector3 _initialPosition = _workbenchArea.transform.localPosition;
			_workbenchContainer.GetComponent<ScrollRect> ().enabled = false;
			GameObject.Find ("Canvas/Ladder/workbench/Scrollbar Horizontal").SetActive (false);
			GameObject.Find ("Canvas/Ladder/workbench/Scrollbar Vertical").SetActive (false);

			addLineBt.gameObject.SetActive (false);
			removeLineBt.gameObject.SetActive (false);

			Vector3 iniPositionIndicator = new Vector3 ();
			switch (Manager.Instance.globalGraphicInterface) {
			case baseSystem.graphicInterfaces.electrical_bench:
				iniPositionIndicator = _situation.GetComponent<ElectricalTestBenchMainClass> ()._upperIndicator.transform.localPosition;
				_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x + 1000f, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.pneumatic_bench:
				iniPositionIndicator = _situation.GetComponent<PnematicTestBenchClass> ()._upperIndicator.transform.localPosition;
				_situation.GetComponent<PnematicTestBenchClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x + 1000f, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.none:
				iniPositionIndicator = _situation.GetComponent<onlyControllerMainClass> ()._upperIndicator.transform.localPosition;
				_situation.GetComponent<onlyControllerMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x + 1000f, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			}

			reportExtraPageStructure _extraPageData = new reportExtraPageStructure ();

			for (int j = 0; j < heightDivisions; j++) {
				if (j == 0) {
					//First page
					_workbenchArea.transform.localPosition = new Vector3 (0, 0, 0);

					reportImgElement _img1 = new reportImgElement ();
					Texture2D _screenShot1 = new Texture2D (100, 100);

					_screenShot1 = screenCapture.captureImage (new Rect (203, (_initialAreaHeight + 40f) / 2f, 1716, _initialAreaHeight), "Main Camera");

					_img1.type = 1;
					_img1.title1 = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_report_title']").InnerText;
					_img1.height1 = 900;
					_img1.image1 = _screenShot1;
					_list.Add (_img1);
				} else {
					//Extra pages
					_workbenchArea.transform.localPosition = new Vector3 (0, _heightMaxSize * (float)j, 0);

					reportImgElement _imgExtra = new reportImgElement ();
					Texture2D _screenShotExtra = new Texture2D (100, 100);
					_screenShotExtra = screenCapture.captureImage (new Rect (203, (_initialAreaHeight + 40f) / 2f, 1716, _initialAreaHeight), "Main Camera");
					_imgExtra.type = 1;
					_imgExtra.title1 = "";
					_imgExtra.height1 = 900;
					_imgExtra.image1 = _screenShotExtra;

					if (_extraPageData._listElements == null) {
						_extraPageData._listElements = new List<reportImgElement> ();
					}

					_extraPageData._listElements.Add (_imgExtra);

					if (_extraPageData._listElements.Count == 2 || (j == (heightDivisions - 1))) {
						_extraPagesData.Add (_extraPageData);
						_extraPageData = new reportExtraPageStructure ();
					}
				}
			}
			_workbenchArea.transform.localPosition = _initialPosition;
			GameObject.Find ("Canvas/Ladder/workbench/Scrollbar Horizontal").SetActive (true);
			GameObject.Find ("Canvas/Ladder/workbench/Scrollbar Vertical").SetActive (true);
			_workbenchContainer.GetComponent<ScrollRect> ().enabled = true;

			addLineBt.gameObject.SetActive (true);
			removeLineBt.gameObject.SetActive (true);

			switch (Manager.Instance.globalGraphicInterface) {
			case baseSystem.graphicInterfaces.electrical_bench:
				_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.pneumatic_bench:
				_situation.GetComponent<PnematicTestBenchClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.none:
				_situation.GetComponent<onlyControllerMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			}

		} else {

			Vector3 _initialPosition = _workbenchArea.transform.localPosition;
			_workbenchContainer.GetComponent<ScrollRect> ().enabled = false;
			GameObject.Find ("Canvas/Ladder/workbench/Scrollbar Horizontal").SetActive (false);
			GameObject.Find ("Canvas/Ladder/workbench/Scrollbar Vertical").SetActive (false);

			addLineBt.gameObject.SetActive (false);
			removeLineBt.gameObject.SetActive (false);

			reportImgElement _img1 = new reportImgElement ();
			Texture2D _screenShot1 = new Texture2D (100, 100);

			Vector3 iniPositionIndicator = new Vector3 ();
			switch (Manager.Instance.globalGraphicInterface) {
			case baseSystem.graphicInterfaces.electrical_bench:
				iniPositionIndicator = _situation.GetComponent<ElectricalTestBenchMainClass> ()._upperIndicator.transform.localPosition;
				_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x + 1000f, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.pneumatic_bench:
				iniPositionIndicator = _situation.GetComponent<PnematicTestBenchClass> ()._upperIndicator.transform.localPosition;
				_situation.GetComponent<PnematicTestBenchClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x + 1000f, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.none:
				iniPositionIndicator = _situation.GetComponent<onlyControllerMainClass> ()._upperIndicator.transform.localPosition;
				_situation.GetComponent<onlyControllerMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x + 1000f, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			}
				
			_screenShot1 = screenCapture.captureImage (new Rect (203, (_initialAreaHeight + 40f) / 2f, 1716, _initialAreaHeight), "Main Camera");

			switch (Manager.Instance.globalGraphicInterface) {
			case baseSystem.graphicInterfaces.electrical_bench:
				_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.pneumatic_bench:
				_situation.GetComponent<PnematicTestBenchClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.none:
				_situation.GetComponent<onlyControllerMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			}

			_img1.type = 1;
			_img1.title1 = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_report_title']").InnerText;
			_img1.height1 = 900;
			_img1.image1 = _screenShot1;
			_list.Add (_img1);

			_workbenchArea.transform.localPosition = _initialPosition;
			GameObject.Find ("Canvas/Ladder/workbench/Scrollbar Horizontal").SetActive (true);
			GameObject.Find ("Canvas/Ladder/workbench/Scrollbar Vertical").SetActive (true);
			_workbenchContainer.GetComponent<ScrollRect> ().enabled = true;

			addLineBt.gameObject.SetActive (true);
			removeLineBt.gameObject.SetActive (true);
		}

		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation.GetComponent<ElectricalTestBenchMainClass>()._evaluation = Instantiate(_situation.GetComponent<ElectricalTestBenchMainClass>()._evaluationPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<ElectricalTestBenchMainClass>()._evaluation.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<ElectricalTestBenchMainClass>()._evaluation.transform.localPosition = new Vector3(0, 1200, 0);
			_situation.GetComponent<ElectricalTestBenchMainClass>()._evaluation.name = "SitEvaluation";
			_situation.GetComponent<ElectricalTestBenchMainClass> ()._evaluation.GetComponent<Evaluation> ().startEvaluation (3, 1, _situation.GetComponent<ElectricalTestBenchMainClass> ().orderOptions, 0f, _list, _extraPagesData);
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation.GetComponent<PnematicTestBenchClass>()._evaluation = Instantiate(_situation.GetComponent<PnematicTestBenchClass>()._evaluationPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<PnematicTestBenchClass>()._evaluation.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<PnematicTestBenchClass>()._evaluation.transform.localPosition = new Vector3(0, 1200, 0);
			_situation.GetComponent<PnematicTestBenchClass>()._evaluation.name = "SitEvaluation";
			_situation.GetComponent<PnematicTestBenchClass> ()._evaluation.GetComponent<Evaluation> ().startEvaluation (3, 1, _situation.GetComponent<PnematicTestBenchClass> ().orderOptions, 0f, _list, _extraPagesData);
			break;
		case baseSystem.graphicInterfaces.none:
			_situation.GetComponent<onlyControllerMainClass>()._evaluation = Instantiate(_situation.GetComponent<onlyControllerMainClass>()._evaluationPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<onlyControllerMainClass>()._evaluation.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<onlyControllerMainClass>()._evaluation.transform.localPosition = new Vector3(0, 1200, 0);
			_situation.GetComponent<onlyControllerMainClass>()._evaluation.name = "SitEvaluation";
			_situation.GetComponent<onlyControllerMainClass> ()._evaluation.GetComponent<Evaluation> ().startEvaluation (3, 1, _situation.GetComponent<onlyControllerMainClass> ().orderOptions, 0f, _list, _extraPagesData);
			break;
		}
	}

	private void trashAction(){
		resetScenario ();
	}

	private void goToAction(){
		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation.GetComponent<ElectricalTestBenchMainClass>().Fundido_.transform.SetAsLastSibling ();
			_situation.GetComponent<ElectricalTestBenchMainClass>().Fundido_.CrossFadeAlpha(1, 0.5f, false);
			_situation.GetComponent<ElectricalTestBenchMainClass>().Fundido_.raycastTarget = true;
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation.GetComponent<PnematicTestBenchClass>().Fundido_.transform.SetAsLastSibling ();
			_situation.GetComponent<PnematicTestBenchClass>().Fundido_.CrossFadeAlpha(1, 0.5f, false);
			_situation.GetComponent<PnematicTestBenchClass>().Fundido_.raycastTarget = true;
			break;
		}

		GameObject.Find ("SystemController").GetComponent<baseSystem>().goToBench();
	}

	private void debugAction(){
		string newladderCompiledStr = generateExportData();

		if (debugMode == false) {
			if (newladderCompiledStr == _ladderCompiledStr) {
				debugMode = true;
			} else {
				showInfoText (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_first_must_comp']").InnerText);
			}
		} else {
			debugMode = false;
		}
	}

	private void languageAction(){
		GameObject _langBt = GameObject.Find ("lang_bt_ladder");
		_langBt.GetComponent<languageBt> ().showMenu (setNewLanguageFromMenu);
	}

	public void setNewLanguageFromMenu(){
		GameObject.Find ("SystemController").GetComponent<baseSystem>().Language = Manager.Instance.globalLanguage;
		StartCoroutine (GameObject.Find ("SystemController").GetComponent<baseSystem> ().loadLanguageXML (false, false));
		setTexts ();
		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation.GetComponent<ElectricalTestBenchMainClass> ().setTextsAfterChangeLanguage ();
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation.GetComponent<PnematicTestBenchClass> ().setTextsAfterChangeLanguage ();
			break;
		case baseSystem.graphicInterfaces.none:
			break;
		}
	}

	private void resetScenario(){
		debugMode = false;
		if(simulationRunning == true){
			simulationRunning = false;
			ladderEngine = null;
		}

		if(lineGroupObjects.Count >= 1){
			for(int i = lineGroupObjects.Count-1; i >= 0; i--){
				Destroy (lineGroupObjects [i]);
				lineGroupObjects.RemoveAt (i);
			}

			createInitialGroupLine ();

			updateWorkbenchArea ();
		}
	}

	public void showInfoText(string _text){
		CancelInvoke ("hideInfoText");
		infoText.text = _text;
		Invoke ("hideInfoText", 3f);
	}

	public void hideInfoText(){
		infoText.text = "";
	}

	public void openEditMenu(GameObject objectToEdit){
		activateBackground ();
		bgEditorActive = true;
		_background.AddComponent<Button> ().onClick.AddListener (hideEditMenu);
		_background.GetComponent<Button> ().transition = Selectable.Transition.None;

		editMenu.GetComponent<editMenuLadder> ().configureButtonsAndShow (objectToEdit);
	}

	public void hideEditMenu(){
		deactivateBackground ();
		if (editorBox.GetComponent<editorBox> ().editorActive == true) {
			editorBox.GetComponent<editorBox> ().editorActive = false;
			editorBox.GetComponent<editorBox> ().hideEditor (true);
		} else {
			editMenu.GetComponent<editMenuLadder> ().hideButtons ();
		}
	}

	private void addLineAction(){
		//lineGroupPrefab
		debugMode = false;
		float offsetHeight = 0;
		int maxBrachLevel = 0;
		if (lineGroupObjects [lineGroupObjects.Count - 1].GetComponent<lineGroupObject> ().branchLines.Count > 0) {
			for(int i = 0; i < lineGroupObjects [lineGroupObjects.Count - 1].GetComponent<lineGroupObject> ().branchLines.Count; i++){
				if(lineGroupObjects [lineGroupObjects.Count - 1].GetComponent<lineGroupObject> ().branchLines[i].GetComponent<lineObject>().branchLevel > maxBrachLevel){
					maxBrachLevel = lineGroupObjects [lineGroupObjects.Count - 1].GetComponent<lineGroupObject> ().branchLines [i].GetComponent<lineObject> ().branchLevel;
				}
			}
			offsetHeight = 70f * (float)maxBrachLevel;
		}

		GameObject newGroupLine = Instantiate(lineGroupPrefab, _workbenchArea.transform);
		float newPosition = lineGroupObjects [lineGroupObjects.Count - 1].GetComponent<RectTransform> ().anchoredPosition.y - 140f - offsetHeight;
		newGroupLine.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (11f, newPosition);
		newGroupLine.GetComponent<lineGroupObject> ().initialPosition = newPosition;
		lineGroupObjects.Add (newGroupLine);

		updateWorkbenchArea ();
	}

	private void removeLineAction(){
		debugMode = false;
		if(lineGroupObjects.Count >= 1){
			Destroy (lineGroupObjects [lineGroupObjects.Count - 1]);
			lineGroupObjects.RemoveAt (lineGroupObjects.Count - 1);

			if(lineGroupObjects.Count == 0){
				createInitialGroupLine ();
			}

			updateWorkbenchArea ();
		}
	}

	public void createInitialGroupLine(){
		GameObject initialGroupLine = Instantiate(lineGroupPrefab, _workbenchArea.transform);
		initialGroupLine.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (11, -100);
		initialGroupLine.GetComponent<lineGroupObject> ().initialPosition = initialGroupLine.GetComponent<RectTransform> ().anchoredPosition.y;
		lineGroupObjects.Add (initialGroupLine);
	}

	public void updateWorkbenchArea(){
		Rect _newWorkbenchRect = _workbenchArea.GetComponent<RectTransform> ().rect;
		_newWorkbenchRect.height = (Mathf.Abs (lineGroupObjects [lineGroupObjects.Count - 1].GetComponent<RectTransform> ().anchoredPosition.y) + lineGroupObjects [lineGroupObjects.Count - 1].GetComponent<RectTransform> ().rect.height);
		_newWorkbenchRect.height = _newWorkbenchRect.height	- Mathf.Abs (lineGroupObjects [0].GetComponent<RectTransform> ().anchoredPosition.y);
		_workbenchArea.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_newWorkbenchRect.width, _newWorkbenchRect.height);
	}

	public void prevPasteLine(GameObject elementToReplace){
		StartCoroutine (pasteCopiedLine (elementToReplace));
	}

	public IEnumerator pasteCopiedLine(GameObject elementToReplace){
		if (copiedLineXML.OuterXml != "") {
			int indexToChange = 0;
			int siblingPrev = 0;
			float initialPositionPrev = 0;
			for (int i = 0; i < lineGroupObjects.Count; i++) {
				if (lineGroupObjects [i].GetInstanceID () == elementToReplace.GetInstanceID ()) {
					indexToChange = i;
					break;
				}
			}

			int maxBranches = lineGroupObjects [indexToChange].GetComponent<lineGroupObject> ().getMaxBranchOffset ();
			if (maxBranches > 0 && indexToChange < (lineGroupObjects.Count - 1)) {
				for (int i = indexToChange + 1; i < lineGroupObjects.Count; i++) {
					lineGroupObjects [i].transform.localPosition = new Vector3 (lineGroupObjects [i].transform.localPosition.x, lineGroupObjects [i].transform.localPosition.y + (140f * maxBranches), lineGroupObjects [i].transform.localPosition.z);
					lineGroupObjects [i].GetComponent<lineGroupObject> ().initialPosition += (140f * maxBranches);
				}
			}

			siblingPrev = lineGroupObjects [indexToChange].transform.GetSiblingIndex ();
			initialPositionPrev = lineGroupObjects [indexToChange].GetComponent<lineGroupObject> ().initialPosition;
			Destroy (lineGroupObjects [indexToChange]);

			GameObject initialGroupLine = Instantiate (lineGroupPrefab, _workbenchArea.transform);
			initialGroupLine.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (11, initialPositionPrev);
			initialGroupLine.GetComponent<lineGroupObject> ().initialPosition = initialPositionPrev;
			initialGroupLine.transform.SetSiblingIndex (siblingPrev);
			lineGroupObjects [indexToChange] = initialGroupLine;

			yield return new WaitForSeconds (0.1f);

			XmlNodeList _inputList;
			_inputList = copiedLineXML.ChildNodes[0].SelectNodes("entrada");

			for (int j = 0; j < _inputList.Count; j++){
				yield return StartCoroutine (openProcessInput (_inputList[j], indexToChange,true));
			}

			XmlNodeList _outputList;
			_outputList = copiedLineXML.ChildNodes [0].SelectNodes ("salida");

			if (_outputList.Count == 1) {
				yield return StartCoroutine (openProcessOutput (_outputList [0], indexToChange));
			}
		}

		yield return new WaitForSeconds (0.1f);
	}

	public void activateBackground(){
		_background = Instantiate(_backgroundPref, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_background.name = "SituationBg";
		_background.transform.SetParent (GameObject.Find("Canvas/Ladder").transform, false);
		_background.transform.localPosition = new Vector3(0,0,0);
		_background.transform.SetAsLastSibling ();
	}

	public void deactivateBackground(){
		if(bgEditorActive == true){
			bgEditorActive = false;
			Image _bg = _background.GetComponent<Image>();
			_bg.CrossFadeAlpha (0f, 1f, false);
			Destroy(_background, 1.2f);	
		}
	}

	// Update is called once per frame
	void Update () {

		if (simulationRunning == true) {
			if (ladderEngine != null) {
				bool[] _digitalInputsArray;
				bool[] _outputVaues;
				switch (Manager.Instance.globalGraphicInterface) {
				case baseSystem.graphicInterfaces.electrical_bench:
					_digitalInputsArray = new bool[_situation.GetComponent<ElectricalTestBenchMainClass> ().inputElements.Length];
					for (int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass> ().inputElements.Length; i++) {
						if (_situation.GetComponent<ElectricalTestBenchMainClass> ().inputElements [i]) {
							if (_situation.GetComponent<ElectricalTestBenchMainClass> ().inputElements [i].GetComponent<dragElement> ().actualValue == 1) {
								_digitalInputsArray [i] = true;
							} else {
								_digitalInputsArray [i] = false;
							}
						} else {
							_digitalInputsArray [i] = false;
						}
					}

					ladderEngine.EntradasBool = _digitalInputsArray;

					if (analogInputs > 0) {
						byte[] _analogInputsArray = new byte[analogInputs];
						ladderEngine.EntradasByte = _analogInputsArray;
					}

					try{
						ladderEngine.runProgram ();

						_outputVaues = ladderEngine.SalidasBool;
						_situation.GetComponent<ElectricalTestBenchMainClass> ().outputsValues = _outputVaues;

						for (int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements.Length; i++) {
							if (_situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements [i]) {
								if (_outputVaues [i] == true) {
									_situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements [i].GetComponent<dragElement> ().setValue (1);
								} else {
									_situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements [i].GetComponent<dragElement> ().setValue (0);
								}
							}
						}
					} catch(Exception error){
						Debug.Log (error);
						simulationRunning = false;
						showInfoText (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='error_comp']").InnerText);
					}

					break;
				case baseSystem.graphicInterfaces.pneumatic_bench:
					_digitalInputsArray = new bool[_situation.GetComponent<PnematicTestBenchClass> ().inputElements.Length];
					for (int i = 0; i < _situation.GetComponent<PnematicTestBenchClass> ().inputElements.Length; i++) {
						if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i]) {
							if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().actualValue == 1) {
								_digitalInputsArray [i] = true;
							} else {
								_digitalInputsArray [i] = false;
							}
						} else {
							_digitalInputsArray [i] = false;
						}
					}

					ladderEngine.EntradasBool = _digitalInputsArray;

					if (analogInputs > 0) {
						byte[] _analogInputsArray = new byte[analogInputs];
						ladderEngine.EntradasByte = _analogInputsArray;
					}
					try{
						ladderEngine.runProgram ();

						_outputVaues = ladderEngine.SalidasBool;
						_situation.GetComponent<PnematicTestBenchClass> ().outputsValues = _outputVaues;

						for (int i = 0; i < _situation.GetComponent<PnematicTestBenchClass> ().outputsElements.Length; i++) {
							if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i]) {

								if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> () != null) {
									if (_outputVaues [i] == true) {
										_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ().setValue (1);
									} else {
										_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ().setValue (0);
									}	
								} else if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> () != null) {
									if (_outputVaues [i] == true) {
										_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ().setValue (1);
									} else {
										_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ().setValue (0);
									}
								}
							}
						}
					} catch(Exception error){
						Debug.Log (error);
						simulationRunning = false;
						showInfoText (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='error_comp']").InnerText);
					}
					break;
				}
			}
		} else {
			switch (Manager.Instance.globalController) {
			case baseSystem.Controllers.Grafcet:
				switch (Manager.Instance.globalGraphicInterface) {
				case baseSystem.graphicInterfaces.electrical_bench:
					bool[] _outputValues = new bool[_situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements.Length];
					_situation.GetComponent<ElectricalTestBenchMainClass> ().outputsValues = _outputValues;

					for (int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements.Length; i++) {
						if (_situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements [i]) {
							if (_outputValues [i] == true) {
								_situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements [i].GetComponent<dragElement> ().setValue (1);
							} else {
								_situation.GetComponent<ElectricalTestBenchMainClass> ().outputsElements [i].GetComponent<dragElement> ().setValue (0);
							}
						}
					}
					break;
				case baseSystem.graphicInterfaces.pneumatic_bench:
					bool[] _outputValues1 = new bool[_situation.GetComponent<PnematicTestBenchClass> ().outputsElements.Length];
					_situation.GetComponent<PnematicTestBenchClass> ().outputsValues = _outputValues1;

					for (int i = 0; i < _situation.GetComponent<PnematicTestBenchClass> ().outputsElements.Length; i++) {
						if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i]) {

							if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> () != null) {
								if (_outputValues1 [i] == true) {
									_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ().setValue (1);
								} else {
									_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<dragElementPneumatic> ().setValue (0);
								}	
							} else if (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> () != null) {
								if (_outputValues1 [i] == true) {
									_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ().setValue (1);
								} else {
									_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ().setValue (0);
								}
							}
						}
					}
					break;
				}
				break;
			}
		}
	}
}
