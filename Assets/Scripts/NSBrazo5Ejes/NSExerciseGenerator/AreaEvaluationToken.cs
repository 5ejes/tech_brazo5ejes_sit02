﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
  public class AreaEvaluationToken : AbstractEvaluationToken
  {

    /// <summary>
    /// Transform con la información sobre la ubicación y el tamaño de la comprobación del area
    /// </summary>
    /// <value></value>
    public Transform PositionInfo { get; set; }

    /// <summary>
    /// Elementos detectados dentro del area definida
    /// </summary>
    /// <value></value>
    public int CheckedElements { get; set; }

    /// <summary>
    /// Número objetivo de elementos a detectar
    /// </summary>
    /// <value></value>
    public int CheckedElementGoal { get; set; }

    public override void Evaluate()
    {
      // Creo un area para hacer la comprobación

      // Obtengo los cuadrados resultantes del chequeo
      var results = Physics.OverlapBox(PositionInfo.position, PositionInfo.localScale / 2f, PositionInfo.rotation, CheckLayerMask, QueryTriggerInteraction.Ignore);

      Debug.DrawRay(PositionInfo.position, Vector3.up * 0.2f, Color.black, 1000f);

      // Si hay al menos un resultado
      if (results.Length > 0)
      {

        // Si existe un CheckEnumID
        if (TargetEnumID != null)
        {
          // Inicio la comprobación de elementos

          // Creo una lista de resultados temporales
          var tempResults = new List<Transform>();

          // Recorro cada resultado
          foreach (var result in results)
          {
            // Obtengo el identificador de enumeración
            var enumIdentificator = result.transform.GetComponent<EnumIdentificator>();

            // Si el identificador no es nulo
            if (enumIdentificator != null)
            {
              // Si la enumeración del identificador y la enumeración objetivo coinciden
              if (enumIdentificator.EnumID.Equals(TargetEnumID))
                // Agrego la entrada a los resultados
                tempResults.Add(result.transform);
            }
          }

          // Defino el resultado de los elementos encontrados
          CheckedElements = results.Length;
        }
        else
        {
          // Si no existe

          // Defino el número de elementos comprobados a los elementos actuales
          CheckedElements = results.Length;
        }
      }

      // Si se cumple el número objetivo de elementos a detectar
      if (CheckedElements > CheckedElementGoal)
      {
        // Invoco la función de exito
        InvokeSuccess();
      }
      else
      {
        // Invoco la función de fallo
        InvokeFailure();
      }
    }
  }
}
