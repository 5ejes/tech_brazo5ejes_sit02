﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace NSBrazo5Ejes.NSDraggingBehaviour
{
	public class DragMoveComponent : MonoBehaviour
	{

		[System.Serializable] private class DraggableComponentEvent : UnityEvent<DraggableComponent> { }

		[Header("References")]
		[SerializeField] private LocalPositionParser _parser = null;
		[SerializeField] private Transform _planeHeightReference = null;
		[Header("Limits")]
		[SerializeField] private Transform _minLimit;
		[SerializeField] private Transform _maxLimit;

		[Header("Pivot")]
		[SerializeField] private Transform _pivot;
		[Header("Ignore Elements")]
		[SerializeField] private List<GameObject> _ignoreGameObjects = new List<GameObject>();

		[Header("Settings")]
		[SerializeField] private LayerMask _checkDraggableLayer = Physics.AllLayers;
		[SerializeField] private float _snap = 0.1f;
		[SerializeField] private float _cutSpace = 0.1f;

		[Header("Events")]
		[SerializeField] private DraggableComponentEvent _onReleaseDraggable;

		private DraggableComponent _currentDraggable;
		public DraggableComponent CurrentDraggable => _currentDraggable;

		/// <summary>
		/// Lista de elementos ignorados por la comprobación de colisión
		/// </summary>
		public List<GameObject> IgnoreGameObjects { get => _ignoreGameObjects; }

		private Vector3 _planeMoveDir;
		private Vector3 _lastPlanePosition;

		private Vector3 _firstTimeDraggablePosition = Vector3.zero;
		private bool _saveComingFromPosition = true;
		private bool _restorePosition = false;

		public Vector3 LastValidMovePosition { get; set; } = Vector3.zero;
		public bool IsLastMoveValid { get; set; } = false;

		private List<DraggableComponent> _draggedComponents = new List<DraggableComponent>();

		private void Update()
		{
			if (Input.GetMouseButtonUp(0))
			{
				MouseUpHandler();
			}
		}

		public bool IsOnDraggedComponentsList(DraggableComponent draggable)
		{
			return _draggedComponents.Contains(draggable);
		}

		public void ClearDraggedComponentList()
		{
			_draggedComponents.Clear();
		}

		public void RegisterOnDraggedComponents(DraggableComponent draggable)
		{
			// Añado el draggable actual si no está en la lista
			if (!_draggedComponents.Contains(CurrentDraggable)) _draggedComponents.Add(CurrentDraggable);
		}
		public void MouseUpHandler()
		{

			// Si la orden de restauración es permitida
			if (_restorePosition)
			{
				// Restaura la posición almacenada al entrar en vigor
				CurrentDraggable.Target.position = _firstTimeDraggablePosition;

				if (CurrentDraggable.Rigidbody != null)
				{
					// Restablezco el estado de los Draggables en la bandeja
					CurrentDraggable.Rigidbody.isKinematic = true;
					CurrentDraggable.Rigidbody.useGravity = true;
				}
			}

			// Restauro los valores de control
			_saveComingFromPosition = true;
			_restorePosition = false;

			// Invoco el evento cuando suelto el draggable actual
			if (CurrentDraggable) _onReleaseDraggable.WrapInvoke(CurrentDraggable);

			// Elimino la referencia al draggable
			_currentDraggable = null;
		}

		public bool TryMoveDraggable(DraggableComponent target, Vector3 worldPosition, Vector3 positionOffset = default, Quaternion rotation = default, TransformBound[] bounds = null, bool evaluateInside = false)
		{

			// Registro el draggable actual
			_currentDraggable = target;

			// Almaceno el valor de comprobante si el objetivo está en el historial
			bool targetOnHistoryDragList = IsOnDraggedComponentsList(target);

			// NOTA: Con esta operación un objeto con un movimiento erróneo puede regresar
			// A su posición de origen

			// Si se permite guardar la posición y el elemento no está en el historial
			if (_saveComingFromPosition && !targetOnHistoryDragList)
			{
				// Almaceno los datos de restauración para los elementos iniciales
				_firstTimeDraggablePosition = _currentDraggable.Target.position;
				_restorePosition = true;
				_saveComingFromPosition = false;
			}

			// Almaceno el valor del target del draggable component
			Transform trueTarget = target.Target;

			// Almaceno el valor sin modificar de worldPosition
			var rawWorldPosition = worldPosition;

			// Obtengo la rotación original del objeto
			var originalRotation = trueTarget.rotation;
			var originalPosition = trueTarget.position;

			// Aplico la rotación especificada
			trueTarget.SetPositionAndRotation(worldPosition, rotation);

			// Obtengo la posición local según el pivote
			worldPosition = _pivot.InverseTransformPoint(worldPosition);

			// Aplico el snap a la posición en el plano
			worldPosition = SnapVector(worldPosition, _snap);

			// Convierto la anterior posición local en global
			worldPosition = _pivot.TransformPoint(worldPosition);

			// Obtengo los vertices del Draggable
			var offset = target.GetOffsetToMin();

			// La escala del transform actual
			var scale = trueTarget.localScale;

			// Defino la variable de coordenadas locales
			Vector3[] localBounds = null;

			// Obtengo el offset desde los límites hasta el centro del draggable
			var limitHeight = trueTarget.position.y - target.GetLimitHeight();
			var limitHeightVector = Vector3.up * limitHeight;

			// Obtengo las coordenadas locales
			localBounds = target.GetCompleteLocalBounds(
				Quaternion.AngleAxis(target.Target.eulerAngles.y, Vector3.up)
		);

			var baseIgnoreList = IgnoreGameObjects.ToArray();

			// Incluyo el draggable actual como elemento a ignorar
			IgnoreGameObjects.Add(trueTarget.gameObject);
			IgnoreGameObjects.AddRange(target.IgnoreGameObjectDraggableList);

			// Defino la altura de la posición al límite
			worldPosition.y += limitHeight;

			// Si el draggable actual, está colisionando
			var isColliding = IsColliding(worldPosition,
				target.GetOffsetToMin().ToAbsolute() * 2f * (1f - _cutSpace),
				trueTarget.rotation,
				IgnoreGameObjects.ToArray());

			// Elimino la referencia al draggable de la lista de ignorados
			IgnoreGameObjects.Clear();
			IgnoreGameObjects.AddRange(baseIgnoreList);

			// Si las coordenadas locales son nulas, lanzo una excepción
			if (localBounds == null) throw new System.NullReferenceException($"Local bounds evaluated to null : {gameObject}");

			// Defino las posiciones límite del Draggable actual sobre la posición del plano
			var draggablePlaneBounds = new Vector3[localBounds.Length];

			// Defino un margen de error
			float margin = 0.025f;

			// Defino un arreglo con los resultados
			bool[] results = new bool[localBounds.Length];

			// Si el objeto actual tiene un override limits component
			var overrideComponentLimits = target.GetComponent<OverrideDraggableMoveComponentLimits>();

			// Genero las variables de límites a usar
			Transform usedMinLimit = overrideComponentLimits ? overrideComponentLimits.MinLimit : _minLimit;
			Transform usedMaxLimit = overrideComponentLimits ? overrideComponentLimits.MaxLimit : _maxLimit;

			// Por cada valor local
			for (int i = 0; i < localBounds.Length; i++)
			{
				// Aplico la escala a cada punto
				localBounds[i].Scale(scale);

				// Escribo cada valor
				draggablePlaneBounds[i] = localBounds[i] + worldPosition;

#if UNITY_EDITOR
				// Rayos para identificar la posición del plano
				Debug.DrawRay(draggablePlaneBounds[i], Vector3.one.normalized * 0.05f, Color.green, 1);
				Debug.DrawRay(target.GetCenterVector(), target.GetOffsetToMin(), Color.red);
#endif

				// Evaluo cada resultado con los límites dados
				results[i] = _parser.IsInside(draggablePlaneBounds[i], margin, usedMinLimit, usedMaxLimit);
			}

			// Si las posiciones y los puntos se encuentran dentro de los límites del parser
			bool isInsideLimits = results.ANDEvaluation();

			// Creo la dirección de los límites del draggable actual
			Vector3 draggableLimitsDir = target.GetMinToMaxDirection();

			// Defino una lista temporal con los límites
			List<Vector3> tempList = new List<Vector3>();
			tempList.AddRange(draggablePlaneBounds);

			// Genero puntos aleatorios dentro del componente
			tempList.AddRange(PointSampler.GenerateInsideXZ(25, worldPosition, target.GetOffsetToMin().ToAbsolute(), 0.1f));

			// Agrego la posición del centro
			tempList.Add(worldPosition);

			// Genero un arreglo con las posiciones a evaluar
			var positionsToEvaluate = tempList.ToArray();

			tempList = null;

			// Evaluo el los límites
			bool boundsSucceded = true;

			// Si existen los límites
			if (bounds != null)
			{
				// Si hay más de un límite
				if (bounds.Length > 0)
				{

					// Creo una lista para evaluar el resultado del las evaluaciones booleanas
					var boundEvaluations = new bool[bounds.Length];

					// Creo una variable para almacenar la evaluación actual
					var currentEvaluationResult = boundsSucceded;

					// Por cada banda
					for (int i = 0; i < bounds.Length; i++)
					{
						// Obtengo el límite actual
						var currentBound = bounds[i];

						// Si evaluo dentro de los límites

						// Evaluo el límitador actual
						// TRUE si está dentro de un límite
						// FALSE si está por fuera de un límite

						// Si evalua por dentro, TRUE, por todos los puntos dentro de
						// Si evalua por fuera, FALSE, por al menos un punto dentro de, que resultaría en TRUE (Para evaluar por fuera es necesario FALSE)
						currentEvaluationResult = ArePointsInside(positionsToEvaluate, currentBound, evaluateInside);

						boundEvaluations[i] = currentEvaluationResult;
					}

					// Evaluo los resultados

					// Si se evalua por dentro, Al menos TODOS los puntos deben de estar dentro de un bound es decir, TRUE
					// Si se evalua por fuera, TODOS los puntos deben estar por fuera de bound, al menos un punto dentro de bound es TRUE, lo cual evaluaria en FALSE con la negación OR
					boundsSucceded = evaluateInside ? boundEvaluations.OREvaluation() : !boundEvaluations.OREvaluation();
				}
			}

			// Si no existe definición para los límites, se da como correcta la evalucación de limitadores

			// Si la posición indicada está dentro de los límites, no está colisionando y la evaluación de los limitadores es correcta
			if (isInsideLimits && !isColliding && boundsSucceded)
			{

				// Si el movimiento es permitido, lo registro en la lista
				RegisterOnDraggedComponents(target);

				// Cancelo la orden de restauración
				_restorePosition = false;

				// Defino la última posición válida
				LastValidMovePosition = worldPosition + positionOffset;

				// Aplico la posición definida
				trueTarget.position = LastValidMovePosition;
				IsLastMoveValid = true;

				// Retorno verdadero
				return true;
			}
			else
			{

				// Si no es la primera vez que el draggable es registrado
				// TODO: Convertir a posible delegado externo
				// var targetOutPosition = targetOnHistoryDragList ? originalPosition : rawWorldPosition + limitHeightVector;

				// Al asignar la posición del target a una posición, se arregla el bug pero elimina el transportar el elemento por el entorno
				// TODO: Ahora sí sería buena idea los callback
				// var targetOutPosition = originalPosition;

				// Restauro la rotación del objeto
				trueTarget.SetPositionAndRotation(originalPosition, originalRotation);
				IsLastMoveValid = false;

				// Retorno false
				return false;
			}
		}

		private static bool ArePointsInside(Vector3[] points, TransformBound currentBound, bool AllPointsInside = true)
		{
			// Genero una variable para los bordes
			var output = true;

			// Por cada límite
			for (int i = 0; i < points.Length; i++)
			{
				// Borde actual
				var currentEdgeVert = points[i];

				// Evaluo el límitador actual;
				output = currentBound.IsInsideXZ(currentEdgeVert);

				// Si solo es necesario que haya un vertice dentro, 
				if (output && !AllPointsInside)
					// Retorno cuando haya un punto dentro
					return output;
				else if (!output && AllPointsInside) // Si hay un vértice que evalua a false
				{
					// Retorno, ya que es necesario que todos los vertices evaluen true
					return false;
				}
			}

			// Retorno la salida
			return output;
		}

		/// <summary>
		/// Comprueba una colisión cúbica en los parámetros especificados
		/// </summary>
		/// <param name="worldSpace">Posición en coordenadas globales</param>
		/// <param name="scale">Escala del cubo comprobante</param>
		/// <param name="rotation">Rotación del cubo comprobante</param>
		/// <param name="IgnoreGOs">Lista de objetos marcados como ignorar la comprobación (para evitar que las colisiones invalidadas cuenten como resultado)</param>
		/// <returns>'true' si el cubo comprobante colisiona con al menos un objeto válido, de lo contrario, 'false'</returns>
		public bool IsColliding(Vector3 worldSpace, Vector3 scale, Quaternion rotation, GameObject[] ignoreGOs = null)
		{

			// Defino la mitad de la escala
			Vector3 halfExtents = scale / 2f;

			// Casteo un overlap box para comprobar las colisiones
			var results = Physics.OverlapBox(worldSpace, halfExtents, rotation, _checkDraggableLayer, QueryTriggerInteraction.Collide);

			Debug.DrawRay(worldSpace, Vector3.one.normalized * 0.2f, Color.red, 1f);

			var rotationEuler = rotation.eulerAngles;
			rotationEuler.x = 0f;
			rotationEuler.z = 0f;

			rotation = Quaternion.Euler(rotationEuler);

			var bounds = new Vector3[]
			{
				rotation * new Vector3(+halfExtents.x, 0, +halfExtents.y) + worldSpace,
				rotation * new Vector3(-halfExtents.x, 0, +halfExtents.y) + worldSpace,
				rotation * new Vector3(-halfExtents.x, 0, -halfExtents.y) + worldSpace,
				rotation * new Vector3(+halfExtents.x, 0, -halfExtents.y) + worldSpace
			};


			int length = bounds.Length;
			for (int i = 0; i < length; i++)
			{

				// Obtengo el punto actual
				var currentPoint = bounds[i];
				Vector3 nextPosition = new Vector3();

				// Dibujo cada punto
				if (i == length - 1)
				{
					nextPosition = bounds[0];
				}
				else
				{
					nextPosition = bounds[i + 1];
				}

				// Dibujo el punto actual
				Debug.DrawRay(currentPoint, Vector3.up * 0.05f, Color.blue, 1f);
				Debug.DrawRay(nextPosition, Vector3.up * 0.05f, Color.blue, 1f);

				Debug.DrawLine(currentPoint, nextPosition, Color.blue, 1f);

			}

			// Creo una variable para colisiones inválidas
			int invalidCount = 0;

			// Si hay objeto de comprobación de colisiones
			if (ignoreGOs != null)
			{
				// Por cada resultado
				foreach (var collision in results)
				{
					// Por cada elemento a ignorar
					foreach (var currentIgnore in ignoreGOs)
					{
						// Verifico si la collision, es contra el objeto desde el que se evalua la colisión
						if (collision.gameObject.Equals(currentIgnore))
						{
							// Incremento la cuenta de elementos inválidos
							invalidCount++;
						}
					}
				}
			}

			// Retorno si el resultado es mayor a cero, omitiendo valores de colision contra si mismo
			return results.Length - invalidCount > 0;
		}

		private void OnDrawGizmosSelected()
		{
			if (_planeHeightReference)
			{
				var position = transform.position;
				position.y = _planeHeightReference.position.y;

				var planeColor = new Color(0, 0.2f, 0.8f, 1f);

				Gizmos.color = planeColor;

				var horizontalLineFrom = position + -Vector3.right * 50;
				var horizontalLineTo = position + Vector3.right * 50;

				var verticalLineFrom = position + -Vector3.forward * 50;
				var verticalLineTo = position + Vector3.forward * 50;

				Gizmos.DrawLine(horizontalLineFrom, horizontalLineTo);
				Gizmos.DrawLine(verticalLineFrom, verticalLineTo);

				planeColor.a = 0.2f;
				Gizmos.color = planeColor;

				Gizmos.DrawCube(position, (Vector3.forward + Vector3.right) * 100f);

			}
		}

		private void OnDrawGizmos()
		{

			// if (IsColliding(CurrentDraggable.GetCenterVector(), CurrentDraggable.GetOffsetToMin().ToAbsolute() * 2f, CurrentDraggable.Target.rotation))

			if (CurrentDraggable && Application.isPlaying)
			{
				var rotationMatrix = Matrix4x4.TRS(_lastPlanePosition, CurrentDraggable.Target.rotation, CurrentDraggable.GetOffsetToMin().ToAbsolute() * 2f * (1f - _cutSpace));
				Gizmos.matrix = rotationMatrix;
				Gizmos.color = Color.green;

				Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
			}
		}

		private Vector3 SnapVector(Vector3 target, float snap)
		{
			// Genero una variable de salida
			Vector3 output;

			// Modifico el vector con el snap
			output.x = Mathf.Round(target.x / snap) * snap;
			output.y = Mathf.Round(target.y / snap) * snap;
			output.z = Mathf.Round(target.z / snap) * snap;

			// Retorno el resultado
			return output;
		}
	}
}