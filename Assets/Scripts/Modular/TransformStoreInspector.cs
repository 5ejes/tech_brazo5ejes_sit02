﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace Modular
{
	public class TransformStoreInspector : MonoBehaviour
	{
		/// <summary>
		/// Guarda la referencia a la posición, rotación y escala de un componente para habilitar su restauración
		/// </summary>
		public class TransformStoreData
		{
			/// <summary>
			/// Valor Transform usado para referenciar los datos
			/// </summary>
			/// <value></value>
			public Transform ReferencedTransform { get; set; } = null;

			/// <summary>
			/// Referencia al valor almacenado de la posición
			/// </summary>
			/// <value></value>
			public Vector3 StoredPosition { get; private set; }
			/// <summary>
			/// Referencia al valor almacenado de la rotación
			/// </summary>
			/// <value></value>
			public Quaternion StoredRotation { get; private set; }
			/// <summary>
			/// Referencia al valor almacenado de la escala
			/// </summary>
			/// <value></value>
			public Vector3 StoredLocalScale { get; private set; }

			public TransformStoreData(Transform reference)
			{
				// Si la referencia existe
				if (reference)
				{
					// Almaceno la referencia
					ReferencedTransform = reference;

					// Almaceno los valores
					StoreCurrentValues();
				}
			}

			public TransformStoreData()
			{
			}

			/// <summary>
			/// Guarda los valores actuales de la referencia
			/// </summary>
			public void StoreCurrentValues()
			{
				// Si existe una referencia al transform
				if (ReferencedTransform)
				{
					// Guardo los datos
					StoreTransformValues(ReferencedTransform);
				}
			}

			/// <summary>
			/// Almacena los valores del Transform
			/// </summary>
			/// <param name="target">Transform a almacenar</param>
			public void StoreTransformValues(Transform target)
			{
				// Si la referencia no existe, retorno
				if (!target) return;

				// Almacena objetos
				StoredPosition = target.position;
				StoredRotation = target.rotation;
				StoredLocalScale = target.localScale;
			}

			/// <summary>
			/// Restaura los valores almacenados al Transform referenciado
			/// </summary>
			public void Restore()
			{
				// Si existe una referencia al transform
				if (ReferencedTransform)
				{
					// Restauro los valores
					RestoreTransform(ReferencedTransform);
				}
			}

			/// <summary>
			/// Restaura los valores almacenados al transform especificado
			/// </summary>
			/// <param name="target">Transform a restaurar</param>
			public void RestoreTransform(Transform target)
			{
				// Si la referencia no existe, retorno
				if (!target) return;

				// Restauro los valores al transform seleccionado
				target.position = StoredPosition;
				target.rotation = StoredRotation;
				target.localScale = StoredLocalScale;
			}

			public static implicit operator TransformStoreData(Transform target)
			{
				return new TransformStoreData(target);
			}

			public static implicit operator Transform(TransformStoreData storeData)
			{
				if (storeData != null)
					return storeData.ReferencedTransform;
				else
					return null;
			}
		}

		[Header("List of values")]
		[SerializeField] private Transform[] _transformsToStore = null;

		[Header("Events")]
		[SerializeField] private UnityEvent _onUpdateValues = new UnityEvent();
		[SerializeField] private UnityEvent _onRestoreValues = new UnityEvent();
    [Header("Single Transform Events")]
    [SerializeField]private TransformEvent _onRestoreValue = new TransformEvent();

		private List<TransformStoreData> _storeDataList = new List<TransformStoreData>();

		private void Start()
		{
			// Si existen elementos almacenados
			if (_transformsToStore != null)
			{
				// Si hay al menos un elemento
				int length = _transformsToStore.Length;
				if (length > 0)
				{
					// Por cada elemento
					foreach (var currentTransform in _transformsToStore)
					{
						// Almaceno un nuevo TransformStoreData
						_storeDataList.Add(currentTransform);
					}
				}
			}
		}

		/// <summary>
		/// Restaura todos los elementos almacenados
		/// </summary>
		public void RestoreAll()
		{
			// Restauro todos los transform
			_storeDataList.ForEach((TransformStoreData item) =>
			{
				// Restauro el elemento
				item.Restore();
			});

			// Ejecuto el evento
			_onRestoreValues.WrapInvoke();
		}

		/// <summary>
		/// Actualiza todos los elementos almacenados
		/// </summary>
		public void UpdateAll()
		{
			// Actualizo todos los transform
			_storeDataList.ForEach((TransformStoreData item) =>
			{
				// Actualizo el elemento
				item.StoreCurrentValues();
			});

			// Ejecuto el evento
			_onUpdateValues.WrapInvoke();
		}

		/// <summary>
		/// Restaura el transform dado, si está registrado en la lista
		/// </summary>
		/// <param name="target">Transform objetivo</param>
		public void RestoreTransform(Transform target)
		{
			if(_storeDataList == null) return;
			if(_storeDataList.Count == 0) return;

			// Busco el tranform
			var transformToRestore = _storeDataList.Find(t => t.ReferencedTransform.Equals(target));
			
			// Si no existe retorno
			if(transformToRestore == null) return;
			
			// Restauro el transform
			transformToRestore.Restore();
      _onRestoreValue?.Invoke(transformToRestore);
		}
	}
}