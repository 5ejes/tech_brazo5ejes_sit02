#pragma warning disable 0649
using System;
using NSSeguridad;
using System.Collections.Generic;
using System.Xml;
using NSCreacionPDF;
using NSScore;
using NSScriptableValues;
using UnityEngine;

namespace NSCalificacionSituacion
{
	[CreateAssetMenu(menuName = "NSCalificacionSituacion/SOCalificacionSituacion", fileName = "CalificacionSituacion")]
	public class SOCalificacionSituacion : ScriptableObject
	{
		#region members

		/// <summary>
		/// Rango de calificacion con posibilidad de definicion, ejemplo, hasta 5 cuando el array es definido con un solo elemento y este es numerico, ó de (5 - 10) 
		/// Cuando el array es definido con 2 elementos numericos, siendo el numero en el indice 0 la la nota mas baja y el numero en el indice 1 la nota mas alta 
		/// ó A - B - C - D - E si los elementos del array son definidos como letras, minimo 2 letras
		/// </summary>
		[Header("Configuracion rangos calificacion"), Tooltip("Rango de calificacion con posibilidad de definicion, ejemplo, hasta 5 cuando el array es definido con un solo elemento y este es numerico, ó de (5 - 10) Cuando el array es definido con 2 elementos numericos, siendo el numero en el indice 0 la la nota mas baja y el numero en el indice 1 la nota mas alta ó A - B - C - D - E si los elementos del array son definidos como letras, minimo 2 letras")]
		[SerializeField]
		private string[] rangoCalificacionLetrasNumeros;

		/// <summary>
		/// Rango de porcentaje para los intervalos, para expresar cada indice a que porcentaje equivale
		/// </summary>
		[SerializeField] private float[] rangoPorcentaje5Letras;

		[Header("Definicion pesos calificacion")]
		[SerializeField] private PesosCalificacionTareasSituacion[] arrayPesosCalificacionTareasSituacion;

		[Header("Calificacion intentos")]
		[SerializeField] private SOSessionData soSessionData;

		[SerializeField] private float pesoCalificacionIntentos;

		[Header("Archivo xml notas")]
		[SerializeField] private SOSecurityConfiguration soSecurityConfiguration;

		[Header("CalificacionFinal")]
		[SerializeField] private ValueFloat valCalificacionFinalSituacion;

		[SerializeField] private ValueString valCalificacionFinalRangoNumerosLetras;
		[SerializeField] private ValueFloat valTryParseFloatCalificacionFinal;
		#endregion

		#region public methods

		/// <summary>
		/// Calcula la calificacion final de toda la situacion basado en cada una de las tareas especificas de la situacion y sus pesos
		/// </summary>
		public void CalcularCalificacionFinal()
		{
			var tmpCalificacionFinal = 0f;

			foreach (var tmpCalificacionTarea in arrayPesosCalificacionTareasSituacion)
				tmpCalificacionFinal += tmpCalificacionTarea.valCalificacionTareaEspecifica * tmpCalificacionTarea.pesoCalificacionTareaEspecifica;

			tmpCalificacionFinal += Mathf.InverseLerp(11, 1, soSessionData.quantityAttempts) * pesoCalificacionIntentos;

			valCalificacionFinalSituacion.Value = tmpCalificacionFinal;
			CalcularRangoCalificacionEnNumerosLetras();
		}
		#endregion

		#region private methods 

		/// <summary>
		/// Traduce la calificacion total de la situacion actual a la calificacion que le corresponde en el rango definido
		/// </summary>
		private void CalcularRangoCalificacionEnNumerosLetras()
		{
			valCalificacionFinalRangoNumerosLetras.Value = FindObjectOfType<ScoreController>().GetScoreFromFactor01(valCalificacionFinalSituacion.Value);
		}

		private void SetFloatValueIfNotNull(float value, ValueFloat valueFloat)
		{
			if (!valueFloat.Equals(null)) valueFloat.Value = value;
		}

		private void CargarNotasDesdeXML()
		{
#if !UNITY_WEBGL
			var tmpXmlDocument = new XmlDocument();

			try
			{
				tmpXmlDocument.Load(soSecurityConfiguration.fileName);
				var tmpListRangos = new List<string>();

				XmlNodeList tmpXmlNodeList = tmpXmlDocument.DocumentElement.GetElementsByTagName("rango");

				for (int i = 0; i < tmpXmlNodeList.Count; i++)
					tmpListRangos.Add(tmpXmlNodeList[i].InnerXml);

				if (tmpListRangos.Count > 0)
					rangoCalificacionLetrasNumeros = tmpListRangos.ToArray();

				Debug.LogError(rangoCalificacionLetrasNumeros.Length + "length" + tmpXmlNodeList.Count);
			}
			catch (Exception e)
			{
				Debug.Log("No se encontro el archivo XML, se usaran las notas por default|| Excepcion : " + e);
			}
#endif
		}

		#endregion
	}

	[Serializable]
	public class PesosCalificacionTareasSituacion
	{
		public ValueFloat valCalificacionTareaEspecifica;

		public float pesoCalificacionTareaEspecifica;
	}
}