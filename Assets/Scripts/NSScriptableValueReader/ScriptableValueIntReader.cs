﻿using Modular;
using NSScriptableValues;
using UnityEngine;

namespace NSScriptableValueReader
{
  public class ScriptableValueIntReader : AbstractScriptableValueReader
  {
    [SerializeField]private ValueInt _valueInt;
    [SerializeField]private IntEvent _readingResult;
    public override void Read()
    {
      _readingResult?.Invoke(_valueInt);
    }
  }
}
