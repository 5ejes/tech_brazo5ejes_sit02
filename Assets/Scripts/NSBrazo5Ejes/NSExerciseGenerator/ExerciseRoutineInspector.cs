﻿using System.Collections;
using System.Collections.Generic;
using NSScriptableValues;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
	/// <summary>
	/// Conecta las acciones principales de ExerciseRoutine al inspector
	/// </summary>
	public class ExerciseRoutineInspector : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private AbstractExerciseRoutine _routine;
		[SerializeField] private ValueFloat _calificacion = null;
		[Space]
		[SerializeField] private bool _showDebug = false;

		public AbstractExerciseRoutine Routine { get => _routine; set => _routine = value; }


		/// <summary>
		/// ¿Es la rutina actual nula?
		/// </summary>
		/// <returns>La booleana que representa si la rutina es nula</returns>
		public bool IsCurrentRutineNull()
		{
			return Routine == null;
		}

		/// <summary>
		/// Evalua la rutina actual
		/// </summary>
		public void EvaluateCurrentRoutine()
		{
			// Si no existe una rutina
			if (IsCurrentRutineNull()) return;

			if (_showDebug) Routine.OnEvaluationCompleted += LogRoutine;
			Routine.OnEvaluationCompleted += UpdateQualification;
			// Evaluo la rutina
			Routine.EvaluateRoutine();
		}

		public void UpdateQualification()
		{
			// Si no existe el valor de float
			if (!_calificacion) return;

			// Modifico el valor de la calificación
			_calificacion.Value = Routine.CurrentQualification;
		}

		public void LogRoutine()
		{
			Debug.Log($"Current Qualification : {Routine.CurrentQualification} || Threshold : {Routine.QualificationThreshold}");
			Routine.OnEvaluationCompleted -= LogRoutine;
		}

	}
}
