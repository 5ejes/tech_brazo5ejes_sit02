using System;
using System.Collections;
using UnityEngine;

namespace NSArm3Axis
{
	[RequireComponent(typeof(Rigidbody))]
	public abstract class AbstractCollectableObject : MonoBehaviour, IOverrideTransform
	{
		private byte collectIndex;
		private Rigidbody _rigidbodyObject;
		private Vector3 _offsetDirectionCollector;
		private Vector3 _positionToMove;
		private Coroutine repositionCoroutine;

		public Vector3 PickedUpLocation
		{
			get => _positionToMove;
			set => _positionToMove = value;
		}

		public Rigidbody CollectableRigidbody => _rigidbodyObject;
		protected Transform _transform;
		public Transform Target => UseOverrideTransform && OverrideTransform != null ? OverrideTransform : _transform;
		public Transform OverrideTransform { get; set; } = null;
		public bool UseOverrideTransform { get; set; } = false;

		private void Awake()
		{
			_transform = transform;
			_rigidbodyObject = GetComponent<Rigidbody>();
		}

		public void CollectObject(Vector3 pickedUpLocation)
		{
			collectIndex = 1;
			_rigidbodyObject.isKinematic = true;

			// Defino la dirección desde el punto definido para tomar el objeto y la posición del objeto, como offset
			_offsetDirectionCollector = Target.position - pickedUpLocation;

			if (repositionCoroutine != null)
				StopCoroutine(repositionCoroutine);
			repositionCoroutine = StartCoroutine(RemoveOffset(1f));

			// Defino el registro de la posición para moverse
			PickedUpLocation = pickedUpLocation;
		}

		public void MoveObject()
		{
			collectIndex = 2;

			// Aplico un offset desde la posición donde fue agarrado el objeto y la posición del objeto a mover
			Target.position = PickedUpLocation + _offsetDirectionCollector;
		}

		private IEnumerator RemoveOffset(float timeout)
		{
			// Creo variables para control de tiempo
			float elapsed = 0f;

			// Creo la posición inicial
			Vector3 startPosition = _offsetDirectionCollector;

			// Mientras el tiempo transcurrido sea menor al tiempo límite
			while (elapsed < timeout)
			{
				// Aumento los segundos actuales
				elapsed += Time.deltaTime;

				// Hallo el porcentaje
				float percent = elapsed / timeout;

				// Genero y aplico el vector LERP
				Vector3 current = Vector3.Lerp(startPosition, Vector3.zero, percent);
				_offsetDirectionCollector = current;
				yield return null;
			}

			// Defino el valor final
			_offsetDirectionCollector = Vector3.zero;
		}

		public void ReleaseObject()
		{
			collectIndex = 3;
			_rigidbodyObject.isKinematic = false;
			_offsetDirectionCollector = Vector3.zero;
		}
	}
}