﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSUtilities;

namespace NSBrazo5Ejes.NSDraggingBehaviour
{
  public class DraggableAutoAdjuster : MonoBehaviour
  {
    [Header("Settings")]
    [SerializeField] private BoxCollider _collider = null;
    [SerializeField] private DraggableComponent _draggable = null;
		[Range(-1,1), SerializeField] private float _offset = 0f;
    [Space]
    [SerializeField] private bool _adjustOnAwake = true;

    /// <summary>
    /// El BoxCollider sobre el que se detecta la colisión
    /// </summary>
    /// <value></value>
    public BoxCollider Collider
    {
      get
      {
        if (_collider == null) _collider = GetComponent<BoxCollider>();
        return _collider;
      }
    }

    /// <summary>
    /// El componente Draggable referenciado
    /// </summary>
    /// <value></value>
    public DraggableComponent Draggable
    {
      get
      {
        if (_draggable == null) _draggable = GetComponent<DraggableComponent>();
        return _draggable;
      }
    }

    public void Awake()
    {
      // Si se ajusta en el Awake
      if (_adjustOnAwake)
      {
        Adjust();
      }
    }

    public void Adjust()
    {
      // Si el BoxCollider y el Draggable están definido
      if (Collider && Draggable)
      {
				// Obtengo el tamaño del colisionador
				var size = _collider.size;

				// Defino la mitad del camaño
				var halfSize = (size /2f).ToAbsolute();

				var valueOffset = halfSize * _offset;

				// Defino la posición de los límites del Draggable basado en el tamaño del collider 
				Draggable.MaxLimit.localPosition = halfSize + valueOffset;
				Draggable.MinLimit.localPosition = -halfSize - valueOffset;
      }
    }
  }
}
