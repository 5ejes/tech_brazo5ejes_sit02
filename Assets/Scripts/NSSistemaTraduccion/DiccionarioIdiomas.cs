﻿#pragma warning disable 0649
#pragma warning disable 0618

using System;
using NSSingleton;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using NSScriptableEvent;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;


namespace NSTraduccionIdiomas
{
    /// <summary>
    /// Carga el idioma desde archivos XML
    /// </summary>
    public class DiccionarioIdiomas : AbstractSingleton<DiccionarioIdiomas>
    {
        #region members

        /// <summary>
        /// Diccionario para las traducciones al idioma English
        /// </summary>
        public Dictionary<string, string> diccionarioIngles = new Dictionary<string, string>();

        /// <summary>
        /// Diccionario para las traducciones al idioma portugues
        /// </summary>
        public Dictionary<string, string> diccionarioPortugues = new Dictionary<string, string>();

        /// <summary>
        ///  Diccionario para las traducciones al idioma Español
        /// </summary>
        public Dictionary<string, string> diccionarioEspaniol = new Dictionary<string, string>();

        /// <summary>
        /// contiene el nombre de los arhivos empezando por español seguido de English y asi dependeiendo del la cantidad de lenguajes
        /// </summary>
        public string[] arrayNombresArchivosDiccionarios;

        /// <summary>
        /// Idioma seleccionado actualmente
        /// </summary>
        public IdiomsList idiomaActual;

        /// <summary>
        /// Para notificar cuando un diccionario fue cargado
        /// </summary>
        public ScriptableEvent seOnDictionaryLoaded;

        #endregion

        #region Events

        [Header("Events")] [SerializeField] private ScriptableEvent seOnButtonSelectSpanishAsIdiom;

        [SerializeField] private ScriptableEvent seOnButtonSelectEnglishAsIdiom;

        #endregion

        #region delegates

        /// <summary>
        /// Delegado al que se suscribe el metodo que traduce un string
        /// </summary>
        /// <param name="argStringParaTraducir">String que se desea traducir</param>
        private delegate string delegateTraduccionString(string argNumbreTextParaTraducir);

        private delegateTraduccionString DltTraduccionString = delegate { return ""; };

        /// <summary>
        /// delegado al que se suscriben el metodo que traduce un text por su object name
        /// </summary>
        public delegate void delegateTraducirporNombreDelObjeto();

        public delegateTraducirporNombreDelObjeto DLTraducirForNameObj = delegate { };

        #endregion

        #region MonoBehaviour

        /*private void OnEnable()
        {
            seOnButtonSelectSpanishAsIdiom.Subscribe(SetIdiomToSpanish);
            seOnButtonSelectEnglishAsIdiom.Subscribe(SetIdiomToEnglish);
        }

        private void OnDisable()
        {
            seOnButtonSelectSpanishAsIdiom.Unsubscribe(SetIdiomToSpanish);
            seOnButtonSelectEnglishAsIdiom.Unsubscribe(SetIdiomToEnglish);
        }*/

        #endregion

        #region private methods

        private void SetIdiomToSpanish()
        {
            SetIdiom(IdiomsList.Spanish);
        }

        private void SetIdiomToEnglish()
        {
            SetIdiom(IdiomsList.English);
        }

        /// <summary>
        ///  metodo para traducir strings a español
        /// </summary>
        /// <param name="argStringParaTraducir">Nombre del objeto que tiene el text para traducir o llave</param>
        /// <returns>String traducido al idioma actual</returns>
        private string GetTraduccionEspaniol(string argNombreTextParaTraducir)
        {
            if (diccionarioEspaniol.ContainsKey(argNombreTextParaTraducir))
                return diccionarioEspaniol[argNombreTextParaTraducir];

            TrackingTextoSinTraduccion(argNombreTextParaTraducir);
            return "Etiqueta : " + argNombreTextParaTraducir + " sin traducción.";
        }

        /// <summary>
        /// Sobrecarga del metodo para traducir strings
        /// </summary>
        /// <param name="argNombreTextParaTraducir">String para traducir</param>
        /// <returns>String traducido al idioma actual</returns>
        private string GetTraduccionIngles(string argNombreTextParaTraducir)
        {
            if (diccionarioIngles.ContainsKey(argNombreTextParaTraducir))
                return diccionarioIngles[argNombreTextParaTraducir];

            TrackingTextoSinTraduccion(argNombreTextParaTraducir);
            return "Etiqueta : " + argNombreTextParaTraducir + " sin traducción.";
        }

        /// <summary>
        /// Sobrecarga del metodo para traducir strings
        /// </summary>
        /// <param name="argStringParaTraducir">String para traducir</param>
        /// <returns>String traducido al idioma actual</returns>
        private string GetTraduccionPortugues(string argNombreTextParaTraducir)
        {
            if (diccionarioPortugues.ContainsKey(argNombreTextParaTraducir))
                return diccionarioPortugues[argNombreTextParaTraducir];

            TrackingTextoSinTraduccion(argNombreTextParaTraducir);
            return "Etiqueta : " + argNombreTextParaTraducir + " sin traducción.";
        }

        /// <summary>
        /// Debug para encontrar facilmente el string que no tiene traduccion en los diccionarios
        /// </summary>
        private void TrackingTextoSinTraduccion(string argStringParaTraducir)
        {
            Debug.LogWarning("String sin traduccion : " + argStringParaTraducir);
        }

        /// <summary>
        /// metodo que llena los diccionarios con las traducciones del xml
        /// </summary>
        public void CargarDiccionarios()
        {
            switch (idiomaActual)
            {
                case IdiomsList.Spanish:
                    Debug.Log(":: Spa");
                    AsignarTraduccionesEspaniol();
                    break;

                case IdiomsList.English:
                    Debug.Log(":: Eng");
                    AsignarTraduccionesIngles();
                    break;

                case IdiomsList.portugues:
                    AsignarTraduccionesPortugues();
                    break;
            }
        }

        /// <summary>
        /// Courutina para leer las traducciones de un XML
        /// </summary>
        /// <param name="argXMLFileName">Nombre del archivo de texto.</param>
        /// <param name="argDiccionario">Diccionario al que se le van a agregar las traducciones.</param>
        private IEnumerator CouLeerTraduccionesXML(string argXMLFileName, Dictionary<string, string> argDiccionario)
        {
            Debug.Log("xml name: " + argXMLFileName);
            var dir = System.IO.Path.Combine(Application.streamingAssetsPath, argXMLFileName + ".xml");
            XmlDocument reader = new XmlDocument();

#if UNITY_ANDROID
            dir = "jar:file://" + Application.dataPath + "!/assets/"+ argXMLFileName + ".xml";
            WWW wwwfile = new WWW(dir);
            yield return wwwfile;

            if (!string.IsNullOrEmpty(wwwfile.error))            
                Debug.Log("no se encontro el archivo");            

            reader.LoadXml(wwwfile.text);
#elif UNITY_WEBGL
            UnityWebRequest www = UnityWebRequest.Get(dir);
            yield return www.SendWebRequest();
            reader.LoadXml( www.downloadHandler.text);
#else
            reader.LoadXml(System.IO.File.ReadAllText(dir));
            yield return null;
#endif

            XmlNodeList _list = reader.ChildNodes[0].ChildNodes;
            Debug.Log("XML -value 1: " + reader.ChildNodes[0].FirstChild.InnerText);
            argDiccionario.Clear();

            Debug.Log("XML -value 2: " + reader.ChildNodes[0].FirstChild.InnerText);
            for (int i = 0; i < _list.Count; i++)
            {
                argDiccionario.Add(_list[i].Name, _list[i].InnerXml);
                Debug.Log(_list[i].Name + " -:- " + _list[i].InnerXml);
            }

            Debug.Log("XML -value 3: " + reader.ChildNodes[0].FirstChild.InnerText);
            seOnDictionaryLoaded.ExecuteEvent();
        }

        #endregion

        #region Traducciones Español

        /// <summary>
        ///  Agregar las traduccion para el idioma Ingles desde el español
        /// </summary>
        private void AsignarTraduccionesEspaniol()
        {
            diccionarioEspaniol.Clear();
            StartCoroutine(CouLeerTraduccionesXML(arrayNombresArchivosDiccionarios[0], diccionarioEspaniol));
        }

        #endregion

        #region Traducciones English

        /// <summary>
        /// Agregar las traduccion para el idioma Ingles desde el español
        /// </summary>
        private void AsignarTraduccionesIngles()
        {
            diccionarioIngles.Clear();
            StartCoroutine(CouLeerTraduccionesXML(arrayNombresArchivosDiccionarios[1], diccionarioIngles));
        }

        #endregion

        #region Traducciones portugues 

        /// <summary>
        /// Agregar las traduccion para el idioma Portugues desde el español
        /// </summary>
        private void AsignarTraduccionesPortugues()
        {
            diccionarioPortugues.Clear();
            StartCoroutine(CouLeerTraduccionesXML(arrayNombresArchivosDiccionarios[2], diccionarioPortugues));
        }

        #endregion

        #region public methods

        /// <summary>
        /// Metodo por medio del cual se solicita la traduccion de un string
        /// </summary>
        /// <param name="argNombreTextParaTraducir"> </param>
        /// <param name="texto">Texto que se va a traducir</param>
        public string Traducir(string argNombreTextParaTraducir)
        {
            return DltTraduccionString(argNombreTextParaTraducir);
        }

        /// <summary>
        /// Asigna un idioma
        /// </summary>
        /// <param name="argIdioma">Idioma que se usara</param>
        public void SetIdiom(IdiomsList argIdioma)
        {
            switch (argIdioma)
            {
                case IdiomsList.Spanish:
                    idiomaActual = argIdioma;
                    DltTraduccionString = GetTraduccionEspaniol;
                    DLTraducirForNameObj();
                    break;

                case IdiomsList.English:
                    idiomaActual = argIdioma;
                    DltTraduccionString = GetTraduccionIngles;
                    DLTraducirForNameObj();
                    break;

                case IdiomsList.portugues:
                    idiomaActual = argIdioma;
                    DltTraduccionString = GetTraduccionPortugues;
                    DLTraducirForNameObj();
                    break;
            }
        }

        #endregion
    }

    public enum IdiomsList
    {
        Spanish,
        English,
        portugues
    }
}