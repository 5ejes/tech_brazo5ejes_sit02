﻿using UnityEngine;

namespace NSBrazo5Ejes
{

	public class PolePositionHandler : MonoBehaviour
	{
		[Header("References")]
		[SerializeField] private Transform _poleReference;

		[Header("Values")]
		[SerializeField]private float _xLerpValue;
		[SerializeField]private float _yLerpValue;
		[SerializeField]private float _zLerpValue;

		[Header("Transform Values")]
		[SerializeField] private MultiValueLerpVector3 _xValues;
		[SerializeField] private MultiValueLerpVector3 _yValues;
		[SerializeField] private MultiValueLerpVector3 _zValues;
		
		private float _previousXLerp = -1f;
		private float _previousYLerp = -1f;
		private float _previousZLerp = -1f;

		private Vector3 _poleConstraintPosition;

		private void Update(){

			if(_previousXLerp != _xLerpValue)
			{
				_previousXLerp = _xLerpValue;
				UpdatePoleXConstraint(_xLerpValue);
			}

			if(_previousYLerp != _yLerpValue)
			{
				_previousYLerp = _yLerpValue;
				UpdatePoleYConstraint(_yLerpValue);
			}

			if(_previousZLerp != _zLerpValue)
			{
				_previousZLerp = _zLerpValue;
				UpdatePoleZConstraint(_zLerpValue);
			}

		}

		public void ForceUpdateConstrain()
		{
			UpdatePoleConstraints(_xLerpValue, _yLerpValue, _zLerpValue);
		}
		public void UpdatePoleConstraints(float x, float y, float z)
		{
				_xValues.LerpValue = x;
				_yValues.LerpValue = y;
				_zValues.LerpValue = z;
		}

		public void UpdatePoleXConstraint(float x)
		{
			if(x == _xLerpValue) return;
			_xLerpValue = x;

			UpdatePoleConstraints(x, _yLerpValue, _zLerpValue);
		}

		public void UpdatePoleYConstraint(float y)
		{
			if(y == _yLerpValue) return;
			_yLerpValue = y;

			UpdatePoleConstraints(_xLerpValue, y, _zLerpValue);
		}

		public void UpdatePoleZConstraint(float z)
		{
			if(z == _zLerpValue) return;
			_zLerpValue = z;

			UpdatePoleConstraints(_xLerpValue, _yLerpValue, z);
		}
	}
}
