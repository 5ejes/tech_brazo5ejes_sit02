﻿using System;
using NSBrazo5Ejes.NSDraggingBehaviour;
using UnityEngine;

namespace NSBrazo5Ejes.NSInterfaz
{
	public class DynamicHUDBinder : MonoBehaviour
	{
		[Header("Reference")]
		[SerializeField] private HUDDynamicCoordinates _hudDynamicCoordinates;
		[SerializeField] private DragRaycastComponent _rayCastComponent;

		public HUDDynamicCoordinates HudDynamicCoordinates { get => _hudDynamicCoordinates; set => _hudDynamicCoordinates = value; }
		public DragRaycastComponent RayCastComponent { get => _rayCastComponent; set => _rayCastComponent = value; }

		private bool _hudActivated = false;

		private void OnEnable()
		{
			_rayCastComponent.MouseDown += RaycastMouseDown;
			_rayCastComponent.MouseUp += RaycastMouseUp;
			_rayCastComponent.PlanePositionDragEvent += RaycastDragEvent;
		}

		private void OnDisable()
		{
			_rayCastComponent.MouseDown -= RaycastMouseDown;
			_rayCastComponent.MouseUp -= RaycastMouseUp;
			_rayCastComponent.PlanePositionDragEvent += RaycastDragEvent;
		}

		private void RaycastMouseDown(DragRaycastComponent sender, DraggableComponent draggable, bool isLastMoveValid, Vector3 dragPosition)
		{
			// Si ha tomado un objeto y si lo ha tomado en el area con un movimiento válido
			if (sender.CurrentDraggable != null && isLastMoveValid)
			{
				HudDynamicCoordinates.Activate();
				_hudActivated = true;
			}
		}

		private void RaycastMouseUp(DragRaycastComponent sender, DraggableComponent draggable, bool isLastMoveValid, Vector3 dragPosition)
		{
			_hudActivated = false;
			HudDynamicCoordinates.Deactivate();
		}

		private void RaycastDragEvent(DragRaycastComponent sender, DraggableComponent draggable, bool isLastMoveValid, Vector3 dragPosition)
		{
			// Si el draggable que tomó no es nulo
			if (draggable != null)
				_hudDynamicCoordinates.HUDWorldPosition = dragPosition;

			// Evaluo si el componente tuvo un movimiento inválido y está activado
			if (_hudActivated && !isLastMoveValid)
			{

				// Desactivo el componente
				_hudActivated = false;
				HudDynamicCoordinates.Deactivate();
			}
			else if (!_hudActivated && isLastMoveValid)
			{
				_hudActivated = true;
				HudDynamicCoordinates.Activate();
			}
		}

	}
}