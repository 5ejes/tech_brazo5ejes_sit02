﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace NSUtilities
{

	public static class ExtensionMethods
	{
		/// <summary>
		/// Retorno los valores absolutos de un Vector3
		/// </summary>
		/// <param name="argValue"> Vector usado para los valores absolutos</param>
		/// <returns> Vector con los valores absolutos haciendo uso de Mathf.Abs</returns>
		public static Vector3 ToAbsolute(this Vector3 argValue)
		{
			return new Vector3(Mathf.Abs(argValue.x), Mathf.Abs(argValue.y), Mathf.Abs(argValue.z));
		}

		/// <summary>
		/// Comprueba que el evento no sea nulo antes de invocarlo
		/// </summary>
		/// <param name="unityEvent">Evento UnityEvent a ejecutar</param>
		public static void WrapInvoke(this UnityEvent unityEvent)
		{
			// Compruebo que el evento no sea nulo
			if (unityEvent != null)
				unityEvent.Invoke();  // Invoco el evento
		}


		/// <summary>
		/// Comprueba que el evento no sea nulo antes de invocarlo
		/// </summary>
		/// <param name="unityEvent">Evento UnityEvent a ejecutar</param>
		/// <param name="value">Valor que usa el evento como parámetro</param>
		/// <typeparam name="T">Tipo del evento de UnityEvent</typeparam>
		public static void WrapInvoke<T>(this UnityEvent<T> unityEvent, T value)
		{
			// Compruebo que el evento no sea nulo
			if (unityEvent != null)
				unityEvent.Invoke(value); // Invoco el evento pasandole el parámetro especificado
		}

		/// <summary>
		/// Fija los parámetros del vector a valores definidos por el snap
		/// </summary>
		/// <param name="target">Vector3 a ser operado</param>
		/// <param name="snap">Valor flotante del ajuste</param>
		/// <returns>Vector3 ajustado según los parámetros</returns>
		public static Vector3 SnapVector(this Vector3 target, float snap)
		{
			// Realizo el snap según los parámetros
			target.x = Mathf.Round(target.x / snap) * snap;
			target.y = Mathf.Round(target.y / snap) * snap;
			target.z = Mathf.Round(target.z / snap) * snap;

			// Retorno el vector procesado
			return target;
		}

		/// <summary>
		/// Fija el valor flotante a un valor definido por el snap
		/// </summary>
		/// <param name="value">Valor flotante a ser procesado</param>
		/// <param name="snap">Valor flotante del ajuste</param>
		/// <returns>Valor flotante ajustado</returns>
		public static float SnapFloat(this float value, float snap)
		{
			// Realizo el snap según los parámetros
			value = Mathf.Round(value / snap) * snap;

			// Retorno el valor procesado
			return value;
		}

		/// <summary>
		/// Evalua un arreglo de boleanos en OR
		/// </summary>
		/// <param name="evaluationParams">Arreglo de todos los booleanos</param>
		/// <returns>Retorna el resultado de la evaluación OR</returns>
		public static bool OREvaluation(this bool[] evaluationParams)
		{
			// Por cada evaluación
			for (int i = 0; i < evaluationParams.Length; i++)
			{
				// Evaluo el resultado actual

				// Si uno de los resultados es true
				if (evaluationParams[i])
				{
					// Retorno true
					return true;
				}
			}

			// Retorno false, todos los resultados son false
			return false;
		}

		/// <summary>
		/// Evalua un arreglo de boleanos en AND
		/// </summary>
		/// <param name="evaluationParams">Arreglo de todos los booleanos</param>
		/// <returns>Retorna el resultado de la evaluación AND</returns>
		public static bool ANDEvaluation(this bool[] evaluationParams)
		{
			// Por cada evaluación
			for (int i = 0; i < evaluationParams.Length; i++)
			{
				// Evaluo el resultado actual

				// Si uno de los resultados es false
				if (!evaluationParams[i])
				{
					// Retorno false
					return false;
				}
			}

			// Retorno true, todos los resultados son true
			return true;
		}

		/// <summary>
		/// Obtiene el valor de Interpolación linear dado los vectores
		/// </summary>
		/// <param name="a">Vector3 a</param>
		/// <param name="b">Vector3 b</param>
		/// <param name="value">Vector3 a evaluar</param>
		/// <returns>Un valor correspondiente a la interpolación, 0 cuando es 'a', 1 cuando es 'b' e intermedios</returns>
		public static float InverseLerp(this Vector3 value, Vector3 a, Vector3 b)
		{
			Vector3 AB = b - a;
			Vector3 AV = value - a;
			return Vector3.Dot(AV, AB) / Vector3.Dot(AB, AB);
		}
	}


}