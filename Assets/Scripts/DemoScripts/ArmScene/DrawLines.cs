using System.Collections.Generic;
using UnityEngine;

public class DrawLines : MonoBehaviour
{

	private IKSolver _solver;
	public void Start()
	{
		_solver = GetComponent<IKSolver>();

		var transformList = new List<Transform>();

		var bones = _solver.bones;

		transformList.Add(_solver.endPointOfLastBone);
		foreach (var bone in bones)
		{
			if (bone.bone != null && bone != null)
				transformList.Add(bone.bone);

		}

		// Creo una instancia de dibujado de lineas para mostrar los puntos del ik
		TransformLineDrawer.AddData(new TransformLineDrawer.TransformLineDrawingData(transformList.ToArray()));
	}
}