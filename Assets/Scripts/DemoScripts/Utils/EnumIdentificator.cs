﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumIdentificator : MonoBehaviour
{
	[Header("Enum")]
	[SerializeField] private ScriptableEnumID _enumId;
	public ScriptableEnumID EnumID => _enumId;
}