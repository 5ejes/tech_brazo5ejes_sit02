﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerScript : MonoBehaviour
{
	[Header("Joystick")]
	[SerializeField] private Joystick _joystick;
	[Header("Settings")]
	[SerializeField]
	private Transform cameraObject;
	[SerializeField]
	private float MultiplicadorAvance;
	[SerializeField]
	private float RelacionDeRotacion;

	[SerializeField] private bool _movementEnabled = true;
	private CharacterController player;
	private Vector3 camaraForward;
	private Vector3 camRight;
	private Vector3 inputplayer;
	private Vector3 moverplayer;

	public bool MovementEnabled { get => _movementEnabled; set => _movementEnabled = value; }

	private void Start()
	{
		player = GetComponent<CharacterController>();
		if (_joystick == null)
			_joystick = FindObjectOfType<Joystick>();
		//TODO: debihacer casooo
	}

	private void Update()
	{

		// Si el movimiento está deshabilitado
		if (!MovementEnabled) return;

		float horizontal = _joystick?.Horizontal ?? 0f;
		float vertical = _joystick?.Vertical ?? 0f;

		inputplayer = new Vector3(horizontal, 0f, vertical);

		CamaraDireccion();

		moverplayer = inputplayer.z * camaraForward;

		player.transform.Rotate(new Vector3(0f, horizontal * RelacionDeRotacion, 0f) * Time.deltaTime);

		player.Move(moverplayer * MultiplicadorAvance);

	}

	private void CamaraDireccion()
	{
		camaraForward = cameraObject.transform.forward;
		camaraForward.y = 0;
		camaraForward = camaraForward.normalized;

	}

}
