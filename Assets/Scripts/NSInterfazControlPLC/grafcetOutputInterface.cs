﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using static NSScriptableEvent.ScriptableEventBool;
using static NSScriptableEvent.ScriptableEventInt;

namespace NSInterfazControlPLC
{
	/// <summary>
	/// Interfaz intermedia para el uso sencillo de las salidas del GRAFCET Controller (WIP)
	/// </summary>
	public class grafcetOutputInterface
	{

		// TODO: Configurar escritura efectiva de los datos
		private static readonly int POSITION_X_MEMORY_READ_DATA_INDEX = 8;
		private static readonly int POSITION_Y_MEMORY_READ_DATA_INDEX = 9;
		private static readonly int POSITION_Z_MEMORY_READ_DATA_INDEX = 10;

		private static readonly int POSITION_X_MEMORY_WRITE_DATA_INDEX = 0;
		private static readonly int POSITION_Y_MEMORY_WRITE_DATA_INDEX = 1;
		private static readonly int POSITION_Z_MEMORY_WRITE_DATA_INDEX = 2;

		private uint _storedPositionX = 999;
		private uint _storedPositionY = 999;
		private uint _storedPositionZ = 999;

		/// <summary>
		/// Constructor de la clase
		/// </summary>
		/// <param name="grafcetMain">Referencia al GRAFCET Controller</param>
		public grafcetOutputInterface(grafcetController controller)
		{
			// Defino la clase interna del grafcetOutputInterface
			GrafcetController = controller;
		}

		/// <summary>
		/// Referencia al GRAFCET Controller
		/// </summary>
		public grafcetController GrafcetController { get; private set; }

		/// <summary>
		/// Referencia al RoboticViewer
		/// </summary>
		/// <value></value>
		public RoboticViewer Viewer { get; set; }

		/// <summary>
		/// Tiene un grafcet controller asignado?
		/// </summary>
		public bool HasController => GrafcetController != null;


		/// <summary>
		/// Posición en X de GRAFCET
		/// </summary>
		public uint PositionX
		{
			get => GrafcetController != null ? GrafcetController.Memorias[POSITION_X_MEMORY_READ_DATA_INDEX] : 0;
			set
			{
				if (!IsViewerAssigned()) return;

				if (!value.Equals(_storedPositionX))
				{
					_storedPositionX = value;
					// Viewer.analogInputs[POSITION_X_MEMORY_WRITE_DATA_INDEX] = System.Convert.ToByte(value);
					Viewer.ai0.text = value.ToString();
				}
			}
		}


		/// <summary>
		/// Posición en Y de GRAFCET
		/// </summary>
		public uint PositionY
		{
			get => GrafcetController != null ? GrafcetController.Memorias[POSITION_Y_MEMORY_READ_DATA_INDEX] : 0;
			set
			{
				if (!IsViewerAssigned()) return;

				if (!value.Equals(_storedPositionY))
				{
					_storedPositionY = value;
					// Viewer.analogInputs[POSITION_Y_MEMORY_WRITE_DATA_INDEX] = System.Convert.ToByte(value);
					Viewer.ai1.text = value.ToString();
				}
			}
		}
		/// <summary>
		/// Posición en Z de GRAFCET
		/// </summary>
		public uint PositionZ
		{
			get => GrafcetController != null ? GrafcetController.Memorias[POSITION_Z_MEMORY_READ_DATA_INDEX] : 0;
			set
			{
				if (!IsViewerAssigned()) return;

				if (!value.Equals(_storedPositionZ))
				{
					_storedPositionZ = value;
					// Viewer.analogInputs[POSITION_Z_MEMORY_WRITE_DATA_INDEX] = System.Convert.ToByte(value);
					Viewer.ai2.text = value.ToString();
				}

			}
		}
		/// <summary>
		/// Estado del GRIP de GRAFCET
		/// </summary>
		public bool GripState
		{
			get => GrafcetController != null ? GrafcetController.SalidasBool[2] : false;
			set
			{
				if (Viewer != null) Viewer.digitalInputs[2] = value;
			}
		}

		/// <summary>
		/// Estado del Eje 5 del brazo
		/// </summary>
		/// <value></value>
		public bool Eje5
		{
			get => GrafcetController != null ? GrafcetController.SalidasBool[4] : false;
			set 
			{
				if(Viewer == null) return;

				// Escribo el valor de retroalimentación
				Viewer.digitalInputs[4] = value;
			}
		}

		/// <summary>
		/// Estado del motor de la banda
		/// </summary>
		/// <value></value>
		public bool BeltMotor { get => GrafcetController != null ? GrafcetController.SalidasBool[1] : false; }

		/// <summary>
		/// Estado del sensor de la banda transportadora, puede ser escrito
		/// </summary>
		/// <value></value>
		public bool BeltSensor
		{
			get
			{
				// Si el controlador es nulo
				if (GrafcetController == null) return false;
				// Obtengo el valor para leer
				bool beltValue = GrafcetController.EntradasBool[1];

				// Retorno el valor del belt
				return beltValue;
			}
			set
			{
				// Si el viewer está definido
				if (Viewer != null)
				{
					// Defino el índice del digital input en el Robotic Viewer
					Viewer.digitalInputs[1] = value;
				}
				else
				{
					// Lanzo un error
					Debug.LogError($"RoboticViewer reference is null; Impossible to write values : {this}");
				}
			}
		}

		/// <summary>
		/// Valor del ID de color
		/// </summary>
		/// <value></value>
		public uint ColorSensorID
		{
			get
			{
				// Si el controller es nulo
				if (GrafcetController == null) return 99;

				// Obtengo el valor para leer
				uint colorIdValue = GrafcetController.Memorias[14];
				// Retorno el valor del color id
				return colorIdValue;
			}
			set
			{
				// Si el viewer está definido
				if (Viewer != null)
				{
					// Defino el índice del digital input en el Robotic Viewer
					Viewer.memoryData[14] = value;
				}
				else
				{
					// Lanzo un error
					Debug.LogError($"RoboticViewer reference is null; Impossible to write values : {this}");
				}
			}
		}

		/// <summary>
		/// Elimino el controlador del Output Interface
		/// </summary>
		public void RemoveController()
		{
			// Elimina el controlador grafcet
			GrafcetController = null;
		}

		private bool IsViewerAssigned()
		{
			if (Viewer == null)
			{
				Debug.LogError($"The RoboticViewer instance is not assigned cannot write");
				return false;
			}
			return true;
		}
	}
}



