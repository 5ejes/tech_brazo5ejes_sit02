﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;
using static NSScriptableEvent.ScriptableEventBool;

public class TransformBoundInspector : MonoBehaviour
{
	[Header("Bounds")]
	[SerializeField] private Transform[] _transformBounds;
	[Header("Events")]
	[SerializeField] private UnityEvent _onPointInside;
	[SerializeField] private BoolEvent _onPointEvaluationResult;
	[Header("Debug")]
	[SerializeField] private bool _drawDebug = false;
	[SerializeField] private Color _drawColor = Color.red;

	private TransformBound[] _bounds;

	public bool DrawDebug { get => _drawDebug; set => _drawDebug = value; }
	public Color DrawColor { get => _drawColor; set => _drawColor = value; }

	private void Update()
	{
		// Defino los límites si son nulos
		if (_bounds == null) _bounds = GenerateBounds(_transformBounds);

		// Si bounds no es nulo
		if (_bounds != null)
		{
			// Si la cuenta de bounds es diferente del arreglo de transform
			int boundLength = _bounds.Length;
			int transformBoundsLength = _transformBounds.Length;

			// Si es distinto la cantidad de límites y de transform
			if (!boundLength.Equals(transformBoundsLength))
			{
				// Genero nuevos límites
				GenerateBounds(_transformBounds);
			}
		}
	}

	/// <summary>
	/// Verifica si el punto está dentro de los límites definidos
	/// </summary>
	/// <param name="point">El punto a verificar</param>
	/// <returns>true: El punto está dentro de los límites, </returns>
	public bool IsPointInside(Vector3 point)
	{
		// Si los bounds están definidos
		if (_bounds != null)
		{
			// Si hay al menos un bound
			int length = _bounds.Length;
			if (length > 0)
			{
				// Verifico cada límite
				for (int boundIndex = 0; boundIndex < length; boundIndex++)
				{
					// Obtengo el límite actual
					var currentBound = _bounds[boundIndex];

					// Si el punto se encuentra dentro
					if (currentBound.IsInside(point))
					{
						// Retorno true
						return true;
					}
				}
			}
		}
		// Retorno false
		return false;
	}

	public void CheckPointInside(Vector3 point)
	{
		// Si el punto está dentro
		var isInside = IsPointInside(point);

		// Si está dentro
		if (isInside)
		{
			// Ejecuto el evento
			_onPointInside.WrapInvoke();
		}

		// Ejecuto el evento
		_onPointEvaluationResult.WrapInvoke(isInside);
	}

	/// <summary>
	/// Genera los límites dado un arreglo de transform
	/// </summary>
	/// <param name="referenceTransforms">Arreglo de transform a convertir</param>
	/// <returns>Retorna un arreglo de TransformBound</returns>
	private TransformBound[] GenerateBounds(Transform[] referenceTransforms)
	{
		// Defino la lista de bounds
		var boundList = new List<TransformBound>();

		// Por cada transform
		int length = referenceTransforms.Length;
		for (int transformIndex = 0; transformIndex < length; transformIndex++)
		{
			// Obtengo el transform actual
			var currentTransform = referenceTransforms[transformIndex];

			// Genero el límite
			boundList.Add(currentTransform);
		}

		// Retorno el resultado
		return boundList.ToArray();
	}

	private void OnDrawGizmos()
	{
		// Si no tengo permitido dibujar debug
		if(!DrawDebug) return;

		// Si las areas restringidas no son nulas
		if (_transformBounds != null)
		{

			// Si hay al menos un area restringida
			if (_transformBounds.Length > 0)
			{

				// Defino el valor de los colores
				Color gizmosLine, gizmosFill = gizmosLine = DrawColor;

				gizmosLine.a = 0.8f;
				gizmosFill.a = 0.2f;

				// Recorro cada zona restringida
				foreach (var area in _transformBounds)
				{
					// Dibujo los indicadores
					Gizmos.color = gizmosFill;
					Gizmos.DrawCube(area.position, area.localScale);

					Gizmos.color = gizmosLine;
					Gizmos.DrawWireCube(area.position, area.localScale);
				}

			}
		}
	}
}
