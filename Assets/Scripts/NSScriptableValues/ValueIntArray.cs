﻿using UnityEngine;

namespace NSScriptableValues
{
    [CreateAssetMenu(fileName = "ValueIntArray", menuName = "NSScriptableValue/ValueIntArray", order = 0)]
    public class ValueIntArray : AbstractScriptableValue<int[]>
    {
        #region sobre carga operadores

        public static implicit operator int[](ValueIntArray argValueA)
        {
            return argValueA.value;
        }
        #endregion
    }
}