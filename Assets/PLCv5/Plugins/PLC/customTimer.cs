﻿using System;
using UnityEngine;
using System.Collections;
using System.Threading;

public class customTimer
{
	public bool AutoReset = false;
	public double Interval = 0;
	public bool running;
	public event EventHandler Elapsed;
	public double ElaspedTime { get { return GetElapsedTime(); } }

    public timerElement.Tipo tipo;

	private DateTime StartTime;
	private DateTime LastTime;

    public customTimer()
    {
        StartTime = DateTime.Now;
        LastTime = StartTime;
    }

    public void Start(){
		StartTime = DateTime.Now;
		running = true;
	}

	public void Stop(){
		running = false;
        LastTime = DateTime.Now;
    }

    public void Reset()
    {
        StartTime = DateTime.Now;
        LastTime = StartTime;
    }

    public void checkState(){
		if (running == true) {
			if(ElaspedTime >= Interval){
				EventHandler handler = Elapsed;
				if (handler != null)
				{
					handler(this, EventArgs.Empty);
				}
				Stop ();
			}
		}
	}

	public double GetElapsedTime()
	{
		if (running == true)
        {
            LastTime = DateTime.Now;
        }
		
		var elaspedTime = LastTime - StartTime;

		return elaspedTime.TotalMilliseconds;
	}
}