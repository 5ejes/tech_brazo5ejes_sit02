﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class loadingClass : MonoBehaviour {

	public GameObject _situation;
	public Image loadingImg;
	public bool isActive = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(isActive == true){
			loadingImg.transform.Rotate (new Vector3 (0, 0, -6));
		}
	}
}
