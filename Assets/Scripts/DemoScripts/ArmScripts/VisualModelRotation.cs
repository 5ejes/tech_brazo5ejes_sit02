﻿using System.Collections;
using System.Collections.Generic;
using Modular;
using NSBrazo5Ejes;
using UnityEngine;

// Script de rotacion demo
public class VisualModelRotation : MonoBehaviour
{
	[Header("Copy Settings")]
	[SerializeField] private IKSolver _refSolver;
	[SerializeField] private Transform[] _copyTransforms = new Transform[0];
	[SerializeField] private Transform _gripBrazo4;

	[Header("Rotation Guide")]
	[SerializeField] private GuideController _gripRotationGuide;
	[Space]
	[Header("Rotation Adjust Settings")]
	[SerializeField] private Vector3 _customRotationOffset = new Vector3();
	[SerializeField] private Vector3 _customRotationAxis = new Vector3(0, 0, 1);
	[Space]

	[SerializeField] private float _horizontalRotationOffset = 0f;
	[Header("Manual Updaters")]
	[SerializeField] private GameObject[] _manualUpdatersGos;

	public float Brazo4GripHorizontalOffset
	{
		get => _horizontalRotationOffset;
		set => _horizontalRotationOffset = value;
	}

	private IManualUpdate[] _manualUpdates;

	private Transform[] _arraySolverBones = new Transform[0];
	private string _logText;

	private void Start()
	{
		if (_refSolver == null)
		{
			_refSolver = FindObjectOfType<IKSolver>();
			if (_refSolver == null) throw new System.Exception($"Solver can't be found : {gameObject.name}");
		}

		// Obtengo los actualizadores
		_manualUpdates = BuildManualUpdaters(_manualUpdatersGos);

		// Creo una lista temporal de huesos copia
		var tempList = new List<Transform>();

		// Añado el ultimo punto del hueso
		tempList.Add(_refSolver.endPointOfLastBone);

		// Añado cada hueso a la lista temporal
		foreach (var bone in _refSolver.bones)
		{
			tempList.Add(bone.bone);
		}

		// Defino el arreglo con la lista temporal de transformaciones
		_arraySolverBones = tempList.ToArray();

	}

	private IManualUpdate[] BuildManualUpdaters(GameObject[] Gos)
	{
		// Defino la lista de salida
		var output = new List<IManualUpdate>();

		int length = Gos.Length;
		for (int GoIndex = 0; GoIndex < length; GoIndex++)
		{
			// Obtengo el objeto actual
			var currentGameObject = Gos[GoIndex];

			// Obtengo el actualizador manual
			var currentManualUpdater = currentGameObject.GetComponents<IManualUpdate>();

			// Si existe el actualizador manual
			if (currentManualUpdater != null)
			{
				// Si hay más de un elemento
				if (currentManualUpdater.Length > 0)
				{
					// Agrego a la lista de actualizadores
					output.AddRange(currentManualUpdater);
				}
			}
		}

		// Retorno el arreglo de los resultados
		return output.ToArray();
	}

	private void Update()
	{

		// Si el solver es nulo o el array no ha sido inicializado, retorno
		if (_refSolver == null) return;
		if (_arraySolverBones == null) return;

		// Si hay más de un hueso, ejecuto la operación
		if (_arraySolverBones.Length > 0)
		{
			for (int i = 0; i < 2; i++)
			{
				CopyVisuals(_arraySolverBones);
			}
		}

		// Actualizo los manual updaters
		UpdateManualUpdaters(_manualUpdates);

		// Actualizo el Guide antes de actualizar la rotación del modelo
		if(_gripRotationGuide.AllowUpdate)
		{
			_gripRotationGuide.CallUpdate();
		}
	}

	private void UpdateManualUpdaters(IManualUpdate[] updaters)
	{
		// Si es nulo retorno
		if (updaters == null) return;

		// Obtengo la longitud
		int length = updaters.Length;
		if (length == 0) return;

		// Recorro cada elemento
		for (int i = 0; i < length; i++)
		{
			// Obtengo el current updater
			var currentUpdater = updaters[i];

			// Si es nulo o no permite la actualización, lo omito
			if (currentUpdater == null) continue;
			if (!currentUpdater.AllowUpdate) continue;

			// Actualizo el updater
			currentUpdater.CallUpdate();
		}
	}

	private void CopyVisuals(Transform[] argReferenceArray)
	{
		// Creo dos instancias de cuenta para los arreglos
		int copyCount = _copyTransforms.Length;
		int refCount = argReferenceArray.Length;

		// Si son exactamente de la misma longitud
		if (copyCount == refCount)
		{

			// Inicializo la rotación del padre
			Transform target = _refSolver.transform;
			Vector3 dirToTarget = (target.position - _copyTransforms[copyCount - 1].position).normalized;
			dirToTarget.y = 0f;

			Quaternion parentRotation = Quaternion.LookRotation(dirToTarget);

			for (int i = copyCount - 1; i >= 0; i--)
			{

				if (i > 0)
				{

					// Obtengo la copia actual
					var currentCopy = _copyTransforms[i];
					var currentRef = argReferenceArray[i];
					var nextRef = argReferenceArray[i - 1];

					dirToTarget = (target.position - _copyTransforms[copyCount - 1].position).normalized;
					dirToTarget.y = 0f;

					Vector3 relativePosition = (nextRef.position - currentCopy.position).normalized;
					Vector3 frontRelativeLookAt = Vector3.Cross(relativePosition, currentCopy.right).normalized;

					var signedAngle = SignedAngleFromDirection(dirToTarget, relativePosition, currentCopy.rotation * (Vector3.right).normalized);

					// var smoothAngle = Mathf.SmoothDampAngle(previousAngle, selectedAngle, ref currentVel, 1f, 1000f);
					var convertedFloat = (float)System.Math.Truncate(100 * signedAngle) / 100;

					var xRotation = Quaternion.Euler(new Vector3(_customRotationAxis.x * convertedFloat, _customRotationAxis.y * convertedFloat, _customRotationAxis.z * convertedFloat));

					// Aplico la rotación

					currentCopy.rotation = parentRotation;
					currentCopy.rotation *= Quaternion.Euler(_customRotationOffset);
					currentCopy.rotation *= xRotation;

					// Si está definido el grip brazo y es la referencia al brazo 3 (i == 1)
					/* DEBUG DRAW
					Debug.DrawRay(currentCopy.position, currentCopy.forward, Color.yellow);
					Debug.DrawRay(currentCopy.position, frontRelativeLookAt.normalized, Color.red);
					Debug.DrawRay(currentCopy.position, relativePosition, Color.magenta);
					*/

					currentCopy.position = currentRef.position;

					if (_gripBrazo4 && i == 1)
					{

						var angle = Vector3.Angle(currentCopy.up, _gripBrazo4.up);
						var offsetAngle = angle > 90f ? 0f : 180f;

						_gripBrazo4.position = nextRef.position;
						_gripBrazo4.rotation = Quaternion.AngleAxis(currentCopy.eulerAngles.y + 180f, Vector3.up) * Quaternion.AngleAxis(_gripRotationGuide.transform.eulerAngles.z + 180f, Vector3.right);
					}
				}

			}

		}
	}

	private void OnGUI()
	{
		// Si existe una cadena de texto almacenada en _logText
		if (!string.IsNullOrEmpty(_logText))
		{
			// Configuro el tamaño de fuente de el textField
			GUI.skin.textField.fontSize = 40 / Screen.width;

			// Muestro en pantalla el valor de la cadena
			GUILayout.TextField(_logText);
		}
	}

	public float SignedAngleFromDirection(Vector3 fromdir, Vector3 todir, Vector3 referenceup)
	{ // calculates the the angle between two direction vectors, with a referenceup a sign in which direction it points can be calculated (clockwise is positive and counter clockwise is negative)
		Vector3 planenormal = Vector3.Cross(fromdir, todir); // calculate the planenormal (perpendicular vector)
		float angle = Vector3.Angle(fromdir, todir); // calculate the angle between the 2 direction vectors (note: its always the smaller one smaller than 180°)
		float orientationdot = Vector3.Dot(planenormal, referenceup); // calculate wether the normal and the referenceup point in the same direction (>0) or not (<0), http://docs.unity3d.com/Documentation/Manual/ComputingNormalPerpendicularVector.html
		if (orientationdot > 0.0f) // the angle is positive (clockwise orientation seen from referenceup)
			return angle;
		return -angle; // the angle is negative (counter-clockwise orientation seen from referenceup)
	}
}