﻿using UnityEngine;

namespace NSBrazo5Ejes.NSDraggingBehaviour
{
	public class OverrideDraggableMoveComponentLimits : MonoBehaviour
	{
		[Header("Overrides")]
		[SerializeField] private Transform _maxLimit;
		[SerializeField] private Transform _minLimit;

		public Transform MaxLimit { get => _maxLimit; set => _maxLimit = value; }
		public Transform MinLimit { get => _minLimit; set => _minLimit = value; }
	}
}