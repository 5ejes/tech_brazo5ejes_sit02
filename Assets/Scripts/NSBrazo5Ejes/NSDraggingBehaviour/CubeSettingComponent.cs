﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSBrazo5Ejes.NSDraggingBehaviour
{
  public class CubeSettingComponent : MonoBehaviour
  {
    [Header("References")]
    [SerializeField] private DraggableComponent[] _draggables;

    private Cube[] _draggableCubeComponents;

    private void Start()
    {
      SetDraggableCubesArray();
      ResetCubesToKinematicState();
    }

    private void SetDraggableCubesArray()
    {
      if (_draggables != null)
      {
        int length = _draggables.Length;
        _draggableCubeComponents = new Cube[length];
        for (int i = 0; i < length; i++)
        {
          _draggableCubeComponents[i] = _draggables[i].GetComponent<Cube>();
        }
      }
    }

    private IEnumerator WaitFor(float seconds, Action callback)
    {
      if (callback == null) yield break;

      if (seconds > 0f)
      {
        yield return new WaitForSeconds(seconds);
      }

      callback?.Invoke();
    }

    private void GetAndProcessComponent<T>(Transform target, Action<T> callback)
    {
      if (callback == null) return;
      if (target == null) return;

      // Obtengo el cubo y lo comunico a través del callback
      T component = target.GetComponent<T>();

      // Si el componente no es nulo
      if (!EqualityComparer<T>.Default.Equals(component, default(T)))
        callback?.Invoke(component);
    }
    public void ResetCubesToKinematicState()
    {
      StartCoroutine(WaitFor(0.1f, () =>
      {
        System.Array.ForEach(_draggables, (DraggableComponent _draggable) =>
        {
          (_draggable as DraggableComponent).Rigidbody.isKinematic = true;
        });
      }));
    }

    public void SetRollingEffectForCubes(bool value)
    {
      System.Array.ForEach<Cube>(_draggableCubeComponents, cube =>
      {
        // Si el cubo es nulo, retorno
        if (cube == null) return;

        // Activo el efecto de rodamiento para los cubos
        cube.EnableRollingEffect = value;
      });
    }

    public void ApplyCubeReset(Transform target)
    {
      // Verifico que el target Transform esté definido
      if (!target) return;

      GetAndProcessComponent<Cube>(target,
      (cube) =>
      {
        // Reinicio el valor
        cube.Reset();
      });
    }

    public void SetPlateCubeBehaviour(Transform target)
    {
      GetAndProcessComponent<Cube>(target,
      (cube) =>
      {
        cube.Rigidbody.isKinematic = true;
        cube.Rigidbody.useGravity = true;
      });
    }
  }
}
