﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using System.Diagnostics;
using System.Timers;
using System.Threading;
using System;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

using NSTraduccionIdiomas;

public class Utilidades
{
    //VARAIBLES

    //----------------------
    //---APP NAME
    const string COMPANY = "CloudLabs";

    //SPA
    static string PRODUCT_SPA = "Cloudlabs - Programación de un brazo articulado para almacenaje de piezas";
    static string PRODUCT_SPA_OSX = "Cloudlabs - Programacion de robots";
    static string PRODUCT_ENG = "Cloudlabs - Automatic handling and storage of chemical supplies";
    static string PRODUCT_ENG_OSX = "Cloudlabs - Robots programming";
    static string PRODUCT_POR = "CloudLabs - Simulador de fotossintese";


    //----------------------
    //---identifier
    static string SPA_ID = "com.cloudlabs.robotica.brazopiezas.v4";
    static string ENG_ID = "com.cloudlabs.eng.robotica.brazopiezas.v4";
    static string POR_ID = "com.cloudlabs.tecnologia.brazoejes.por";


    //----------------------
    //---LANG

    //DiccionarioIdiomas.Instance.SetIdiom(IdiomsList.Spanish);
    static NSTraduccionIdiomas.IdiomsList Lang_SPA = IdiomsList.Spanish;
    static NSTraduccionIdiomas.IdiomsList Lang_ENG = IdiomsList.English;
    static NSTraduccionIdiomas.IdiomsList Lang_POR = IdiomsList.portugues;


    //----
    static bool SECURITY;

    //keyword
    static string simKeyword = "robo_5ejes02";
    static string simKeyword_U = "robo_5ejes02_u";
    static string simKeyword_K12 = "robo_5ejes02_k";
    static string std_folder = @"DEPLOY\Release\STD\";
    static string mono_folder = @"DEPLOY\Release\MONO\";

    [MenuItem("IE/Seguridad")]
    public static void Seguridad()
    {

        segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isSecurityOn = !segController.soSecurityConfiguration.isSecurityOn;

        if (segController.soSecurityConfiguration.isSecurityOn)
        {
            EditorUtility.DisplayDialog("SEGURIDAD", "SEGURIDAD ACTIVADA", "ok");
        }
        else
        {
            EditorUtility.DisplayDialog("SEGURIDAD", "NOT", "ok");
        }

    }

    static NSSeguridad.Seguridad segController;
    static DiccionarioIdiomas IdioDict;
    static NSInitSimulator.InitSimulator initCntroller;

    static GameObject segObj;
    static SeguridadBuild segObjScript;

    [MenuItem("IE/Build")]
    static void Build()
    {
        segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isSecurityOn = true;
        SECURITY = segController.soSecurityConfiguration.isSecurityOn;

        //--language
        initCntroller = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();

        segController.soSecurityConfiguration.isModeClassroom = false;

        //webAula
        //sim_SPA_webGL_AULA();
        //sim_SPA_webGL_AULA_nuevo();
        //sim_ENG_webGL_AULA();

        //webGL <--
        //sim_SPA_webGL("k12");
        sim_ENG_webGL("k12");
        //sim_ENG_webGL_AULA_nuevo();

        //PC -->----------------
        //sim_SPA_PC("k12");
        //sim_ENG_PC("k12");

        //OSX <--
        //sim_SPA_OSX("k");
        //sim_ENG_OSX("k");

        //APK <--
        //sim_SPA_APK("k");
        //sim_ENG_APK("k");

    }

    static BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
    static void groupScenes()
    {


        buildPlayerOptionsScene.scenes = new[] { "Assets/Scenes/5Axis3DScene.unity",
            "Assets/Scenes/Login.unity",
            "Assets/Scenes/CanvasGeneral.unity",
            "Assets/Scenes/CanvasPractica2.unity"};
    }

    //Exportaciones
    //PC - 
    static void sim_SPA_PC(string complexType)
    {

        //-------------------------------------------------------------
        //CMD
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        //--idioma option
        initCntroller = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();
        initCntroller.actualIdiom_init = Lang_SPA;
        initCntroller.simulatorIsBilingual.Value = false;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.idiomaActual = Lang_SPA;

        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = true;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_U + "_spa_std/" + simKeyword_U + "_spa_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_K12 + "_spa_std/" + simKeyword_K12 + "_spa_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_k12.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);
        }

        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_PC(string complexType)
    {

        //-------------------------------------------------------------
        //CMD
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        //--idioma option
        initCntroller = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();
        initCntroller.actualIdiom_init = Lang_ENG;
        initCntroller.simulatorIsBilingual.Value = false;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.idiomaActual = Lang_ENG;

        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = true;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_U + "_eng_std/" + simKeyword_U + "_eng_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_K12 + "_eng_std/" + simKeyword_K12 + "_eng_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_k12.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);
        }

        //----------------------------------------------------------------------------------------
    }

    //webGL - free
    static void sim_SPA_webGL_free(string complexType)
    {
        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        //--idioma set
        //IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        //IdioDict.SetIdiom(Lang_SPA);

        //--idioma option
        //initCntroller.simulatorIsBilingual.Value = false;

        segObj.gameObject.SetActive(true);
        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;
        segObj.gameObject.SetActive(false);


        string path = std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        /*if (complexType == "U")
        {
            GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_lti_u/" + simKeyword + "_spa_lti_u", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
            AssetDatabase.Refresh();

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_weblti_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {*/
        //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_lti_free/" + simKeyword + "_spa_lti_free", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_weblti_free.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/


    }

    //webGL - 
    static void sim_SPA_webGL(string complexType)
    {

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        //--idioma option
        initCntroller = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();
        initCntroller.actualIdiom_init = Lang_SPA;
        initCntroller.simulatorIsBilingual.Value = false;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.idiomaActual = Lang_SPA;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);

        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = false;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = true;


        string path = std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_weblti.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

        segController.soSecurityConfiguration.isLtiActive = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

    }
    static void sim_ENG_webGL(string complexType)
    {

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        //--idioma option
        initCntroller = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();
        initCntroller.actualIdiom_init = Lang_ENG;
        initCntroller.simulatorIsBilingual.Value = false;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.idiomaActual = Lang_ENG;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = false;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = true;


        string path = std_folder + simKeyword + "_eng_lti/" + simKeyword + "_eng_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_lti_u/" + simKeyword + "_eng_lti_u", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_weblti_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);/**/

        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_lti/" + simKeyword + "_eng_lti", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_weblti.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);/**/
        }/**/

    }
    static void sim_POR_webGL(string complexType)
    {
        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_POR;
        PlayerSettings.applicationIdentifier = POR_ID;

        //--idioma set
        // IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        // IdioDict.SetIdiom(Lang_ENG);

        //--idioma option
        initCntroller.actualIdiom_init = Lang_POR;
        initCntroller.simulatorIsBilingual.Value = false;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = true;


        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_lti_u/" + simKeyword + "_por_lti_u", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_std_weblti_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);/**/

        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_lti/" + simKeyword + "_por_lti", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_std_weblti.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);/**/
        }/**/

    }
    static void sim_B_webGL(string complexType)
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        /*Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();
        if (complexType == "U")
        {
            GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
        }
        else
        {
            GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
        }*/

        /*GameObject.FindObjectOfType<BaseSimulator>().SecurityModule = false;
        GameObject.FindObjectOfType<BaseSimulator>().MonoUser = false;

        GameObject.FindObjectOfType<BaseSimulator>().Language = Lang_ENG;
        GameObject.FindObjectOfType<BaseSimulator>().LanguageConf = SPA_ENG;
        GameObject.FindObjectOfType<BaseSimulator>().LanguageEnabled = true;*/


        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_bspaeng_lti_u/" + simKeyword + "_bspaeng_lti_u", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BSPAENG_std_weblti_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);/**/

        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_bspaeng_lti/" + simKeyword + "_bspaeng_lti", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BSPAENG_std_weblti.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);/**/
        }/**/

    }

    static void sim_SPA_webGL_AULA()
    {

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        //--idioma option
        initCntroller = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();
        initCntroller.actualIdiom_init = Lang_SPA;
        initCntroller.simulatorIsBilingual.Value = false;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.idiomaActual = Lang_SPA;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);

        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = false;
        segController.soSecurityConfiguration.isModeClassroom = true;
        segController.soSecurityConfiguration.isLtiActive = false;

        segController.soSecurityConfiguration.WebAulaUrl = "http://servercloudlabs.com/validarSimulador/informacionUsuario";

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_spa_webAULA/" + simKeyword + "_spa_webAULA";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_webAULA/" + simKeyword + "_spa_webAULA", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_webAULA.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/


        segController.soSecurityConfiguration.isModeClassroom = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

    }

    static void sim_SPA_webGL_AULA_nuevo()
    {

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        //--idioma option
        initCntroller = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();
        initCntroller.actualIdiom_init = Lang_SPA;
        initCntroller.simulatorIsBilingual.Value = false;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.idiomaActual = Lang_SPA;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);

        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = false;
        segController.soSecurityConfiguration.isModeClassroom = true;
        segController.soSecurityConfiguration.isLtiActive = false;

        segController.soSecurityConfiguration.WebAulaUrl = "https://menu.cloudlabs.us/validarSimulador/informacionUsuario";

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_spa_webAULA_nuevo/" + simKeyword + "_spa_webAULA_nuevo";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_webAULA_nuevo/" + simKeyword + "_spa_webAULA_nuevo", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_webAULA_nuevo.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/


        segController.soSecurityConfiguration.isModeClassroom = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

    }
    static void sim_ENG_webGL_AULA()
    {

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        //--idioma option
        initCntroller = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();
        initCntroller.actualIdiom_init = Lang_ENG;
        initCntroller.simulatorIsBilingual.Value = false;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.idiomaActual = Lang_ENG;

        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = false;
        segController.soSecurityConfiguration.isModeClassroom = true;
        segController.soSecurityConfiguration.isLtiActive = false;

        segController.soSecurityConfiguration.WebAulaUrl = "https://menu.cloudlabs.us/validarSimulador/informacionUsuario";


        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_eng_webAULA/" + simKeyword + "_eng_webAULA";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_webAULA/" + simKeyword + "_eng_webAULA", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_webAULA.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/


        segController.soSecurityConfiguration.isModeClassroom = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

    }
    static void sim_POR_webGL_AULA()
    {

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_POR;
        PlayerSettings.applicationIdentifier = POR_ID;

        //--idioma set
        //IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        //IdioDict.SetIdiom(Lang_SPA);

        //--idioma option
        initCntroller.actualIdiom_init = Lang_POR;
        initCntroller.simulatorIsBilingual.Value = false;

        //segObj.gameObject.SetActive(true);
        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = false;
        segController.soSecurityConfiguration.isModeClassroom = true;
        segController.soSecurityConfiguration.isLtiActive = false;
        //segObj.gameObject.SetActive(false);

        segController.soSecurityConfiguration.WebAulaUrl = "https://menu.cloudlabs.us/validarSimulador/informacionUsuario";


        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_eng_webAULA/" + simKeyword + "_eng_webAULA";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_webAULA/" + simKeyword + "_eng_webAULA", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_webAULA.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/


        segController.soSecurityConfiguration.isModeClassroom = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

    }

    static void sim_ENG_webGL_AULA_nuevo()
    {
        //--idioma set
        //IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        //IdioDict.SetIdiom(Lang_SPA);

        //--idioma option
        initCntroller.actualIdiom_init = Lang_ENG;
        initCntroller.simulatorIsBilingual.Value = false;

        //segObj.gameObject.SetActive(true);
        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = false;
        segController.soSecurityConfiguration.isModeClassroom = true;
        segController.soSecurityConfiguration.isLtiActive = false;
        //segObj.gameObject.SetActive(false);

        segController.soSecurityConfiguration.WebAulaUrl = "https://menu.cloudlabs.us/validarSimulador/informacionUsuario";


        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_eng_webAULA_nuevo/" + simKeyword + "_eng_webAULA_nuevo";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_webAULA_nuevo/" + simKeyword + "_eng_webAULA_nuevo", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_webAULA_nuevo.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/


        segController.soSecurityConfiguration.isModeClassroom = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

    }


    //osx 
    static void sim_SPA_OSX(string complexType)
    {
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        //--idioma option
        initCntroller = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();
        initCntroller.actualIdiom_init = Lang_SPA;
        initCntroller.simulatorIsBilingual.Value = false;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.idiomaActual = Lang_SPA;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);

        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = true;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        //----------------------------------------------------------------------------------------

        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_U + "_spa_s_x/" + PRODUCT_SPA_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_std_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_K12 + "_spa_s_x/" + PRODUCT_SPA_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_std_k12.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);
        }
    }
    static void sim_ENG_OSX(string complexType)
    {
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        //--idioma option
        initCntroller = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();
        initCntroller.actualIdiom_init = Lang_ENG;
        initCntroller.simulatorIsBilingual.Value = false;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.idiomaActual = Lang_ENG;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);

        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = true;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        //----------------------------------------------------------------------------------------

        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_U + "_eng_s_x/" + PRODUCT_ENG_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_OSX_std_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_K12 + "_eng_s_x/" + PRODUCT_ENG_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_OSX_std_k12.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);
        }
    }


    //apk
    static void sim_SPA_APK(string complexType)
    {
        //GameObject.FindObjectOfType<BaseSimulator>().MonoUser = false;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, SPA_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;

        //--idioma option
        initCntroller = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();
        initCntroller.actualIdiom_init = Lang_SPA;
        initCntroller.simulatorIsBilingual.Value = false;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.idiomaActual = Lang_SPA;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);

        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = true;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;


        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();



        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_U + "_spa_std_u.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_K12 + "_spa_std_k12.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);

        }
        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_APK(string complexType)
    {
        //GameObject.FindObjectOfType<BaseSimulator>().MonoUser = false;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, ENG_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;

        //--idioma option
        initCntroller = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();
        initCntroller.actualIdiom_init = Lang_ENG;
        initCntroller.simulatorIsBilingual.Value = false;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.idiomaActual = Lang_ENG;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);

        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = true;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;


        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();



        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_U + "_eng_std_u.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_K12 + "_eng_std_k12.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);

        }
        //----------------------------------------------------------------------------------------
    }

}