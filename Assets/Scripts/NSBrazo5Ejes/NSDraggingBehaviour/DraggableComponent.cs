﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NSBrazo5Ejes.NSDraggingBehaviour
{
	public class DraggableComponent : TransformLimits
	{
		[Space]
		[SerializeField] private Rigidbody _rigidbody;
		[SerializeField] private Transform _target = null;
		/// <summary>
		/// Target a mover del DraggableComponent
		/// </summary>
		/// <returns></returns>
		public Transform Target => (_target != null) ? _target : transform;


		public GameObject[] IgnoreGameObjectDraggableList { get => _ignoreGameObjectDraggableList; set => _ignoreGameObjectDraggableList = value; }
		public Rigidbody Rigidbody { get => _rigidbody; set => _rigidbody = value; }

		[SerializeField] private GameObject[] _ignoreGameObjectDraggableList = null;

		private void Awake()
		{
			if (_rigidbody == null) _rigidbody = GetComponent<Rigidbody>();
		}
		private void OnDrawGizmos()
		{
			// Si ambos límites están definidos
			if (MaxLimit && MinLimit)
			{
				// Defino la posición de Y
				float height = GetLimitHeight();
				// Defino el centro de los límites
				var center = GetCenterVector();
				var difference = MinLimit.position - MaxLimit.position;

				// Define the height
				center.y = height;
				difference.y = 0f;

				// Dibujo el rectangulo
				Gizmos.DrawWireCube(center, difference);
			}
		}
	}
}
