﻿using System;
using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace Modular.Timer
{

	/// <summary>
	/// Temporizador de Inspector para Unity
	/// </summary>
	public class Timer : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private float _seconds = 1f;
		[SerializeField] private bool _fireOnAwake = false;
		[SerializeField] private bool _showDebug = false;

		[Header("Events")]
		[SerializeField] private UnityEvent _onTimerEnd = new UnityEvent();
		[SerializeField] private FloatEvent _onSecondsPassed = new FloatEvent();
		[SerializeField] private FloatEvent _onSecondsPassedNormalized = new FloatEvent();
		[SerializeField] private UnityEvent _onTimerStopped = new UnityEvent();


		private bool _stopped = false;

		/// <summary>
		/// ¿Está el temporizador en ejecución?
		/// </summary>
		/// <value>'true' si el temporizador está en ejecución, si no 'false'</value>
		public bool TimerRunning { get; private set; } = false;

		/// <summary>
		/// Segundos definidos del temporizador
		/// </summary>
		/// <value>Valor 'float' de los segundos</value>
		public float Seconds
		{ get => _seconds; set => _seconds = value; }

		/// <summary>
		/// Evento de ejecución al finalizar el temporizador
		/// </summary>
		/// <value>UnityEvent para el evento de ejecución</value>
		public UnityEvent OnTimerEnd { get => _onTimerEnd; set => _onTimerEnd = value; }
		/// <summary>
		/// Evento de ejecución al interrumpir el temporizador
		/// </summary>
		/// <value>UnityEvent para el evento de ejecución</value>
		public UnityEvent OnTimerStopped { get => _onTimerStopped; set => _onTimerStopped = value; }
		public FloatEvent OnSecondsPassed { get => _onSecondsPassed; set => _onSecondsPassed = value; }
		public FloatEvent OnSecondsPassedNormalized { get => _onSecondsPassedNormalized; set => _onSecondsPassedNormalized = value; }

		private void Awake()
		{
			// Si se puede disparar el temporizador
			if (_fireOnAwake)
			{
				// Disparo el temporizador en Awake
				FireTimer();
			}
		}

		/// <summary>
		/// Ejecuto el temporizador con el tiempo definido
		/// </summary>
		public void FireTimer()
		{

			if (_showDebug) Debug.Log($"Fire", this);

			// Ejecuto la rutina de temporizador
			StartCoroutine(
				TimeEvent(_seconds,             // Segundos a ejecutar el temporizador
				() => OnTimerEnd.Invoke(),      // Evento de ejecución al finalizar
				() => OnTimerStopped.Invoke(),  // Evento de ejecución al detener el temporizador
				(float secondsElapsed) =>
				{
					// Ejecuto los eventos de segundos transcurridos
					_onSecondsPassed.WrapInvoke(secondsElapsed);
					_onSecondsPassedNormalized.WrapInvoke(secondsElapsed / _seconds);
				}
				)
			);

		}

		/// <summary>
		/// Detengo el temporizador
		/// </summary>
		public void StopTimer()
		{
			// Si el temporizador está corriendo
			if (TimerRunning)
			{
				// Detengo el temporizador
				_stopped = true;
			}
		}

		public IEnumerator TimeEvent(float seconds, Action onFinished = null, Action onCancelled = null, Action<float> onSecondsElapsed = null)
		{

			// Si el temporizador se está ejecutando
			if (TimerRunning) yield break;  // No ejecuto la rutina

			if (_showDebug) Debug.Log($"Executing Timer : {gameObject.name}");

			// Defino como en ejecución la rutina
			TimerRunning = true;
			_stopped = false;

			// Ejecuto la espera
			// Defino el tiempo transcurrido inicial
			float elapsed = 0f;

			// Ejecuto los eventos (si existen) de los segundos transcurridos
			if (onSecondsElapsed != null) onSecondsElapsed(0f);

			do
			{
				// Aumento la cuenta del tiempo transcurrido, por el tiempo delta
				elapsed += Time.deltaTime;

				// Ejecuto los eventos (si existen) de los segundos transcurridos
				if (onSecondsElapsed != null) onSecondsElapsed(elapsed);


				// Espero un frame
				yield return null;
			}
			while (elapsed < seconds && !_stopped); // Mientras la ejecución no supere el tiempo límite y no se haya detenido por orden externa

			// Ejecuto los eventos (si existen) de los segundos transcurridos
			if (onSecondsElapsed != null) onSecondsElapsed(seconds);


			// Si fue detenido el temporizador
			if (_stopped)
			{
				// Restablezco el estado actual de la orden de salida
				_stopped = false;

				// Ejecuto el evento de interrupción
				if (onCancelled != null) onCancelled();
			}
			else
			{
				// Si no fue detenido el temporizador

				// Ejecuto la rutina de finalización del temporizador
				if (onFinished != null) onFinished();
			}

			// Defino como finalizada la rutina
			TimerRunning = false;
		}
	}
}