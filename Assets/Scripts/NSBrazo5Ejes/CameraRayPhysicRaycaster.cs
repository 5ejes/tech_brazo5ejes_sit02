﻿using Modular;
using NSUtilities;
using UnityEngine;

namespace NSBrazo5Ejes
{
	public class CameraRayPhysicRaycaster : MonoBehaviour
	{
		[SerializeField] private Camera _targetCamera = null;
		[Header("Settings")]
		[SerializeField] private LayerMask _layerMask = Physics.AllLayers;
		[SerializeField] private float _rayDistance = 24f;
		[SerializeField] private ScriptableEnumID _requiredID = null;
		[Header("Event")]
		[SerializeField] private GameObjectEvent _onGameObjectHitResult = new GameObjectEvent();

		public Camera TargetCamera { get => _targetCamera; set => _targetCamera = value; }
		public LayerMask LayerMask { get => _layerMask; set => _layerMask = value; }
		public float RayDistance { get => _rayDistance; set => _rayDistance = value; }
		public ScriptableEnumID RequiredID { get => _requiredID; set => _requiredID = value; }

		public void RayCastFromCamera(Vector3 position)
		{
			if (TargetCamera == null) return;

			Ray cameraRay = TargetCamera.ScreenPointToRay(position);
			RaycastHit hitResult;

			// Casteo un rayo físico desde la cámara
			if (Physics.Raycast(cameraRay, out hitResult, RayDistance, LayerMask, QueryTriggerInteraction.Collide))
			{
				GameObject hitGameObject = hitResult.transform.gameObject;
				
				// Si el hit Enum es requerido para la comunicación del objeto resultado
				if (RequiredID != null)
				{
					var hitEnumId = hitGameObject.GetComponent<EnumIdentificator>();

					if (hitEnumId)
					{
						if (hitEnumId.EnumID.Equals(RequiredID))
						{
							_onGameObjectHitResult.WrapInvoke(hitGameObject);
						}
					}
				}
				else
				{
					// El hit Enum no es requerido ya que es nulo
					_onGameObjectHitResult.WrapInvoke(hitGameObject);
				}
			}
		}
	}
}
