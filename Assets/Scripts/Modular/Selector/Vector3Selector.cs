﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;

namespace Modular.Selector
{
	public class Vector3Selector : GenericSelector<Vector3>
	{
		[SerializeField] private Vector3Event _onVector3Select;
		[SerializeField] private List<Vector3> _vector3Data;

		private void OnEnable()
		{
			// Inicializo el selector
			InitializeSelector(_onVector3Select);

			// Referencio la lista
			Items = _vector3Data;
		}
	}
}
