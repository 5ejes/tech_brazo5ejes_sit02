﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System;
using SimpleJSON;

public class Login : MonoBehaviour {

	//labels
	public Text titleLabelElement;
	public Text UserLabelElement;
	public Text CourseLabelElement;
	public Text CourseIdLabelElement;
	public Text InstitutionLabelElement;
	//inputfields
	public InputField UserInputElement;
	public InputField CourseInputElement;
	public InputField IdCourseInputElement;
	public InputField InstitutionInputElement;

	//buttons
	public Button loginBt;

	//backgrounds
	public Image background1;
	public Image background2;

	//elements
	public GameObject _alert;
	public GameObject _alertPrefab;

	public GameObject _backgroundPref;
	private GameObject _background;

	void Start () {

		GameObject bg = GameObject.Find ("TransversalBg");
		if (!bg) {
			_background = Instantiate(_backgroundPref, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_background.name = "TransversalBg";
			_background.transform.SetParent (GameObject.Find("Canvas").transform, false);
			_background.transform.localPosition = new Vector3(0,0,0);
			_background.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () - 1);
		}

		configureElement ();

	}

	void configureElement(){

		titleLabelElement.font = (Font)Resources.Load("Fonts/ArialBold28");
		UserLabelElement.font = (Font)Resources.Load("Fonts/ArialBold31");
		CourseLabelElement.font = (Font)Resources.Load("Fonts/ArialBold31");
		CourseIdLabelElement.font = (Font)Resources.Load("Fonts/ArialBold31");
		InstitutionLabelElement.font = (Font)Resources.Load("Fonts/ArialBold31");
		loginBt.GetComponentInChildren<Text>().font = (Font)Resources.Load("Fonts/ArialBold34");

		if (Manager.Instance.globalAula == true) {
			background1.enabled = false;
			CourseIdLabelElement.enabled = false;
			InstitutionLabelElement.enabled = false;
			CourseInputElement.inputType = InputField.InputType.Password;
			IdCourseInputElement.gameObject.SetActive (false);
			InstitutionInputElement.gameObject.SetActive (false);
			loginBt.transform.localPosition = new Vector3 (13.6f,-164f,0f);

			titleLabelElement.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='login']").InnerText;
			UserLabelElement.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='user']").InnerText;
			CourseLabelElement.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='password']").InnerText;

			UserInputElement.placeholder.GetComponent<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='user']").InnerText + "...";
			CourseInputElement.placeholder.GetComponent<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='password']").InnerText + "...";

			if (Manager.Instance.globalMonoUser == true) {
				UserInputElement.text = Manager.Instance.globalMonoUserEmail;
				UserInputElement.enabled = false;
				CourseInputElement.text = Manager.Instance.globalMonoUserEmail;
				CourseInputElement.enabled = false;
			} else {
				UserInputElement.text = "";
				CourseInputElement.text = "";
			}

			loginBt.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='login_bt']").InnerText;
			Button btn1 = loginBt.GetComponent<Button>();
			btn1.onClick.RemoveAllListeners();
			btn1.onClick.AddListener(LoginAulaClick);

		} else {
			background2.enabled = false;
			background1.enabled = true;
			CourseIdLabelElement.enabled = true;
			InstitutionLabelElement.enabled = true;
			CourseInputElement.inputType = InputField.InputType.Standard;
			IdCourseInputElement.gameObject.SetActive (true);
			InstitutionInputElement.gameObject.SetActive (true);
			loginBt.transform.localPosition = new Vector3 (13.6f,-347f,0f);

			titleLabelElement.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='login']").InnerText;
			UserLabelElement.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='user']").InnerText;
			CourseLabelElement.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='course']").InnerText;
			CourseIdLabelElement.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='idcourse']").InnerText;
			InstitutionLabelElement.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='institution']").InnerText;

			UserInputElement.placeholder.GetComponent<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='user']").InnerText + "...";
			CourseInputElement.placeholder.GetComponent<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='course']").InnerText + "...";
			IdCourseInputElement.placeholder.GetComponent<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='idcourse']").InnerText + "...";
			InstitutionInputElement.placeholder.GetComponent<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='institution']").InnerText + "...";

			if (Manager.Instance.globalUser != "") {
				UserInputElement.text = Manager.Instance.globalUser;
				UserInputElement.enabled = false;
			} else {
				UserInputElement.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='user_input']").InnerText;	
			}

			if (Manager.Instance.globalCourse != "") {
				CourseInputElement.text = Manager.Instance.globalCourse;
				CourseInputElement.enabled = false;
			} else {
				CourseInputElement.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='course_input']").InnerText;
			}

			if (Manager.Instance.globalCourseId != "") {
				IdCourseInputElement.text = Manager.Instance.globalCourseId;
				IdCourseInputElement.enabled = false;
			} else {
				IdCourseInputElement.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='idcourse_input']").InnerText;
			}

			if (Manager.Instance.globalInstitution != "") {
				InstitutionInputElement.text = Manager.Instance.globalInstitution;
				InstitutionInputElement.enabled = false;
			} else {
				InstitutionInputElement.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='institution_input']").InnerText;
			}

			if (Manager.Instance.globalMonoUser == true) {
				UserInputElement.text = Manager.Instance.globalMonoUserName;
				UserInputElement.enabled = false;
				InstitutionInputElement.text = Manager.Instance.globalMonoUserInstitution;
				InstitutionInputElement.enabled = false;
			}

			loginBt.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='login_bt']").InnerText;
			Button btn1 = loginBt.GetComponent<Button>();
			btn1.onClick.RemoveAllListeners();
			btn1.onClick.AddListener(LoginClick);	
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void LoginClick()
	{
		//Output this to console when the Button is clicked
		if (UserInputElement.text == "" || CourseInputElement.text == "" || IdCourseInputElement.text == "" || InstitutionInputElement.text == "") {
			_alert = Instantiate(_alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_alert.transform.SetParent (GameObject.Find("Canvas").transform, false);
			_alert.transform.localPosition = new Vector3(-700,700,0);
			_alert.GetComponent<Alert> ().showAlert (1, "", "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='login_empty']").InnerText, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText);	
		} else {
			Manager.Instance.logged = true;
			Manager.Instance.globalUser = UserInputElement.text;
			Manager.Instance.globalCourse = CourseInputElement.text;
			Manager.Instance.globalCourseId = IdCourseInputElement.text;
			Manager.Instance.globalInstitution = InstitutionInputElement.text;

			Vector3 _position = new Vector3 (0, 1200, 0);

			iTween.MoveTo(
				gameObject,
				iTween.Hash(
					"position", _position,
					"looktarget", Camera.main,
					"easeType", iTween.EaseType.easeOutExpo,
					"time", 1f,
					"islocal",true
				)
			);

			Image _bg = GameObject.Find ("TransversalBg").GetComponent<Image>();
			_bg.CrossFadeAlpha (0f, 1f, false);
			Destroy(GameObject.Find ("TransversalBg"), 1.4f);

			Destroy(gameObject, 1.5f);
		}
	}

	void LoginAulaClick(){
		if (UserInputElement.text == "" || CourseInputElement.text == "") {
			_alert = Instantiate (_alertPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
			_alert.transform.SetParent (GameObject.Find ("Canvas").transform, false);
			_alert.transform.localPosition = new Vector3 (-700, 700, 0);
			_alert.GetComponent<Alert> ().showAlert (1, "", "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='login_empty']").InnerText, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText);	
		} else {
			Manager.Instance.globalUserAula = UserInputElement.text;
			string _params = "{\"user\":\"" + UserInputElement.text + "\",\"pass\":\"" + Md5Sum(CourseInputElement.text) + "\"}";

			byte[] bytesToEncode = Encoding.UTF8.GetBytes(_params);
			string encodedText = Convert.ToBase64String(bytesToEncode);

			string url_get_aula = Manager.Instance.globalUrlAula + "/externals/login?data=" + encodedText;

			Debug.Log(url_get_aula);

			WWW wwwAula = new WWW(url_get_aula);
			StartCoroutine(WaitForRequestAula(wwwAula));

			loginBt.enabled = false;

		}
	}

	IEnumerator WaitForRequestAula(WWW www)
	{
		yield return www;

		if (www.error == null)
		{
			byte[] decodedBytes = Convert.FromBase64String(www.text);
			string decodedText = Encoding.UTF8.GetString(decodedBytes);
			Debug.Log(decodedText);
			var _data = JSON.Parse(decodedText);

			if (_data["state"] != null)
			{
				if (_data["state"].Value == "true")
				{
					if (_data["res_code"] != null)
					{
						switch (_data["res_code"].Value)
						{
						case "INVALID_USER_PASS":
							loginBt.enabled = true;
							_alert = Instantiate (_alertPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
							_alert.transform.SetParent (GameObject.Find ("Canvas").transform, false);
							_alert.transform.localPosition = new Vector3 (-700, 700, 0);
							_alert.GetComponent<Alert> ().showAlert (1, "", "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='invalid_user_pass']").InnerText, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText);
							break;
						case "LAB_NOT_ASSIGNED":
							loginBt.enabled = true;
							_alert = Instantiate (_alertPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
							_alert.transform.SetParent (GameObject.Find ("Canvas").transform, false);
							_alert.transform.localPosition = new Vector3 (-700, 700, 0);
							_alert.GetComponent<Alert> ().showAlert (1, "", "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='lab_not_assigned']").InnerText, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText);
							break;
						case "LOGIN_OK":
							Manager.Instance.logged = true;
							Manager.Instance.globalUser = _data["name"].Value + " " + _data["last_name"].Value;
							Manager.Instance.globalCourse = _data["class_group"].Value;
							Manager.Instance.globalCourseId = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='idcourse_input']").InnerText;
							Manager.Instance.globalInstitution = _data["school_name"].Value;

							Vector3 _position = new Vector3 (0, 1200, 0);

							iTween.MoveTo(
								gameObject,
								iTween.Hash(
									"position", _position,
									"looktarget", Camera.main,
									"easeType", iTween.EaseType.easeOutExpo,
									"time", 1f,
									"islocal",true
								)
							);

							Image _bg = GameObject.Find ("TransversalBg").GetComponent<Image>();
							_bg.CrossFadeAlpha (0f, 1f, false);
							Destroy(GameObject.Find ("TransversalBg"), 1.4f);

							Destroy(gameObject, 1.5f);
							break;
						case "DB_EXCEPTION":
							loginBt.enabled = true;
							_alert = Instantiate (_alertPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
							_alert.transform.SetParent (GameObject.Find ("Canvas").transform, false);
							_alert.transform.localPosition = new Vector3 (-700, 700, 0);
							_alert.GetComponent<Alert> ().showAlert (1, "", "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='db_exception']").InnerText, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText);
							break;
						case "LICENSE_EXPIRED":
							loginBt.enabled = true;
							_alert = Instantiate (_alertPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
							_alert.transform.SetParent (GameObject.Find ("Canvas").transform, false);
							_alert.transform.localPosition = new Vector3 (-700, 700, 0);
							_alert.GetComponent<Alert> ().showAlert (1, "", "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='license_expired_offline']").InnerText, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText);
							break;
						}
						Debug.Log("check + :" + _data["res_code"].Value);
					}
					else
					{
						loginBt.enabled = true;
						_alert = Instantiate (_alertPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
						_alert.transform.SetParent (GameObject.Find ("Canvas").transform, false);
						_alert.transform.localPosition = new Vector3 (-700, 700, 0);
						_alert.GetComponent<Alert> ().showAlert (1, "", "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='invalid_response']").InnerText, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText);
					}
				}
				else
				{
					loginBt.enabled = true;
					_alert = Instantiate (_alertPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
					_alert.transform.SetParent (GameObject.Find ("Canvas").transform, false);
					_alert.transform.localPosition = new Vector3 (-700, 700, 0);
					_alert.GetComponent<Alert> ().showAlert (1, "", "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='invalid_response']").InnerText, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText);
				}
			}
			else
			{
				loginBt.enabled = true;
				_alert = Instantiate (_alertPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
				_alert.transform.SetParent (GameObject.Find ("Canvas").transform, false);
				_alert.transform.localPosition = new Vector3 (-700, 700, 0);
				_alert.GetComponent<Alert> ().showAlert (1, "", "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='invalid_response']").InnerText, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText);
			}

		}
		else
		{
			//two buttons
			loginBt.enabled = true;
			_alert = Instantiate (_alertPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
			_alert.transform.SetParent (GameObject.Find ("Canvas").transform, false);
			_alert.transform.localPosition = new Vector3 (-700, 700, 0);
			_alert.GetComponent<Alert> ().showAlert (2, "", "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='no_internet_aula']").InnerText, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='try_again']").InnerText,Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='offline']").InnerText, tryAgainAction, offLineAction);
		}
	}

	void tryAgainAction(){
		_alert.GetComponent<Alert> ().closeAlert ();
		LoginAulaClick ();
	}

	void offLineAction(){
		Manager.Instance.globalAula = false;
		configureElement ();
		_alert.GetComponent<Alert> ().closeAlert ();
	}

	public string Md5Sum(string strToEncrypt)
	{
		UTF8Encoding ue = new System.Text.UTF8Encoding();
		byte[] bytes = ue.GetBytes(strToEncrypt);

		// encrypt bytes
		System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
		byte[] hashBytes = md5.ComputeHash(bytes);

		// Convert the encrypted bytes back to a string (base 16)
		string hashString = "";

		for (int i = 0; i < hashBytes.Length; i++)
		{
			hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
		}

		return hashString.PadLeft(32, '0');
	}
}
