﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class solenoidClass : MonoBehaviour {

	public enum typesSolenoid
	{
		Simple,
		Double
	}

	public typesSolenoid _typeSolenoid;
	public GameObject _parent;
	public int numberSolenoid;
	public Image _actualImage;

	public int actualValue = 0;

	// Use this for initialization
	void Start () {
		
	}

	public string getText(){
		string _result = "";

		switch(_typeSolenoid){
		case typesSolenoid.Simple:
			_result = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='simple_solenoid']").InnerText;
			break;
		case typesSolenoid.Double:
			if (numberSolenoid == 1) {
				_result = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='double_solenoid_a']").InnerText;
			} else {
				_result = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='double_solenoid_b']").InnerText;
			}
			break;
		}

		return _result;
	}

	public int getUsedRow(){
		if(_parent.GetComponent<actuatorElement>() != null){
			return (int)_parent.GetComponent<actuatorElement> ().rowsColumnsUsed [0].x;
		}
		else if(_parent.GetComponent<motorActuatorElement>() != null){
			return (int)_parent.GetComponent<motorActuatorElement> ().rowsColumnsUsed [0].x;
		}
		else {
			return -1;
		}
	}

	public int getUsedColumn(){
		if(_parent.GetComponent<actuatorElement>() != null){
			return (int)_parent.GetComponent<actuatorElement> ().rowsColumnsUsed [0].y;
		}
		else if(_parent.GetComponent<motorActuatorElement>() != null){
			return (int)_parent.GetComponent<motorActuatorElement> ().rowsColumnsUsed [0].y;
		}
		else {
			return -1;
		}
	}

	public void setValue(int _value){
		actualValue = _value;
	}

	// Update is called once per frame
	void Update () {
		if(_typeSolenoid == typesSolenoid.Double && numberSolenoid == 2){
			_actualImage.enabled = false;
		}
	}
}
