﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ElectricalTestBenchMainClass : BaseSituation{

	[Header("SITUATION GAMEOBJECTS", order = 2)]
	public GameObject _upperBars;
	public GameObject _deleteGrid;
	public GameObject _backgroundPref;
	public GameObject _leftOvers;
	public GameObject _rightOvers;
	public Text _msgArea;
	public Text _btnRedText;

	[Header("SITUATION VARIABLES", order = 3)]
	public int simulatorInputs = 16;
	public int simulatorOutputs = 16;

	public int usedInputs = 0;
	public int usedOutputs = 0;
	public GameObject[] inputElements;
	public GameObject[] outputsElements;

	public bool[] outputsValues;

	public Image Fundido_;

	[HideInInspector]
	public GameObject _background;
	[HideInInspector]
	public string DescriptionPractice;
	[HideInInspector]
	public string var_scene;

	private CalCient ScriptCalCient;
	private Text TextBoton;

	private bool upperIndicatorUpdated = false;

	void Start () {
		outputsValues = new bool[simulatorOutputs];
	}

	public void checkLanguageComponent(){
		if (Manager.Instance.globalLanguageEnabled == false && Manager.Instance.globalGraphicInterface == baseSystem.graphicInterfaces.electrical_bench) {
			GameObject _langBtToHide = GameObject.Find ("lang_bt");
			_langBtToHide.SetActive (false);
		}
	}

	public void setTexts(){
		_upperBars.GetComponent<upperBars> ().setTexts ();
		_btnRedText.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='delete']").InnerText;
	}

	public void loadSituation(){
		_cam1.enabled = true;
		_cam2.enabled = false;
		//TODO: check report name, for situation 1 is name_report_1

		base.StartBaseSituation();

		if (Manager.Instance.recoveryUpper == true) {
			//Manager.Instance.recoveryUpper = false;
		}

		//configure situation
		setTexts();

		inputElements = new GameObject[simulatorInputs];
		outputsElements = new GameObject[simulatorOutputs];
		usedInputs = 0;
		usedOutputs = 0;

		GameObject infoBt = GameObject.Find("info_bt");
		Button btn1 = infoBt.GetComponent<Button>();
		btn1.onClick.AddListener(callInfoAlert);

		GameObject _help_obj = GameObject.Find("help_bt");
		_help_obj.GetComponent<Button>().onClick.AddListener(callHelpAlert);

		GameObject trashBt = GameObject.Find("trash_bt");
		trashBt.GetComponent<Button> ().onClick.AddListener (callTrashAction);

		GameObject _goto_obj = GameObject.Find("goto_bt");
		_goto_obj.GetComponent<Button>().onClick.AddListener(callGoToAction);

		GameObject _language_obj = GameObject.Find("language_bt");
		if (_language_obj != null) {
			_language_obj.GetComponent<Button> ().onClick.AddListener (callLanguageAction);
		}

		//Fundido_ = GameObject.Find("Fundido").GetComponent<Image>();

		//Fundido_.transform.SetAsLastSibling ();
		//Fundido_.CrossFadeAlpha(0, 0.5f, false);
		//Fundido_.raycastTarget = false;
		switch (Manager.Instance.globalController) {
		case baseSystem.Controllers.Grafcet:
			Manager.Instance.currentPDFName = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='name_report_1']").InnerText;
			break;
		case baseSystem.Controllers.Ladder:
			Manager.Instance.currentPDFName = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='name_report_3']").InnerText;
			break;
		}
	}

	public int addInputElement(GameObject _element){
		int _result = 0;

		for(int i = 0; i < simulatorInputs; i++){
			if (inputElements [i]) {
				
			} else {
				inputElements [i] = _element;
				_result = i;
				break;
			}
		}

		usedInputs++;
		_upperBars.GetComponent<upperBars> ().updateInputsDropdown ();

		return _result;
	}

	public int addOutputElement(GameObject _element){
		int _result = 0;

		for(int i = 0; i < simulatorOutputs; i++){
			if (outputsElements [i]) {

			} else {
				outputsElements [i] = _element;
				_result = i;
				break;
			}
		}

		usedOutputs++;
		_upperBars.GetComponent<upperBars> ().updateOutputsDropdown ();

		return _result;
	}

	public void redButtonAction(){
		_deleteGrid.GetComponent<gridDelete> ().showGrid ();
	}

	public void callBack()
    {
        var_scene = "menu";
        _alert = Instantiate(_alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        _alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
        _alert.transform.localPosition = new Vector3(-700, 700, 0);
		_alert.GetComponent<Alert>().showAlert(2, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='simulator_title']").InnerText, Manager.Instance.globalInfo.SelectSingleNode("/data/" + base.situationTag + "/name_practice").InnerText, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='desea_salir']").InnerText, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='accept']").InnerText, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='cancel']").InnerText, acceptback);
    }

    private void acceptback(){

        SceneManager.LoadScene(var_scene);
    }

    private void callInfoAlert()
    {
        _alert = Instantiate(_alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        _alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
        _alert.transform.localPosition = new Vector3(-700, 700, 0);
		DescriptionPractice = Manager.Instance.globalInfo.SelectSingleNode("/data/" + base.situationTag + "/description_practice").InnerText;
		_alert.GetComponent<Alert>().showAlert(7, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='situation']").InnerText + "|" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='procedure']").InnerText, Manager.Instance.globalInfo.SelectSingleNode("/data/" + base.situationTag + "/name_practice").InnerText, DescriptionPractice + "|" + Manager.Instance.globalInfo.SelectSingleNode("/data/" + base.situationTag + "/procedure").InnerText);
    }

	private void callGoToAction(){
		Fundido_.transform.SetAsLastSibling ();
		Fundido_.CrossFadeAlpha(1, 0.5f, false);
		Fundido_.raycastTarget = true;

		GameObject.Find ("SystemController").GetComponent<baseSystem>().goToController();
	}

	private void callTrashAction(){
		int _row = -1;
		int _column = -1;

		for (int i = 0; i < inputElements.Length; i++) {
			if (inputElements [i]) {
				_row = inputElements [i].GetComponent<dragElement> ().rowUsed;
				_column = inputElements [i].GetComponent<dragElement> ().columnUsed;

				_leftOvers.GetComponent<benchAvailability> ().setAreaAvailable (_row, _column);	

				if(inputElements[i].GetComponent<dragElement>()._leftCable){
					Destroy (inputElements[i].GetComponent<dragElement>()._leftCable);
				}
				if(inputElements[i].GetComponent<dragElement>()._rightCable){
					Destroy (inputElements[i].GetComponent<dragElement>()._rightCable);
				}
				if(inputElements[i].GetComponent<dragElement>()._topCable){
					Destroy (inputElements[i].GetComponent<dragElement>()._topCable);
				}

				Destroy(inputElements[i]);
				inputElements [i] = null;
				usedInputs--;
			}
		}

		for (int i = 0; i < outputsElements.Length; i++) {
			if (outputsElements [i]) {
				_row = outputsElements [i].GetComponent<dragElement> ().rowUsed;
				_column = outputsElements [i].GetComponent<dragElement> ().columnUsed;

				if (outputsElements [i].GetComponent<dragElement> ()._leftCable) {
					Destroy (outputsElements [i].GetComponent<dragElement> ()._leftCable);
				}
				if (outputsElements [i].GetComponent<dragElement> ()._rightCable) {
					Destroy (outputsElements [i].GetComponent<dragElement> ()._rightCable);
				}
				if (outputsElements [i].GetComponent<dragElement> ()._topCable) {
					Destroy (outputsElements [i].GetComponent<dragElement> ()._topCable);
				}

				Destroy (outputsElements [i]);
				outputsElements [i] = null;
				usedOutputs--;

				_rightOvers.GetComponent<benchAvailability> ().setAreaAvailable (_row, _column);
			}
		}

		_upperBars.GetComponent<upperBars> ().updateOutputsDropdown ();
		_upperBars.GetComponent<upperBars> ().updateInputsDropdown ();
		_upperBars.GetComponent<upperBars> ().setInputValueIndicators ();

	}

    private void callHelpAlert()
    {
        _alert = Instantiate(_alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        _alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
        _alert.transform.localPosition = new Vector3(-700, 700, 0);
		_alert.GetComponent<Alert>().showAlert(4, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='help']").InnerText, "", Manager.Instance.globalInfo.SelectSingleNode("/data/" + base.situationTag + "/tips").InnerText);
    }

	private void callLanguageAction()
    {
		GameObject _langBt = GameObject.Find ("lang_bt");
		_langBt.GetComponent<languageBt> ().showMenu (setNewLanguageFromMenu);
    }

	public void setNewLanguageFromMenu(){
		GameObject.Find ("SystemController").GetComponent<baseSystem>().Language = Manager.Instance.globalLanguage;
		StartCoroutine (GameObject.Find ("SystemController").GetComponent<baseSystem> ().loadLanguageXML (false, false));
		setTextsAfterChangeLanguage ();
		GameObject.Find ("SystemController").GetComponent<baseSystem> ()._grafcet.GetComponent<GrafcetMainClass> ().setTexts ();
	}

	public void setTextsAfterChangeLanguage(){
		_upperBars.GetComponent<upperBars> ().setTexts ();
		_btnRedText.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='delete']").InnerText;
		_upperBars.GetComponent<upperBars> ().updateInputsDropdown ();
		_upperBars.GetComponent<upperBars> ().updateOutputsDropdown ();
	}

	public void activateBackground(){
		_background = Instantiate(_backgroundPref, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_background.name = "SituationBg";
		_background.transform.SetParent (GameObject.Find("Canvas/ElectricalBench").transform, false);
		_background.transform.localPosition = new Vector3(0,0,0);
		_background.transform.SetAsLastSibling ();
	}

	public void deactivateBackground(){
		Image _bg = _background.GetComponent<Image>();
		_bg.CrossFadeAlpha (0f, 1f, false);
		Destroy(_background, 1.2f);
	}
		
	void Update(){

		/*
		if(postLanguageSelectorReady == true){
			postLanguageSelectorReady = false;

			_upperBars.GetComponent<upperBars> ().setTexts ();
			_btnRedText.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='delete']").InnerText;
			_upperBars.GetComponent<upperBars> ().updateInputsDropdown ();
			_upperBars.GetComponent<upperBars> ().updateOutputsDropdown ();

		}

		if (Manager.Instance.logged == true) {
			if(upperIndicatorUpdated == false){
				upperIndicatorUpdated = true;
				_upperIndicator.SetActive (true);
				_upperIndicator.GetComponent<UpperIndicator> ().UserName.text = Manager.Instance.globalUser;

				_upperIndicator.GetComponent<UpperIndicator> ().startTimer ();
			}
		} else {
			_upperIndicator.SetActive (false);
		}
		*/
	}
		
}
