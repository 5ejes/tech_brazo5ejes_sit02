﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class dragElementLadder : MonoBehaviour {

	public enum _type_elements
	{
		open_contact,
		close_contact,
		open_coil,
		close_coil,
		set_coil,
		reset_coil,
		add,
		subs,
		prod,
		division,
		equal,
		different,
		great,
		great_equal,
		less,
		less_equal,
		and,
		or,
		not,
		xor,
		mov,
		ctu,
		ctd,
		ton,
		toff,
		tp,
		reset,
		none,
		pid
	}

	public enum _typesIO
	{
		Input,
		Output,
		none
	}

	public GameObject _situation;

	public _type_elements typeElement;
	public _typesIO typeIO;
	public string textTag;

	public bool dragEnabled = true;
	private bool objectDragged;
	private Vector3 initialPosition;
	private Vector3 startPosition;
	private int initialIndex;
	private int startIndex;
	private GameObject _initialParent;

	private bool _mouseOver = false;

	// Use this for initialization
	void Start () {
		initialPosition = gameObject.transform.localPosition;
		initialIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

		_initialParent = gameObject.transform.parent.gameObject;

		gameObject.AddComponent<EventTrigger> ();

		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();

		EventTrigger.Entry entry_0 = new EventTrigger.Entry();
		entry_0.eventID = EventTriggerType.BeginDrag;
		entry_0.callback.AddListener((data) => { BeginDrag_((PointerEventData)data); });

		EventTrigger.Entry entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.Drag;
		entry_1.callback.AddListener((data) => { ObjectDrag_((PointerEventData)data); });

		EventTrigger.Entry entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.EndDrag;
		entry_2.callback.AddListener((data) => { ObjectDrop_((PointerEventData)data); });

		EventTrigger.Entry entry_3 = new EventTrigger.Entry();
		entry_3.eventID = EventTriggerType.PointerEnter;
		entry_3.callback.AddListener((data) => { ObjectMouseOver_((PointerEventData)data); });

		EventTrigger.Entry entry_4 = new EventTrigger.Entry();
		entry_4.eventID = EventTriggerType.PointerExit;
		entry_4.callback.AddListener((data) => { ObjectMouseOut_((PointerEventData)data); });

		trigger_Object.triggers.Add(entry_0);
		trigger_Object.triggers.Add(entry_1);
		trigger_Object.triggers.Add(entry_2);
		trigger_Object.triggers.Add(entry_3);
		trigger_Object.triggers.Add(entry_4);

		_situation = GameObject.Find ("Ladder");
	}

	private void BeginDrag_(PointerEventData data)
	{
		if(dragEnabled == true){
			objectDragged = true;

			gameObject.transform.SetParent (GameObject.Find ("Canvas/Ladder").transform, true);

			startPosition = gameObject.transform.position;
			startIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

			gameObject.GetComponent<RectTransform> ().SetAsLastSibling();

			lineGroupObject[] lineGroupObjects = _situation.GetComponent<LadderMainClass>()._workbenchArea.GetComponentsInChildren<lineGroupObject>();
			if (lineGroupObjects.Length > 0) {
				for(int i = 0; i < lineGroupObjects.Length; i++){
					lineGroupObjects [i].showWorkbenchColliders ();
				}
			}
		}
	}

	private void ObjectDrag_(PointerEventData data)
	{
		if (objectDragged == true) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			gameObject.GetComponent<RectTransform>().position = new Vector3(mousePos.x, mousePos.y, 0);

			if (data.pointerEnter) {
				if(data.pointerEnter.GetComponent<ladderColliderComponent>() != null ){
					GameObject actualObject = data.pointerEnter.GetComponent<ladderColliderComponent> ().parentObject;
					if(actualObject.GetComponent<lineObject> ().typeElement == _type_elements.none){
						actualObject.GetComponent<lineObject> ().overActive = true;
						actualObject.GetComponent<lineObject> ().setOverComponent (typeElement, typeIO);	
					}
				}
			}
		}
	}

	private void ObjectDrop_(PointerEventData data)
	{
		if (objectDragged == true) {
			objectDragged = false;
			lineGroupObject[] lineGroupObjects = _situation.GetComponent<LadderMainClass>()._workbenchArea.GetComponentsInChildren<lineGroupObject>();
			if (lineGroupObjects.Length > 0) {
				for(int i = 0; i < lineGroupObjects.Length; i++){
					lineGroupObjects [i].hideWorkbenchColliders ();
				}
			}
				
			if (data.pointerEnter) {
				if (data.pointerEnter.GetComponent<ladderColliderComponent> () != null) {
					GameObject actualObject = data.pointerEnter.GetComponent<ladderColliderComponent> ().parentObject;
					if (actualObject.GetComponent<lineObject> ().typeElement == _type_elements.none) {
						_situation.GetComponent<LadderMainClass>().debugMode = false;
						actualObject.GetComponent<lineObject> ().overActive = false;
						actualObject.GetComponent<lineObject> ().setComponent (typeElement, typeIO);

						gameObject.transform.position = startPosition;
						goToInitialState ();
					} else {
						tweenToInitialPosition ();
					}
				} else {
					tweenToInitialPosition ();
				}
			} else {
				tweenToInitialPosition ();
			}
		}
	}

	private void ObjectMouseOver_(PointerEventData data){
		if(objectDragged == false){
			_mouseOver = true;
			_situation.GetComponent<LadderMainClass> ().showInfoText (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='" + textTag + "']").InnerText);	
		}
	}

	private void ObjectMouseOut_(PointerEventData data){
		if(_mouseOver == true){
			_mouseOver = false;
			_situation.GetComponent<LadderMainClass> ().hideInfoText ();
		}
	}

	private void tweenToInitialPosition(){
		iTween.MoveTo(
			gameObject,
			iTween.Hash(
				"position", startPosition,
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 0.2f,
				"islocal",false
			)
		);

		Invoke ("goToInitialState", 0.2f);
	}

	private void goToInitialState(){
		gameObject.GetComponent<RectTransform> ().SetSiblingIndex (initialIndex);
		gameObject.transform.SetParent (_initialParent.transform, true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
