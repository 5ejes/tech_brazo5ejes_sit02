#pragma warning disable 0649
using UnityEngine;

namespace NSUtilities
{
    public class CanvasScale : MonoBehaviour
    {
        #region members

        [SerializeField] private Canvas canvasMain;
        
        private float canvasWidthFactor;

        private float canvasHeightFactor;

        public float CanvasWidthFactor
        {
            get { return canvasWidthFactor; }
        }

        public float CanvasHeightFactor
        {
            get { return canvasHeightFactor; }
        }

        #endregion
        
        #region mono behaviour

        private void Awake()
        {
            var tmpCanvasPixelPerfect = canvasMain.pixelRect;
            canvasWidthFactor = tmpCanvasPixelPerfect.width / 1920f;
            canvasHeightFactor = tmpCanvasPixelPerfect.height / 1080f;
        }

        #endregion
    }
}