﻿using System.Collections;
using System.Collections.Generic;
using NSBrazo5Ejes;
using NSCamera3DControl;
using NSInspectorValue;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace NSContextosLocalesBrazo
{
	public class ArmLocalContext : MonoBehaviour
	{
		[Header("Arm Context Settings")]
		[SerializeField] private ArmPositionHandler _positionHandler = null;
		[SerializeField] private ArmGrafcetReader _grafcetReader = null;
		[SerializeField] private CameraManager _localCameraManager = null;
		[SerializeField] private CubeManager _localCubeManager = null;
		[SerializeField] private FreeLookCameraHandler _localFreelook = null;
		[SerializeField] private BoolInspectorValue _gripValueState = null;

		[Header("Events")]
		[SerializeField] private ArmLocalContextEvent _onContextActivation;
		[SerializeField] private UnityEvent _playButtonHandler;

		public CameraManager LocalCameraManager { get => _localCameraManager; set => _localCameraManager = value; }
		public ArmGrafcetReader GrafcetReader { get => _grafcetReader; set => _grafcetReader = value; }
		public CubeManager LocalCubeManager { get => _localCubeManager; set => _localCubeManager = value; }
		public ArmPositionHandler PositionHandler { get => _positionHandler; set => _positionHandler = value; }
		public FreeLookCameraHandler LocalFreelook { get => _localFreelook; set => _localFreelook = value; }
		public UnityEvent PlayButtonHandler { get => _playButtonHandler; set => _playButtonHandler = value; }
		public BoolInspectorValue GripValueState { get => _gripValueState; set => _gripValueState = value; }

		public void Send()
		{
			// Envio el evento via el editor
			_onContextActivation.WrapInvoke(this);
		}

	}
}
