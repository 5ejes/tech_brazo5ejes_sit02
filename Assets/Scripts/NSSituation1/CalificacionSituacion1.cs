﻿
using UnityEngine;

namespace NSSituacion1
{
    public class CalificacionSituacion1 : MonoBehaviour
    {
        private byte mensajesEnviadosCentroControl;

        #region accesores

        public byte _mensajesEnviadosCentroControl
        {
            get
            {
                return mensajesEnviadosCentroControl;
            }
        }

        #endregion
        public void AgregarMensajesEnviados()
        {
            mensajesEnviadosCentroControl++;
        }
    } 
}