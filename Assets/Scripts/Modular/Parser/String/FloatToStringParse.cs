﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using static NSScriptableEvent.ScriptableEventString;

namespace Modular.Parser
{
	public class FloatToStringParse : GenericParser<float>
	{
		[SerializeField] private StringEvent _onResultValue = new StringEvent();
		[Header("Settings")]
		[SerializeField] private string _toStringFormat = string.Empty;
		public StringEvent OnResultValue { get => _onResultValue; set => _onResultValue = value; }

		public override void Parse(float valueToParse)
		{

			// Disparo el evento
			OnParseAction.WrapInvoke();

			// Recibo el valor y lo convierto en un output
			var output = valueToParse.ToString(_toStringFormat);

			// Si el valor output existe
			if (!string.IsNullOrEmpty(output))
			{
				// Comunico el resultado
				OnResultValue.WrapInvoke(output);
			}
		}
	}
}
