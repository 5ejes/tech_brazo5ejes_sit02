﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;

namespace Modular
{
	public class Vector3Lerp : MonoBehaviour
	{
		[Header("Values")]
		[SerializeField] private Vector3 _a;
		[SerializeField] private Vector3 _b;
		[Header("Settings")]
		[SerializeField] private float _timeout = 1f;
		[SerializeField] private bool _useSLerp;
		[Header("Events")]
		[SerializeField] private Vector3Event _onVector3LerpStart;
		[SerializeField] private Vector3Event _onVector3Lerp;
		[SerializeField] private Vector3Event _onVector3LerpEnd;

		[Header("Debug")]
		[SerializeField] private Vector3 _currentValue;

		public Vector3 A { get => _a; set => _a = value; }
		public Vector3 B { get => _b; set => _b = value; }
		public float Timeout { get => _timeout; set => _timeout = value; }
		public bool UseSLerp { get => _useSLerp; set => _useSLerp = value; }

		public Vector3 CurrentValue { get => _currentValue; private set => _currentValue = value; }

		public bool IsLerpToggle { get; set; }
		public bool IsLerping { get; set; }

		public void ToggleLerp()
		{
			// Defino los datos de la corrutina
			var a = IsLerpToggle ? A : B;
			var b = !IsLerpToggle ? A : B;

			// Ejecuto el toggle
			ToggleAndLerp(a, b);
		}

		private void ToggleAndLerp(Vector3 a, Vector3 b)
		{
			// Defino la corutina
			var coroutine = LerpRoutine(a, b, Timeout, UseSLerp);

			// Hago switch al toggle
			IsLerpToggle = !IsLerpToggle;

			// Detengo e inicializo la corutina
			StopCoroutine(coroutine);
			StartCoroutine(coroutine);
		}

		public void ToggleLerpCurrent()
		{
			// Defino los datos de la corrutina

			// Obtengo el lerp entre los valores
			var lerpValue = CurrentValue.InverseLerp(A, B);

			// Defino el punto a utilizar
			var closerToA = Mathf.Round(lerpValue / 0.5f) * 0.5f < 0.5f;

			// Defino los valores del Lerp
			var a = CurrentValue;
			var b = closerToA ? B : A;

			// Ejecuto el toggle
			ToggleAndLerp(a, b);
		}

		public void SwitchLerp(bool value)
		{
			// Defino los datos de la corrutina
			var a = value ? A : B;
			var b = !value ? A : B;

			// Ejecuto el toggle
			ToggleAndLerp(a, b);
		}

		public void SwitchLerpCurrentToA(bool value)
		{
			// Interpolo hacia uno u otro valor
			if(value) LerpToAFromCurrent();
			else LerpToBFromCurrent();
		}

		public void LerpToB()
		{
			// Creo la corrutina
			var coroutine = LerpRoutine(A, B, Timeout, UseSLerp);

			// Detengo e inicializo la corrutina
			StopCoroutine(coroutine);
			StartCoroutine(coroutine);
		}

		public void LerpToA()
		{
			// Creo la corrutina inversa
			var coroutine = LerpRoutine(B, A, Timeout, UseSLerp);

			// Detengo e inicializo la corrutina
			StopCoroutine(coroutine);
			StartCoroutine(coroutine);
		}

		public void LerpToBFromCurrent()
		{
			// Creo la corrutina
			var coroutine = LerpRoutine(CurrentValue, B, Timeout, UseSLerp);

			// Detengo e inicializo la corrutina
			StopCoroutine(coroutine);
			StartCoroutine(coroutine);
		}

		public void LerpToAFromCurrent()
		{
			// Creo la corrutina
			var coroutine = LerpRoutine(CurrentValue, A, Timeout, UseSLerp);

			// Detengo e inicializo la corrutina
			StopCoroutine(coroutine);
			StartCoroutine(coroutine);
		}

		private IEnumerator LerpRoutine(Vector3 a, Vector3 b, float timeout, bool useSLerp)
		{
			// Inicializo la variable de control
			IsLerping = true;

			// Ejecuto el evento de inicialización
			_onVector3LerpStart.WrapInvoke(a);

			// Inicio el tiempo transcurrido
			float elapsed = 0f;

			// Inicializo el Vector de interpolación
			Vector3 lerpVector = a;

			while (elapsed < timeout)
			{
				yield return null;

				// Aumento el tiempo transcurrido
				elapsed += Time.deltaTime;

				// Obtengo el lerp
				float lerpTime = elapsed / timeout;

				// Obtengo el lerp

				// Si uso el Spherical Linear Interpolation
				if (useSLerp)
				{
					lerpVector = Vector3.Slerp(a, b, lerpTime);
				}
				else
				{
					lerpVector = Vector3.Lerp(a, b, lerpTime);
				}

				// Comunico el lerp a través del evento
				_onVector3Lerp.WrapInvoke(lerpVector);
				CurrentValue = lerpVector;
			}

			// Ejecuto el evento de finalización
			_onVector3LerpEnd.WrapInvoke(b);
			CurrentValue = b;

			// Finalizo la variable de control
			IsLerping = false;

		}

	}
}
