﻿#pragma warning disable 0649
using NSAvancedUI;
using NSTraduccionIdiomas;

namespace NSInterfaz
{
    public class PanelInterfazSeleccionLenguajeInglesEspaniol : AbstractSingletonPanelUIAnimation<PanelInterfazSeleccionLenguajeInglesEspaniol>
    { 
        #region public methods

        /// <summary>
        /// cambia el idioma del simulador a español
        /// </summary>
        public void mtdTraducirEspañol()
        {
            DiccionarioIdiomas.Instance.SetIdiom(IdiomsList.Spanish);
        }

        /// <summary>
        /// ccmabia el idioma del simmulador a English
        /// </summary>
        public void mtdTraducirIngles()
        {
            DiccionarioIdiomas.Instance.SetIdiom(IdiomsList.English);
        }
        #endregion
    }
}